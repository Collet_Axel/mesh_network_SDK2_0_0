var group__nrf__log__backend__flash =
[
    [ "Log flash backend configuration", "group__nrf__log__backend__flash__config.html", "group__nrf__log__backend__flash__config" ],
    [ "nrf_log_backend_flashlog_t", "structnrf__log__backend__flashlog__t.html", [
      [ "backend", "structnrf__log__backend__flashlog__t.html#a0b031c0616870a4fd227dada78ba03d4", null ]
    ] ],
    [ "nrf_log_backend_crashlog_t", "structnrf__log__backend__crashlog__t.html", [
      [ "backend", "structnrf__log__backend__crashlog__t.html#a971d1044d9011860f9c8237d370f4e20", null ]
    ] ],
    [ "NRF_LOG_BACKEND_CRASHLOG_DEF", "group__nrf__log__backend__flash.html#gabd4c5ec470a7b4293f9a073a8c181b19", null ],
    [ "NRF_LOG_BACKEND_FLASHLOG_DEF", "group__nrf__log__backend__flash.html#gadfbacadfa992082e74ec12d7c077baf6", null ],
    [ "nrf_log_backend_flash_erase", "group__nrf__log__backend__flash.html#ga8a98eee6d90d90574a26ad0f308bbb65", null ],
    [ "nrf_log_backend_flash_init", "group__nrf__log__backend__flash.html#ga71267c08a09c97ff1de70d6949a105ff", null ],
    [ "nrf_log_backend_flash_next_entry_get", "group__nrf__log__backend__flash.html#ga7fe718a650e593a5ef6f229b30487473", null ],
    [ "nrf_log_backend_crashlog_api", "group__nrf__log__backend__flash.html#ga7592ecdd161d20abe7ba54d866283e01", null ],
    [ "nrf_log_backend_flashlog_api", "group__nrf__log__backend__flash.html#gaa4c2bcd01f851a0af0e419177101acd9", null ]
];