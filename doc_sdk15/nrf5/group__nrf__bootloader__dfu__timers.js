var group__nrf__bootloader__dfu__timers =
[
    [ "nrf_bootloader_dfu_timeout_callback_t", "group__nrf__bootloader__dfu__timers.html#gaacd319610567cf8d6fa38aa63d850921", null ],
    [ "nrf_bootloader_dfu_inactivity_timer_restart", "group__nrf__bootloader__dfu__timers.html#ga9ea64307b901923dc99e5bb116c3bd61", null ],
    [ "nrf_bootloader_wdt_feed_timer_start", "group__nrf__bootloader__dfu__timers.html#ga284d90ef04336b292f97394104e9a8a5", null ]
];