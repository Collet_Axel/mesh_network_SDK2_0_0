var structnrf__drv__usbd__evt__t =
[
    [ "data", "structnrf__drv__usbd__evt__t.html#a722a37420a469e466d5c0837106974f5", null ],
    [ "ep", "structnrf__drv__usbd__evt__t.html#ae614ca5ccc3631e36e0942205f92716b", null ],
    [ "eptransfer", "structnrf__drv__usbd__evt__t.html#ac132cf990991b4c17f19626fbfe297fd", null ],
    [ "framecnt", "structnrf__drv__usbd__evt__t.html#a60f70ecbcaffb92e0de6aea3c19743ff", null ],
    [ "isocrc", "structnrf__drv__usbd__evt__t.html#aee7751a5e687888f518cfb1b2b7180ed", null ],
    [ "sof", "structnrf__drv__usbd__evt__t.html#a752c16bd48141e4971d2c3d70e619224", null ],
    [ "status", "structnrf__drv__usbd__evt__t.html#a9a6f9e81a9729d8560a019da75841f30", null ],
    [ "type", "structnrf__drv__usbd__evt__t.html#a1aab1ebd54e700689bbf53080f2b8b80", null ]
];