var NAVTREE =
[
  [ "nRF5 SDK", "index.html", [
    [ "Introduction", "index.html", null ],
    [ "Getting Started", "nrf51_getting_started.html", "nrf51_getting_started" ],
    [ "Hardware Drivers", "hardware_drivers.html", "hardware_drivers" ],
    [ "Libraries", "general_libraries.html", "general_libraries" ],
    [ "Examples", "examples.html", "examples" ],
    [ "User Guides", "user_guides.html", "user_guides" ],
    [ "API Reference", "modules.html", "modules" ],
    [ "Data Structures", "usergroup0.html", [
      [ "Data Structures", "annotated.html", "annotated" ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Enumerator", "functions_eval.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"ble_sdk_app_gatts.html",
"functions_vars_0x67.html",
"group___u_u_i_d___s_e_r_v_i_c_e_s.html#ga4090520f3007fe6f90f06a6f4f8d5845",
"group__ant__bsc__config.html#ga3d1041313ed90695964936ec63fd2d4e",
"group__ant__fs.html#ggacdb008570aaf592c9345f5bacf0af086a23c5c7e241692158b65d15da1345d05c",
"group__ant__sdk__profiles__hrm__utils.html#ga5647f7602ec6ab1c55958495ec01a411",
"group__app__timer.html#ga3b8721032df5eef94314ca58168c3f6d",
"group__app__usbd__audio__types.html#ggae78ac87610c7cefe625f39eee20a2102a3a6e22584f3bebc26d103694890df49d",
"group__app__usbd__config.html#gacf2cd19ac46de55f691b3bcc3ed74083",
"group__app__usbd__hid__kbd__internal.html#ga3c17f942098be2429d3eacbe1b9faf0e",
"group__app__usbd__nrf__dfu__trigger__types.html#ga82424002de6e5a9a4270c12a95f5577c",
"group__ble__advertising.html#ga4c1be1a7b547102c4147e524e6c80925",
"group__ble__bms.html#gga73d8f3ae8888d46f7efb83b783246b3dac97cca30c5eece92986529dcb60d4a22",
"group__ble__dis__c.html#gga701f78ba403d081a4b053a1c136a6436a9ffa7c339be8efb02c4267aaa828d33a",
"group__ble__gap__evt__app.html#ga693e65fbc3d089a0d14c425326258b17",
"group__ble__gls.html#gaed9f6e13c501f46e47d0637d9ee7c11f",
"group__ble__nus__c.html#ga9dad386773750e9b5d0e1d8d805fddbc",
"group__ble__sdk__srv__lncp.html#gga020a4d9a5bbd3ce0562c89789eb2b7bfa3174076aad4eaa627a803e4d21b8fb25",
"group__crys__aesccm.html#ggac49ede2cc9a27f363488a213a04577f2a22268f982e51317e4516f571a69f3aa9",
"group__crys__ecpki__error.html#ga3111ee9bafdfdf134ebd69136ff27f9e",
"group__crys__hkdf.html#gaa8e536efa94f3b3aacfd0a0cc0605fee",
"group__crys__rsa__error.html#gac84fbd28ea3b413d9c7233e42f9208d5",
"group__eddystone__adv__timing.html#ga1bba7d32fddb61310041a239e9860a90",
"group__gr_i_f_x_i2_c.html#ga2a3667bfe8720816f444d04c4d498705",
"group__gzp__02__api.html#ga587ecf4915c37fc22dbb6354931d2386",
"group__icmp6__code.html#ga1c2fb78a35975ce0fcd016c6140e6aa5",
"group__iot__sdk__coap__api.html#gada4a13b8b20188abe6dcd0845f150db4",
"group__iot__socket__fifo.html#gadae3dc43b0ca7ad93ebac8e43a48729f",
"group__mac__common.html#gga5cbc12c72ed93482bc0e881b7cb8fad8a1c0be6704f2499393ef4c405c23fc937",
"group__nfc__ble__pair__common.html#ga1168bdf0ea54938449f6baf26e353d38",
"group__nfc__t4t__apdu.html#ga4bb54656f41ff2b344b526b577ee5126",
"group__nrf__adc__hal.html#ga0403cc8baa68147ae21762eb2a5df9aa",
"group__nrf__ble__ots__c__l2cap__config.html#ga85bb7509ec902d4c384e87154639db37",
"group__nrf__clock__hal.html#gga7ab8220532dae7b1dde0f5c329e81041a0820ea89bcb64904238b43bb758ebfaa",
"group__nrf__crypto__ecc.html#ggaefd14927610d570d6968ba6d4e1e38a0a35d80307a6c76de4aecb74d07ff08524",
"group__nrf__crypto__ecdh__secp384r1.html",
"group__nrf__dfu__config.html#gad01a4adee15d7a11e3226ac666d0742e",
"group__nrf__drv__gpiote.html#gad44a7f979b352e47ddb0129982519bc3",
"group__nrf__drv__qspi__config.html#gaca661146c5981f6324d46b9e8d57f74a",
"group__nrf__drv__twi.html#ga65f9ba4dfb694dd3625957d1d17eabfe",
"group__nrf__egu__hal.html#ga792ef3eb0fd19c387d8a35cd1a18c2e2",
"group__nrf__gpio__hal.html#gga07e0c8c0b30499cf0a7b79dbe62d3fccad60f768c699495d9976a45fa7a9f51c6",
"group__nrf__log__backend__uart__config.html#gaded989b014cb7b29b08ad637ec7ff96c",
"group__nrf__lpcomp__hal.html#gga0c957a3dd1132acd90e420b70477cef0ac39e8d50d6b00ebd6391fed80e5c2530",
"group__nrf__ppi__hal.html#ggabb15d5be643cacaa7fa78d89ba1b9e76aed5c33cb1a04f76f9916b98d44f9ce11",
"group__nrf__qspi__hal.html#gga2195660aadd2cb2848efa1cbaa0acafeae0a969f402d2072c514ae74bf504ba0c",
"group__nrf__saadc__hal.html#ggad540c22a2e06a7828a5ce6c71bc8a5d4a45e4751bab9abc4fb0e8cf281f2010fc",
"group__nrf__spi__mngr.html#gadcb41933dad4f8f24a1acc29763c064a",
"group__nrf__timer__hal.html#gga55adb42b75222256195fa003f8c0dc20a7f49a5f098147d06ff40ba96f4b08019",
"group__nrf__uart__hal.html#ga8f8c852e9d2eca9a20348c0640d7db5e",
"group__nrf__usbd__hal.html#gga0187f1cfcba4bbd00158f9b04ec06fe6af21e07f0978c2fbeb063a8ae6155276d",
"group__nrfx__glue.html#gaa1580d2a7d2a6ec67c471fac00b84576",
"group__nrfx__pwm__config.html#gac9903d067f3de646114c1984d48c74eb",
"group__nrfx__swi.html#ga7f2d6aa077e33b7fe8d4cc23de389a25",
"group__nrfx__uarte.html#gga11a91e3594938e682dc90b8d2efaa762a611da59f7c1ac9071506a5a6ec63f240",
"group__sdk__common__errors.html#ga8b0201b9527b42e64e5878f7916b854f",
"group__ser__phy__hci.html#ga5b7bc3ebe9fde93d37fdf9300c7a0b6f",
"group__ssi__aes.html#ga62fd13889e76f580c475542f0efa17c2",
"group__sys__crc.html",
"hardware_driver_comp.html#hardware_driver_comp_config",
"iot_sdk_app_lwip_tcp_server.html#iot_sdk_app_lwip_tcp_server_setup_led",
"lib_bootloader_dfu_banks.html#lib_bootloader_dfu_dual_banks_sd",
"lib_error.html",
"lib_iot_stack_dns.html#IOT_DNS_CODE_2",
"lib_usbd_class_msc.html#usb_msc_usage",
"qspi_bootloader_example.html",
"struct_c_r_y_s___r_n_d___state__t.html#aaf050911f9ed774763b0e3d04dfd895e",
"structant__bpwr__simulator__cb__t.html#a3943ccdf7ee8f62bab917d0742f1186f",
"structant__sdm__simulator__cb__t.html#a5f54aad8b05a19ab2343663f66fa9973",
"structapp__usbd__cdc__acm__notify__t.html#a11bf9cc90a41ecc00ca3e90f16d9a984",
"structapp__usbd__msc__inst__t.html#a2189f0740fbf7cad93c69a69565c7156",
"structble__advdata__t.html#ad05db9cd3fe0425b48d8beb01913b70c",
"structble__conn__params__init__t.html#abf5ac3620c0afeef3c3a04608094119a",
"structble__hids__hid__information__t.html#aadd0326cc45c31ea9e16b2f4cf0000fd",
"structble__lncp__s.html#adf82e1b30b864c03cba575b9148ddd78",
"structble__ots__s.html#a2107a610cc5dc68efd29ba033c9e9841",
"structdfu__init__command__t.html#a5b1719475283dc09407899094d081413",
"structhal__uart__descriptor__t.html#a7cd9f07d38b4d01aae5bd5c3b3a45be0",
"structipso__digital__input__t.html#a729944b7363e6c1ec3d18dd1b0dc65c8",
"structlps22hb__raw__data__t.html#af5095cc324800a71a99294dd593ec97b",
"structmcp4725__pins__config__t.html#a7e32792845538635926cbd4410b57676",
"structnfc__ble__oob__pairing__data__t.html#a8504075b3fdee8ebdbfc8e366ec3fa40",
"structnrf__ble__gatts__c__evt__t.html",
"structnrf__cli__rtt__internal__t.html#abad203ae39aaee611bf86c13b1764db9",
"structnrf__drv__twi__t.html#a7622da2cf41de4adfe04b0dc4e3aad0f",
"structnrf__log__module__reduced__dynamic__data__t.html",
"structnrf__twi__sensor__read__cmd__t.html",
"structnrfx__twi__t.html",
"structsdk__mapped__flags__key__list__t.html",
"structudp6__socket__t.html#aa3c355a3e4b310ad294cfb9d6bad09dc",
"unionnrf__crypto__ecdh__context__t.html#ac23ec22f9c581be5c8417ddc1254dc39"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';
var navTreeSubIndices = new Array();

function getData(varName)
{
  var i = varName.lastIndexOf('/');
  var n = i>=0 ? varName.substring(i+1) : varName;
  return eval(n.replace(/\-/g,'_'));
}

function stripPath(uri)
{
  return uri.substring(uri.lastIndexOf('/')+1);
}

function stripPath2(uri)
{
  var i = uri.lastIndexOf('/');
  var s = uri.substring(i+1);
  var m = uri.substring(0,i+1).match(/\/d\w\/d\w\w\/$/);
  return m ? uri.substring(i-6) : s;
}

function localStorageSupported()
{
  try {
    return 'localStorage' in window && window['localStorage'] !== null && window.localStorage.getItem;
  }
  catch(e) {
    return false;
  }
}


function storeLink(link)
{
  if (!$("#nav-sync").hasClass('sync') && localStorageSupported()) {
      window.localStorage.setItem('navpath',link);
  }
}

function deleteLink()
{
  if (localStorageSupported()) {
    window.localStorage.setItem('navpath','');
  } 
}

function cachedLink()
{
  if (localStorageSupported()) {
    return window.localStorage.getItem('navpath');
  } else {
    return '';
  }
}

function getScript(scriptName,func,show)
{
  var head = document.getElementsByTagName("head")[0]; 
  var script = document.createElement('script');
  script.id = scriptName;
  script.type = 'text/javascript';
  script.onload = func; 
  script.src = scriptName+'.js'; 
  if ($.browser.msie && $.browser.version<=8) { 
    // script.onload does not work with older versions of IE
    script.onreadystatechange = function() {
      if (script.readyState=='complete' || script.readyState=='loaded') { 
        func(); if (show) showRoot(); 
      }
    }
  }
  head.appendChild(script); 
}

function createIndent(o,domNode,node,level)
{
  var level=-1;
  var n = node;
  while (n.parentNode) { level++; n=n.parentNode; }
  var imgNode = document.createElement("img");
  imgNode.style.paddingLeft=(16*level).toString()+'px';
  imgNode.width  = 16;
  imgNode.height = 22;
  imgNode.border = 0;
  if (node.childrenData) {
    node.plus_img = imgNode;
    node.expandToggle = document.createElement("a");
    node.expandToggle.href = "javascript:void(0)";
    node.expandToggle.onclick = function() {
      if (node.expanded) {
        $(node.getChildrenUL()).slideUp("fast");
        node.plus_img.src = node.relpath+"ftv2pnode.png";
        node.expanded = false;
      } else {
        expandNode(o, node, false, false);
      }
    }
    node.expandToggle.appendChild(imgNode);
    domNode.appendChild(node.expandToggle);
    imgNode.src = node.relpath+"ftv2pnode.png";
  } else {
    imgNode.src = node.relpath+"ftv2node.png";
    domNode.appendChild(imgNode);
  } 
}

var animationInProgress = false;

function gotoAnchor(anchor,aname,updateLocation)
{
  var pos, docContent = $('#doc-content');
  if (anchor.parent().attr('class')=='memItemLeft' ||
      anchor.parent().attr('class')=='fieldtype' ||
      anchor.parent().is(':header')) 
  {
    pos = anchor.parent().position().top;
  } else if (anchor.position()) {
    pos = anchor.position().top;
  }
  if (pos) {
    var dist = Math.abs(Math.min(
               pos-docContent.offset().top,
               docContent[0].scrollHeight-
               docContent.height()-docContent.scrollTop()));
    animationInProgress=true;
    docContent.animate({
      scrollTop: pos + docContent.scrollTop() - docContent.offset().top
    },Math.max(50,Math.min(500,dist)),function(){
      if (updateLocation) window.location.href=aname;
      animationInProgress=false;
    });
  }
}

function newNode(o, po, text, link, childrenData, lastNode)
{
  var node = new Object();
  node.children = Array();
  node.childrenData = childrenData;
  node.depth = po.depth + 1;
  node.relpath = po.relpath;
  node.isLast = lastNode;

  node.li = document.createElement("li");
  po.getChildrenUL().appendChild(node.li);
  node.parentNode = po;

  node.itemDiv = document.createElement("div");
  node.itemDiv.className = "item";

  node.labelSpan = document.createElement("span");
  node.labelSpan.className = "label";

  createIndent(o,node.itemDiv,node,0);
  node.itemDiv.appendChild(node.labelSpan);
  node.li.appendChild(node.itemDiv);

  var a = document.createElement("a");
  node.labelSpan.appendChild(a);
  node.label = document.createTextNode(text);
  node.expanded = false;
  a.appendChild(node.label);
  if (link) {
    var url;
    if (link.substring(0,1)=='^') {
      url = link.substring(1);
      link = url;
    } else {
      url = node.relpath+link;
    }
    a.className = stripPath(link.replace('#',':'));
    if (link.indexOf('#')!=-1) {
      var aname = '#'+link.split('#')[1];
      var srcPage = stripPath($(location).attr('pathname'));
      var targetPage = stripPath(link.split('#')[0]);
      a.href = srcPage!=targetPage ? url : "javascript:void(0)"; 
      a.onclick = function(){
        storeLink(link);
        if (!$(a).parent().parent().hasClass('selected'))
        {
          $('.item').removeClass('selected');
          $('.item').removeAttr('id');
          $(a).parent().parent().addClass('selected');
          $(a).parent().parent().attr('id','selected');
        }
        var anchor = $(aname);
        gotoAnchor(anchor,aname,true);
      };
    } else {
      a.href = url;
      a.onclick = function() { storeLink(link); }
    }
  } else {
    if (childrenData != null) 
    {
      a.className = "nolink";
      a.href = "javascript:void(0)";
      a.onclick = node.expandToggle.onclick;
    }
  }

  node.childrenUL = null;
  node.getChildrenUL = function() {
    if (!node.childrenUL) {
      node.childrenUL = document.createElement("ul");
      node.childrenUL.className = "children_ul";
      node.childrenUL.style.display = "none";
      node.li.appendChild(node.childrenUL);
    }
    return node.childrenUL;
  };

  return node;
}

function showRoot()
{
  var headerHeight = $("#top").height();
  var footerHeight = $("#nav-path").height();
  var windowHeight = $(window).height() - headerHeight - footerHeight;
  (function (){ // retry until we can scroll to the selected item
    try {
      var navtree=$('#nav-tree');
      navtree.scrollTo('#selected',0,{offset:-windowHeight/2});
    } catch (err) {
      setTimeout(arguments.callee, 0);
    }
  })();
}

function expandNode(o, node, imm, showRoot)
{
  if (node.childrenData && !node.expanded) {
    if (typeof(node.childrenData)==='string') {
      var varName    = node.childrenData;
      getScript(node.relpath+varName,function(){
        node.childrenData = getData(varName);
        expandNode(o, node, imm, showRoot);
      }, showRoot);
    } else {
      if (!node.childrenVisited) {
        getNode(o, node);
      } if (imm || ($.browser.msie && $.browser.version>8)) { 
        // somehow slideDown jumps to the start of tree for IE9 :-(
        $(node.getChildrenUL()).show();
      } else {
        $(node.getChildrenUL()).slideDown("fast");
      }
      if (node.isLast) {
        node.plus_img.src = node.relpath+"ftv2mlastnode.png";
      } else {
        node.plus_img.src = node.relpath+"ftv2mnode.png";
      }
      node.expanded = true;
    }
  }
}

function glowEffect(n,duration)
{
  n.addClass('glow').delay(duration).queue(function(next){
    $(this).removeClass('glow');next();
  });
}

function highlightAnchor()
{
  var aname = $(location).attr('hash');
  var anchor = $(aname);
  if (anchor.parent().attr('class')=='memItemLeft'){
    var rows = $('.memberdecls tr[class$="'+
               window.location.hash.substring(1)+'"]');
    glowEffect(rows.children(),300); // member without details
  } else if (anchor.parents().slice(2).prop('tagName')=='TR') {
    glowEffect(anchor.parents('div.memitem'),1000); // enum value
  } else if (anchor.parent().attr('class')=='fieldtype'){
    glowEffect(anchor.parent().parent(),1000); // struct field
  } else if (anchor.parent().is(":header")) {
    glowEffect(anchor.parent(),1000); // section header
  } else {
    glowEffect(anchor.next(),1000); // normal member
  }
  gotoAnchor(anchor,aname,false);
}

function selectAndHighlight(hash,n)
{
  var a;
  if (hash) {
    var link=stripPath($(location).attr('pathname'))+':'+hash.substring(1);
    a=$('.item a[class$="'+link+'"]');
  }
  if (a && a.length) {
    a.parent().parent().addClass('selected');
    a.parent().parent().attr('id','selected');
    highlightAnchor();
  } else if (n) {
    $(n.itemDiv).addClass('selected');
    $(n.itemDiv).attr('id','selected');
  }
  if ($('#nav-tree-contents .item:first').hasClass('selected')) {
    $('#nav-sync').css('top','30px');
  } else {
    $('#nav-sync').css('top','5px');
  }
  showRoot();
}

function showNode(o, node, index, hash)
{
  if (node && node.childrenData) {
    if (typeof(node.childrenData)==='string') {
      var varName    = node.childrenData;
      getScript(node.relpath+varName,function(){
        node.childrenData = getData(varName);
        showNode(o,node,index,hash);
      },true);
    } else {
      if (!node.childrenVisited) {
        getNode(o, node);
      }
      $(node.getChildrenUL()).show();
      if (node.isLast) {
        node.plus_img.src = node.relpath+"ftv2mlastnode.png";
      } else {
        node.plus_img.src = node.relpath+"ftv2mnode.png";
      }
      node.expanded = true;
      var n = node.children[o.breadcrumbs[index]];
      if (index+1<o.breadcrumbs.length) {
        showNode(o,n,index+1,hash);
      } else {
        if (typeof(n.childrenData)==='string') {
          var varName = n.childrenData;
          getScript(n.relpath+varName,function(){
            n.childrenData = getData(varName);
            node.expanded=false;
            showNode(o,node,index,hash); // retry with child node expanded
          },true);
        } else {
          var rootBase = stripPath(o.toroot.replace(/\..+$/, ''));
          if (rootBase=="index" || rootBase=="pages" || rootBase=="search") {
            expandNode(o, n, true, true);
          }
          selectAndHighlight(hash,n);
        }
      }
    }
  } else {
    selectAndHighlight(hash);
  }
}

function getNode(o, po)
{
  po.childrenVisited = true;
  var l = po.childrenData.length-1;
  for (var i in po.childrenData) {
    var nodeData = po.childrenData[i];
    po.children[i] = newNode(o, po, nodeData[0], nodeData[1], nodeData[2],
      i==l);
  }
}

function gotoNode(o,subIndex,root,hash,relpath)
{
  var nti = navTreeSubIndices[subIndex][root+hash];
  o.breadcrumbs = $.extend(true, [], nti ? nti : navTreeSubIndices[subIndex][root]);
  if (!o.breadcrumbs && root!=NAVTREE[0][1]) { // fallback: show index
    navTo(o,NAVTREE[0][1],"",relpath);
    $('.item').removeClass('selected');
    $('.item').removeAttr('id');
  }
  if (o.breadcrumbs) {
    o.breadcrumbs.unshift(0); // add 0 for root node
    showNode(o, o.node, 0, hash);
  }
}

function navTo(o,root,hash,relpath)
{
  var link = cachedLink();
  if (link) {
    var parts = link.split('#');
    root = parts[0];
    if (parts.length>1) hash = '#'+parts[1];
    else hash='';
  }
  if (hash.match(/^#l\d+$/)) {
    var anchor=$('a[name='+hash.substring(1)+']');
    glowEffect(anchor.parent(),1000); // line number
    hash=''; // strip line number anchors
    //root=root.replace(/_source\./,'.'); // source link to doc link
  }
  var url=root+hash;
  var i=-1;
  while (NAVTREEINDEX[i+1]<=url) i++;
  if (i==-1) { i=0; root=NAVTREE[0][1]; } // fallback: show index
  if (navTreeSubIndices[i]) {
    gotoNode(o,i,root,hash,relpath)
  } else {
    getScript(relpath+'navtreeindex'+i,function(){
      navTreeSubIndices[i] = eval('NAVTREEINDEX'+i);
      if (navTreeSubIndices[i]) {
        gotoNode(o,i,root,hash,relpath);
      }
    },true);
  }
}

function showSyncOff(n,relpath)
{
    n.html('<img src="'+relpath+'sync_off.png" title="'+SYNCOFFMSG+'"/>');
}

function showSyncOn(n,relpath)
{
    n.html('<img src="'+relpath+'sync_on.png" title="'+SYNCONMSG+'"/>');
}

function toggleSyncButton(relpath)
{
  var navSync = $('#nav-sync');
  if (navSync.hasClass('sync')) {
    navSync.removeClass('sync');
    showSyncOff(navSync,relpath);
    storeLink(stripPath2($(location).attr('pathname'))+$(location).attr('hash'));
  } else {
    navSync.addClass('sync');
    showSyncOn(navSync,relpath);
    deleteLink();
  }
}

function initNavTree(toroot,relpath)
{
  var o = new Object();
  o.toroot = toroot;
  o.node = new Object();
  o.node.li = document.getElementById("nav-tree-contents");
  o.node.childrenData = NAVTREE;
  o.node.children = new Array();
  o.node.childrenUL = document.createElement("ul");
  o.node.getChildrenUL = function() { return o.node.childrenUL; };
  o.node.li.appendChild(o.node.childrenUL);
  o.node.depth = 0;
  o.node.relpath = relpath;
  o.node.expanded = false;
  o.node.isLast = true;
  o.node.plus_img = document.createElement("img");
  o.node.plus_img.src = relpath+"ftv2pnode.png";
  o.node.plus_img.width = 16;
  o.node.plus_img.height = 22;

  if (localStorageSupported()) {
    var navSync = $('#nav-sync');
    if (cachedLink()) {
      showSyncOff(navSync,relpath);
      navSync.removeClass('sync');
    } else {
      showSyncOn(navSync,relpath);
    }
    navSync.click(function(){ toggleSyncButton(relpath); });
  }

  navTo(o,toroot,window.location.hash,relpath);

  $(window).bind('hashchange', function(){
     if (window.location.hash && window.location.hash.length>1){
       var a;
       if ($(location).attr('hash')){
         var clslink=stripPath($(location).attr('pathname'))+':'+
                               $(location).attr('hash').substring(1);
         a=$('.item a[class$="'+clslink+'"]');
       }
       if (a==null || !$(a).parent().parent().hasClass('selected')){
         $('.item').removeClass('selected');
         $('.item').removeAttr('id');
       }
       var link=stripPath2($(location).attr('pathname'));
       navTo(o,link,$(location).attr('hash'),relpath);
     } else if (!animationInProgress) {
       $('#doc-content').scrollTop(0);
       $('.item').removeClass('selected');
       $('.item').removeAttr('id');
       navTo(o,toroot,window.location.hash,relpath);
     }
  })

  $(window).load(showRoot);
}

