var nrf51_getting_started =
[
    [ "Running precompiled examples", "getting_started_precompiled.html", null ],
    [ "Installing the SDK", "getting_started_installing.html", null ],
    [ "Compiling and running a first example", "getting_started_examples.html", [
      [ "Erasing the board", "getting_started_examples.html#gs_examples_erasing", null ],
      [ "Running the example", "getting_started_examples.html#gs_examples_running", null ]
    ] ],
    [ "Compiling and running the first application from the QSPI peripheral using XiP", "getting_started_qspi.html", [
      [ "Prerequisites", "getting_started_qspi.html#gs_qspi_prerequesite", null ],
      [ "Procedure", "getting_started_qspi.html#gs_qspi_main_job", null ],
      [ "Preparing an application to work with QSPI XiP", "getting_started_qspi.html#gs_qspi_my_app", null ],
      [ "Preparing the bootloader", "getting_started_qspi.html#gs_qspi_my_bootloader", null ],
      [ "Troubleshooting", "getting_started_qspi.html#gs_qspi_troubleshooting", null ]
    ] ],
    [ "Running examples that use a SoftDevice", "getting_started_softdevice.html", [
      [ "Programming SoftDevices", "getting_started_softdevice.html#getting_started_sd", [
        [ "nRFgo Studio", "getting_started_softdevice.html#getting_started_sd_studio", null ],
        [ "ARM Keil", "getting_started_softdevice.html#getting_started_sd_keil", null ],
        [ "GCC makefile", "getting_started_softdevice.html#getting_started_sd_gcc", null ]
      ] ],
      [ "Running ANT examples", "getting_started_softdevice.html#getting_started_sd_ant52", null ]
    ] ],
    [ "Running a serialized application", "nrf51_setups_serialization.html", [
      [ "Serialization hardware setup", "nrf51_setups_serialization.html#serialization_hardware", [
        [ "UART (BLE)", "nrf51_setups_serialization.html#serialization_hardware_uart", null ],
        [ "SPI (BLE)", "nrf51_setups_serialization.html#serialization_hardware_spi", null ],
        [ "UART (ANT)", "nrf51_setups_serialization.html#serialization_hardware_uart_ant", null ]
      ] ],
      [ "Serialization software setup", "nrf51_setups_serialization.html#serialization_software", null ]
    ] ],
    [ "Using the SDK with other boards", "sdk_for_custom_boards.html", [
      [ "Supported boards", "sdk_for_custom_boards.html#supported_board", null ],
      [ "Enabling support for a board", "sdk_for_custom_boards.html#bsp_location", null ],
      [ "Adding support for a custom board", "sdk_for_custom_boards.html#custom_board_support", null ]
    ] ],
    [ "SDK configuration header file", "sdk_config.html", [
      [ "Generic sdk_config.h and linker files", "sdk_config.html#sdk_config_generic", null ],
      [ "CMSIS Configuration Annotations Wizard", "sdk_config.html#sdk_config_annotations", null ],
      [ "Template projects", "sdk_config.html#sdk_config_template", null ],
      [ "Standard projects", "sdk_config.html#sdk_config_standard", null ],
      [ "Overriding the sdk_config.h configuration", "sdk_config.html#sdk_config_overriding", null ],
      [ "Editing sdk_config.h in IDE", "sdk_config.html#sdk_config_ide", [
        [ "ARM Keil uVision", "sdk_config.html#sdk_config_ide_keil", null ],
        [ "Segger Embedded Studio", "sdk_config.html#sdk_config_ide_ses", null ],
        [ "ARM GCC", "sdk_config.html#sdk_config_ide_gcc", null ],
        [ "Other IDEs", "sdk_config.html#sdk_config_ide_others", null ]
      ] ]
    ] ],
    [ "Introduction to BLE IoT", "iot_intro.html", "iot_intro" ],
    [ "Migration guide", "migration.html", [
      [ "Bluetooth low energy (BLE)", "migration.html#migration_ble", [
        [ "Advertising module (ble_advertising)", "migration.html#migr_ble_adv_module", [
          [ "Advertising Extensions using the Advertising module", "migration.html#migr_ble_adv_module_ext", null ],
          [ "Directed advertising defines renamed", "migration.html#migr_ble_adv_module_direct", null ]
        ] ],
        [ "Advertising and Scan Response Data Encoder (ble_advdata)", "migration.html#migr_ble_adv_module_bleadvdata", [
          [ "Multilink support for selected services", "migration.html#migr_ble_adv_module_multilink", null ]
        ] ],
        [ "BLE Connection State", "migration.html#migr_ble_adv_module_bleconnstate", null ],
        [ "Peer Manager", "migration.html#migr_ble_adv_module_pm", null ]
      ] ],
      [ "DFU/Bootloader", "migration.html#migration_dfu", [
        [ "Backwards Compatibility", "migration.html#migr_dfu_backcomp", null ],
        [ "Files", "migration.html#migr_dfu_files", null ],
        [ "Compiler definitions", "migration.html#migr_dfu_compdef", null ],
        [ "Configuration parameters", "migration.html#migr_dfu_configs", null ],
        [ "Cryptography in DFU", "migration.html#migr_dfu_crypto", [
          [ "Porting from µECC to nrf_oberon", "migration.html#migr_dfu_crypto_oberon", null ]
        ] ],
        [ "DFU (nrf_dfu.h)", "migration.html#migr_dfu_dfu", [
          [ "Refactoring from DFU to bootloader", "migration.html#migr_dfu_dfu_refactor", null ],
          [ "nrf_dfu_init()", "migration.html#migr_dfu_dfu_init", null ]
        ] ],
        [ "Request handling", "migration.html#migr_dfu_reqhandling", null ]
      ] ],
      [ "USB", "migration.html#migration_usb", [
        [ "IN direction setup data handling", "migration.html#migr_usb_indirsetup", null ],
        [ "Global class instance definitions and descriptor generating macros", "migration.html#migr_usb_globalclass", [
          [ "CDC ACM", "migration.html#migr_usb_globalclass_cdc", null ],
          [ "HID mouse", "migration.html#migr_usb_globalclass_mouse", null ],
          [ "HID keyboard", "migration.html#migr_usb_globalclass_keyboard", null ],
          [ "HID generic", "migration.html#migr_usb_globalclass_generic", null ],
          [ "Audio", "migration.html#migr_usb_globalclass_audio", null ],
          [ "get_descriptors class method", "migration.html#migr_usb_globalclass_getdesc", null ]
        ] ],
        [ "Added double buffering for CDC ACM", "migration.html#migr_usb_buffer", null ],
        [ "USB power events moved to main event queue", "migration.html#migr_usb_powerevents", null ],
        [ "Updated configuration parameters names", "migration.html#migr_usb_configs", null ],
        [ "Renamed setup_device_req_std", "migration.html#migr_usb_rename", null ],
        [ "APP_USBD_CONFIG_MAX_POWER defined", "migration.html#migr_usb_power", null ],
        [ "app_usbd_init() does not initialize the clock now", "migration.html#migr_usb_clock", null ],
        [ "Added SOF compression and interrupt handling", "migration.html#migr_usb_sof", null ]
      ] ],
      [ "Crypto", "migration.html#migration_crypto", [
        [ "Introduction", "migration.html#migration_crypto_intro", null ],
        [ "Configuration", "migration.html#migration_crypto_config", [
          [ "Enabling nrf_crypto in sdk_config.h", "migration.html#migration_crypto_config_enable", null ],
          [ "Enabling one or more nrf_crypto backends", "migration.html#migration_crypto_config_backends", null ],
          [ "Disabling unneccessary functionality", "migration.html#migration_crypto_config_disable", null ]
        ] ],
        [ "nrf_crypto initialization", "migration.html#migration_crypto_init", null ],
        [ "nrf_crypto error codes", "migration.html#migration_crypto_error", null ],
        [ "nrf_crypto variable declarations", "migration.html#migration_crypto_variable", null ],
        [ "value_length inputs", "migration.html#migration_crypto_valuelength", null ],
        [ "Automatic memory allocation", "migration.html#migration_crypto_memalloc", null ],
        [ "nrf_crypto automatic memory management", "migration.html#migration_crypto_memmanage", [
          [ "Memory management in mbed TLS backend", "migration.html#migration_crypto_memmanage_mbed", null ],
          [ "Memory management in IAR builds", "migration.html#migration_crypto_memmanage_iar", null ]
        ] ],
        [ "nrf_crypto endianness changed to big-endian", "migration.html#migration_crypto_endianness", [
          [ "little-endian support in SDFU", "migration.html#migration_crypto_endianness_little", null ],
          [ "Endian conversion functions", "migration.html#migration_crypto_endianness_conversion", null ]
        ] ],
        [ "nrf_crypto API changes", "migration.html#migration_crypto_api", [
          [ "SHA-256 hash calculation", "migration.html#migration_crypto_api_hash", null ],
          [ "ECDH shared secret calculation", "migration.html#migration_crypto_api_ecdh", null ],
          [ "ECDSA verify", "migration.html#migration_crypto_api_ecdsa", null ]
        ] ]
      ] ],
      [ "Drivers", "migration.html#migration_drivers", [
        [ "nrfx", "migration.html#migration_drivers_nrfx", null ],
        [ "Other changes in drivers APIs", "migration.html#migration_drivers_other", null ]
      ] ],
      [ "Libraries", "migration.html#migration_libs", [
        [ "nrf_atomic", "migration.html#migration_libs_atomic", null ],
        [ "nrf_balloc, nrf_queue, nrf_atfifo", "migration.html#migration_libs_bqa", null ],
        [ "nrf_cli", "migration.html#migration_libs_cli", null ],
        [ "nrf_spi_mngr", "migration.html#migration_libs_spi", null ],
        [ "nrf_twi_mngr", "migration.html#migration_libs_twi", null ]
      ] ],
      [ "Proprietary", "migration.html#migration_proprietary", null ]
    ] ]
];