var lib_nrf_log =
[
    [ "Logs processing", "lib_nrf_log.html#nrf_log_processing", null ],
    [ "Configuration", "lib_nrf_log.html#nrf_log_config", null ],
    [ "Usage", "lib_nrf_log.html#nrf_log_usage", [
      [ "Controlling the logger", "lib_nrf_log.html#nrf_log_usage_control", null ],
      [ "Logging", "lib_nrf_log.html#nrf_log_usage_logging", null ]
    ] ],
    [ "Filtering logs on instance level", "lib_nrf_log.html#nrf_log_instance_logging", [
      [ "Setting up instance level filtering", "lib_nrf_log.html#nrf_log_instance_setup", null ],
      [ "Instance logging", "lib_nrf_log.html#nrf_log_instance_usage", null ]
    ] ],
    [ "Performance", "lib_nrf_log.html#nrf_log_performance", null ],
    [ "Logger backend interface", "lib_nrf_log.html#nrf_log_custom", null ],
    [ "Flash logger backend", "lib_nrf_log_backend_flash.html", [
      [ "Flash operations", "lib_nrf_log_backend_flash.html#nrf_log_backend_flash_operations", null ],
      [ "Flashlog", "lib_nrf_log_backend_flash.html#nrf_log_backend_flash_flashlog", null ],
      [ "Crashlog", "lib_nrf_log_backend_flash.html#nrf_log_backend_flash_crashlog", null ],
      [ "Command line interface support", "lib_nrf_log_backend_flash.html#nrf_log_backend_flash_cli", null ]
    ] ]
];