var lib_bootloader_modules =
[
    [ "Bootloader", "lib_bootloader.html", [
      [ "Bootloader Settings page", "lib_bootloader.html#lib_bootloader_settings_page", null ],
      [ "Firmware activation", "lib_bootloader.html#lib_bootloader_firmware_activation", null ],
      [ "DFU mode", "lib_bootloader.html#lib_bootloader_dfu_mode", null ],
      [ "Starting the application", "lib_bootloader.html#lib_bootloader_app_start", null ],
      [ "Watchdog timer support", "lib_bootloader.html#lib_bootloader_watchdog", null ],
      [ "Bootloader dependencies", "lib_bootloader.html#lib_bootloader_deps", null ],
      [ "Programming the bootloader", "lib_bootloader.html#lib_bootloader_programming", [
        [ "Using Keil", "lib_bootloader.html#lib_bootloader_programming_keil", null ],
        [ "Using IAR", "lib_bootloader.html#lib_bootloader_programming_iar", null ],
        [ "Using nrfjprog directly", "lib_bootloader.html#lib_bootloader_programming_nrfjprog", null ]
      ] ],
      [ "Memory layout", "lib_bootloader.html#lib_bootloader_memory", null ]
    ] ],
    [ "Device Firmware Update process", "lib_bootloader_dfu_process.html", "lib_bootloader_dfu_process" ],
    [ "DFU protocol", "lib_dfu_transport.html", "lib_dfu_transport" ],
    [ "DFU Trigger Library (USB)", "lib_dfu_trigger_usb.html", [
      [ "Serial number", "lib_dfu_trigger_usb.html#lib_dfu_trigger_usb_serial_num", null ],
      [ "Protocol", "lib_dfu_trigger_usb.html#lib_dfu_trigger_usb_protocol", null ]
    ] ]
];