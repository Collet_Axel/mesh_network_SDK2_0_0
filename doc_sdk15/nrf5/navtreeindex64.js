var NAVTREEINDEX64 =
{
"lib_iot_stack_dns.html#IOT_DNS_CODE_2":[3,24,1,9,5,1,1],
"lib_iot_stack_dns.html#IOT_DNS_CONF":[3,24,1,9,5,2],
"lib_iot_stack_icmp6.html":[3,24,1,9,3],
"lib_iot_stack_icmp6.html#ICMP6_DISABLE_API_PARAM_CHECK":[3,24,1,9,3,2,1],
"lib_iot_stack_icmp6.html#ICMP6_DISABLE_LOGS":[3,24,1,9,3,2,0],
"lib_iot_stack_icmp6.html#ICMP6_ENABLE_ALL_MESSAGES_TO_APPLICATION":[3,24,1,9,3,2,3],
"lib_iot_stack_icmp6.html#ICMP6_ENABLE_HANDLE_ECHO_REQUEST_TO_APPLICATION":[3,24,1,9,3,2,4],
"lib_iot_stack_icmp6.html#ICMP6_ENABLE_ND6_MESSAGES_TO_APPLICATION":[3,24,1,9,3,2,2],
"lib_iot_stack_icmp6.html#IOT_ICMP6_CALLBACK":[3,24,1,9,3,0],
"lib_iot_stack_icmp6.html#IOT_ICMP6_CODE":[3,24,1,9,3,1],
"lib_iot_stack_icmp6.html#IOT_ICMP6_CODE_1":[3,24,1,9,3,1,0],
"lib_iot_stack_icmp6.html#IOT_ICMP6_CODE_2":[3,24,1,9,3,1,1],
"lib_iot_stack_icmp6.html#IOT_ICMP6_CODE_3":[3,24,1,9,3,1,2],
"lib_iot_stack_icmp6.html#IOT_ICMP6_CONF":[3,24,1,9,3,2],
"lib_iot_stack_ipv6.html":[3,24,1,9,2],
"lib_iot_stack_ipv6.html#IOT_IPV6_CALLBACK":[3,24,1,9,2,0],
"lib_iot_stack_ipv6.html#IOT_IPV6_CODE":[3,24,1,9,2,1],
"lib_iot_stack_ipv6.html#IOT_IPV6_CODE_1":[3,24,1,9,2,1,0],
"lib_iot_stack_ipv6.html#IOT_IPV6_CONF":[3,24,1,9,2,2],
"lib_iot_stack_ipv6.html#IPV6_DEFAULT_HOP_LIMIT":[3,24,1,9,2,2,3],
"lib_iot_stack_ipv6.html#IPV6_DISABLE_API_PARAM_CHECK":[3,24,1,9,2,2,1],
"lib_iot_stack_ipv6.html#IPV6_DISABLE_LOGS":[3,24,1,9,2,2,0],
"lib_iot_stack_ipv6.html#IPV6_ENABLE_UNSUPPORTED_PROTOCOLS_TO_APPLICATION":[3,24,1,9,2,2,4],
"lib_iot_stack_ipv6.html#IPV6_MAX_INTERFACE":[3,24,1,9,2,2,2],
"lib_iot_stack_memory.html":[3,24,1,9,1],
"lib_iot_stack_memory.html#LIB_IOT_ARCH_SS_1":[3,24,1,9,1,0],
"lib_iot_stack_memory.html#LIB_IOT_ARCH_SS_2":[3,24,1,9,1,1],
"lib_iot_stack_memory.html#LIB_IOT_ARCH_SS_3":[3,24,1,9,1,2],
"lib_iot_stack_memory.html#LIB_IOT_ARCH_SS_4":[3,24,1,9,1,3],
"lib_iot_stack_memory.html#LIB_IOT_ARCH_SS_5":[3,24,1,9,1,4],
"lib_iot_stack_tftp.html":[3,24,1,9,7],
"lib_iot_stack_tftp.html#IOT_TFTP_ARCH_client":[3,24,1,9,7,0],
"lib_iot_stack_tftp.html#IOT_TFTP_ARCH_client_abort":[3,24,1,9,7,0,3],
"lib_iot_stack_tftp.html#IOT_TFTP_ARCH_client_negotiation":[3,24,1,9,7,0,0],
"lib_iot_stack_tftp.html#IOT_TFTP_ARCH_client_speed":[3,24,1,9,7,0,2],
"lib_iot_stack_tftp.html#IOT_TFTP_ARCH_client_transfer":[3,24,1,9,7,0,1],
"lib_iot_stack_tftp.html#IOT_TFTP_CONF":[3,24,1,9,7,1],
"lib_iot_stack_tftp.html#IOT_TFTP_LIMITATIONS":[3,24,1,9,7,2],
"lib_iot_stack_tftp.html#IOT_TFTP_LIMITATION_Features":[3,24,1,9,7,2,0],
"lib_iot_stack_tftp.html#IOT_TFTP_LIMITATION_limitations":[3,24,1,9,7,2,1],
"lib_iot_stack_tftp.html#IOT_TFTP_REFERENCES":[3,24,1,9,7,3],
"lib_iot_stack_tftp.html#TFTP_DISABLE_API_PARAM_CHECK":[3,24,1,9,7,1,1],
"lib_iot_stack_tftp.html#TFTP_DISABLE_LOGS":[3,24,1,9,7,1,0],
"lib_iot_stack_tftp.html#TFTP_MAX_INSTANCES":[3,24,1,9,7,1,3],
"lib_iot_stack_tftp.html#TFTP_MAX_RETRANSMISSION_COUNT":[3,24,1,9,7,1,2],
"lib_iot_stack_udp.html":[3,24,1,9,4],
"lib_iot_stack_udp.html#IOT_UDP_CALLBACK":[3,24,1,9,4,0],
"lib_iot_stack_udp.html#IOT_UDP_CODE":[3,24,1,9,4,1],
"lib_iot_stack_udp.html#IOT_UDP_CODE_1":[3,24,1,9,4,1,0],
"lib_iot_stack_udp.html#IOT_UDP_CODE_2":[3,24,1,9,4,1,1],
"lib_iot_stack_udp.html#IOT_UDP_CONF":[3,24,1,9,4,2],
"lib_iot_stack_udp.html#UDP6_DISABLE_API_PARAM_CHECK":[3,24,1,9,4,2,1],
"lib_iot_stack_udp.html#UDP6_DISABLE_LOG":[3,24,1,9,4,2,0],
"lib_iot_stack_udp.html#UDP6_MAX_SOCKET_COUNT":[3,24,1,9,4,2,2],
"lib_iot_timer.html":[3,24,1,6],
"lib_iot_timer.html#IOT_TIMER_CODE":[3,24,1,6,0],
"lib_iot_timer.html#IOT_TIMER_CODE_1":[3,24,1,6,0,0],
"lib_iot_timer.html#IOT_TIMER_CODE_2":[3,24,1,6,0,1],
"lib_iot_timer.html#IOT_TIMER_CODE_3":[3,24,1,6,0,2],
"lib_iot_timer.html#IOT_TIMER_CONF":[3,24,1,6,1],
"lib_iot_timer.html#IOT_TIMER_DISABLE_API_PARAM_CHECK":[3,24,1,6,1,1],
"lib_iot_timer.html#IOT_TIMER_LIMITATIONS":[3,24,1,6,2],
"lib_iot_timer.html#IOT_TIMER_RESOLUTION_IN_MS":[3,24,1,6,1,0],
"lib_led_softblink.html":[3,25],
"lib_led_softblink.html#lib_led_softblink_init":[3,25,0],
"lib_led_softblink.html#usage_example":[3,25,1],
"lib_led_softblink.html#usage_with_sd":[3,25,2],
"lib_low_power_pwm.html":[3,27],
"lib_low_power_pwm.html#lib_pwm_init":[3,27,0],
"lib_low_power_pwm.html#limitations":[3,27,1],
"lib_low_power_pwm.html#usage_example":[3,27,2],
"lib_low_power_pwm.html#usage_with_sd":[3,27,3],
"lib_mapped_flags.html":[3,28],
"lib_mem_manager.html":[3,29],
"lib_mem_manager.html#MEM_MANAGER":[3,29,0],
"lib_mem_manager.html#MEM_MANAGER_CONF":[3,29,1],
"lib_mem_manager.html#mem_manager_params_api_check":[3,29,1,3],
"lib_mem_manager.html#mem_manager_params_count":[3,29,1,4],
"lib_mem_manager.html#mem_manager_params_diag_only":[3,29,1,2],
"lib_mem_manager.html#mem_manager_params_diagnostics":[3,29,1,1],
"lib_mem_manager.html#mem_manager_params_logs":[3,29,1,0],
"lib_mem_manager.html#mem_manager_params_size":[3,29,1,5],
"lib_mem_pool.html":[3,48,1],
"lib_nrf_log.html":[3,26],
"lib_nrf_log.html#nrf_log_config":[3,26,1],
"lib_nrf_log.html#nrf_log_custom":[3,26,5],
"lib_nrf_log.html#nrf_log_instance_logging":[3,26,3],
"lib_nrf_log.html#nrf_log_instance_setup":[3,26,3,0],
"lib_nrf_log.html#nrf_log_instance_usage":[3,26,3,1],
"lib_nrf_log.html#nrf_log_performance":[3,26,4],
"lib_nrf_log.html#nrf_log_processing":[3,26,0],
"lib_nrf_log.html#nrf_log_usage":[3,26,2],
"lib_nrf_log.html#nrf_log_usage_control":[3,26,2,0],
"lib_nrf_log.html#nrf_log_usage_logging":[3,26,2,1],
"lib_nrf_log_backend_flash.html":[3,26,6],
"lib_nrf_log_backend_flash.html#nrf_log_backend_flash_cli":[3,26,6,3],
"lib_nrf_log_backend_flash.html#nrf_log_backend_flash_crashlog":[3,26,6,2],
"lib_nrf_log_backend_flash.html#nrf_log_backend_flash_flashlog":[3,26,6,1],
"lib_nrf_log_backend_flash.html#nrf_log_backend_flash_operations":[3,26,6,0],
"lib_nrf_spi_mngr.html":[3,45],
"lib_nrf_twi_mngr.html":[3,49],
"lib_peer_manager.html":[3,2,7],
"lib_pm_architecture.html":[3,2,7,0],
"lib_pm_functionality.html":[3,2,7,1],
"lib_pm_functionality.html#lib_pm_functionality_peers":[3,2,7,1,1],
"lib_pm_functionality.html#lib_pm_functionality_security":[3,2,7,1,0],
"lib_pm_functionality.html#lib_pm_functionality_whitelist":[3,2,7,1,2],
"lib_pm_migration.html":[3,2,7,3],
"lib_pm_usage.html":[3,2,7,2],
"lib_pm_usage.html#lib_pm_usage_data":[3,2,7,2,3],
"lib_pm_usage.html#lib_pm_usage_events":[3,2,7,2,2],
"lib_pm_usage.html#lib_pm_usage_init":[3,2,7,2,0],
"lib_pm_usage.html#lib_pm_usage_security":[3,2,7,2,1],
"lib_pm_usage.html#lib_pm_usage_whitelist":[3,2,7,2,4],
"lib_pwm.html":[3,32],
"lib_pwm.html#lib_pwm_init":[3,32,1],
"lib_pwm.html#lib_pwm_resources":[3,32,0],
"lib_pwm.html#limitations":[3,32,2],
"lib_pwm.html#usage_example":[3,32,3],
"lib_pwm.html#usage_with_sd":[3,32,4],
"lib_pwr_mgmt.html":[3,31],
"lib_pwr_mgmt.html#app_scheduler_support":[3,31,3],
"lib_pwr_mgmt.html#cpu_usage_tracer":[3,31,0],
"lib_pwr_mgmt.html#fpu_support":[3,31,4],
"lib_pwr_mgmt.html#shutdown_retry_mechanism":[3,31,2],
"lib_pwr_mgmt.html#standby_timeout":[3,31,1],
"lib_queue.html":[3,33],
"lib_queue.html#lib_queue_create":[3,33,0],
"lib_queue.html#mailbox_usage":[3,33,1],
"lib_rtos.html":[3,34],
"lib_scheduler.html":[3,35],
"lib_scheduler.html#app_scheduler_req":[3,35,0],
"lib_scheduler.html#int_context_logic":[3,35,0,1],
"lib_scheduler.html#main_context_logic":[3,35,0,0],
"lib_scheduler.html#seq_diagrams_no_sched":[3,35,2],
"lib_scheduler.html#seq_diagrams_sched":[3,35,1],
"lib_sdcard.html":[3,36],
"lib_sdcard.html#sdcard_config":[3,36,0],
"lib_sdcard.html#sdcard_init":[3,36,1],
"lib_sdcard.html#sdcard_usage":[3,36,2],
"lib_section_iter.html":[3,53,3],
"lib_section_iter.html#lib_section_iter_usage_module_usage":[3,53,3,2],
"lib_section_iter.html#lib_section_vars_usage_regdata":[3,53,3,1],
"lib_section_iter.html#lib_sectioniter_usage_create":[3,53,3,0],
"lib_section_vars.html":[3,53],
"lib_section_vars.html#lib_section_vars_usage_create":[3,53,0],
"lib_section_vars.html#lib_section_vars_usage_module_usage":[3,53,2],
"lib_section_vars.html#lib_section_vars_usage_regdata":[3,53,1],
"lib_sensorsim.html":[3,37],
"lib_serial.html":[3,38],
"lib_serialization.html":[3,39],
"lib_serialization.html#lib_serialization_app_chip":[3,39,0],
"lib_serialization.html#lib_serialization_conn_chip":[3,39,1],
"lib_sha256.html":[3,40],
"lib_simple_timer.html":[3,41],
"lib_slip.html":[3,42],
"lib_slip.html#lib_slip_decoding":[3,42,2],
"lib_slip.html#lib_slip_encoding":[3,42,1],
"lib_slip.html#lib_slip_general":[3,42,0],
"lib_sntp_client.html":[3,24,1,9,6],
"lib_sntp_client.html#SNTP_CLIENT_CODE":[3,24,1,9,6,0],
"lib_sntp_client.html#SNTP_CLIENT_CODE_1":[3,24,1,9,6,0,0],
"lib_sntp_client.html#SNTP_CLIENT_CODE_2":[3,24,1,9,6,0,1],
"lib_sntp_client.html#SNTP_CLIENT_CODE_3":[3,24,1,9,6,0,2],
"lib_sntp_client.html#SNTP_CLIENT_CODE_4":[3,24,1,9,6,0,3],
"lib_sntp_client.html#SNTP_CLIENT_CONF":[3,24,1,9,6,1],
"lib_sntp_client.html#SNTP_CLIENT_DISABLE_API_PARAM_CHECK":[3,24,1,9,6,1,2],
"lib_sntp_client.html#SNTP_CLIENT_DISABLE_LOGS":[3,24,1,9,6,1,3],
"lib_sntp_client.html#SNTP_CLIENT_LIMITATIONS":[3,24,1,9,6,2],
"lib_sntp_client.html#SNTP_MAX_RETRANSMISSION_COUNT":[3,24,1,9,6,1,0],
"lib_sntp_client.html#SNTP_RETRANSMISSION_INTERVAL":[3,24,1,9,6,1,1],
"lib_softdevice_handler.html":[3,43],
"lib_softdevice_handler.html#compiler_defines":[3,43,9],
"lib_softdevice_handler.html#lib_sdh_ble_example":[3,43,4],
"lib_softdevice_handler.html#lib_sdh_components":[3,43,0],
"lib_softdevice_handler.html#lib_sdh_components_functions":[3,43,1],
"lib_softdevice_handler.html#lib_sdh_components_nrf_sdh":[3,43,0,0],
"lib_softdevice_handler.html#lib_sdh_components_nrf_sdh_ant":[3,43,0,2],
"lib_softdevice_handler.html#lib_sdh_components_nrf_sdh_ble":[3,43,0,3],
"lib_softdevice_handler.html#lib_sdh_components_nrf_sdh_soc":[3,43,0,1],
"lib_softdevice_handler.html#lib_sdh_example_delay":[3,43,6],
"lib_softdevice_handler.html#lib_sdh_example_delay_msc":[3,43,8],
"lib_softdevice_handler.html#lib_sdh_priority":[3,43,2,1],
"lib_softdevice_handler.html#lib_sdh_register":[3,43,2],
"lib_softdevice_handler.html#lib_sdh_register_observer":[3,43,2,0],
"lib_softdevice_handler.html#lib_sdh_request_example":[3,43,7],
"lib_softdevice_handler.html#lib_sdh_stack_init":[3,43,3],
"lib_softdevice_handler.html#lib_sdh_state_example":[3,43,5],
"lib_sortlist.html":[3,44],
"lib_sortlist.html#lib_sortlist_create":[3,44,0],
"lib_sortlist.html#sortlist_usage":[3,44,1],
"lib_strerror.html":[3,12],
"lib_svc.html":[3,46],
"lib_svc.html#lib_Svc_async_interface_convenience_code":[3,46,4,7],
"lib_svc.html#lib_svc_async_caller":[3,46,4,2],
"lib_svc.html#lib_svc_async_caller_code":[3,46,4,3],
"lib_svc.html#lib_svc_async_handler":[3,46,4,4],
"lib_svc.html#lib_svc_async_handler_code":[3,46,4,5],
"lib_svc.html#lib_svc_async_shared":[3,46,4,1],
"lib_svc.html#lib_svc_caller":[3,46,2],
"lib_svc.html#lib_svc_caller_code":[3,46,2,0],
"lib_svc.html#lib_svc_caller_indirect_code":[3,46,2,1],
"lib_svc.html#lib_svc_details":[3,46,0],
"lib_svc.html#lib_svc_details_indirect":[3,46,0,0],
"lib_svc.html#lib_svc_handler":[3,46,3],
"lib_svc.html#lib_svc_handler_code":[3,46,3,0],
"lib_svc.html#lib_svc_indirect_handler_code":[3,46,3,1],
"lib_svc.html#lib_svc_limitations":[3,46,0,1],
"lib_svc.html#lib_svc_redirect_handler":[3,46,0,2],
"lib_svc.html#lib_svc_redirect_handler_code":[3,46,0,3],
"lib_svc.html#lib_svc_reserved_svc_number":[3,46,1],
"lib_svc.html#lib_svc_reserved_svc_numbers_bootloader":[3,46,1,0],
"lib_svc.html#lib_svci_async_interface":[3,46,4],
"lib_svc.html#lib_svci_async_interface_convenience":[3,46,4,6],
"lib_svc.html#lib_svci_async_interface_limitations":[3,46,4,0],
"lib_task_manager.html":[3,54],
"lib_task_manager.html#task_manager_power_mngmt":[3,54,1],
"lib_task_manager.html#task_manager_usage":[3,54,0],
"lib_timer.html":[3,47],
"lib_timer.html#lib_timer_debugging":[3,47,0,0],
"lib_timer.html#lib_timer_migration":[3,47,1],
"lib_timer.html#lib_timer_usage":[3,47,0],
"lib_usbd.html":[3,55],
"lib_usbd.html#lib_usbd_class_brick":[3,55,0],
"lib_usbd.html#lib_usbd_controller":[3,55,1],
"lib_usbd.html#lib_usbd_core":[3,55,2],
"lib_usbd.html#lib_usbd_driver":[3,55,3],
"lib_usbd.html#lib_usbd_std_classes":[3,55,5],
"lib_usbd.html#lib_usbd_strings":[3,55,4],
"lib_usbd.html#lib_usbd_usage":[3,55,6],
"lib_usbd.html#lib_usbd_usage_class":[3,55,6,1],
"lib_usbd.html#lib_usbd_usage_init":[3,55,6,0],
"lib_usbd.html#lib_usbd_usage_start":[3,55,6,2],
"lib_usbd_class.html":[3,55,8],
"lib_usbd_class_audio.html":[3,55,8,0],
"lib_usbd_class_audio.html#define_audio_class":[3,55,8,0,1],
"lib_usbd_class_audio.html#define_audio_descriptor":[3,55,8,0,0],
"lib_usbd_class_audio.html#event_handling_audio_class":[3,55,8,0,3],
"lib_usbd_class_audio.html#register_audio_class":[3,55,8,0,4],
"lib_usbd_class_audio.html#sof_in_audio_class":[3,55,8,0,2],
"lib_usbd_class_cdc.html":[3,55,8,3],
"lib_usbd_class_cdc.html#usb_cdc_interfaces":[3,55,8,3,0],
"lib_usbd_class_cdc.html#usb_cdc_send_receive":[3,55,8,3,1],
"lib_usbd_class_hid.html":[3,55,8,1],
"lib_usbd_class_hid.html#usb_generic_report":[3,55,8,1,0,2],
"lib_usbd_class_hid.html#usb_hid_reports":[3,55,8,1,0],
"lib_usbd_class_hid.html#usb_keyboard_report":[3,55,8,1,0,1],
"lib_usbd_class_hid.html#usb_mouse_report":[3,55,8,1,0,0],
"lib_usbd_class_msc.html":[3,55,8,2],
"lib_usbd_class_msc.html#usb_msc_create":[3,55,8,2,1]
};
