var group__nrf__drv__usbd__errata =
[
    [ "NRF_DRV_USBD_ERRATA_ENABLE", "group__nrf__drv__usbd__errata.html#gaa5d733b6e091ccd01e14ae3d3e447ca6", null ],
    [ "nrf_drv_usb_errata_199", "group__nrf__drv__usbd__errata.html#gada79cdd5c2efb6877fcf9fb3250f03eb", null ],
    [ "nrf_drv_usbd_errata_104", "group__nrf__drv__usbd__errata.html#ga5df69103547510ebfd23af7220b8b19e", null ],
    [ "nrf_drv_usbd_errata_154", "group__nrf__drv__usbd__errata.html#gad81e97d9787b4e4b64fe5005b5e8b549", null ],
    [ "nrf_drv_usbd_errata_166", "group__nrf__drv__usbd__errata.html#ga7fbb3405bb4276d0ee52fe414b30cba0", null ],
    [ "nrf_drv_usbd_errata_171", "group__nrf__drv__usbd__errata.html#gaa5a1c6a51abd7e66eb37b72729b1d306", null ],
    [ "nrf_drv_usbd_errata_187", "group__nrf__drv__usbd__errata.html#gabfba299a1772928bcf41156fb797591a", null ],
    [ "nrf_drv_usbd_errata_sizeepout_rw", "group__nrf__drv__usbd__errata.html#ga240ce8c658cc2af73e2711127999b12e", null ],
    [ "nrf_drv_usbd_errata_type_52840", "group__nrf__drv__usbd__errata.html#gafe12a8aa3251f2a7ebcfc691d33f6327", null ],
    [ "nrf_drv_usbd_errata_type_52840_fp1", "group__nrf__drv__usbd__errata.html#ga3588bbeb77edaa22226cd7bdc8b14fec", null ],
    [ "nrf_drv_usbd_errata_type_52840_proto1", "group__nrf__drv__usbd__errata.html#ga9184e32032aa9aea2eeeed081377d3fc", null ]
];