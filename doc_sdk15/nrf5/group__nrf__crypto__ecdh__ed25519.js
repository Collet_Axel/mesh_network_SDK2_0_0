var group__nrf__crypto__ecdh__ed25519 =
[
    [ "NRF_CRYPTO_ECDH_ED25519_SHARED_SECRET_SIZE", "group__nrf__crypto__ecdh__ed25519.html#ga9e298b079ad2111886c80c21f32b7c93", null ],
    [ "nrf_crypto_ecdh_ed25519_context_t", "group__nrf__crypto__ecdh__ed25519.html#ga9391ca8ada58faa38a91a2b3fb543370", null ],
    [ "nrf_crypto_ecdh_ed25519_shared_secret_t", "group__nrf__crypto__ecdh__ed25519.html#ga33dbfed740960de637dfe5c6e9629b4b", null ]
];