var crypto_examples_nrf_crypto =
[
    [ "AES Example", "nrf_crypto_aes_example.html", [
      [ "Testing AES CTR mode", "nrf_crypto_aes_example.html#nrf_crypto_aes_example_testing_ctr", null ],
      [ "Testing AES CBC_MAC mode", "nrf_crypto_aes_example.html#nrf_crypto_aes_example_testing_cbc_mac", null ],
      [ "Testing AES CCM mode", "nrf_crypto_aes_example.html#nrf_crypto_aes_example_testing_ccm", null ],
      [ "Testing all AES modes", "nrf_crypto_aes_example.html#nrf_crypto_aes_example_testing_all", null ]
    ] ],
    [ "ChaCha-Poly Example", "nrf_crypto_chacha_poly_example.html", [
      [ "Testing", "nrf_crypto_chacha_poly_example.html#nrf_crypto_chacha_poly_example_testing", null ]
    ] ],
    [ "ECDH Example", "lib_crypto_ecdh_example.html", [
      [ "Testing", "lib_crypto_ecdh_example.html#lib_crypto_ecdh_example_testing", null ]
    ] ],
    [ "ECDSA Example", "lib_crypto_ecdsa_example.html", [
      [ "Testing", "lib_crypto_ecdsa_example.html#lib_crypto_ecdsa_example_testing", null ]
    ] ],
    [ "Hash Example", "nrf_crypto_hash_example.html", [
      [ "Testing", "nrf_crypto_hash_example.html#nrf_crypto_hash_example_testing", null ]
    ] ],
    [ "HKDF Example", "nrf_crypto_hkdf_example.html", [
      [ "Testing", "nrf_crypto_hkdf_example.html#nrf_crypto_hkdf_example_testing", null ]
    ] ],
    [ "HMAC Example", "nrf_crypto_hmac_example.html", [
      [ "Testing", "nrf_crypto_hmac_example.html#nrf_crypto_hmac_example_testing", null ]
    ] ],
    [ "RNG Example", "nrf_crypto_rng_example.html", [
      [ "Testing", "nrf_crypto_rng_example.html#nrf_crypto_rng_rng_testing", null ]
    ] ],
    [ "Test Example", "nrf_crypto_test_example.html", [
      [ "Testing", "nrf_crypto_test_example.html#nrf_crypto_test_example_testing", null ],
      [ "Adding additional test cases and test vectors", "nrf_crypto_test_example.html#nrf_crypto_test_example_adding_test", [
        [ "Test Case", "nrf_crypto_test_example.html#nrf_crypto_test_example_test_case", null ],
        [ "Registering a test case", "nrf_crypto_test_example.html#nrf_crypto_test_example_test_case_register", null ],
        [ "Test Vectors", "nrf_crypto_test_example.html#nrf_crypto_test_example_test_vector", null ],
        [ "Register a test vector", "nrf_crypto_test_example.html#nrf_crypto_test_example_test_vector_register", null ],
        [ "Output logging", "nrf_crypto_test_example.html#nrf_crypto_test_example_test_output", null ]
      ] ]
    ] ]
];