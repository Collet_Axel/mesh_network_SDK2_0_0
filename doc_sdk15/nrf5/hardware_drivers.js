var hardware_drivers =
[
    [ "Drivers descriptions", "hardware_drivers_desc.html", "hardware_drivers_desc" ],
    [ "Driver Support Matrix", "nrfx_drv_supp_matrix.html", null ],
    [ "nRF52810 Drivers", "nrf52810_drivers.html", null ],
    [ "nRF52832 Drivers", "nrf52832_drivers.html", null ],
    [ "nRF52840 Drivers", "nrf52840_drivers.html", null ]
];