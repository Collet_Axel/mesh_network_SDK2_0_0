var group__nrf__crypto__ecdsa__ed25519 =
[
    [ "NRF_CRYPTO_ECDSA_ED25519_SIGNATURE_SIZE", "group__nrf__crypto__ecdsa__ed25519.html#gac90f2cf66576c0d7229eaebd9e3b69c4", null ],
    [ "nrf_crypto_ecdsa_ed25519_sign_context_t", "group__nrf__crypto__ecdsa__ed25519.html#ga21e4f21d1dc0e9326cd659a00bfe579c", null ],
    [ "nrf_crypto_ecdsa_ed25519_signature_t", "group__nrf__crypto__ecdsa__ed25519.html#ga47d524974eb07d1c9039b46d75068dab", null ],
    [ "nrf_crypto_ecdsa_ed25519_verify_context_t", "group__nrf__crypto__ecdsa__ed25519.html#gaf9785604d04b64164cbe58ee54e4111c", null ]
];