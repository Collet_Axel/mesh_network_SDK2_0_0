var NAVTREEINDEX65 =
{
"lib_usbd_class_msc.html#usb_msc_usage":[3,55,8,2,0],
"lib_usbd_string_desc.html":[3,55,7],
"lib_usbd_string_desc.html#lib_usbd_string_desc_extern":[3,55,7,1],
"lib_usbd_string_desc.html#lib_usbd_string_desc_standard":[3,55,7,0],
"lib_usbd_string_desc.html#lib_usbd_string_desc_user":[3,55,7,2],
"library_nfc.html":[3,30],
"low_power_pwm_example.html":[4,5,16],
"low_power_pwm_example.html#low_power_pwm_example_testing":[4,5,16,0],
"lpcomp_example.html":[4,5,17],
"lpcomp_example.html#lpcomp_example_testing":[4,5,17,0],
"migration.html":[1,9],
"migration.html#migr_ble_adv_module":[1,9,0,0],
"migration.html#migr_ble_adv_module_bleadvdata":[1,9,0,1],
"migration.html#migr_ble_adv_module_bleconnstate":[1,9,0,2],
"migration.html#migr_ble_adv_module_direct":[1,9,0,0,1],
"migration.html#migr_ble_adv_module_ext":[1,9,0,0,0],
"migration.html#migr_ble_adv_module_multilink":[1,9,0,1,0],
"migration.html#migr_ble_adv_module_pm":[1,9,0,3],
"migration.html#migr_dfu_backcomp":[1,9,1,0],
"migration.html#migr_dfu_compdef":[1,9,1,2],
"migration.html#migr_dfu_configs":[1,9,1,3],
"migration.html#migr_dfu_crypto":[1,9,1,4],
"migration.html#migr_dfu_crypto_oberon":[1,9,1,4,0],
"migration.html#migr_dfu_dfu":[1,9,1,5],
"migration.html#migr_dfu_dfu_init":[1,9,1,5,1],
"migration.html#migr_dfu_dfu_refactor":[1,9,1,5,0],
"migration.html#migr_dfu_files":[1,9,1,1],
"migration.html#migr_dfu_reqhandling":[1,9,1,6],
"migration.html#migr_usb_buffer":[1,9,2,2],
"migration.html#migr_usb_clock":[1,9,2,7],
"migration.html#migr_usb_configs":[1,9,2,4],
"migration.html#migr_usb_globalclass":[1,9,2,1],
"migration.html#migr_usb_globalclass_audio":[1,9,2,1,4],
"migration.html#migr_usb_globalclass_cdc":[1,9,2,1,0],
"migration.html#migr_usb_globalclass_generic":[1,9,2,1,3],
"migration.html#migr_usb_globalclass_getdesc":[1,9,2,1,5],
"migration.html#migr_usb_globalclass_keyboard":[1,9,2,1,2],
"migration.html#migr_usb_globalclass_mouse":[1,9,2,1,1],
"migration.html#migr_usb_indirsetup":[1,9,2,0],
"migration.html#migr_usb_power":[1,9,2,6],
"migration.html#migr_usb_powerevents":[1,9,2,3],
"migration.html#migr_usb_rename":[1,9,2,5],
"migration.html#migr_usb_sof":[1,9,2,8],
"migration.html#migration_ble":[1,9,0],
"migration.html#migration_crypto":[1,9,3],
"migration.html#migration_crypto_api":[1,9,3,9],
"migration.html#migration_crypto_api_ecdh":[1,9,3,9,1],
"migration.html#migration_crypto_api_ecdsa":[1,9,3,9,2],
"migration.html#migration_crypto_api_hash":[1,9,3,9,0],
"migration.html#migration_crypto_config":[1,9,3,1],
"migration.html#migration_crypto_config_backends":[1,9,3,1,1],
"migration.html#migration_crypto_config_disable":[1,9,3,1,2],
"migration.html#migration_crypto_config_enable":[1,9,3,1,0],
"migration.html#migration_crypto_endianness":[1,9,3,8],
"migration.html#migration_crypto_endianness_conversion":[1,9,3,8,1],
"migration.html#migration_crypto_endianness_little":[1,9,3,8,0],
"migration.html#migration_crypto_error":[1,9,3,3],
"migration.html#migration_crypto_init":[1,9,3,2],
"migration.html#migration_crypto_intro":[1,9,3,0],
"migration.html#migration_crypto_memalloc":[1,9,3,6],
"migration.html#migration_crypto_memmanage":[1,9,3,7],
"migration.html#migration_crypto_memmanage_iar":[1,9,3,7,1],
"migration.html#migration_crypto_memmanage_mbed":[1,9,3,7,0],
"migration.html#migration_crypto_valuelength":[1,9,3,5],
"migration.html#migration_crypto_variable":[1,9,3,4],
"migration.html#migration_dfu":[1,9,1],
"migration.html#migration_drivers":[1,9,4],
"migration.html#migration_drivers_nrfx":[1,9,4,0],
"migration.html#migration_drivers_other":[1,9,4,1],
"migration.html#migration_libs":[1,9,5],
"migration.html#migration_libs_atomic":[1,9,5,0],
"migration.html#migration_libs_bqa":[1,9,5,1],
"migration.html#migration_libs_cli":[1,9,5,2],
"migration.html#migration_libs_spi":[1,9,5,3],
"migration.html#migration_libs_twi":[1,9,5,4],
"migration.html#migration_proprietary":[1,9,6],
"migration.html#migration_usb":[1,9,2],
"modules.html":[6],
"nfc_adafruit_with_tag_parser.html":[4,7,0],
"nfc_adafruit_with_tag_parser.html#nfc_adafruit_reader_testing":[4,7,0,0],
"nfc_ble_pair_lib_dox.html":[3,30,0],
"nfc_ble_pair_lib_dox.html#nfc_ble_pair_lib_architecture":[3,30,0,0],
"nfc_ble_pair_lib_dox.html#nfc_ble_pair_lib_usage":[3,30,0,1],
"nfc_connection_handover_msg_parser_dox.html":[3,30,2,1,4],
"nfc_connection_handover_msg_parser_dox.html#Usage":[3,30,2,1,4,0],
"nfc_ndef_ble_pair_message_module_dox.html":[3,30,2,1,3],
"nfc_ndef_dox.html":[3,30,2],
"nfc_ndef_format_dox.html":[3,30,2,0],
"nfc_ndef_format_dox.html#nfc_ndef_format_flags":[3,30,2,0,0,0],
"nfc_ndef_format_dox.html#nfc_ndef_format_header":[3,30,2,0,0],
"nfc_ndef_format_dox.html#nfc_ndef_format_payload":[3,30,2,0,1],
"nfc_ndef_generation_dox.html":[3,30,2,1],
"nfc_ndef_launchapp_message_module_dox.html":[3,30,2,1,2],
"nfc_ndef_message_module_dox.html":[3,30,2,1,5],
"nfc_ndef_message_module_dox.html#nfc_ndef_msg_gen":[3,30,2,1,5,1],
"nfc_ndef_message_module_dox.html#nfc_ndef_msg_rec":[3,30,2,1,5,2],
"nfc_ndef_message_module_dox.html#nfc_ndef_record_gen":[3,30,2,1,5,0],
"nfc_ndef_parse_module_dox.html":[3,30,2,2],
"nfc_ndef_text_record_module_dox.html":[3,30,2,1,0],
"nfc_ndef_uri_message_module_dox.html":[3,30,2,1,1],
"nfc_record_launch_app.html":[4,7,1],
"nfc_record_launch_app.html#project_url_record_test":[4,7,1,0],
"nfc_record_text.html":[4,7,2],
"nfc_record_text.html#project_text_record_test":[4,7,2,0],
"nfc_record_url.html":[4,7,3],
"nfc_record_url.html#project_url_record_test":[4,7,3,0],
"nfc_t4t_apdu_dox.html":[3,30,4,1,1],
"nfc_t4t_apdu_dox.html#nfc_t4t_apdu_format":[3,30,4,1,1,1],
"nfc_t4t_apdu_dox.html#nfc_t4t_apdu_generation":[3,30,4,1,1,2],
"nfc_t4t_apdu_dox.html#nfc_t4t_apdu_types":[3,30,4,1,1,0],
"nfc_t4t_cc_parser_dox.html":[3,30,4,1,2],
"nfc_t4t_cc_parser_dox.html#nfc_t4t_cc_parser_examples":[3,30,4,1,2,1],
"nfc_t4t_cc_parser_dox.html#nfc_t4t_cc_parser_format":[3,30,4,1,2,0],
"nfc_t4t_hl_detection_procedures_dox.html":[3,30,4,1],
"nfc_t4t_hl_detection_procedures_dox.html#nfc_t4t_hl_detection_procedures_examples":[3,30,4,1,0],
"nfc_type2tag_commands_dox.html":[3,30,1,2],
"nfc_type2tag_commands_dox.html#read_cmd":[3,30,1,2,0],
"nfc_type2tag_dox.html":[3,30,1],
"nfc_type2tag_format_dox.html":[3,30,1,1],
"nfc_type2tag_format_dox.html#nfc_t2t_format_cc":[3,30,1,1,0,2],
"nfc_type2tag_format_dox.html#nfc_t2t_format_data":[3,30,1,1,1],
"nfc_type2tag_format_dox.html#nfc_t2t_format_internal":[3,30,1,1,0,0],
"nfc_type2tag_format_dox.html#nfc_t2t_format_lock":[3,30,1,1,2],
"nfc_type2tag_format_dox.html#nfc_t2t_format_reserved":[3,30,1,1,0],
"nfc_type2tag_format_dox.html#nfc_t2t_format_reserved_lock":[3,30,1,1,0,1],
"nfc_type2tag_implement_dox.html":[3,30,1,0],
"nfc_type2tag_parser_dox.html":[3,30,1,3],
"nfc_type4tag_dox.html":[3,30,4],
"nfc_type4tag_dox.html#nfc_t4t_ndef_file_format_dox":[3,30,4,0],
"nfc_uart.html":[4,7,4],
"nfc_uart.html#nfc_uart_poller":[4,7,4,1],
"nfc_uart.html#nfc_uart_tag":[4,7,4,0],
"nfc_uart.html#project_nfc_uart_test":[4,7,4,2],
"nfc_wake_on_nfc.html":[4,7,5],
"nfc_wake_on_nfc.html#project_wake_on_nfc_test":[4,7,5,0],
"nfc_writable_ndef_msg.html":[4,7,6],
"nfc_writable_ndef_msg.html#project_writable_ndef_msg_test":[4,7,6,0],
"nrf51_getting_started.html":[1],
"nrf51_setups_serialization.html":[1,5],
"nrf51_setups_serialization.html#serialization_hardware":[1,5,0],
"nrf51_setups_serialization.html#serialization_hardware_spi":[1,5,0,1],
"nrf51_setups_serialization.html#serialization_hardware_uart":[1,5,0,0],
"nrf51_setups_serialization.html#serialization_hardware_uart_ant":[1,5,0,2],
"nrf51_setups_serialization.html#serialization_software":[1,5,1],
"nrf52810_drivers.html":[2,2],
"nrf52810_user_guide.html":[5,0],
"nrf52810_user_guide.html#ug_52810_emulated_project":[5,0,3,0],
"nrf52810_user_guide.html#ug_52810_hardware":[5,0,1],
"nrf52810_user_guide.html#ug_52810_intro":[5,0,0],
"nrf52810_user_guide.html#ug_52810_limitations":[5,0,1,0],
"nrf52810_user_guide.html#ug_52810_own_project":[5,0,2,0],
"nrf52810_user_guide.html#ug_52810_project":[5,0,3],
"nrf52810_user_guide.html#ug_52810_software":[5,0,2],
"nrf52832_drivers.html":[2,3],
"nrf52840_drivers.html":[2,4],
"nrf_crypto_aes_example.html":[4,2,0,0],
"nrf_crypto_aes_example.html#nrf_crypto_aes_example_testing_all":[4,2,0,0,3],
"nrf_crypto_aes_example.html#nrf_crypto_aes_example_testing_cbc_mac":[4,2,0,0,1],
"nrf_crypto_aes_example.html#nrf_crypto_aes_example_testing_ccm":[4,2,0,0,2],
"nrf_crypto_aes_example.html#nrf_crypto_aes_example_testing_ctr":[4,2,0,0,0],
"nrf_crypto_chacha_poly_example.html":[4,2,0,1],
"nrf_crypto_chacha_poly_example.html#nrf_crypto_chacha_poly_example_testing":[4,2,0,1,0],
"nrf_crypto_hash_example.html":[4,2,0,4],
"nrf_crypto_hash_example.html#nrf_crypto_hash_example_testing":[4,2,0,4,0],
"nrf_crypto_hkdf_example.html":[4,2,0,5],
"nrf_crypto_hkdf_example.html#nrf_crypto_hkdf_example_testing":[4,2,0,5,0],
"nrf_crypto_hmac_example.html":[4,2,0,6],
"nrf_crypto_hmac_example.html#nrf_crypto_hmac_example_testing":[4,2,0,6,0],
"nrf_crypto_rng_example.html":[4,2,0,7],
"nrf_crypto_rng_example.html#nrf_crypto_rng_rng_testing":[4,2,0,7,0],
"nrf_crypto_test_example.html":[4,2,0,8],
"nrf_crypto_test_example.html#nrf_crypto_test_example_adding_test":[4,2,0,8,1],
"nrf_crypto_test_example.html#nrf_crypto_test_example_test_case":[4,2,0,8,1,0],
"nrf_crypto_test_example.html#nrf_crypto_test_example_test_case_register":[4,2,0,8,1,1],
"nrf_crypto_test_example.html#nrf_crypto_test_example_test_output":[4,2,0,8,1,4],
"nrf_crypto_test_example.html#nrf_crypto_test_example_test_vector":[4,2,0,8,1,2],
"nrf_crypto_test_example.html#nrf_crypto_test_example_test_vector_register":[4,2,0,8,1,3],
"nrf_crypto_test_example.html#nrf_crypto_test_example_testing":[4,2,0,8,0],
"nrf_dev_radio_rx_example.html":[4,5,27],
"nrf_dev_radio_rx_example.html#nrf_dev_radio_rx_example_testing":[4,5,27,0],
"nrf_dev_radio_tx_example.html":[4,5,28],
"nrf_dev_radio_tx_example.html#nrf_dev_radio_tx_example_testing":[4,5,28,0],
"nrf_dev_saadc_example.html":[4,5,33],
"nrf_dev_saadc_example.html#nrf_dev_saadc_example_testing":[4,5,33,0],
"nrf_dev_simple_timer_example.html":[4,5,46],
"nrf_dev_simple_timer_example.html#nrf_dev_simple_timer_example_testing":[4,5,46,0],
"nrf_dev_timer_example.html":[4,5,45],
"nrf_dev_timer_example.html#nrf_dev_timer_example_testing":[4,5,45,0],
"nrf_dev_wdt_example.html":[4,5,50],
"nrf_dev_wdt_example.html#nrf_dev_wdt_example_testing":[4,5,50,0],
"nrf_freertos_example.html":[4,5,1],
"nrf_freertos_example.html#freertos_example_app_blinky_setup":[4,5,1,0],
"nrf_freertos_example.html#freertos_example_app_blinky_testing":[4,5,1,1],
"nrf_gpiote_example.html":[4,5,13],
"nrf_gpiote_example.html#gpiote_example_testing":[4,5,13,1],
"nrf_gpiote_example.html#nrf_gpiote_example_setup":[4,5,13,0],
"nrf_radio_test_example.html":[4,5,29],
"nrf_radio_test_example.html#nrf_radio_test_example_testing":[4,5,29,0],
"nrf_rtc_freertos_example.html":[4,5,2],
"nrf_rtc_freertos_example.html#freertos_rtc_example_app_blinky_setup":[4,5,2,0],
"nrf_rtc_freertos_example.html#freertos_rtc_example_app_blinky_testing":[4,5,2,1],
"nrf_spi_mngr_example.html":[4,5,39],
"nrf_spi_mngr_example.html#spi_transaction_mgr_example_testing":[4,5,39,0],
"nrfx_drv_supp_matrix.html":[2,1],
"nrfx_spim_example.html":[4,5,37],
"nrfx_spim_example.html#nrfx_spim_example_testing":[4,5,37,0],
"pages.html":[],
"pin_change_int_example.html":[4,5,18],
"pin_change_int_example.html#pin_change_int_example_setup":[4,5,18,0],
"pin_change_int_example.html#pin_change_int_example_testing":[4,5,18,1],
"ppi_example.html":[4,5,20],
"ppi_example.html#ppi_example_testing":[4,5,20,0],
"preflash_example.html":[4,5,21],
"preflash_example.html#preflash_example_testing":[4,5,21,0],
"profile_ant_bpwr.html":[3,0,5,0],
"profile_ant_bpwr.html#profile_ant_bpwr_config":[3,0,5,0,0],
"profile_ant_bpwr.html#profile_ant_bpwr_rx":[3,0,5,0,1],
"profile_ant_bpwr.html#profile_ant_bpwr_tx":[3,0,5,0,2],
"profile_ant_bsc.html":[3,0,5,1],
"profile_ant_bsc.html#profile_ant_bsc_config":[3,0,5,1,0],
"profile_ant_bsc.html#profile_ant_bsc_rx":[3,0,5,1,1],
"profile_ant_bsc.html#profile_ant_bsc_tx":[3,0,5,1,2],
"profile_ant_hrm.html":[3,0,5,2],
"profile_ant_hrm.html#profile_ant_hrm_config":[3,0,5,2,0],
"profile_ant_hrm.html#profile_ant_hrm_rx":[3,0,5,2,1],
"profile_ant_hrm.html#profile_ant_hrm_tx":[3,0,5,2,2],
"profile_ant_sdm.html":[3,0,5,3],
"profile_ant_sdm.html#profile_ant_sdm_config":[3,0,5,3,0],
"profile_ant_sdm.html#profile_ant_sdm_rx":[3,0,5,3,1],
"profile_ant_sdm.html#profile_ant_sdm_tx":[3,0,5,3,2],
"pwm_example.html":[4,5,23],
"pwm_example.html#pwm_example_setup":[4,5,23,0],
"pwm_example.html#pwm_example_testing":[4,5,23,1],
"pwm_hw_example.html":[4,5,22],
"pwm_hw_example.html#pwm_hw_example_demo1":[4,5,22,0,0],
"pwm_hw_example.html#pwm_hw_example_demo2":[4,5,22,0,1],
"pwm_hw_example.html#pwm_hw_example_demo3":[4,5,22,0,2],
"pwm_hw_example.html#pwm_hw_example_demo4":[4,5,22,0,3],
"pwm_hw_example.html#pwm_hw_example_demo5":[4,5,22,0,4],
"pwm_hw_example.html#pwm_hw_example_demos":[4,5,22,0],
"pwm_hw_example.html#pwm_hw_example_testing":[4,5,22,1],
"pwr_mgmt_example.html":[4,5,19],
"pwr_mgmt_example.html#pwr_mgmt_example_testing":[4,5,19,0],
"pwr_mgmt_example.html#pwr_mgmt_example_testing_1":[4,5,19,0,0],
"pwr_mgmt_example.html#pwr_mgmt_example_testing_2":[4,5,19,0,1],
"pwr_mgmt_example.html#pwr_mgmt_example_testing_3":[4,5,19,0,2],
"pwr_mgmt_example.html#pwr_mgmt_example_testing_4":[4,5,19,0,3],
"qdec_example.html":[4,5,24],
"qdec_example.html#qdec_example_setup":[4,5,24,0],
"qdec_example.html#qdec_example_testing":[4,5,24,1]
};
