var group__peer__manager__config =
[
    [ "PEER_MANAGER_ENABLED", "group__peer__manager__config.html#ga0ef747c881e6913c01b07cd77e170c23", null ],
    [ "PM_CENTRAL_ENABLED", "group__peer__manager__config.html#ga08bc424dfe0c267d9d47c00c182a90eb", null ],
    [ "PM_FLASH_BUFFERS", "group__peer__manager__config.html#ga3a521736dd5a02b79c23b16a529a6ddb", null ],
    [ "PM_MAX_REGISTRANTS", "group__peer__manager__config.html#gacf2f2bb51d0e8f618446d840b3844627", null ],
    [ "PM_PEER_RANKS_ENABLED", "group__peer__manager__config.html#ga810aff10490e8d9d00c147242af0b9c9", null ],
    [ "PM_SERVICE_CHANGED_ENABLED", "group__peer__manager__config.html#ga32355467741b67375a73277afdf89b22", null ]
];