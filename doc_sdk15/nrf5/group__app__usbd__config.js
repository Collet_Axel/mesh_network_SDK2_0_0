var group__app__usbd__config =
[
    [ "APP_USBD_CONFIG_DEBUG_COLOR", "group__app__usbd__config.html#ga5e66b65dc3694b5c6e6520783fabe619", null ],
    [ "APP_USBD_CONFIG_EVENT_QUEUE_ENABLE", "group__app__usbd__config.html#gacf2cd19ac46de55f691b3bcc3ed74083", null ],
    [ "APP_USBD_CONFIG_EVENT_QUEUE_SIZE", "group__app__usbd__config.html#ga13d7e690ccb16b5cb0c1262a41a8a9b6", null ],
    [ "APP_USBD_CONFIG_INFO_COLOR", "group__app__usbd__config.html#ga07e8795ab235a31a546c67dbbd8e8d95", null ],
    [ "APP_USBD_CONFIG_LOG_ENABLED", "group__app__usbd__config.html#gab2a9d5c79aee267329e1442a3ba39add", null ],
    [ "APP_USBD_CONFIG_LOG_LEVEL", "group__app__usbd__config.html#gab4695355f54397e6305749db832e073e", null ],
    [ "APP_USBD_CONFIG_MAX_POWER", "group__app__usbd__config.html#gac532d45826c21561f1a2ef68ea00a02d", null ],
    [ "APP_USBD_CONFIG_POWER_EVENTS_PROCESS", "group__app__usbd__config.html#ga6fef1b964a42d2b1d029db0cf9d55aa3", null ],
    [ "APP_USBD_CONFIG_SELF_POWERED", "group__app__usbd__config.html#ga2dc3dc40c904f752c3c0a7f69e454b08", null ],
    [ "APP_USBD_CONFIG_SOF_HANDLING_MODE", "group__app__usbd__config.html#ga2ca66ab9c6fcabb967a00e513d24782a", null ],
    [ "APP_USBD_CONFIG_SOF_TIMESTAMP_PROVIDE", "group__app__usbd__config.html#ga83192e0b2ed053f902ccf71ded19f5da", null ],
    [ "APP_USBD_DEVICE_VER_MAJOR", "group__app__usbd__config.html#ga47dbe4f3ea1d94eeca91ccdf67b4c303", null ],
    [ "APP_USBD_DEVICE_VER_MINOR", "group__app__usbd__config.html#gac5239b2ef03da9be939b8681d64502e1", null ],
    [ "APP_USBD_ENABLED", "group__app__usbd__config.html#gabbf3cea88cf5bd297d05c92751ba35f7", null ],
    [ "APP_USBD_PID", "group__app__usbd__config.html#ga17debe758d96a04133975285fb006c77", null ],
    [ "APP_USBD_VID", "group__app__usbd__config.html#gad4a2062e5b74ed19c1de897001a61b83", null ]
];