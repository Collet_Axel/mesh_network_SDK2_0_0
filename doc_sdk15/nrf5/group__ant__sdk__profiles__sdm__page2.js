var group__ant__sdk__profiles__sdm__page2 =
[
    [ "ant_sdm_page2_data_t", "structant__sdm__page2__data__t.html", [
      [ "battery", "structant__sdm__page2__data__t.html#a335054afe1b1e767a6245e1036f14f0a", null ],
      [ "byte", "structant__sdm__page2__data__t.html#a897a3a181b3ef6ba82329d2b4d744850", null ],
      [ "cadence", "structant__sdm__page2__data__t.html#a1c3937eab5004ad4df42c3a8a1c20235", null ],
      [ "health", "structant__sdm__page2__data__t.html#ada73f36df1e7335956c90a050a1c1eac", null ],
      [ "items", "structant__sdm__page2__data__t.html#a83cb3c0a209e00dda77efb4a16351dbd", null ],
      [ "location", "structant__sdm__page2__data__t.html#ac2bafa1a38d729e66e139957b1438cda", null ],
      [ "state", "structant__sdm__page2__data__t.html#a76104a45f95650b1a011bd55db039b83", null ],
      [ "status", "structant__sdm__page2__data__t.html#a6b7f17ecc787a3d80683179367bb7b8e", null ]
    ] ],
    [ "DEFAULT_ANT_SDM_PAGE2", "group__ant__sdk__profiles__sdm__page2.html#ga66b5b8b7a4dbd6bde55a9a75d0b10e2e", null ],
    [ "ant_sdm_page_2_decode", "group__ant__sdk__profiles__sdm__page2.html#ga8cce75b436fe480e83ece6ba6680b599", null ],
    [ "ant_sdm_page_2_encode", "group__ant__sdk__profiles__sdm__page2.html#ga80161e53cf24c6657f4b5318166124c0", null ]
];