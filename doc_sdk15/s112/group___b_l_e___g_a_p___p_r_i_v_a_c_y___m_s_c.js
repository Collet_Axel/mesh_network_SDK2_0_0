var group___b_l_e___g_a_p___p_r_i_v_a_c_y___m_s_c =
[
    [ "Directed Advertising", "group___b_l_e___g_a_p___p_r_i_v_a_c_y___a_d_v___d_i_r___p_r_i_v___m_s_c.html", null ],
    [ "Peripheral Connection Establishment with Private Peer", "group___b_l_e___g_a_p___p_e_r_i_p_h___c_o_n_n___p_r_i_v___m_s_c.html", null ],
    [ "Peripheral PHY Update", "group___b_l_e___g_a_p___p_e_r_i_p_h_e_r_a_l___p_h_y___u_p_d_a_t_e.html", null ],
    [ "Private Advertising", "group___b_l_e___g_a_p___p_r_i_v_a_c_y___a_d_v___m_s_c.html", null ]
];