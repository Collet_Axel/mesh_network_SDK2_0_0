var group__HEALTH__SERVER =
[
    [ "health_server_selftest_t", "structhealth__server__selftest__t.html", [
      [ "test_id", "structhealth__server__selftest__t.html#a6766f6bf32fb0993d93af21c65f18449", null ],
      [ "selftest_function", "structhealth__server__selftest__t.html#a2db707d54310518267961d7338b76330", null ]
    ] ],
    [ "health_server_t", "struct____health__server__t.html", [
      [ "model_handle", "struct____health__server__t.html#ab5693cbd3c7df50459a1a7ea650da489", null ],
      [ "fast_period_divisor", "struct____health__server__t.html#a05d6edd32e5be5e21890fd9d87f7fdd1", null ],
      [ "regular_publish_res", "struct____health__server__t.html#a7aab8227ab03bd0f3e10c28e63187788", null ],
      [ "regular_publish_steps", "struct____health__server__t.html#a9654ec1bad241ab02e7803138df825aa", null ],
      [ "p_selftests", "struct____health__server__t.html#a25b2d09fa4990c291bb6aed56f251cad", null ],
      [ "num_selftests", "struct____health__server__t.html#ac60c803bd29960603de1363fed75cae0", null ],
      [ "previous_test_id", "struct____health__server__t.html#a0dd7a1355975bfc5055b38c6fbfd024f", null ],
      [ "company_id", "struct____health__server__t.html#a65133f2d7f5e88c77a94931d8c98b2ff", null ],
      [ "registered_faults", "struct____health__server__t.html#a4493060358a713ff422d3af5d5066712", null ],
      [ "current_faults", "struct____health__server__t.html#a52404ffecfb4e346e5050eafb5400a93", null ],
      [ "attention_handler", "struct____health__server__t.html#ad9de9ef074adc805cd9b19a728e40694", null ],
      [ "attention_timer", "struct____health__server__t.html#a1415887caacb88831d73a60accef246b", null ],
      [ "p_next", "struct____health__server__t.html#af0036588321436ff7c78e03d86c42e9b", null ]
    ] ],
    [ "HEALTH_SERVER_FAULT_ARRAY_SIZE", "group__HEALTH__SERVER.html#ga0fc920dbd748aaa0da370fd87014f4c0", null ],
    [ "health_server_attention_cb_t", "group__HEALTH__SERVER.html#ga26606d955ff76d95f090225e0446bbe8", null ],
    [ "health_server_selftest_cb_t", "group__HEALTH__SERVER.html#ga39ee29ee9f61c4b5dd16c55ee7e4e66e", null ],
    [ "health_server_fault_array_t", "group__HEALTH__SERVER.html#gaaf34e45bc6ebd027def97b12e6757cad", null ],
    [ "health_server_fault_register", "group__HEALTH__SERVER.html#ga63e9eab454f7b6d760a2912884c17d3c", null ],
    [ "health_server_fault_clear", "group__HEALTH__SERVER.html#ga7170b6fb684002214829ba2e497bb1fd", null ],
    [ "health_server_fault_is_set", "group__HEALTH__SERVER.html#gafd615b7667d3d6b1fc8d9760458e6ea3", null ],
    [ "health_server_fault_count_get", "group__HEALTH__SERVER.html#ga89758b0115c5da9f85ab25fcf77c7325", null ],
    [ "health_server_attention_get", "group__HEALTH__SERVER.html#gad3e6b89e149b43689fd4365e003ffe93", null ],
    [ "health_server_attention_set", "group__HEALTH__SERVER.html#ga415462096e2ab3cd4c1805f9a246c9ba", null ],
    [ "health_server_init", "group__HEALTH__SERVER.html#ga5a844a8893196ed28be77358eacb42ca", null ]
];