var structnrf__mesh__evt__message__t =
[
    [ "p_buffer", "structnrf__mesh__evt__message__t.html#ab0503fa847446d288d53ca7cd1ed3679", null ],
    [ "length", "structnrf__mesh__evt__message__t.html#a9e6613ed62e2ddd706b605b044b2c0b3", null ],
    [ "src", "structnrf__mesh__evt__message__t.html#a2b546364b29b5f90dfbba03f45c3116f", null ],
    [ "dst", "structnrf__mesh__evt__message__t.html#a90df61096470736f82c04693d36d0e45", null ],
    [ "secmat", "structnrf__mesh__evt__message__t.html#af50d5f27a08dfad5932961a5602c5403", null ],
    [ "ttl", "structnrf__mesh__evt__message__t.html#ac19b5d19aff6161382251c5953e9ff01", null ],
    [ "p_metadata", "structnrf__mesh__evt__message__t.html#a9b671a5c8d2dbe56e755ab79f162e74a", null ]
];