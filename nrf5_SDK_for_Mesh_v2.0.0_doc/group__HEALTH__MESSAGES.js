var group__HEALTH__MESSAGES =
[
    [ "health_msg_fault_status_t", "structhealth__msg__fault__status__t.html", [
      [ "test_id", "structhealth__msg__fault__status__t.html#a3c3eb250f0bba4adf9a525bbbfb08423", null ],
      [ "company_id", "structhealth__msg__fault__status__t.html#a80254c5e9975b4aa4814ce2e483d0c7c", null ],
      [ "fault_array", "structhealth__msg__fault__status__t.html#ab3f52b3d8a46226c262f6e86ab2e0c92", null ]
    ] ],
    [ "health_msg_period_status_t", "structhealth__msg__period__status__t.html", [
      [ "fast_period_divisor", "structhealth__msg__period__status__t.html#a1698a31714d719120fe9f063b182f878", null ]
    ] ],
    [ "health_msg_attention_status_t", "structhealth__msg__attention__status__t.html", [
      [ "attention", "structhealth__msg__attention__status__t.html#a428f7cb55d48c4241b6a08a7db378106", null ]
    ] ],
    [ "health_msg_fault_get_t", "structhealth__msg__fault__get__t.html", [
      [ "company_id", "structhealth__msg__fault__get__t.html#ae9a6d297e8661f01aea5c727e9ac367a", null ]
    ] ],
    [ "health_msg_fault_clear_t", "structhealth__msg__fault__clear__t.html", [
      [ "company_id", "structhealth__msg__fault__clear__t.html#a8f6f1ae832a3b7cbd282e3c42b6ef144", null ]
    ] ],
    [ "health_msg_fault_test_t", "structhealth__msg__fault__test__t.html", [
      [ "test_id", "structhealth__msg__fault__test__t.html#ab8ec0fb06bd580683c383dfcee749566", null ],
      [ "company_id", "structhealth__msg__fault__test__t.html#ac8457b8f7b50038f8a17fe4cc0fad9fc", null ]
    ] ],
    [ "health_msg_period_set_t", "structhealth__msg__period__set__t.html", [
      [ "fast_period_divisor", "structhealth__msg__period__set__t.html#a8716ec1636207fc95e636d174487518e", null ]
    ] ],
    [ "health_msg_attention_set_t", "structhealth__msg__attention__set__t.html", [
      [ "attention", "structhealth__msg__attention__set__t.html#aa5c954a224c96ff915deab0ca179e0ee", null ]
    ] ]
];