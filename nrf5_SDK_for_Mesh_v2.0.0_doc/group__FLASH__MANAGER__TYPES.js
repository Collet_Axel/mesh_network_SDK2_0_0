var group__FLASH__MANAGER__TYPES =
[
    [ "fm_header_t", "structfm__header__t.html", [
      [ "len_words", "structfm__header__t.html#a92ea77b2875082fa3f7bd2fba08b42b8", null ],
      [ "handle", "structfm__header__t.html#a912c619c5fc7116710a77057b0392b33", null ]
    ] ],
    [ "fm_handle_filter_t", "structfm__handle__filter__t.html", [
      [ "mask", "structfm__handle__filter__t.html#ab7e4a88ab84d432092be32706b87e182", null ],
      [ "match", "structfm__handle__filter__t.html#a5f74fe0270bf0d4fa4a951fb53e8fb1e", null ]
    ] ],
    [ "fm_entry_t", "structfm__entry__t.html", [
      [ "header", "structfm__entry__t.html#a8fd2539af8e510da9893eb1e92043688", null ],
      [ "data", "structfm__entry__t.html#a5f72ebf5d776bc9a513770fcb97355c9", null ]
    ] ],
    [ "flash_manager_metadata_t", "structflash__manager__metadata__t.html", [
      [ "metadata_len", "structflash__manager__metadata__t.html#adf0d6f2b919b483566509e1049d0cdad", null ],
      [ "entry_header_length", "structflash__manager__metadata__t.html#a147c8e1dbd7939525105cbca56106306", null ],
      [ "entry_len_length_bits", "structflash__manager__metadata__t.html#a97cf55bc39f06766c64c4ab75b8ca578", null ],
      [ "entry_type_length_bits", "structflash__manager__metadata__t.html#a76a6979778f2b8952d8fc63cd0b7483d", null ],
      [ "pages_in_area", "structflash__manager__metadata__t.html#a62a0babe6d35a8581cf1120520ea7998", null ],
      [ "page_index", "structflash__manager__metadata__t.html#abef21152b9a11b1e420eeaf0e7baaf92", null ],
      [ "_padding", "structflash__manager__metadata__t.html#ad7f0cb5cef5a6e4b629352e87c2b67b7", null ]
    ] ],
    [ "flash_manager_page_t", "unionflash__manager__page__t.html", [
      [ "metadata", "unionflash__manager__page__t.html#a9816318e0663f3c659f9948763b52186", null ],
      [ "raw", "unionflash__manager__page__t.html#a1eda90916b7cfb6d4401ada11849b85c", null ]
    ] ],
    [ "flash_manager_config_t", "structflash__manager__config__t.html", [
      [ "p_area", "structflash__manager__config__t.html#a90e62b01a0813bc7bec5a570defa44c4", null ],
      [ "page_count", "structflash__manager__config__t.html#a96ddda237674154c975f50fb8fe2ea6d", null ],
      [ "min_available_space", "structflash__manager__config__t.html#ab9348efaf5cb1ff2bb36cf8aa0c9451c", null ],
      [ "write_complete_cb", "structflash__manager__config__t.html#a005424f311d9c879d2937fc77eae4685", null ],
      [ "invalidate_complete_cb", "structflash__manager__config__t.html#a485f634c8cbb5167fc62fd8345aabc6a", null ],
      [ "remove_complete_cb", "structflash__manager__config__t.html#a4b145549026df53dd4c6fb393f96fe77", null ]
    ] ],
    [ "flash_manager_internal_state_t", "structflash__manager__internal__state__t.html", [
      [ "state", "structflash__manager__internal__state__t.html#abbafa5de1edb07bc01f69fdf4e1418b5", null ],
      [ "invalid_bytes", "structflash__manager__internal__state__t.html#a74edbab82ed6e541e888d57df24888bb", null ],
      [ "p_seal", "structflash__manager__internal__state__t.html#ab3c2124634cb549943ca52beccc684dc", null ]
    ] ],
    [ "flash_manager_t", "structflash__manager.html", [
      [ "internal", "structflash__manager.html#a89dac2bbba4b8dcb04760beddbc5e8f7", null ],
      [ "config", "structflash__manager.html#ac23c1dc754b99b4b1c2271f57a4478f4", null ]
    ] ],
    [ "fm_mem_listener_t", "structfm__mem__listener__t.html", [
      [ "queue_elem", "structfm__mem__listener__t.html#a52f2a4f851f04ed43c76f8fc7565e2b3", null ],
      [ "callback", "structfm__mem__listener__t.html#ab4fb499a6551af6e7154df3b26a08b8a", null ],
      [ "p_args", "structfm__mem__listener__t.html#a0cdd1b844c12418ef7714fe887fff863", null ]
    ] ],
    [ "fm_handle_t", "group__FLASH__MANAGER__TYPES.html#ga513874318ba367a4c361469bc3561126", null ],
    [ "flash_manager_write_complete_cb_t", "group__FLASH__MANAGER__TYPES.html#gab6b5d0044ee96995dd9dc4209d7d163b", null ],
    [ "flash_manager_invalidate_complete_cb_t", "group__FLASH__MANAGER__TYPES.html#ga4f56320a6e1b02b3cbe77707a9127280", null ],
    [ "flash_manager_remove_complete_cb_t", "group__FLASH__MANAGER__TYPES.html#gaf73fcc0dd34db04a0d31881d6c94d61f", null ],
    [ "flash_manager_queue_empty_cb_t", "group__FLASH__MANAGER__TYPES.html#gab535c37c5ce2f9e561ed788b30585059", null ],
    [ "flash_manager_mem_listener_cb_t", "group__FLASH__MANAGER__TYPES.html#ga810c6cab809cb2d9fe43e80a938ed016", null ],
    [ "fm_state_t", "group__FLASH__MANAGER__TYPES.html#gaf621cc8ac2d8a3de48a445a725869eb5", null ],
    [ "fm_result_t", "group__FLASH__MANAGER__TYPES.html#ga38a2b21a3eafcbbc299e0c840adc6060", [
      [ "FM_RESULT_SUCCESS", "group__FLASH__MANAGER__TYPES.html#gga38a2b21a3eafcbbc299e0c840adc6060a88cbd5e830585a91ee607251b6b21d72", null ],
      [ "FM_RESULT_ERROR_AREA_FULL", "group__FLASH__MANAGER__TYPES.html#gga38a2b21a3eafcbbc299e0c840adc6060a0c3f8135e12ed7bceca8017d11cfb922", null ],
      [ "FM_RESULT_ERROR_NOT_FOUND", "group__FLASH__MANAGER__TYPES.html#gga38a2b21a3eafcbbc299e0c840adc6060a1a013c40fed97f12bad5129f6cda88dc", null ],
      [ "FM_RESULT_ERROR_FLASH_MALFUNCTION", "group__FLASH__MANAGER__TYPES.html#gga38a2b21a3eafcbbc299e0c840adc6060ac5a135959e08ed99b738701ab2e93740", null ]
    ] ]
];