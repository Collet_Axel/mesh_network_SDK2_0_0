var group__MESH__ADV =
[
    [ "MESH_SOFTDEVICE_CONN_CFG_TAG", "group__MESH__ADV.html#gaf20ddfbc1355b878609ade6a13399433", null ],
    [ "MESH_ADV_INTERVAL_DEFAULT", "group__MESH__ADV.html#gabdb9d4b40314bf44b9d8b2e3efdf711d", null ],
    [ "MESH_ADV_TIMEOUT_INFINITE", "group__MESH__ADV.html#ga09db97f3317d57fa0789c18220ea18cd", null ],
    [ "mesh_adv_data_set", "group__MESH__ADV.html#ga3a90c3901ac49edf967e9805a09ceaad", null ],
    [ "mesh_adv_params_set", "group__MESH__ADV.html#ga2d8e5fb1fcef0490f47671286a8c0c79", null ],
    [ "mesh_adv_start", "group__MESH__ADV.html#ga0f78bb6ab45bd9dadc6f0ac60579e820", null ],
    [ "mesh_adv_stop", "group__MESH__ADV.html#gaf8b3e9ac1914009120686716332cc2d0", null ]
];