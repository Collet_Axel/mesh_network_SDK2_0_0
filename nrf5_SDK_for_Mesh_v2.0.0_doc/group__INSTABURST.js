var group__INSTABURST =
[
    [ "Defines", "group__INSTABURST__DEFINES.html", "group__INSTABURST__DEFINES" ],
    [ "Experimental Instaburst RX module", "group__INSTABURST__RX.html", "group__INSTABURST__RX" ],
    [ "Experimental Instaburst TX module", "group__INSTABURST__TX.html", "group__INSTABURST__TX" ],
    [ "instaburst_init", "group__INSTABURST.html#ga3b395848914279bf49c84fd7336cf2e0", null ]
];