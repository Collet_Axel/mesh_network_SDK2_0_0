var group__MESH__CONFIG__BEARER =
[
    [ "APPLICATION_TX_COMPLETE_OVERHEAD_US", "group__MESH__CONFIG__BEARER.html#gae26d5a58b4b0e97b639570b3935a56bb", null ],
    [ "BEARER_ACCESS_ADDR_DEFAULT", "group__MESH__CONFIG__BEARER.html#gad7bfe0d7bdc64cf24dbbb2858e2a6679", null ],
    [ "BEARER_ADV_CHANNELS_MAX", "group__MESH__CONFIG__BEARER.html#ga0f8fc5b3c18ccb64a09f7f81560306de", null ],
    [ "BEARER_ADV_INT_DEFAULT_MS", "group__MESH__CONFIG__BEARER.html#gac5b6acbc328ca1cc23ee1278e0c56d79", null ],
    [ "BEARER_SCAN_INT_DEFAULT_MS", "group__MESH__CONFIG__BEARER.html#gabdf3b07a5df11f5f6ea62a4a17cfe8d9", null ],
    [ "BEARER_SCAN_WINDOW_DEFAULT_MS", "group__MESH__CONFIG__BEARER.html#ga5901fe7867c41ee1b8fa8bd73abff961", null ],
    [ "SCANNER_BUFFER_SIZE", "group__MESH__CONFIG__BEARER.html#ga6d1fe8ce81e54d6d70e734bf39dee4b0", null ],
    [ "INSTABURST_RX_BUFFER_SIZE", "group__MESH__CONFIG__BEARER.html#ga6d78fe1e30deeee4d9c8e753459cb934", null ],
    [ "EXPERIMENTAL_INSTABURST_ENABLED", "group__MESH__CONFIG__BEARER.html#gae33734ea950eecda45df2006c64d8d86", null ]
];