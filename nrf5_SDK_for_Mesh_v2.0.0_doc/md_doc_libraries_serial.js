var md_doc_libraries_serial =
[
    [ "Serial commands", "md_doc_libraries_serial_cmd.html", [
      [ "Serial command overview", "md_doc_libraries_serial_cmd.html#serial-commands", [
        [ "Device Commands", "md_doc_libraries_serial_cmd.html#device-commands", null ],
        [ "Application Commands", "md_doc_libraries_serial_cmd.html#application-commands", null ],
        [ "Segmentation And Reassembly Commands", "md_doc_libraries_serial_cmd.html#segmentation-and-reassembly-commands", null ],
        [ "Configuration Commands", "md_doc_libraries_serial_cmd.html#configuration-commands", null ],
        [ "Provisioning Commands", "md_doc_libraries_serial_cmd.html#provisioning-commands", null ],
        [ "nRF Open Mesh Commands", "md_doc_libraries_serial_cmd.html#nrf-open-mesh-commands", null ],
        [ "Bluetooth Mesh Commands", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-commands", null ],
        [ "Direct Firmware Upgrade Commands", "md_doc_libraries_serial_cmd.html#direct-firmware-upgrade-commands", null ],
        [ "Access Layer Commands", "md_doc_libraries_serial_cmd.html#access-layer-commands", null ],
        [ "Model Specific Commands", "md_doc_libraries_serial_cmd.html#model-specific-commands", null ],
        [ "Serial Command Details", "md_doc_libraries_serial_cmd.html#serial-command-details", [
          [ "Device Echo", "md_doc_libraries_serial_cmd.html#device-echo", null ],
          [ "Device Internal Events Report", "md_doc_libraries_serial_cmd.html#device-internal-events-report", null ],
          [ "Device Serial Version Get", "md_doc_libraries_serial_cmd.html#device-serial-version-get", null ],
          [ "Device FW Info Get", "md_doc_libraries_serial_cmd.html#device-fw-info-get", null ],
          [ "Device Radio Reset", "md_doc_libraries_serial_cmd.html#device-radio-reset", null ],
          [ "Device Beacon Start", "md_doc_libraries_serial_cmd.html#device-beacon-start", null ],
          [ "Device Beacon Stop", "md_doc_libraries_serial_cmd.html#device-beacon-stop", null ],
          [ "Device Beacon Params Get", "md_doc_libraries_serial_cmd.html#device-beacon-params-get", null ],
          [ "Device Beacon Params Set", "md_doc_libraries_serial_cmd.html#device-beacon-params-set", null ],
          [ "Application Application", "md_doc_libraries_serial_cmd.html#application-application", null ],
          [ "Segmentation And Reassembly Start", "md_doc_libraries_serial_cmd.html#segmentation-and-reassembly-start", null ],
          [ "Segmentation And Reassembly Continue", "md_doc_libraries_serial_cmd.html#segmentation-and-reassembly-continue", null ],
          [ "Configuration Adv Addr Set", "md_doc_libraries_serial_cmd.html#configuration-adv-addr-set", null ],
          [ "Configuration Adv Addr Get", "md_doc_libraries_serial_cmd.html#configuration-adv-addr-get", null ],
          [ "Configuration Channel Map Set", "md_doc_libraries_serial_cmd.html#configuration-channel-map-set", null ],
          [ "Configuration Channel Map Get", "md_doc_libraries_serial_cmd.html#configuration-channel-map-get", null ],
          [ "Configuration TX Power Set", "md_doc_libraries_serial_cmd.html#configuration-tx-power-set", null ],
          [ "Configuration TX Power Get", "md_doc_libraries_serial_cmd.html#configuration-tx-power-get", null ],
          [ "Configuration UUID Set", "md_doc_libraries_serial_cmd.html#configuration-uuid-set", null ],
          [ "Configuration UUID Get", "md_doc_libraries_serial_cmd.html#configuration-uuid-get", null ],
          [ "Provisioning Scan Start", "md_doc_libraries_serial_cmd.html#provisioning-scan-start", null ],
          [ "Provisioning Scan Stop", "md_doc_libraries_serial_cmd.html#provisioning-scan-stop", null ],
          [ "Provisioning Provision", "md_doc_libraries_serial_cmd.html#provisioning-provision", null ],
          [ "Provisioning Listen", "md_doc_libraries_serial_cmd.html#provisioning-listen", null ],
          [ "Provisioning OOB Use", "md_doc_libraries_serial_cmd.html#provisioning-oob-use", null ],
          [ "Provisioning Auth Data", "md_doc_libraries_serial_cmd.html#provisioning-auth-data", null ],
          [ "Provisioning ECDH Secret", "md_doc_libraries_serial_cmd.html#provisioning-ecdh-secret", null ],
          [ "Provisioning Keypair Set", "md_doc_libraries_serial_cmd.html#provisioning-keypair-set", null ],
          [ "Provisioning Capabilities Set", "md_doc_libraries_serial_cmd.html#provisioning-capabilities-set", null ],
          [ "nRF Open Mesh Init", "md_doc_libraries_serial_cmd.html#nrf-open-mesh-init", null ],
          [ "nRF Open Mesh Value Set", "md_doc_libraries_serial_cmd.html#nrf-open-mesh-value-set", null ],
          [ "nRF Open Mesh Value Enable", "md_doc_libraries_serial_cmd.html#nrf-open-mesh-value-enable", null ],
          [ "nRF Open Mesh Value Disable", "md_doc_libraries_serial_cmd.html#nrf-open-mesh-value-disable", null ],
          [ "nRF Open Mesh Start", "md_doc_libraries_serial_cmd.html#nrf-open-mesh-start", null ],
          [ "nRF Open Mesh Stop", "md_doc_libraries_serial_cmd.html#nrf-open-mesh-stop", null ],
          [ "nRF Open Mesh Flag Set", "md_doc_libraries_serial_cmd.html#nrf-open-mesh-flag-set", null ],
          [ "nRF Open Mesh Flag Get", "md_doc_libraries_serial_cmd.html#nrf-open-mesh-flag-get", null ],
          [ "nRF Open Mesh DFU Data", "md_doc_libraries_serial_cmd.html#nrf-open-mesh-dfu-data", null ],
          [ "nRF Open Mesh Value Get", "md_doc_libraries_serial_cmd.html#nrf-open-mesh-value-get", null ],
          [ "nRF Open Mesh Build Version Get", "md_doc_libraries_serial_cmd.html#nrf-open-mesh-build-version-get", null ],
          [ "nRF Open Mesh Access Addr Get", "md_doc_libraries_serial_cmd.html#nrf-open-mesh-access-addr-get", null ],
          [ "nRF Open Mesh Channel Get", "md_doc_libraries_serial_cmd.html#nrf-open-mesh-channel-get", null ],
          [ "nRF Open Mesh Interval Min ms Get", "md_doc_libraries_serial_cmd.html#nrf-open-mesh-interval-min-ms-get", null ],
          [ "Bluetooth Mesh Enable", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-enable", null ],
          [ "Bluetooth Mesh Disable", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-disable", null ],
          [ "Bluetooth Mesh Subnet Add", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-subnet-add", null ],
          [ "Bluetooth Mesh Subnet Update", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-subnet-update", null ],
          [ "Bluetooth Mesh Subnet Delete", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-subnet-delete", null ],
          [ "Bluetooth Mesh Subnet Get All", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-subnet-get-all", null ],
          [ "Bluetooth Mesh Subnet Count Max Get", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-subnet-count-max-get", null ],
          [ "Bluetooth Mesh Appkey Add", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-appkey-add", null ],
          [ "Bluetooth Mesh Appkey Update", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-appkey-update", null ],
          [ "Bluetooth Mesh Appkey Delete", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-appkey-delete", null ],
          [ "Bluetooth Mesh Appkey Get All", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-appkey-get-all", null ],
          [ "Bluetooth Mesh Appkey Count Max Get", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-appkey-count-max-get", null ],
          [ "Bluetooth Mesh Devkey Add", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-devkey-add", null ],
          [ "Bluetooth Mesh Devkey Delete", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-devkey-delete", null ],
          [ "Bluetooth Mesh Devkey Count Max Get", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-devkey-count-max-get", null ],
          [ "Bluetooth Mesh Addr Local Unicast Set", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-addr-local-unicast-set", null ],
          [ "Bluetooth Mesh Addr Local Unicast Get", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-addr-local-unicast-get", null ],
          [ "Bluetooth Mesh Addr Get", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-addr-get", null ],
          [ "Bluetooth Mesh Addr Get All", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-addr-get-all", null ],
          [ "Bluetooth Mesh Addr Nonvirtual Count Max Get", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-addr-nonvirtual-count-max-get", null ],
          [ "Bluetooth Mesh Addr Virtual Count Max Get", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-addr-virtual-count-max-get", null ],
          [ "Bluetooth Mesh Addr Subscription Add", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-addr-subscription-add", null ],
          [ "Bluetooth Mesh Addr Subscription Add Virtual", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-addr-subscription-add-virtual", null ],
          [ "Bluetooth Mesh Addr Subscription Remove", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-addr-subscription-remove", null ],
          [ "Bluetooth Mesh Addr Publication Add", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-addr-publication-add", null ],
          [ "Bluetooth Mesh Addr Publication Add Virtual", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-addr-publication-add-virtual", null ],
          [ "Bluetooth Mesh Addr Publication Remove", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-addr-publication-remove", null ],
          [ "Bluetooth Mesh Packet Send", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-packet-send", null ],
          [ "Bluetooth Mesh State Clear", "md_doc_libraries_serial_cmd.html#bluetooth-mesh-state-clear", null ],
          [ "Direct Firmware Upgrade Jump To Bootloader", "md_doc_libraries_serial_cmd.html#direct-firmware-upgrade-jump-to-bootloader", null ],
          [ "Direct Firmware Upgrade Request", "md_doc_libraries_serial_cmd.html#direct-firmware-upgrade-request", null ],
          [ "Direct Firmware Upgrade Relay", "md_doc_libraries_serial_cmd.html#direct-firmware-upgrade-relay", null ],
          [ "Direct Firmware Upgrade Abort", "md_doc_libraries_serial_cmd.html#direct-firmware-upgrade-abort", null ],
          [ "Direct Firmware Upgrade Bank Info Get", "md_doc_libraries_serial_cmd.html#direct-firmware-upgrade-bank-info-get", null ],
          [ "Direct Firmware Upgrade Bank Flash", "md_doc_libraries_serial_cmd.html#direct-firmware-upgrade-bank-flash", null ],
          [ "Direct Firmware Upgrade State Get", "md_doc_libraries_serial_cmd.html#direct-firmware-upgrade-state-get", null ],
          [ "Access Layer Model Pub Addr Set", "md_doc_libraries_serial_cmd.html#access-layer-model-pub-addr-set", null ],
          [ "Access Layer Model Pub Addr Get", "md_doc_libraries_serial_cmd.html#access-layer-model-pub-addr-get", null ],
          [ "Access Layer Model Pub Period Set", "md_doc_libraries_serial_cmd.html#access-layer-model-pub-period-set", null ],
          [ "Access Layer Model Pub Period Get", "md_doc_libraries_serial_cmd.html#access-layer-model-pub-period-get", null ],
          [ "Access Layer Model Subs Add", "md_doc_libraries_serial_cmd.html#access-layer-model-subs-add", null ],
          [ "Access Layer Model Subs Remove", "md_doc_libraries_serial_cmd.html#access-layer-model-subs-remove", null ],
          [ "Access Layer Model Subs Get", "md_doc_libraries_serial_cmd.html#access-layer-model-subs-get", null ],
          [ "Access Layer Model App Bind", "md_doc_libraries_serial_cmd.html#access-layer-model-app-bind", null ],
          [ "Access Layer Model App Unbind", "md_doc_libraries_serial_cmd.html#access-layer-model-app-unbind", null ],
          [ "Access Layer Model App Get", "md_doc_libraries_serial_cmd.html#access-layer-model-app-get", null ],
          [ "Access Layer Model Pub App Set", "md_doc_libraries_serial_cmd.html#access-layer-model-pub-app-set", null ],
          [ "Access Layer Model Pub App Get", "md_doc_libraries_serial_cmd.html#access-layer-model-pub-app-get", null ],
          [ "Access Layer Model Pub TTL Set", "md_doc_libraries_serial_cmd.html#access-layer-model-pub-ttl-set", null ],
          [ "Access Layer Model Pub TTL Get", "md_doc_libraries_serial_cmd.html#access-layer-model-pub-ttl-get", null ],
          [ "Access Layer Elem Loc Set", "md_doc_libraries_serial_cmd.html#access-layer-elem-loc-set", null ],
          [ "Access Layer Elem Loc Get", "md_doc_libraries_serial_cmd.html#access-layer-elem-loc-get", null ],
          [ "Access Layer Elem Sig Model Count Get", "md_doc_libraries_serial_cmd.html#access-layer-elem-sig-model-count-get", null ],
          [ "Access Layer Elem Vendor Model Count Get", "md_doc_libraries_serial_cmd.html#access-layer-elem-vendor-model-count-get", null ],
          [ "Access Layer Model ID Get", "md_doc_libraries_serial_cmd.html#access-layer-model-id-get", null ],
          [ "Access Layer Handle Get", "md_doc_libraries_serial_cmd.html#access-layer-handle-get", null ],
          [ "Access Layer Elem Models Get", "md_doc_libraries_serial_cmd.html#access-layer-elem-models-get", null ],
          [ "Access Layer Access Flash Store", "md_doc_libraries_serial_cmd.html#access-layer-access-flash-store", null ],
          [ "Model Specific Models Get", "md_doc_libraries_serial_cmd.html#model-specific-models-get", null ],
          [ "Model Specific Init", "md_doc_libraries_serial_cmd.html#model-specific-init", null ],
          [ "Model Specific Command", "md_doc_libraries_serial_cmd.html#model-specific-command", null ]
        ] ]
      ] ]
    ] ],
    [ "Serial events", "md_doc_libraries_serial_evt.html", [
      [ "Serial Event Overview", "md_doc_libraries_serial_evt.html#serial-events", [
        [ "Serial Event Details", "md_doc_libraries_serial_evt.html#serial-event-details", [
          [ "Cmd Rsp", "md_doc_libraries_serial_evt.html#cmd-rsp", null ],
          [ "Device Started", "md_doc_libraries_serial_evt.html#device-started", null ],
          [ "Device Echo Rsp", "md_doc_libraries_serial_evt.html#device-echo-rsp", null ],
          [ "Device Internal Event", "md_doc_libraries_serial_evt.html#device-internal-event", null ],
          [ "Application", "md_doc_libraries_serial_evt.html#application", null ],
          [ "SAR Start", "md_doc_libraries_serial_evt.html#sar-start", null ],
          [ "SAR Continue", "md_doc_libraries_serial_evt.html#sar-continue", null ],
          [ "DFU Req Relay", "md_doc_libraries_serial_evt.html#dfu-req-relay", null ],
          [ "DFU Req Source", "md_doc_libraries_serial_evt.html#dfu-req-source", null ],
          [ "DFU Start", "md_doc_libraries_serial_evt.html#dfu-start", null ],
          [ "DFU End", "md_doc_libraries_serial_evt.html#dfu-end", null ],
          [ "DFU Bank Available", "md_doc_libraries_serial_evt.html#dfu-bank-available", null ],
          [ "DFU Firmware Outdated", "md_doc_libraries_serial_evt.html#dfu-firmware-outdated", null ],
          [ "DFU Firmware Outdated No Auth", "md_doc_libraries_serial_evt.html#dfu-firmware-outdated-no-auth", null ],
          [ "Openmesh New", "md_doc_libraries_serial_evt.html#openmesh-new", null ],
          [ "Openmesh Update", "md_doc_libraries_serial_evt.html#openmesh-update", null ],
          [ "Openmesh Conflicting", "md_doc_libraries_serial_evt.html#openmesh-conflicting", null ],
          [ "Openmesh TX", "md_doc_libraries_serial_evt.html#openmesh-tx", null ],
          [ "Prov Unprovisioned Received", "md_doc_libraries_serial_evt.html#prov-unprovisioned-received", null ],
          [ "Prov Link Established", "md_doc_libraries_serial_evt.html#prov-link-established", null ],
          [ "Prov Link Closed", "md_doc_libraries_serial_evt.html#prov-link-closed", null ],
          [ "Prov Caps Received", "md_doc_libraries_serial_evt.html#prov-caps-received", null ],
          [ "Prov Complete", "md_doc_libraries_serial_evt.html#prov-complete", null ],
          [ "Prov Auth Request", "md_doc_libraries_serial_evt.html#prov-auth-request", null ],
          [ "Prov ECDH Request", "md_doc_libraries_serial_evt.html#prov-ecdh-request", null ],
          [ "Prov Output Request", "md_doc_libraries_serial_evt.html#prov-output-request", null ],
          [ "Prov Failed", "md_doc_libraries_serial_evt.html#prov-failed", null ],
          [ "Mesh Message Received Unicast", "md_doc_libraries_serial_evt.html#mesh-message-received-unicast", null ],
          [ "Mesh Message Received Subscription", "md_doc_libraries_serial_evt.html#mesh-message-received-subscription", null ],
          [ "Mesh TX Complete", "md_doc_libraries_serial_evt.html#mesh-tx-complete", null ],
          [ "Mesh IV Update Notification", "md_doc_libraries_serial_evt.html#mesh-iv-update-notification", null ],
          [ "Mesh Key Refresh Notification", "md_doc_libraries_serial_evt.html#mesh-key-refresh-notification", null ],
          [ "Mesh SAR Failed", "md_doc_libraries_serial_evt.html#mesh-sar-failed", null ],
          [ "Model Specific", "md_doc_libraries_serial_evt.html#model-specific", null ]
        ] ]
      ] ]
    ] ],
    [ "Serial status codes", "md_doc_libraries_serial_status.html", null ],
    [ "Interactive PyACI", "md_scripts_interactive_pyaci_README.html", "md_scripts_interactive_pyaci_README" ]
];