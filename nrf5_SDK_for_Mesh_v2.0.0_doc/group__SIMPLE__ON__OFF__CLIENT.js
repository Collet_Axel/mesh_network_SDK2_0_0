var group__SIMPLE__ON__OFF__CLIENT =
[
    [ "simple_on_off_client_t", "struct____simple__on__off__client.html", [
      [ "model_handle", "struct____simple__on__off__client.html#a238fc799571bd4ae7cbfc1722e679e55", null ],
      [ "status_cb", "struct____simple__on__off__client.html#a1203943a05414b3039529ce4a34bf813", null ],
      [ "timeout_cb", "struct____simple__on__off__client.html#aabcbe08b8d0c6e66ba3c0f9648ee8d83", null ],
      [ "reliable_transfer_active", "struct____simple__on__off__client.html#af1dc1f0b4f75c0667fa6606d78363cf5", null ],
      [ "data", "struct____simple__on__off__client.html#a8dba57542d69f4ff1fc58edfcca76d47", null ],
      [ "state", "struct____simple__on__off__client.html#adbde7501a65d892259aaf939681d53d0", null ]
    ] ],
    [ "SIMPLE_ON_OFF_CLIENT_MODEL_ID", "group__SIMPLE__ON__OFF__CLIENT.html#gaa5dd8d09e90a2947df7ff9649e142e1e", null ],
    [ "simple_on_off_status_cb_t", "group__SIMPLE__ON__OFF__CLIENT.html#ga7083bbfd583bae2e31bbaf8d0f0d5166", null ],
    [ "simple_on_off_timeout_cb_t", "group__SIMPLE__ON__OFF__CLIENT.html#gac938c97e29a9fca24f83dc521543bf64", null ],
    [ "simple_on_off_status_t", "group__SIMPLE__ON__OFF__CLIENT.html#gabe95b35c7eb90dbee7c0942724319150", [
      [ "SIMPLE_ON_OFF_STATUS_ON", "group__SIMPLE__ON__OFF__CLIENT.html#ggabe95b35c7eb90dbee7c0942724319150a68a4aa35e2134cbb8b7e07358dc88674", null ],
      [ "SIMPLE_ON_OFF_STATUS_OFF", "group__SIMPLE__ON__OFF__CLIENT.html#ggabe95b35c7eb90dbee7c0942724319150a89d2fa59d4ceb9a0d2c53185f6585d36", null ],
      [ "SIMPLE_ON_OFF_STATUS_ERROR_NO_REPLY", "group__SIMPLE__ON__OFF__CLIENT.html#ggabe95b35c7eb90dbee7c0942724319150a3d52ffd932aed53c342c9b106b8128a3", null ],
      [ "SIMPLE_ON_OFF_STATUS_CANCELLED", "group__SIMPLE__ON__OFF__CLIENT.html#ggabe95b35c7eb90dbee7c0942724319150ad92e54855073c7c7130665fdfee681b4", null ]
    ] ],
    [ "simple_on_off_client_init", "group__SIMPLE__ON__OFF__CLIENT.html#gae374f14a58ea4b243b97dbd16f8bdbd5", null ],
    [ "simple_on_off_client_set", "group__SIMPLE__ON__OFF__CLIENT.html#ga4b24b0b23202083012f60fd50af3ec96", null ],
    [ "simple_on_off_client_set_unreliable", "group__SIMPLE__ON__OFF__CLIENT.html#ga767fba52228f833749d12b87187b26a9", null ],
    [ "simple_on_off_client_get", "group__SIMPLE__ON__OFF__CLIENT.html#ga7ac4f2e34aa436cf5b653cb8dfd8aa07", null ],
    [ "simple_on_off_client_pending_msg_cancel", "group__SIMPLE__ON__OFF__CLIENT.html#gab3557ea7891fe6de6ec5b0b4c6bb577d", null ]
];