var group__CONFIG__MODEL =
[
    [ "Composition data module", "group__COMPOSITION__DATA.html", "group__COMPOSITION__DATA" ],
    [ "Configuration client", "group__CONFIG__CLIENT.html", "group__CONFIG__CLIENT" ],
    [ "Message formats", "group__CONFIG__MESSAGES.html", "group__CONFIG__MESSAGES" ],
    [ "Opcodes", "group__CONFIG__OPCODES.html", "group__CONFIG__OPCODES" ],
    [ "Configuration server", "group__CONFIG__SERVER.html", "group__CONFIG__SERVER" ],
    [ "Packed index list", "group__PACKED__INDEX__LIST.html", "group__PACKED__INDEX__LIST" ]
];