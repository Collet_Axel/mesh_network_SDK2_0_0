var group__SIMPLE__ON__OFF__SERVER =
[
    [ "simple_on_off_server_t", "struct____simple__on__off__server.html", [
      [ "model_handle", "struct____simple__on__off__server.html#ab34cd10b48d19b90b00198317acc0e26", null ],
      [ "get_cb", "struct____simple__on__off__server.html#ad7d874da13647a951e9bd52557991549", null ],
      [ "set_cb", "struct____simple__on__off__server.html#a2fafcf9fd6dd20a2a02aa0877f294920", null ]
    ] ],
    [ "SIMPLE_ON_OFF_SERVER_MODEL_ID", "group__SIMPLE__ON__OFF__SERVER.html#gaea23488a2d22b6bb0b07763b98cb2f38", null ],
    [ "simple_on_off_get_cb_t", "group__SIMPLE__ON__OFF__SERVER.html#ga6c45931ded7d3e068213337e5e3ec7a2", null ],
    [ "simple_on_off_set_cb_t", "group__SIMPLE__ON__OFF__SERVER.html#gaab88d1a3c386beab817cf433e21c2c16", null ],
    [ "simple_on_off_server_init", "group__SIMPLE__ON__OFF__SERVER.html#gaf8b60b2a0b02edfbba35c4b71b785ec8", null ],
    [ "simple_on_off_server_status_publish", "group__SIMPLE__ON__OFF__SERVER.html#ga504213445266c213d6617d37b3293a4a", null ]
];