var group__NRF__MESH__CONFIG__CORE =
[
    [ "General configuration", "group__MESH__CONFIG__GENERAL.html", "group__MESH__CONFIG__GENERAL" ],
    [ "Encryption configuration", "group__MESH__CONFIG__ENC.html", "group__MESH__CONFIG__ENC" ],
    [ "Core TX configuration", "group__MESH__CONFIG__CORE__TX.html", "group__MESH__CONFIG__CORE__TX" ],
    [ "AES-CCM module configuration", "group__MESH__CONFIG__CCM.html", "group__MESH__CONFIG__CCM" ],
    [ "FIFO configuration", "group__MESH__CONFIG__FIFO.html", "group__MESH__CONFIG__FIFO" ],
    [ "Internal event logging configuration", "group__MESH__CONFIG__INTERNAL__EVENTS.html", "group__MESH__CONFIG__INTERNAL__EVENTS" ],
    [ "Log module configuration", "group__MESH__CONFIG__LOG.html", "group__MESH__CONFIG__LOG" ],
    [ "Message cache configuration", "group__MESH__CONFIG__MSG__CACHE.html", "group__MESH__CONFIG__MSG__CACHE" ],
    [ "Network configuration", "group__MESH__CONFIG__NETWORK.html", "group__MESH__CONFIG__NETWORK" ],
    [ "Transport layer configuration", "group__MESH__CONFIG__TRANSPORT.html", "group__MESH__CONFIG__TRANSPORT" ],
    [ "Packet manager configuration", "group__MESH__CONFIG__PACMAN.html", "group__MESH__CONFIG__PACMAN" ],
    [ "Packet buffer configuration", "group__MESH__CONFIG__PACKET__BUFFER.html", "group__MESH__CONFIG__PACKET__BUFFER" ],
    [ "Replay cache configuration.", "group__MESH__CONFIG__REPLAY__CACHE.html", "group__MESH__CONFIG__REPLAY__CACHE" ],
    [ "Flash manager configuration defines", "group__MESH__CONFIG__FLASH__MANAGER.html", "group__MESH__CONFIG__FLASH__MANAGER" ],
    [ "GATT configuration defines", "group__MESH__CONFIG__GATT.html", "group__MESH__CONFIG__GATT" ]
];