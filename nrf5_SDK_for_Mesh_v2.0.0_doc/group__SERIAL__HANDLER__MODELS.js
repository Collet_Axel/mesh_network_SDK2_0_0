var group__SERIAL__HANDLER__MODELS =
[
    [ "serial_handler_models_info_t", "structserial__handler__models__info__t.html", [
      [ "model_id", "structserial__handler__models__info__t.html#a4c596eb0b1ee968df3f8919391810db9", null ],
      [ "model_initialize", "structserial__handler__models__info__t.html#a7c13138936e30c1cff1dc521e9ccdca9", null ],
      [ "model_command", "structserial__handler__models__info__t.html#a20eacb12e04e2b41026993e665671eff", null ]
    ] ],
    [ "serial_handler_models_model_init_cb_t", "group__SERIAL__HANDLER__MODELS.html#ga3f0097e877f99c985ac05f35283826e1", null ],
    [ "serial_handler_models_model_command_cb_t", "group__SERIAL__HANDLER__MODELS.html#ga85f226df835633c39bf59184a6038427", null ],
    [ "serial_handler_models_rx", "group__SERIAL__HANDLER__MODELS.html#ga367c3c4b463fec6d745faffe597fa564", null ],
    [ "serial_handler_models_register", "group__SERIAL__HANDLER__MODELS.html#ga6fc96ef0fb03a382c4ad739f4403fe06", null ]
];