var group__MESH__DEFINES__NETWORK =
[
    [ "NETWORK_SEQNUM_BITS", "group__MESH__DEFINES__NETWORK.html#ga39093b385a99041e21c37d4c6d574886", null ],
    [ "NETWORK_SEQNUM_MAX", "group__MESH__DEFINES__NETWORK.html#gaeef70c109f3dc150d7cc67fa17c92a9e", null ],
    [ "NETWORK_RELAY_RETRANSMITS_MAX", "group__MESH__DEFINES__NETWORK.html#ga4413e9f92ac6fcc2cb5c14d831da081f", null ],
    [ "NETWORK_RELAY_INTERVAL_STEPS_MAX", "group__MESH__DEFINES__NETWORK.html#ga3ccb70723db87a82c6e1fec66dbff0fa", null ],
    [ "NETWORK_MIN_IV_UPDATE_INTERVAL_MINUTES", "group__MESH__DEFINES__NETWORK.html#ga678797b3e32475affa61a7533a18c7c2", null ],
    [ "NETWORK_IV_RECOVERY_LIMIT", "group__MESH__DEFINES__NETWORK.html#gaf09e90b961a0971c3ffba6e1e602b7ae", null ],
    [ "NETWORK_BEARER", "group__MESH__DEFINES__NETWORK.html#ga9b84528b8a5ac80c89858ee86a7df36e", null ]
];