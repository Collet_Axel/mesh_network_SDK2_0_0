var group__NRF__MESH__SERIAL =
[
    [ "Defines", "group__SERIAL__DEFINES.html", "group__SERIAL__DEFINES" ],
    [ "Types", "group__SERIAL__TYPES.html", "group__SERIAL__TYPES" ],
    [ "nrf_mesh_serial_init", "group__NRF__MESH__SERIAL.html#gab94f7de8a9e5e8350d2aea736e1f2c81", null ],
    [ "nrf_mesh_serial_enable", "group__NRF__MESH__SERIAL.html#ga4b921726404b72d87c19042e53eab14c", null ],
    [ "nrf_mesh_serial_state_get", "group__NRF__MESH__SERIAL.html#gafad415f5983acc4d35c3d4e1f0745e9a", null ],
    [ "nrf_mesh_serial_tx", "group__NRF__MESH__SERIAL.html#ga1fc56121ac117963e4fa2aa06be974fc", null ]
];