var group__SIMPLE__ON__OFF__COMMON =
[
    [ "simple_on_off_msg_set_t", "structsimple__on__off__msg__set__t.html", [
      [ "on_off", "structsimple__on__off__msg__set__t.html#a9115d78c2d9daa31b7f6a472f62bd493", null ],
      [ "tid", "structsimple__on__off__msg__set__t.html#a72a32b8bb8ad21b882be262487137dff", null ]
    ] ],
    [ "simple_on_off_msg_set_unreliable_t", "structsimple__on__off__msg__set__unreliable__t.html", [
      [ "on_off", "structsimple__on__off__msg__set__unreliable__t.html#a86242b3302f80582912a690dd886af07", null ],
      [ "tid", "structsimple__on__off__msg__set__unreliable__t.html#a071e491aed03f5f64b2ab6b3a956a8c2", null ]
    ] ],
    [ "simple_on_off_msg_status_t", "structsimple__on__off__msg__status__t.html", [
      [ "present_on_off", "structsimple__on__off__msg__status__t.html#a82f9fb92a64d6746092e298181bc0d20", null ]
    ] ],
    [ "SIMPLE_ON_OFF_COMPANY_ID", "group__SIMPLE__ON__OFF__COMMON.html#ga27db1dd5e0bf381f1835a38c1bdbfa65", null ],
    [ "simple_on_off_opcode_t", "group__SIMPLE__ON__OFF__COMMON.html#ga01b1de1caa26301a36fdf2c5046194f2", [
      [ "SIMPLE_ON_OFF_OPCODE_SET", "group__SIMPLE__ON__OFF__COMMON.html#gga01b1de1caa26301a36fdf2c5046194f2a0a8342bea329fe7d3f5571a23ca03b6f", null ],
      [ "SIMPLE_ON_OFF_OPCODE_GET", "group__SIMPLE__ON__OFF__COMMON.html#gga01b1de1caa26301a36fdf2c5046194f2a7004299254aea28d14262ebbdd5714b4", null ],
      [ "SIMPLE_ON_OFF_OPCODE_SET_UNRELIABLE", "group__SIMPLE__ON__OFF__COMMON.html#gga01b1de1caa26301a36fdf2c5046194f2a4ed3983bf115f6bf46d4312501da6f23", null ],
      [ "SIMPLE_ON_OFF_OPCODE_STATUS", "group__SIMPLE__ON__OFF__COMMON.html#gga01b1de1caa26301a36fdf2c5046194f2a237e1a6e4cc5953baa07995ceebbd376", null ]
    ] ]
];