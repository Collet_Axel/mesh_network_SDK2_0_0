var group__NRF__MESH__DFU__DEFINES =
[
    [ "NRF_MESH_DFU_AUTH_LEVEL_MIN", "group__NRF__MESH__DFU__DEFINES.html#gaaa8282f959fd0050eca2eea24e528271", null ],
    [ "NRF_MESH_DFU_AUTH_LEVEL_MAX", "group__NRF__MESH__DFU__DEFINES.html#gac8e21c926f487adb159148f15bdf6434", null ],
    [ "NRF_MESH_DFU_PUBLIC_KEY_LEN", "group__NRF__MESH__DFU__DEFINES.html#gaee2a5ee3742790b32b22cbb776817ce0", null ],
    [ "NRF_MESH_DFU_SIGNATURE_LEN", "group__NRF__MESH__DFU__DEFINES.html#ga7f74d7c054598e75e8d3bfbb88ed1b70", null ],
    [ "NRF_MESH_DFU_SEGMENT_LENGTH", "group__NRF__MESH__DFU__DEFINES.html#gab6150a6493e3f1d386a89f2abfc9e0b9", null ]
];