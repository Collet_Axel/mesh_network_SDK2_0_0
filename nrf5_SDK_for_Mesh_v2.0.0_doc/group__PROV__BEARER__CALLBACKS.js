var group__PROV__BEARER__CALLBACKS =
[
    [ "prov_bearer_callbacks_t", "structprov__bearer__callbacks__t.html", [
      [ "rx", "structprov__bearer__callbacks__t.html#a0bdb4cb59425d8b528ccb5522bead9ce", null ],
      [ "ack", "structprov__bearer__callbacks__t.html#ad5a9ca8bce575a819f86a2ec85c9f242", null ],
      [ "opened", "structprov__bearer__callbacks__t.html#af822b1525b7f1bfb6e0bb8880158ec9b", null ],
      [ "closed", "structprov__bearer__callbacks__t.html#a33c5df0cdd64ef33c7005612d107af1b", null ]
    ] ],
    [ "prov_bearer_cb_rx_t", "group__PROV__BEARER__CALLBACKS.html#gaa0537567e43468ba914b34a71201918b", null ],
    [ "prov_bearer_cb_ack_t", "group__PROV__BEARER__CALLBACKS.html#gae4b377a5da4e11089f8252fd725aec04", null ],
    [ "prov_bearer_cb_link_opened_t", "group__PROV__BEARER__CALLBACKS.html#ga33460b2523cbf12bca1bd9fa7a413fca", null ],
    [ "prov_bearer_cb_link_closed_t", "group__PROV__BEARER__CALLBACKS.html#gac594bf2cb5a9d483660250bfbad6ab87", null ]
];