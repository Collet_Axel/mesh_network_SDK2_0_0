var group__MESH__SERIAL__HANDLER =
[
    [ "Access layer serial handler", "group__SERIAL__HANDLER__ACCESS.html", "group__SERIAL__HANDLER__ACCESS" ],
    [ "Application serial handler", "group__MESH__SERIAL__HANDLER__APP.html", "group__MESH__SERIAL__HANDLER__APP" ],
    [ "Common serial handler functions", "group__SERIAL__HANDLER__COMMON.html", "group__SERIAL__HANDLER__COMMON" ],
    [ "Config serial handler", "group__MESH__SERIAL__HANDLER__CONFIG.html", "group__MESH__SERIAL__HANDLER__CONFIG" ],
    [ "Device serial handler", "group__SERIAL__HANDLER__DEVICE.html", "group__SERIAL__HANDLER__DEVICE" ],
    [ "DFU serial handler", "group__SERIAL__HANDLER__DFU.html", "group__SERIAL__HANDLER__DFU" ],
    [ "Mesh serial handler", "group__SERIAL__HANDLER__MESH.html", "group__SERIAL__HANDLER__MESH" ],
    [ "Generic model serial handler", "group__SERIAL__HANDLER__MODELS.html", "group__SERIAL__HANDLER__MODELS" ],
    [ "OpenMesh serial handler", "group__MESH__SERIAL__HANDLER__OPENMESH.html", "group__MESH__SERIAL__HANDLER__OPENMESH" ],
    [ "Provisioning serial handler (PB-ADV)", "group__SERIAL__HANDLER__PROV.html", "group__SERIAL__HANDLER__PROV" ]
];