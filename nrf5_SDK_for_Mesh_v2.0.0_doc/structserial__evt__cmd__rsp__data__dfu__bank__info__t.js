var structserial__evt__cmd__rsp__data__dfu__bank__info__t =
[
    [ "dfu_type", "structserial__evt__cmd__rsp__data__dfu__bank__info__t.html#acec74ee5d1a9476817dab112b787ee0b", null ],
    [ "fwid", "structserial__evt__cmd__rsp__data__dfu__bank__info__t.html#a2c2c004ffcbef7c1fa09a01be27bdcfe", null ],
    [ "is_signed", "structserial__evt__cmd__rsp__data__dfu__bank__info__t.html#a71ed6fcfe22176330123c90d8eaa8cf8", null ],
    [ "start_addr", "structserial__evt__cmd__rsp__data__dfu__bank__info__t.html#a663b8b0993daaa6667990c2657c8f228", null ],
    [ "length", "structserial__evt__cmd__rsp__data__dfu__bank__info__t.html#af5115a7fccbaa2fc538530d87bc6a565", null ]
];