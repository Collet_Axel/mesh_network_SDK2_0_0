var group__SERIAL__HANDLER__COMMON =
[
    [ "serial_handler_common_opcode_to_fp_map_t", "structserial__handler__common__opcode__to__fp__map__t.html", [
      [ "opcode", "structserial__handler__common__opcode__to__fp__map__t.html#aa406b82c2b154d0e33e3daea9b68d09d", null ],
      [ "payload_minlen", "structserial__handler__common__opcode__to__fp__map__t.html#a778760c12471c6ddbb9459d353c386fb", null ],
      [ "payload_optional_extra_bytes", "structserial__handler__common__opcode__to__fp__map__t.html#ac4869a112f18c3a002fbbc0b8bb2934d", null ],
      [ "callback", "structserial__handler__common__opcode__to__fp__map__t.html#adbfcd1f75faa6497797c05dfffe28a87", null ]
    ] ],
    [ "serial_handler_common_cmd_cb_t", "group__SERIAL__HANDLER__COMMON.html#ga27c8b2bc28b0bb4610d803f34114e504", null ],
    [ "serial_handler_common_cmd_rsp_nodata_on_error", "group__SERIAL__HANDLER__COMMON.html#ga0c6fe6601160f9857b64806e3251eb8a", null ],
    [ "serial_handler_common_rx", "group__SERIAL__HANDLER__COMMON.html#ga9505b790c5ea0094d6e9a9804653fe63", null ]
];