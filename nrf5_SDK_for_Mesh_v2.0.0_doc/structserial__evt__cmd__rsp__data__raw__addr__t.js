var structserial__evt__cmd__rsp__data__raw__addr__t =
[
    [ "address_handle", "structserial__evt__cmd__rsp__data__raw__addr__t.html#a0ad34b3259ba734d1238eac774cf43c4", null ],
    [ "addr_type", "structserial__evt__cmd__rsp__data__raw__addr__t.html#ae3603a68d18cc166f5262a4ced415cdf", null ],
    [ "subscribed", "structserial__evt__cmd__rsp__data__raw__addr__t.html#acc98b45282264b991703631a61f9db78", null ],
    [ "raw_short_addr", "structserial__evt__cmd__rsp__data__raw__addr__t.html#a9b1007570625d4ba5899f1676638ac3a", null ],
    [ "virtual_uuid", "structserial__evt__cmd__rsp__data__raw__addr__t.html#aaa80219de378349d8df78efd9cdd064b", null ]
];