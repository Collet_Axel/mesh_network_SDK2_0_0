var group__PB__REMOTE__SERVER =
[
    [ "pb_remote_server_t", "structpb__remote__server__t.html", [
      [ "model_handle", "structpb__remote__server__t.html#a2f607dbf18929c9353c4b8526c115af3", null ],
      [ "state", "structpb__remote__server__t.html#a3a799d476b0d75b1366b60603ba99081", null ],
      [ "prev_state", "structpb__remote__server__t.html#a9d8248625e3689114800b29dfd13e964", null ],
      [ "p_prov_bearer", "structpb__remote__server__t.html#adf97f5933acdecec0f3da2f0f9331e7f", null ],
      [ "reliable", "structpb__remote__server__t.html#ab4035e81e17155513bd9459a0c43621e", null ],
      [ "return_to_scan_enabled", "structpb__remote__server__t.html#aa19e2f0b48077b54ebac0624fdb2b0a9", null ],
      [ "ctid", "structpb__remote__server__t.html#a7848ce7903068d4344ee800964edbc90", null ]
    ] ],
    [ "PB_REMOTE_SERVER_MODEL_ID", "group__PB__REMOTE__SERVER.html#ga0255150d49b8e17e19d9526afadd7776", null ],
    [ "PB_REMOTE_SERVER_UUID_LIST_SIZE", "group__PB__REMOTE__SERVER.html#gaed6f777eb883e7dd9c756ed373fd0d2b", null ],
    [ "pb_remote_server_state_t", "group__PB__REMOTE__SERVER.html#ga42f38d6a5c051913d8c0e3a5548429c7", [
      [ "PB_REMOTE_SERVER_STATE_NONE", "group__PB__REMOTE__SERVER.html#gga42f38d6a5c051913d8c0e3a5548429c7ace471c3fa4832f35da92432975856f61", null ],
      [ "PB_REMOTE_SERVER_STATE_DISABLED", "group__PB__REMOTE__SERVER.html#gga42f38d6a5c051913d8c0e3a5548429c7a95c5d8da5b6e21d1c987a192bda16561", null ],
      [ "PB_REMOTE_SERVER_STATE_IDLE", "group__PB__REMOTE__SERVER.html#gga42f38d6a5c051913d8c0e3a5548429c7a7fb235852139c3bf479e1d5d4792efc0", null ],
      [ "PB_REMOTE_SERVER_STATE_SCANNING", "group__PB__REMOTE__SERVER.html#gga42f38d6a5c051913d8c0e3a5548429c7ae9e04b2f48971e0799a623b277b15564", null ],
      [ "PB_REMOTE_SERVER_STATE_SCANNING_FILTER", "group__PB__REMOTE__SERVER.html#gga42f38d6a5c051913d8c0e3a5548429c7af757243c28c4af602cd8001ad427a3f8", null ],
      [ "PB_REMOTE_SERVER_STATE_LINK_OPENING", "group__PB__REMOTE__SERVER.html#gga42f38d6a5c051913d8c0e3a5548429c7aa5bebe28e0c30ca20e1760c2fd03fa61", null ],
      [ "PB_REMOTE_SERVER_STATE_LINK_CLOSING", "group__PB__REMOTE__SERVER.html#gga42f38d6a5c051913d8c0e3a5548429c7a156d5ad96ce15eecf6d90804cb000130", null ],
      [ "PB_REMOTE_SERVER_STATE_LINK_ESTABLISHED", "group__PB__REMOTE__SERVER.html#gga42f38d6a5c051913d8c0e3a5548429c7a7acbfd00f4a38b27ed267f726fa77ee5", null ],
      [ "PB_REMOTE_SERVER_STATE_WAIT_ACK_LOCAL", "group__PB__REMOTE__SERVER.html#gga42f38d6a5c051913d8c0e3a5548429c7a62a002f186c0028a4ffa4a9195c53331", null ],
      [ "PB_REMOTE_SERVER_STATE_WAIT_ACK_TRANSFER", "group__PB__REMOTE__SERVER.html#gga42f38d6a5c051913d8c0e3a5548429c7a0ed7ac0a03f6bb28b5dfdef765d57b62", null ],
      [ "PB_REMOTE_SERVER_STATE_WAIT_ACK_LINK_OPEN", "group__PB__REMOTE__SERVER.html#gga42f38d6a5c051913d8c0e3a5548429c7ab45c0ebd0313b97868c155f876a03b2b", null ],
      [ "PB_REMOTE_SERVER_STATE_WAIT_ACK_LINK_CLOSE", "group__PB__REMOTE__SERVER.html#gga42f38d6a5c051913d8c0e3a5548429c7a686cba0ade52621050b0173ad7e337e3", null ],
      [ "PB_REMOTE_SERVER_STATE_WAIT_ACK_SCAN_REPORT", "group__PB__REMOTE__SERVER.html#gga42f38d6a5c051913d8c0e3a5548429c7ad73b913dc5d999f521a7872ed8f9de8d", null ],
      [ "PB_REMOTE_SERVER_STATE_WAIT_ACK_SCAN_REPORT_FILTER", "group__PB__REMOTE__SERVER.html#gga42f38d6a5c051913d8c0e3a5548429c7a0b8a0908c26729a826e12ec12a1603c5", null ]
    ] ],
    [ "pb_remote_server_init", "group__PB__REMOTE__SERVER.html#ga9c4c05ee5438d934191eed0f8fd65a09", null ],
    [ "pb_remote_server_enable", "group__PB__REMOTE__SERVER.html#ga809c983116d20de567e240989fc3b92e", null ],
    [ "pb_remote_server_disable", "group__PB__REMOTE__SERVER.html#gafe270f5f26c8a33e2aaceed6cb41b682", null ],
    [ "pb_remote_server_return_to_scan_set", "group__PB__REMOTE__SERVER.html#gaa1ab133822b8858c5ac92d2469565bd5", null ],
    [ "pb_remote_server_prov_bearer_set", "group__PB__REMOTE__SERVER.html#ga12c45617fe9322f9ce5dede5d584d7f8", null ]
];