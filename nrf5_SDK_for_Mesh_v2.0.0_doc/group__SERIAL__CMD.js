var group__SERIAL__CMD =
[
    [ "serial_cmd_device_echo_t", "structserial__cmd__device__echo__t.html", [
      [ "data", "structserial__cmd__device__echo__t.html#a2af47fecaf3b33e2ae113a55d835c326", null ]
    ] ],
    [ "serial_cmd_device_beacon_start_t", "structserial__cmd__device__beacon__start__t.html", [
      [ "beacon_slot", "structserial__cmd__device__beacon__start__t.html#ab399d9f974410ade4fd62faa4fa2447c", null ],
      [ "data", "structserial__cmd__device__beacon__start__t.html#ad567ea7d550652fe437803c97ee9ddbb", null ]
    ] ],
    [ "serial_cmd_device_beacon_stop_t", "structserial__cmd__device__beacon__stop__t.html", [
      [ "beacon_slot", "structserial__cmd__device__beacon__stop__t.html#a77f936a2ebcb0035de351eb6a61616c4", null ]
    ] ],
    [ "serial_cmd_device_beacon_params_set_t", "structserial__cmd__device__beacon__params__set__t.html", [
      [ "beacon_slot", "structserial__cmd__device__beacon__params__set__t.html#ab4ea513b62b2069b095cf4bb66b4c5bf", null ],
      [ "tx_power", "structserial__cmd__device__beacon__params__set__t.html#ae794fdaac9a4222b32f592fe54e69a77", null ],
      [ "channel_map", "structserial__cmd__device__beacon__params__set__t.html#ae2a75fe0014a0236a51969063e0a6735", null ],
      [ "interval_ms", "structserial__cmd__device__beacon__params__set__t.html#ae3965749f7206752a5a81e958ce36b54", null ]
    ] ],
    [ "serial_cmd_device_beacon_params_get_t", "structserial__cmd__device__beacon__params__get__t.html", [
      [ "beacon_slot", "structserial__cmd__device__beacon__params__get__t.html#a6e4fabc3f22e1b4d6f0ba4494305e253", null ]
    ] ],
    [ "serial_cmd_device_t", "unionserial__cmd__device__t.html", [
      [ "echo", "unionserial__cmd__device__t.html#a5c115737cd25ea4b3856b944c4500d5b", null ],
      [ "beacon_start", "unionserial__cmd__device__t.html#a854a831a503d040c3eb84ffeed55621d", null ],
      [ "beacon_stop", "unionserial__cmd__device__t.html#ad3400f3ebbcde0804a74e49c54f8d753", null ],
      [ "beacon_params_set", "unionserial__cmd__device__t.html#ae9df3f874c24d22739f56de0eb5e501a", null ],
      [ "beacon_params_get", "unionserial__cmd__device__t.html#a7ecc82960f8289adf7aa5e2f21bb392f", null ]
    ] ],
    [ "serial_cmd_config_adv_addr_t", "structserial__cmd__config__adv__addr__t.html", [
      [ "addr_type", "structserial__cmd__config__adv__addr__t.html#af36ce4eecf114fd0357f93a215ae186c", null ],
      [ "adv_addr", "structserial__cmd__config__adv__addr__t.html#a954057b2be2442d5272241157313be35", null ]
    ] ],
    [ "serial_cmd_config_channel_map_t", "structserial__cmd__config__channel__map__t.html", [
      [ "channel_map", "structserial__cmd__config__channel__map__t.html#a89ceb626398402e374b7b46f34816a22", null ]
    ] ],
    [ "serial_cmd_config_tx_power_t", "structserial__cmd__config__tx__power__t.html", [
      [ "tx_power", "structserial__cmd__config__tx__power__t.html#ab27f0467cbf64304159a265e9d2f67e9", null ]
    ] ],
    [ "serial_cmd_config_uuid_t", "structserial__cmd__config__uuid__t.html", [
      [ "uuid", "structserial__cmd__config__uuid__t.html#a51d497490834101331f095265cca5ac2", null ]
    ] ],
    [ "serial_cmd_config_t", "unionserial__cmd__config__t.html", [
      [ "adv_addr", "unionserial__cmd__config__t.html#a7bf8b78570d0b4edbe354effa67d7e2d", null ],
      [ "channel_map", "unionserial__cmd__config__t.html#a6d232b9927be85e28509316357ee0642", null ],
      [ "tx_power", "unionserial__cmd__config__t.html#a0f4490e03be102737aa17e3bf2f26010", null ],
      [ "uuid", "unionserial__cmd__config__t.html#a9f9086a65f85cf078713177ea9d1eb38", null ]
    ] ],
    [ "serial_cmd_openmesh_dfu_data_t", "structserial__cmd__openmesh__dfu__data__t.html", [
      [ "dfu_packet", "structserial__cmd__openmesh__dfu__data__t.html#ae50346f22e3d17be5e509666c9ea9654", null ]
    ] ],
    [ "serial_cmd_openmesh_t", "unionserial__cmd__openmesh__t.html", [
      [ "dfu_data", "unionserial__cmd__openmesh__t.html#a447df8ae2a6d6e32d43920bdb55daef5", null ]
    ] ],
    [ "serial_cmd_prov_keypair_t", "structserial__cmd__prov__keypair__t.html", [
      [ "private_key", "structserial__cmd__prov__keypair__t.html#a5d3967ec52be30b0cce2122e8fc95175", null ],
      [ "public_key", "structserial__cmd__prov__keypair__t.html#a214440d68c7c6d19eb499257419c8af6", null ]
    ] ],
    [ "serial_cmd_prov_caps_t", "structserial__cmd__prov__caps__t.html", [
      [ "num_elements", "structserial__cmd__prov__caps__t.html#a812a2d1034418970deb3825c2cdb03a4", null ],
      [ "public_key_type", "structserial__cmd__prov__caps__t.html#ad16d6be61fedd42a438daf888954db59", null ],
      [ "static_oob_types", "structserial__cmd__prov__caps__t.html#a2f5d3780aa9f53b8533469a3d3e29a59", null ],
      [ "output_oob_size", "structserial__cmd__prov__caps__t.html#a308da47632e3dfa7b28aae5ba9410d19", null ],
      [ "output_oob_actions", "structserial__cmd__prov__caps__t.html#a97b3563e088c0c622a1e563e6692f91a", null ],
      [ "input_oob_size", "structserial__cmd__prov__caps__t.html#a7700284f28905703a8a8b5e4164eb270", null ],
      [ "input_oob_actions", "structserial__cmd__prov__caps__t.html#a7d3a4898326eaca68efe7d0340c6c9b8", null ]
    ] ],
    [ "serial_cmd_prov_data_t", "structserial__cmd__prov__data__t.html", [
      [ "context_id", "structserial__cmd__prov__data__t.html#ad4e53a91b805118af9ef2d0d5ad03670", null ],
      [ "target_uuid", "structserial__cmd__prov__data__t.html#a36a345ffc42824531f71509e4f0848bc", null ],
      [ "network_key", "structserial__cmd__prov__data__t.html#a4e7db09a8e9c2aef01881b7a68a5c78b", null ],
      [ "network_key_index", "structserial__cmd__prov__data__t.html#afe2ae66e19b37e1fcc21ed59945c9de3", null ],
      [ "iv_index", "structserial__cmd__prov__data__t.html#a408a83eb6609f21ef57ebc0c26a8806e", null ],
      [ "address", "structserial__cmd__prov__data__t.html#aa2fa64ad52ac0c1156f1898edfb742d2", null ],
      [ "iv_update_flag", "structserial__cmd__prov__data__t.html#a67c04acd149d654fbe889c20b717e3c8", null ],
      [ "key_refresh_flag", "structserial__cmd__prov__data__t.html#a5a9f3c77ecc42da1cb7cda5ef34534e6", null ]
    ] ],
    [ "serial_cmd_prov_oob_use_t", "structserial__cmd__prov__oob__use__t.html", [
      [ "context_id", "structserial__cmd__prov__oob__use__t.html#aa81b357b3a20b094370501943155d2cd", null ],
      [ "oob_method", "structserial__cmd__prov__oob__use__t.html#acde6589d5e0d611cf2fb5baebab2beae", null ],
      [ "oob_action", "structserial__cmd__prov__oob__use__t.html#a4795654d77126a71c72eb62ee7c82f7e", null ],
      [ "size", "structserial__cmd__prov__oob__use__t.html#abc5ed0e1ec764072709efe34e42ff078", null ]
    ] ],
    [ "serial_cmd_prov_auth_data_t", "structserial__cmd__prov__auth__data__t.html", [
      [ "context_id", "structserial__cmd__prov__auth__data__t.html#a7ede158c51999ddc986ed5a66150e75f", null ],
      [ "data", "structserial__cmd__prov__auth__data__t.html#ad104323810e8bb6fc2424cfef8279862", null ]
    ] ],
    [ "serial_cmd_prov_ecdh_data_t", "structserial__cmd__prov__ecdh__data__t.html", [
      [ "context_id", "structserial__cmd__prov__ecdh__data__t.html#af7ec6441dc0bb3ea5f53f1aa69815667", null ],
      [ "shared_secret", "structserial__cmd__prov__ecdh__data__t.html#aa2540601de1114376ac2e8ffcaa9fe3f", null ]
    ] ],
    [ "serial_cmd_prov_t", "unionserial__cmd__prov__t.html", [
      [ "keypair", "unionserial__cmd__prov__t.html#abbc38d5e5f88ccacee7c8ed8a046d0d3", null ],
      [ "caps", "unionserial__cmd__prov__t.html#abcf4c2c1f0d941fc490f20c1a2aae267", null ],
      [ "data", "unionserial__cmd__prov__t.html#a76740e7cfac31c796b7ca9857b24d25d", null ],
      [ "oob_use", "unionserial__cmd__prov__t.html#a1232edbd96e4ad3f6b64f958a1e6aa0b", null ],
      [ "auth_data", "unionserial__cmd__prov__t.html#a3e7d571be63e3437d6411c88a6a04329", null ],
      [ "ecdh_data", "unionserial__cmd__prov__t.html#a239f4e5a7fc158dcbe247213772280d7", null ]
    ] ],
    [ "serial_cmd_mesh_subnet_add_t", "structserial__cmd__mesh__subnet__add__t.html", [
      [ "net_key_index", "structserial__cmd__mesh__subnet__add__t.html#abdfb9c9d41e052a504c7a91427442be7", null ],
      [ "key", "structserial__cmd__mesh__subnet__add__t.html#a33b6adb00c4b881030961c178f07eab8", null ]
    ] ],
    [ "serial_cmd_mesh_subnet_update_t", "structserial__cmd__mesh__subnet__update__t.html", [
      [ "subnet_handle", "structserial__cmd__mesh__subnet__update__t.html#a92f76cb2d6bf30342a61661e35e7bd13", null ],
      [ "key", "structserial__cmd__mesh__subnet__update__t.html#aba4cba9846027002fd8c7c13ea62c260", null ]
    ] ],
    [ "serial_cmd_mesh_subnet_delete_t", "structserial__cmd__mesh__subnet__delete__t.html", [
      [ "subnet_handle", "structserial__cmd__mesh__subnet__delete__t.html#a385a87871582b2874d7d195e774402b8", null ]
    ] ],
    [ "serial_cmd_mesh_appkey_add_t", "structserial__cmd__mesh__appkey__add__t.html", [
      [ "app_key_index", "structserial__cmd__mesh__appkey__add__t.html#a1772ccd140f608f0b8d036789c8bbe4c", null ],
      [ "subnet_handle", "structserial__cmd__mesh__appkey__add__t.html#aae117ff49f6b4c1916c9cffa5c62a10e", null ],
      [ "key", "structserial__cmd__mesh__appkey__add__t.html#a4250b8dda310b74166c92ba705ea30da", null ]
    ] ],
    [ "serial_cmd_mesh_appkey_update_t", "structserial__cmd__mesh__appkey__update__t.html", [
      [ "appkey_handle", "structserial__cmd__mesh__appkey__update__t.html#a8131d881fc1d17574c52bf91e35a5af5", null ],
      [ "key", "structserial__cmd__mesh__appkey__update__t.html#a99298161175d9db6fda294c16666df45", null ]
    ] ],
    [ "serial_cmd_mesh_appkey_delete_t", "structserial__cmd__mesh__appkey__delete__t.html", [
      [ "appkey_handle", "structserial__cmd__mesh__appkey__delete__t.html#a9445aac2ee82553029a59a726d3616de", null ]
    ] ],
    [ "serial_cmd_mesh_appkey_get_all_t", "structserial__cmd__mesh__appkey__get__all__t.html", [
      [ "subnet_handle", "structserial__cmd__mesh__appkey__get__all__t.html#ac5ae70d0a9a835eb4ba81326615e27d1", null ]
    ] ],
    [ "serial_cmd_mesh_devkey_add_t", "structserial__cmd__mesh__devkey__add__t.html", [
      [ "owner_addr", "structserial__cmd__mesh__devkey__add__t.html#abc6cd028dc8e18e419e6e15a870e72ab", null ],
      [ "subnet_handle", "structserial__cmd__mesh__devkey__add__t.html#a74726a41d39213995105eb016909b915", null ],
      [ "key", "structserial__cmd__mesh__devkey__add__t.html#a364482548c713b13d6a650cbfe5a3563", null ]
    ] ],
    [ "serial_cmd_mesh_devkey_delete_t", "structserial__cmd__mesh__devkey__delete__t.html", [
      [ "devkey_handle", "structserial__cmd__mesh__devkey__delete__t.html#a19e42883e441814e589afc89daa09489", null ]
    ] ],
    [ "serial_cmd_mesh_addr_local_unicast_set_t", "structserial__cmd__mesh__addr__local__unicast__set__t.html", [
      [ "start_address", "structserial__cmd__mesh__addr__local__unicast__set__t.html#a25d41a9310f1b9fe6b8f2204d615932f", null ],
      [ "count", "structserial__cmd__mesh__addr__local__unicast__set__t.html#a19b56001443bea8787ffadbfb1620710", null ]
    ] ],
    [ "serial_cmd_mesh_addr_add_t", "structserial__cmd__mesh__addr__add__t.html", [
      [ "raw_address", "structserial__cmd__mesh__addr__add__t.html#a60b1d884b101db0b9c3a49ef5df84444", null ]
    ] ],
    [ "serial_cmd_mesh_addr_virtual_add_t", "structserial__cmd__mesh__addr__virtual__add__t.html", [
      [ "virtual_addr_uuid", "structserial__cmd__mesh__addr__virtual__add__t.html#a155b37ccf9d484cef41bf76dd88b37d8", null ]
    ] ],
    [ "serial_cmd_mesh_addr_get_t", "structserial__cmd__mesh__addr__get__t.html", [
      [ "address_handle", "structserial__cmd__mesh__addr__get__t.html#aed90aeffed2b72b0c7a15e4cdd60f219", null ]
    ] ],
    [ "serial_cmd_mesh_addr_subscription_add_t", "structserial__cmd__mesh__addr__subscription__add__t.html", [
      [ "address", "structserial__cmd__mesh__addr__subscription__add__t.html#aa0f7f4c2da3d0c3f8964ca6c0dc7e913", null ]
    ] ],
    [ "serial_cmd_mesh_addr_subscription_add_virtual_t", "structserial__cmd__mesh__addr__subscription__add__virtual__t.html", [
      [ "uuid", "structserial__cmd__mesh__addr__subscription__add__virtual__t.html#aee2a6a1ea3776ba140ae1e1b4dbfcb00", null ]
    ] ],
    [ "serial_cmd_mesh_addr_subscription_remove_t", "structserial__cmd__mesh__addr__subscription__remove__t.html", [
      [ "address_handle", "structserial__cmd__mesh__addr__subscription__remove__t.html#ad6a797d380dac1b5648414c74872bb87", null ]
    ] ],
    [ "serial_cmd_mesh_addr_publication_add_t", "structserial__cmd__mesh__addr__publication__add__t.html", [
      [ "address", "structserial__cmd__mesh__addr__publication__add__t.html#a68514ced5542fa03d9ec34142888433d", null ]
    ] ],
    [ "serial_cmd_mesh_addr_publication_add_virtual_t", "structserial__cmd__mesh__addr__publication__add__virtual__t.html", [
      [ "uuid", "structserial__cmd__mesh__addr__publication__add__virtual__t.html#ad9d7deeeeba7c029fff759ff7525946e", null ]
    ] ],
    [ "serial_cmd_mesh_addr_publication_remove_t", "structserial__cmd__mesh__addr__publication__remove__t.html", [
      [ "address_handle", "structserial__cmd__mesh__addr__publication__remove__t.html#acf3e970f063576026fa8570ef2347300", null ]
    ] ],
    [ "serial_cmd_mesh_packet_send_t", "structserial__cmd__mesh__packet__send__t.html", [
      [ "appkey_handle", "structserial__cmd__mesh__packet__send__t.html#a5b5568ed1b4f3f1bf14d065b23d8b5c6", null ],
      [ "src_addr", "structserial__cmd__mesh__packet__send__t.html#a4deafdd6f16c6b76f085e8ae82eb5d97", null ],
      [ "dst_addr_handle", "structserial__cmd__mesh__packet__send__t.html#a0352b0f4cb5844dd3e397a46cd7769af", null ],
      [ "ttl", "structserial__cmd__mesh__packet__send__t.html#a58f70678d2c96e4fe5c34dddb48848b1", null ],
      [ "force_segmented", "structserial__cmd__mesh__packet__send__t.html#a4d75ac9566d25c2c5bc48ceb8acee07f", null ],
      [ "transmic_size", "structserial__cmd__mesh__packet__send__t.html#a4949999bf117fa5d708f39c9e6040ca9", null ],
      [ "data", "structserial__cmd__mesh__packet__send__t.html#ab15796d6444ee4aa628d48385841d616", null ]
    ] ],
    [ "serial_cmd_mesh_t", "unionserial__cmd__mesh__t.html", [
      [ "subnet_add", "unionserial__cmd__mesh__t.html#a481c58d0bb59669acb32e1701cebf8db", null ],
      [ "subnet_update", "unionserial__cmd__mesh__t.html#a7db7e7e677860815881482b0f625dd38", null ],
      [ "subnet_delete", "unionserial__cmd__mesh__t.html#aa0cccd93d97c3a60d2525d54ba70c279", null ],
      [ "appkey_add", "unionserial__cmd__mesh__t.html#a4a913d8b57167a2bd9f7e52ca9b3268a", null ],
      [ "appkey_update", "unionserial__cmd__mesh__t.html#a7b6373186e8a409ecc71d434100e3c99", null ],
      [ "appkey_delete", "unionserial__cmd__mesh__t.html#a5f9e5b2112f03f6954e0f3e69d87b2bb", null ],
      [ "appkey_get_all", "unionserial__cmd__mesh__t.html#a321a3509ef68edba28e489a6dd505512", null ],
      [ "devkey_add", "unionserial__cmd__mesh__t.html#a97273825e04683ce5e4097a1090f8536", null ],
      [ "devkey_delete", "unionserial__cmd__mesh__t.html#a8cd07a2743da87d2c5007aeb56373262", null ],
      [ "local_unicast_addr_set", "unionserial__cmd__mesh__t.html#afafce591844a1665ffe5600851ffebbc", null ],
      [ "addr_add", "unionserial__cmd__mesh__t.html#acebba1264e770ad66032c04ddb8f105c", null ],
      [ "addr_virtual_add", "unionserial__cmd__mesh__t.html#a4dd2960d7ee893bc6f9c4e3623b2941c", null ],
      [ "addr_get", "unionserial__cmd__mesh__t.html#ab1bbe9e9867d20bc8e7865a9ec44ec27", null ],
      [ "addr_subscription_add", "unionserial__cmd__mesh__t.html#abb405946edd9d3a54a6b220286c3e7cb", null ],
      [ "addr_subscription_add_virtual", "unionserial__cmd__mesh__t.html#a7db487cb573748c8c2773ef379d1dd00", null ],
      [ "addr_subscription_remove", "unionserial__cmd__mesh__t.html#a8fe0b0666f997ebbe69b4d770363385f", null ],
      [ "addr_publication_add", "unionserial__cmd__mesh__t.html#a399a71bb1f50a198955aa8754c88a509", null ],
      [ "addr_publication_add_virtual", "unionserial__cmd__mesh__t.html#a13fdac70ecbc455a191c76a609f3ba20", null ],
      [ "addr_publication_remove", "unionserial__cmd__mesh__t.html#a3a35547bd842dbb5aa1b537b2b6f176b", null ],
      [ "packet_send", "unionserial__cmd__mesh__t.html#ac8513198702c611db3e038ea76c7efc5", null ]
    ] ],
    [ "serial_cmd_pb_remote_client_init_t", "structserial__cmd__pb__remote__client__init__t.html", [
      [ "element_index", "structserial__cmd__pb__remote__client__init__t.html#a3d2d21d5fc13e83a3fbc245bfbd5e253", null ],
      [ "prov_context_index", "structserial__cmd__pb__remote__client__init__t.html#a361cfe6b86c968ba56b812c86c67bf43", null ],
      [ "application_index", "structserial__cmd__pb__remote__client__init__t.html#a40ce14ae72743ca9b70f71a33b213f8c", null ]
    ] ],
    [ "serial_cmd_pb_remote_client_remote_scan_start_t", "structserial__cmd__pb__remote__client__remote__scan__start__t.html", [
      [ "server_address", "structserial__cmd__pb__remote__client__remote__scan__start__t.html#a283cb6ead1a5988b2b5320b66f703a32", null ]
    ] ],
    [ "serial_cmd_pb_remote_client_remote_scan_cancel_t", "structserial__cmd__pb__remote__client__remote__scan__cancel__t.html", [
      [ "server_address", "structserial__cmd__pb__remote__client__remote__scan__cancel__t.html#ac3ce9221ee1b03859644f3444148a0f6", null ]
    ] ],
    [ "serial_cmd_pb_remote_client_remote_provision_t", "structserial__cmd__pb__remote__client__remote__provision__t.html", [
      [ "server_address", "structserial__cmd__pb__remote__client__remote__provision__t.html#aa9ef70e46ef15bd966e6e6b741c28bda", null ],
      [ "unprovisioned_device_index", "structserial__cmd__pb__remote__client__remote__provision__t.html#a1140e0b1f8e125a508e2d24b109b1dba", null ],
      [ "network_key", "structserial__cmd__pb__remote__client__remote__provision__t.html#aa8f7825489e2b1ac5602d9f58b256777", null ],
      [ "iv_index", "structserial__cmd__pb__remote__client__remote__provision__t.html#aa20c91469be2a7608dfc16e9fed75651", null ],
      [ "address", "structserial__cmd__pb__remote__client__remote__provision__t.html#ab9a84db197a2d1e8ee136c3f55381be4", null ]
    ] ],
    [ "serial_cmd_pb_remote_t", "unionserial__cmd__pb__remote__t.html", [
      [ "init", "unionserial__cmd__pb__remote__t.html#a0ffef569bccc0e4adbed3af370640df9", null ],
      [ "remote_scan_start", "unionserial__cmd__pb__remote__t.html#a4498bbb5b07de987449b0e69edd462b7", null ],
      [ "remote_scan_cancel", "unionserial__cmd__pb__remote__t.html#a2d690c3ca8c0fd41d704777b6047a47c", null ],
      [ "remote_provision", "unionserial__cmd__pb__remote__t.html#a379d42a5491a3f1e8ed7a8a2b11e9aff", null ]
    ] ],
    [ "serial_cmd_dfu_request_t", "structserial__cmd__dfu__request__t.html", [
      [ "dfu_type", "structserial__cmd__dfu__request__t.html#a890b83568bae2b2e15a7131777bb020c", null ],
      [ "fwid", "structserial__cmd__dfu__request__t.html#ab2e36f8deb2c8a5c4e1699927f4cb17f", null ],
      [ "bank_addr", "structserial__cmd__dfu__request__t.html#a5a68dadbeeee093f3cfbd4c2457af4d6", null ]
    ] ],
    [ "serial_cmd_dfu_relay_t", "structserial__cmd__dfu__relay__t.html", [
      [ "dfu_type", "structserial__cmd__dfu__relay__t.html#aca0c3ba7ec6449c3b9ea57df2a86f8fe", null ],
      [ "fwid", "structserial__cmd__dfu__relay__t.html#a4aca28222e87731db65aa064ff351337", null ]
    ] ],
    [ "serial_cmd_dfu_bank_info_get_t", "structserial__cmd__dfu__bank__info__get__t.html", [
      [ "dfu_type", "structserial__cmd__dfu__bank__info__get__t.html#a68456af43da30774bbef8014c414997a", null ]
    ] ],
    [ "serial_cmd_dfu_bank_flash_t", "structserial__cmd__dfu__bank__flash__t.html", [
      [ "dfu_type", "structserial__cmd__dfu__bank__flash__t.html#a1f31a625f1189d3c718de9a3dcc5bbf1", null ]
    ] ],
    [ "serial_cmd_dfu_t", "unionserial__cmd__dfu__t.html", [
      [ "request", "unionserial__cmd__dfu__t.html#ad2071425138af7566e17447fe89595c4", null ],
      [ "relay", "unionserial__cmd__dfu__t.html#ab633312826f6b5e179d259f84a7c0fc0", null ],
      [ "bank_info", "unionserial__cmd__dfu__t.html#a7eca5450e2a091465ffab8bc3f5e8e13", null ],
      [ "bank_flash", "unionserial__cmd__dfu__t.html#aaf42525332b72af67ffba975bd06c269", null ]
    ] ],
    [ "serial_cmd_access_handle_pair_t", "structserial__cmd__access__handle__pair__t.html", [
      [ "model_handle", "structserial__cmd__access__handle__pair__t.html#aeab22706ce45470083913f205c133e8e", null ],
      [ "dsm_handle", "structserial__cmd__access__handle__pair__t.html#af6e9d35fd4fb87fd4242dfbe1a25602c", null ]
    ] ],
    [ "serial_cmd_access_model_handle_t", "structserial__cmd__access__model__handle__t.html", [
      [ "handle", "structserial__cmd__access__model__handle__t.html#a6f29ace0300760233e79968ce549575a", null ]
    ] ],
    [ "serial_cmd_access_element_loc_set_t", "structserial__cmd__access__element__loc__set__t.html", [
      [ "element_index", "structserial__cmd__access__element__loc__set__t.html#a6df823d1d71e3e9feb4b0dd7d411267d", null ],
      [ "location", "structserial__cmd__access__element__loc__set__t.html#a1e82f01b6955a82e4467f6b342dc978d", null ]
    ] ],
    [ "serial_cmd_access_model_pub_ttl_set_t", "structserial__cmd__access__model__pub__ttl__set__t.html", [
      [ "model_handle", "structserial__cmd__access__model__pub__ttl__set__t.html#a69a7f76b293fd5d08f72dfadc6aa0af8", null ],
      [ "ttl", "structserial__cmd__access__model__pub__ttl__set__t.html#ad5d23a8d7493f94d2f897c252a9c5b94", null ]
    ] ],
    [ "serial_cmd_access_handle_get_t", "structserial__cmd__access__handle__get__t.html", [
      [ "element_index", "structserial__cmd__access__handle__get__t.html#a56517d60d5c550d5305bf3a5cde419ca", null ],
      [ "model_id", "structserial__cmd__access__handle__get__t.html#ab7bec3e738f571867148277fe0f433c3", null ]
    ] ],
    [ "serial_cmd_access_pub_period_set_t", "structserial__cmd__access__pub__period__set__t.html", [
      [ "model_handle", "structserial__cmd__access__pub__period__set__t.html#a979e0194cbe6e5d948211c5740bf827f", null ],
      [ "resolution", "structserial__cmd__access__pub__period__set__t.html#a5213590f3933a6a2175cb477e62513a5", null ],
      [ "step_number", "structserial__cmd__access__pub__period__set__t.html#ab69d9ae036601b7e1ac12b7e14c1297a", null ]
    ] ],
    [ "serial_cmd_access_element_index_t", "structserial__cmd__access__element__index__t.html", [
      [ "element_index", "structserial__cmd__access__element__index__t.html#af66b5999e3c9eeb62682ee6981363bb9", null ]
    ] ],
    [ "serial_cmd_model_specific_init_t", "structserial__cmd__model__specific__init__t.html", [
      [ "model_init_info", "structserial__cmd__model__specific__init__t.html#aa346fadecb54c09a613a8c9586f71b98", null ],
      [ "data", "structserial__cmd__model__specific__init__t.html#ada809116f4a725194e301e4b7a57c154", null ]
    ] ],
    [ "serial_cmd_model_specific_command_t", "structserial__cmd__model__specific__command__t.html", [
      [ "model_cmd_info", "structserial__cmd__model__specific__command__t.html#a5debdb2ef5d41ddb5c48e79a2c61dc48", null ],
      [ "data", "structserial__cmd__model__specific__command__t.html#a4b3b01fc9e84f633ba6eca61aa82466e", null ]
    ] ],
    [ "serial_cmd_access_t", "unionserial__cmd__access__t.html", [
      [ "handle_pair", "unionserial__cmd__access__t.html#ab7ca8375daf1c7e8a0e8d7c902a7324e", null ],
      [ "model_handle", "unionserial__cmd__access__t.html#a74a66a84762412e0494057c4f885d39e", null ],
      [ "elem_loc", "unionserial__cmd__access__t.html#a709e70d68387bfca3a4141a679767dde", null ],
      [ "model_ttl", "unionserial__cmd__access__t.html#a45b68eec15cf22e5e2412602ba630337", null ],
      [ "handle_get", "unionserial__cmd__access__t.html#af27796e4e9ee1bf61004f63af2596be6", null ],
      [ "publish_period", "unionserial__cmd__access__t.html#a4592c5d953ab66be349f2c01bc9262a8", null ],
      [ "index", "unionserial__cmd__access__t.html#ad6ce67ba2218abd2e25e06452b337b3d", null ],
      [ "model_init", "unionserial__cmd__access__t.html#abfd6c2bf9fab7d355e87801ed424cb62", null ],
      [ "model_cmd", "unionserial__cmd__access__t.html#a802be1ccf0c3fe339e618d606d3dc41e", null ]
    ] ],
    [ "serial_cmd_application_t", "structserial__cmd__application__t.html", [
      [ "data", "structserial__cmd__application__t.html#a021fb7919a5e30ce9dfc69e859740913", null ]
    ] ],
    [ "serial_cmd_t", "unionserial__cmd__t.html", [
      [ "access", "unionserial__cmd__t.html#af17d4f53715f27e767bed82922c44340", null ],
      [ "device", "unionserial__cmd__t.html#a244d9e7389e623fcc746438c79bda0e7", null ],
      [ "config", "unionserial__cmd__t.html#afc2ff06d5468836040553e19a0d27648", null ],
      [ "openmesh", "unionserial__cmd__t.html#aa1ebdb5bded845c9919deb7f70a37b64", null ],
      [ "prov", "unionserial__cmd__t.html#aef113fc5ce7cbe9a0a4763d7f3ed335c", null ],
      [ "mesh", "unionserial__cmd__t.html#a10d227e3c955463e94f40c0f71fe0080", null ],
      [ "dfu", "unionserial__cmd__t.html#ae47abf773ffb4612de8345eaa5f8b751", null ],
      [ "pb_remote", "unionserial__cmd__t.html#aa2f90b2764121f597a3b841bd372fcb7", null ],
      [ "application", "unionserial__cmd__t.html#ae643e8bb9db647c915ef22864bd54424", null ]
    ] ],
    [ "SERIAL_OPCODE_CMD_RANGE_DEVICE_START", "group__SERIAL__CMD.html#gaf0351e8753afa56978fabbaefb091cf3", null ],
    [ "SERIAL_OPCODE_CMD_DEVICE_ECHO", "group__SERIAL__CMD.html#gaeb1a8bce0669ec6e114472948a3d40eb", null ],
    [ "SERIAL_OPCODE_CMD_DEVICE_INTERNAL_EVENTS_REPORT", "group__SERIAL__CMD.html#gae000945596b8af554600536ecb5063ea", null ],
    [ "SERIAL_OPCODE_CMD_DEVICE_SERIAL_VERSION_GET", "group__SERIAL__CMD.html#gaf1a0f394e11feaae8f484ebc894032ee", null ],
    [ "SERIAL_OPCODE_CMD_DEVICE_FW_INFO_GET", "group__SERIAL__CMD.html#gad74e58562171b30bfdd67218f5886416", null ],
    [ "SERIAL_OPCODE_CMD_DEVICE_RADIO_RESET", "group__SERIAL__CMD.html#ga8a381b90e71b12a7660fadc39e7e10eb", null ],
    [ "SERIAL_OPCODE_CMD_DEVICE_BEACON_START", "group__SERIAL__CMD.html#gaeaf4dfeba8c5f95967922a0d8ec6b556", null ],
    [ "SERIAL_OPCODE_CMD_DEVICE_BEACON_STOP", "group__SERIAL__CMD.html#ga96ecd209eee70200ab1d7ac0b96ded76", null ],
    [ "SERIAL_OPCODE_CMD_DEVICE_BEACON_PARAMS_SET", "group__SERIAL__CMD.html#ga31c84822a97a10ae59d9f9826961cadb", null ],
    [ "SERIAL_OPCODE_CMD_DEVICE_BEACON_PARAMS_GET", "group__SERIAL__CMD.html#gae59d96fa031f8a54d71e76d26673ce7f", null ],
    [ "SERIAL_OPCODE_CMD_RANGE_DEVICE_END", "group__SERIAL__CMD.html#ga07f3266e36d1a5f4f94f86879e082094", null ],
    [ "SERIAL_OPCODE_CMD_RANGE_APP_START", "group__SERIAL__CMD.html#ga419522e4caeaaf2dc406f06cf54a3010", null ],
    [ "SERIAL_OPCODE_CMD_APP_APPLICATION", "group__SERIAL__CMD.html#gac0d6b7b5c3e3f5999d2577c7a9e1b0ca", null ],
    [ "SERIAL_OPCODE_CMD_RANGE_APP_END", "group__SERIAL__CMD.html#ga4deb096bd767d36e496ad7f38220ab38", null ],
    [ "SERIAL_OPCODE_CMD_RANGE_SAR_START", "group__SERIAL__CMD.html#gac428586b189a4ff0f223055da8fb1202", null ],
    [ "SERIAL_OPCODE_CMD_SAR_START", "group__SERIAL__CMD.html#gadf09b63f6d24295f26cc966d84de741e", null ],
    [ "SERIAL_OPCODE_CMD_SAR_CONTINUE", "group__SERIAL__CMD.html#ga1a7f3793b150694b917041a94a21d14e", null ],
    [ "SERIAL_OPCODE_CMD_RANGE_SAR_END", "group__SERIAL__CMD.html#ga11a8a8df1c43a53e4ebfd4c81f1ed6e3", null ],
    [ "SERIAL_OPCODE_CMD_RANGE_CONFIG_START", "group__SERIAL__CMD.html#ga45f2871b234954051fd9166a8995baf8", null ],
    [ "SERIAL_OPCODE_CMD_CONFIG_ADV_ADDR_SET", "group__SERIAL__CMD.html#gafbb62e66105441d77f648df36913a285", null ],
    [ "SERIAL_OPCODE_CMD_CONFIG_ADV_ADDR_GET", "group__SERIAL__CMD.html#ga83adc680afdb003f0d185e2286099ecf", null ],
    [ "SERIAL_OPCODE_CMD_CONFIG_CHANNEL_MAP_SET", "group__SERIAL__CMD.html#ga21a66aad1beae85d99bfe85e10d5f1eb", null ],
    [ "SERIAL_OPCODE_CMD_CONFIG_CHANNEL_MAP_GET", "group__SERIAL__CMD.html#ga087325d5060b77253b509771283fa46e", null ],
    [ "SERIAL_OPCODE_CMD_CONFIG_TX_POWER_SET", "group__SERIAL__CMD.html#ga1b344969c91b912b52fcc143c5d19fc7", null ],
    [ "SERIAL_OPCODE_CMD_CONFIG_TX_POWER_GET", "group__SERIAL__CMD.html#ga5dcb89cfbfb6e430089d87d823d747f0", null ],
    [ "SERIAL_OPCODE_CMD_CONFIG_UUID_SET", "group__SERIAL__CMD.html#gac06549e327df9d4a4d72fcc167064cfc", null ],
    [ "SERIAL_OPCODE_CMD_CONFIG_UUID_GET", "group__SERIAL__CMD.html#ga422c6c762cdc67c4fc72554169b321ea", null ],
    [ "SERIAL_OPCODE_CMD_RANGE_CONFIG_END", "group__SERIAL__CMD.html#gaf9b830384af22c2aa51a82193927bdd9", null ],
    [ "SERIAL_OPCODE_CMD_RANGE_PROV_START", "group__SERIAL__CMD.html#gae275e9ff5257c39df590a2b59ac7d624", null ],
    [ "SERIAL_OPCODE_CMD_PROV_SCAN_START", "group__SERIAL__CMD.html#ga381f4361fc602f5d7d9e7fc2bc36f188", null ],
    [ "SERIAL_OPCODE_CMD_PROV_SCAN_STOP", "group__SERIAL__CMD.html#ga22ce490a10191bbe99c21e34ff008c3d", null ],
    [ "SERIAL_OPCODE_CMD_PROV_PROVISION", "group__SERIAL__CMD.html#ga31f09cae9fcd56255e8cdbf2a11f6b6e", null ],
    [ "SERIAL_OPCODE_CMD_PROV_LISTEN", "group__SERIAL__CMD.html#gaa9aef890f9b5e9b4ec5c3b25edb2de01", null ],
    [ "SERIAL_OPCODE_CMD_PROV_OOB_USE", "group__SERIAL__CMD.html#ga4866a1f09f3b0e168a35e091f447ac95", null ],
    [ "SERIAL_OPCODE_CMD_PROV_AUTH_DATA", "group__SERIAL__CMD.html#gab0a4d571e4454daeadd72e3e0d255f57", null ],
    [ "SERIAL_OPCODE_CMD_PROV_ECDH_SECRET", "group__SERIAL__CMD.html#ga9d75395e78abea3e0e491baab07a99e4", null ],
    [ "SERIAL_OPCODE_CMD_PROV_KEYPAIR_SET", "group__SERIAL__CMD.html#gaf8cc8736efa18b246453a0edae4a6d45", null ],
    [ "SERIAL_OPCODE_CMD_PROV_CAPABILITIES_SET", "group__SERIAL__CMD.html#ga4bbc84d161477aa32f9ec6ee7aa52c33", null ],
    [ "SERIAL_OPCODE_CMD_RANGE_PROV_END", "group__SERIAL__CMD.html#ga8cd723b87ecade7626df77404c2ece97", null ],
    [ "SERIAL_OPCODE_CMD_RANGE_OPENMESH_START", "group__SERIAL__CMD.html#gaf04235063a366ca67c8f51c7250c82a2", null ],
    [ "SERIAL_OPCODE_CMD_OPENMESH_INIT", "group__SERIAL__CMD.html#ga1c72fc84cb515c400202b67e6b72247c", null ],
    [ "SERIAL_OPCODE_CMD_OPENMESH_VALUE_SET", "group__SERIAL__CMD.html#ga34e337f8f27706a1c2f9e2016734fd36", null ],
    [ "SERIAL_OPCODE_CMD_OPENMESH_VALUE_ENABLE", "group__SERIAL__CMD.html#ga58882f1d5605b070dbe802b81546b040", null ],
    [ "SERIAL_OPCODE_CMD_OPENMESH_VALUE_DISABLE", "group__SERIAL__CMD.html#gac6baa7485330233272ef5e6f8b9bc05b", null ],
    [ "SERIAL_OPCODE_CMD_OPENMESH_START", "group__SERIAL__CMD.html#ga5e0c5f601cdcecd051a02e60cbb33eef", null ],
    [ "SERIAL_OPCODE_CMD_OPENMESH_STOP", "group__SERIAL__CMD.html#ga38a5dd40c013c170451c150f97c136b9", null ],
    [ "SERIAL_OPCODE_CMD_OPENMESH_FLAG_SET", "group__SERIAL__CMD.html#ga247286a781eae2a275fc9a62bec4ce70", null ],
    [ "SERIAL_OPCODE_CMD_OPENMESH_FLAG_GET", "group__SERIAL__CMD.html#ga1af4098d900919c3ec8e24435a0509eb", null ],
    [ "SERIAL_OPCODE_CMD_OPENMESH_DFU_DATA", "group__SERIAL__CMD.html#ga02896cfb764dbc2e42bf017e0e8c6b77", null ],
    [ "SERIAL_OPCODE_CMD_OPENMESH_VALUE_GET", "group__SERIAL__CMD.html#ga6f7603a86cec769f786c6604222937af", null ],
    [ "SERIAL_OPCODE_CMD_OPENMESH_BUILD_VERSION_GET", "group__SERIAL__CMD.html#ga273cf84ce28b894fdbc52e6ca70c8ad9", null ],
    [ "SERIAL_OPCODE_CMD_OPENMESH_ACCESS_ADDR_GET", "group__SERIAL__CMD.html#gaf983ca097b7a01f54bb9a9299d4adebc", null ],
    [ "SERIAL_OPCODE_CMD_OPENMESH_CHANNEL_GET", "group__SERIAL__CMD.html#ga1b2ae15beb3b6ee733adf8a36e192254", null ],
    [ "SERIAL_OPCODE_CMD_OPENMESH_INTERVAL_MIN_MS_GET", "group__SERIAL__CMD.html#gab37542a488b542b9698257aea4dbe586", null ],
    [ "SERIAL_OPCODE_CMD_RANGE_OPENMESH_END", "group__SERIAL__CMD.html#ga4fbec32cd2308b20001d5c6627401d99", null ],
    [ "SERIAL_OPCODE_CMD_RANGE_MESH_START", "group__SERIAL__CMD.html#ga98c7072cb48c4562c93c77209d4d83f6", null ],
    [ "SERIAL_OPCODE_CMD_MESH_ENABLE", "group__SERIAL__CMD.html#ga8bee92ca3b8ac3a3981904d781648bd1", null ],
    [ "SERIAL_OPCODE_CMD_MESH_DISABLE", "group__SERIAL__CMD.html#ga64bde426b401c11e041700d047209625", null ],
    [ "SERIAL_OPCODE_CMD_MESH_SUBNET_ADD", "group__SERIAL__CMD.html#ga1db5cca3c8b9d6b771c94f4b1c8652ab", null ],
    [ "SERIAL_OPCODE_CMD_MESH_SUBNET_UPDATE", "group__SERIAL__CMD.html#gaeb493707f2940bb4c11af8cdd8af888c", null ],
    [ "SERIAL_OPCODE_CMD_MESH_SUBNET_DELETE", "group__SERIAL__CMD.html#gab0b9cfcf98a5b82d796a089c3506492e", null ],
    [ "SERIAL_OPCODE_CMD_MESH_SUBNET_GET_ALL", "group__SERIAL__CMD.html#gad70db99b4617fd1b9cad6bfa36b7638f", null ],
    [ "SERIAL_OPCODE_CMD_MESH_SUBNET_COUNT_MAX_GET", "group__SERIAL__CMD.html#ga0aaa61f28536edc60a6bbde5bee2ad75", null ],
    [ "SERIAL_OPCODE_CMD_MESH_APPKEY_ADD", "group__SERIAL__CMD.html#ga17a427db21bc374ebff33eeb22ae4d24", null ],
    [ "SERIAL_OPCODE_CMD_MESH_APPKEY_UPDATE", "group__SERIAL__CMD.html#ga5662ec7c066eb429a07cb2e02e3f2d23", null ],
    [ "SERIAL_OPCODE_CMD_MESH_APPKEY_DELETE", "group__SERIAL__CMD.html#ga52ad6b9ea84a37e55ee907e099e61852", null ],
    [ "SERIAL_OPCODE_CMD_MESH_APPKEY_GET_ALL", "group__SERIAL__CMD.html#ga7302503ceeb46c1d7619b05b6586ccd2", null ],
    [ "SERIAL_OPCODE_CMD_MESH_APPKEY_COUNT_MAX_GET", "group__SERIAL__CMD.html#ga7d956ef85cf19a17edf1b7a4341a31d5", null ],
    [ "SERIAL_OPCODE_CMD_MESH_DEVKEY_ADD", "group__SERIAL__CMD.html#ga7093f6e75562d2b9427d419843ce75da", null ],
    [ "SERIAL_OPCODE_CMD_MESH_DEVKEY_DELETE", "group__SERIAL__CMD.html#ga13c9e9c99acfbeecfaf36dfe8f8df55e", null ],
    [ "SERIAL_OPCODE_CMD_MESH_DEVKEY_COUNT_MAX_GET", "group__SERIAL__CMD.html#gaf0a2d1a73ddcdec51579ca14cbdcef54", null ],
    [ "SERIAL_OPCODE_CMD_MESH_ADDR_LOCAL_UNICAST_SET", "group__SERIAL__CMD.html#ga1270e2be17024de5cddf4e2fcf4c5361", null ],
    [ "SERIAL_OPCODE_CMD_MESH_ADDR_LOCAL_UNICAST_GET", "group__SERIAL__CMD.html#gaa88697d710956f87db57757608ab2575", null ],
    [ "SERIAL_OPCODE_CMD_MESH_ADDR_SUBSCRIPTION_ADD", "group__SERIAL__CMD.html#gaf4959ca0b537a75f3868cf89320a13be", null ],
    [ "SERIAL_OPCODE_CMD_MESH_ADDR_SUBSCRIPTION_ADD_VIRTUAL", "group__SERIAL__CMD.html#ga073e2780617f00de41a2c5d52df7b140", null ],
    [ "SERIAL_OPCODE_CMD_MESH_ADDR_SUBSCRIPTION_REMOVE", "group__SERIAL__CMD.html#gaab7fd78e427803e25a8b6b7108903266", null ],
    [ "SERIAL_OPCODE_CMD_MESH_ADDR_PUBLICATION_ADD", "group__SERIAL__CMD.html#gab2c7ec124abb5f4af06449160f7d45ad", null ],
    [ "SERIAL_OPCODE_CMD_MESH_ADDR_PUBLICATION_ADD_VIRTUAL", "group__SERIAL__CMD.html#gafdc9abaad703eefb73705d32ac03b546", null ],
    [ "SERIAL_OPCODE_CMD_MESH_ADDR_PUBLICATION_REMOVE", "group__SERIAL__CMD.html#ga3f184e417171286c5bb7ad83e3a1421e", null ],
    [ "SERIAL_OPCODE_CMD_MESH_ADDR_GET", "group__SERIAL__CMD.html#gac98be99cdd4a86ba9ede5a9c87914c59", null ],
    [ "SERIAL_OPCODE_CMD_MESH_ADDR_GET_ALL", "group__SERIAL__CMD.html#gae0fb32bd4825627ff6975961d998a193", null ],
    [ "SERIAL_OPCODE_CMD_MESH_ADDR_NONVIRTUAL_COUNT_MAX_GET", "group__SERIAL__CMD.html#gab6b4b0192bac26e8a3bf8d6263b89ca4", null ],
    [ "SERIAL_OPCODE_CMD_MESH_ADDR_VIRTUAL_COUNT_MAX_GET", "group__SERIAL__CMD.html#ga8ffd65ecd9bbb1cdf6d789ef65e62e2f", null ],
    [ "SERIAL_OPCODE_CMD_MESH_PACKET_SEND", "group__SERIAL__CMD.html#ga4e06aa3977d7ae6778c96e324d5414ee", null ],
    [ "SERIAL_OPCODE_CMD_MESH_STATE_CLEAR", "group__SERIAL__CMD.html#ga8377376b28b10715a954a0156df431de", null ],
    [ "SERIAL_OPCODE_CMD_RANGE_MESH_END", "group__SERIAL__CMD.html#gaf91398e49b7a0c4c94e8004fff7d6a8b", null ],
    [ "SERIAL_OPCODE_CMD_RANGE_DFU_START", "group__SERIAL__CMD.html#ga403b65c9a7402241ac5dcee91b509a6b", null ],
    [ "SERIAL_OPCODE_CMD_DFU_JUMP_TO_BOOTLOADER", "group__SERIAL__CMD.html#gae00588293873147f5426430084b5ecb6", null ],
    [ "SERIAL_OPCODE_CMD_DFU_REQUEST", "group__SERIAL__CMD.html#gae594d7d6fdfe9e5f06fc9c4145e261f3", null ],
    [ "SERIAL_OPCODE_CMD_DFU_RELAY", "group__SERIAL__CMD.html#ga4c60fe6e2154d7e61cc2aa001a5df4f3", null ],
    [ "SERIAL_OPCODE_CMD_DFU_ABORT", "group__SERIAL__CMD.html#ga05dc2f4b325b72fa656256db5dce5995", null ],
    [ "SERIAL_OPCODE_CMD_DFU_BANK_INFO_GET", "group__SERIAL__CMD.html#gae9c03d46b7f7e5ffe7d07bca08265a27", null ],
    [ "SERIAL_OPCODE_CMD_DFU_BANK_FLASH", "group__SERIAL__CMD.html#ga34d652759d983b668340375eb30fbdc6", null ],
    [ "SERIAL_OPCODE_CMD_DFU_STATE_GET", "group__SERIAL__CMD.html#gaec5f6636c881ab12a35eca0421e4a9a0", null ],
    [ "SERIAL_OPCODE_CMD_RANGE_DFU_END", "group__SERIAL__CMD.html#ga326e3b6e96a34a86cd945e9f9bd3c72c", null ],
    [ "SERIAL_OPCODE_CMD_RANGE_ACCESS_START", "group__SERIAL__CMD.html#gad8df1f34765ad53689781d859e11af4d", null ],
    [ "SERIAL_OPCODE_CMD_ACCESS_MODEL_PUB_ADDR_SET", "group__SERIAL__CMD.html#ga7c667681eafa7f83bc762ca976b9858b", null ],
    [ "SERIAL_OPCODE_CMD_ACCESS_MODEL_PUB_ADDR_GET", "group__SERIAL__CMD.html#ga4462b2578c4e736a534d40b97e3e890c", null ],
    [ "SERIAL_OPCODE_CMD_ACCESS_MODEL_PUB_PERIOD_SET", "group__SERIAL__CMD.html#gad35261eba962fbaa43afec86e9e91fd3", null ],
    [ "SERIAL_OPCODE_CMD_ACCESS_MODEL_PUB_PERIOD_GET", "group__SERIAL__CMD.html#ga00bf5067cf01777aeca0994901c94cbe", null ],
    [ "SERIAL_OPCODE_CMD_ACCESS_MODEL_SUBS_ADD", "group__SERIAL__CMD.html#ga169a0430c7ca4b12bad0a57f89c7483d", null ],
    [ "SERIAL_OPCODE_CMD_ACCESS_MODEL_SUBS_REMOVE", "group__SERIAL__CMD.html#ga4d00ee4cdac17e39d8dda7d4997193e8", null ],
    [ "SERIAL_OPCODE_CMD_ACCESS_MODEL_SUBS_GET", "group__SERIAL__CMD.html#gab436382cfb64bb5ef6cd5f71eac81a9b", null ],
    [ "SERIAL_OPCODE_CMD_ACCESS_MODEL_APP_BIND", "group__SERIAL__CMD.html#ga3b591cec939d350238c1cd333bc16c99", null ],
    [ "SERIAL_OPCODE_CMD_ACCESS_MODEL_APP_UNBIND", "group__SERIAL__CMD.html#ga579843b5046bd8fef1c4e181de82dab5", null ],
    [ "SERIAL_OPCODE_CMD_ACCESS_MODEL_APP_GET", "group__SERIAL__CMD.html#gaf3e1757bc0e5a658077a0ac49c6aae6e", null ],
    [ "SERIAL_OPCODE_CMD_ACCESS_MODEL_PUB_APP_SET", "group__SERIAL__CMD.html#gafcf24fa301bba69d35fe2a41c0e98da7", null ],
    [ "SERIAL_OPCODE_CMD_ACCESS_MODEL_PUB_APP_GET", "group__SERIAL__CMD.html#ga3ad224ab215d693a74cd4f8926c9a3b9", null ],
    [ "SERIAL_OPCODE_CMD_ACCESS_MODEL_PUB_TTL_SET", "group__SERIAL__CMD.html#ga6cf284bd1d49f1c3857df8a2f3fef140", null ],
    [ "SERIAL_OPCODE_CMD_ACCESS_MODEL_PUB_TTL_GET", "group__SERIAL__CMD.html#gaf548e41fe9dd24181746f7c2691dc2ce", null ],
    [ "SERIAL_OPCODE_CMD_ACCESS_ELEM_LOC_SET", "group__SERIAL__CMD.html#gafe96709b6b1b94f927cb7a11306b00c9", null ],
    [ "SERIAL_OPCODE_CMD_ACCESS_ELEM_LOC_GET", "group__SERIAL__CMD.html#gafdb009c7db066dcf766473c354d1359a", null ],
    [ "SERIAL_OPCODE_CMD_ACCESS_ELEM_SIG_MODEL_COUNT_GET", "group__SERIAL__CMD.html#ga0666fe0d73acb00e21d34c0f2500f495", null ],
    [ "SERIAL_OPCODE_CMD_ACCESS_ELEM_VENDOR_MODEL_COUNT_GET", "group__SERIAL__CMD.html#ga61f43a5f7b3a431e7d0f9cec2f2a9edc", null ],
    [ "SERIAL_OPCODE_CMD_ACCESS_MODEL_ID_GET", "group__SERIAL__CMD.html#ga06c6977f0df294cf781f0de894edfe0f", null ],
    [ "SERIAL_OPCODE_CMD_ACCESS_HANDLE_GET", "group__SERIAL__CMD.html#ga97be1db2abf61ef54c0a2e6b379e63b4", null ],
    [ "SERIAL_OPCODE_CMD_ACCESS_ELEM_MODELS_GET", "group__SERIAL__CMD.html#gaea8eac9ad37874bbca7d3eff1caadbae", null ],
    [ "SERIAL_OPCODE_CMD_ACCESS_ACCESS_FLASH_STORE", "group__SERIAL__CMD.html#ga16cbb59205c71b867eb3ca14c8b4d838", null ],
    [ "SERIAL_OPCODE_CMD_RANGE_ACCESS_END", "group__SERIAL__CMD.html#ga92ab8efc91b096dbd7b6cfe87116bca8", null ],
    [ "SERIAL_OPCODE_CMD_RANGE_MODEL_SPECIFIC_START", "group__SERIAL__CMD.html#gad6e9755277dd37e2643d8f9b6a17bb43", null ],
    [ "SERIAL_OPCODE_CMD_MODEL_SPECIFIC_MODELS_GET", "group__SERIAL__CMD.html#ga07683122cb18302e59344aaa9351145a", null ],
    [ "SERIAL_OPCODE_CMD_MODEL_SPECIFIC_INIT", "group__SERIAL__CMD.html#ga03079ee39c55a1babdec041b2e9188a9", null ],
    [ "SERIAL_OPCODE_CMD_MODEL_SPECIFIC_COMMAND", "group__SERIAL__CMD.html#gaccd8d84c2e2ef3157f305090181180da", null ],
    [ "SERIAL_OPCODE_CMD_RANGE_MODEL_SPECIFIC_END", "group__SERIAL__CMD.html#ga8dcadd65e7a60862ef31ef886764cb28", null ]
];