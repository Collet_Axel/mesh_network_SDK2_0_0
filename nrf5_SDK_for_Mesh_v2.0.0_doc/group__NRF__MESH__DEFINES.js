var group__NRF__MESH__DEFINES =
[
    [ "API level definitions", "group__MESH__DEFINES__API.html", "group__MESH__DEFINES__API" ],
    [ "Network layer definitions", "group__MESH__DEFINES__NETWORK.html", "group__MESH__DEFINES__NETWORK" ],
    [ "Transport layer definitions", "group__MESH__DEFINES__TRANSPORT.html", "group__MESH__DEFINES__TRANSPORT" ],
    [ "Heartbeat definitions", "group__MESH__DEFINES__HEARTBEAT.html", "group__MESH__DEFINES__HEARTBEAT" ]
];