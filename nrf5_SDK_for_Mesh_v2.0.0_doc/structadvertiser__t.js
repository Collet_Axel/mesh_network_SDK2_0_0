var structadvertiser__t =
[
    [ "enabled", "structadvertiser__t.html#a9e5ac2e925c56132c562dc6fe0645114", null ],
    [ "p_packet", "structadvertiser__t.html#adf56fc3f57eed500720c7ac5656571f5", null ],
    [ "broadcast", "structadvertiser__t.html#a00fff2f75b74795214009bcee7b1eba6", null ],
    [ "timer", "structadvertiser__t.html#a7025562e2da9be7263dbe25dc25309f0", null ],
    [ "buf", "structadvertiser__t.html#a026b1cbb7c81ad5492b4cd5c269fda9a", null ],
    [ "config", "structadvertiser__t.html#a5b860c53204ea07650f28f13d904a3e3", null ],
    [ "tx_complete_callback", "structadvertiser__t.html#ac8dd9d0bd26425a569676d89aad8581b", null ],
    [ "tx_complete_event", "structadvertiser__t.html#a63820f38d27d513b985339a32bbf99fa", null ],
    [ "tx_complete_params", "structadvertiser__t.html#a065d5100a98f4fc6f27befafcb8598c7", null ]
];