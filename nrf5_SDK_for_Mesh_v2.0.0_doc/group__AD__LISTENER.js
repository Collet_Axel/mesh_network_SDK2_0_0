var group__AD__LISTENER =
[
    [ "ad_listener_t", "structad__listener__t.html", [
      [ "ad_type", "structad__listener__t.html#aedcbac09890c562a3dec20db1ed292f0", null ],
      [ "adv_packet_type", "structad__listener__t.html#a29377613e5499795d1ac613e6760beaa", null ],
      [ "handler", "structad__listener__t.html#a7c04ab4f6eba3027038d8865616ac18e", null ],
      [ "node", "structad__listener__t.html#a6f78f3bcd4c257b847ee19a8ae876598", null ]
    ] ],
    [ "ad_listener_subscribe", "group__AD__LISTENER.html#ga911a3f14493ddd1d0da1760b0a9a3394", null ],
    [ "ad_listener_unsubscribe", "group__AD__LISTENER.html#ga583a6af105a18b14929291937221e76c", null ],
    [ "ad_listener_process", "group__AD__LISTENER.html#ga15239cd2c92e491f1c966b1684eb6beb", null ]
];