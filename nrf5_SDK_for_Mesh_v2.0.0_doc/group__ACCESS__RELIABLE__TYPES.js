var group__ACCESS__RELIABLE__TYPES =
[
    [ "access_reliable_t", "structaccess__reliable__t.html", [
      [ "model_handle", "structaccess__reliable__t.html#af0093e61e59c49d144506fe19c90cf71", null ],
      [ "message", "structaccess__reliable__t.html#ac53796aaae6be50243e6fc1110aad41e", null ],
      [ "reply_opcode", "structaccess__reliable__t.html#a34ee06db5d2dc4d9785906f0dbc33849", null ],
      [ "timeout", "structaccess__reliable__t.html#a84a51dccc0719f8b9d56b700fb39dc78", null ],
      [ "status_cb", "structaccess__reliable__t.html#a2a5177d598502b1e9ecc281797fec310", null ]
    ] ],
    [ "access_reliable_cb_t", "group__ACCESS__RELIABLE__TYPES.html#gae0e403630dc00547857b0e2861652862", null ],
    [ "access_reliable_status_t", "group__ACCESS__RELIABLE__TYPES.html#gafc996bea7113455a67f387da0f62961c", [
      [ "ACCESS_RELIABLE_TRANSFER_SUCCESS", "group__ACCESS__RELIABLE__TYPES.html#ggafc996bea7113455a67f387da0f62961cabbbfa0f020e288fe86cc29f49e554554", null ],
      [ "ACCESS_RELIABLE_TRANSFER_TIMEOUT", "group__ACCESS__RELIABLE__TYPES.html#ggafc996bea7113455a67f387da0f62961cab073fa8f40445eb5d767b7fe926ecb25", null ],
      [ "ACCESS_RELIABLE_TRANSFER_CANCELLED", "group__ACCESS__RELIABLE__TYPES.html#ggafc996bea7113455a67f387da0f62961ca125e4e2142ed9ebcf56ba70cb7d4fa72", null ]
    ] ]
];