var group__INSTABURST__TX =
[
    [ "instaburst_tx_config_t", "structinstaburst__tx__config__t.html", [
      [ "set_id", "structinstaburst__tx__config__t.html#ada59971be2aa99e4b7a1d78002350410", null ],
      [ "p_channels", "structinstaburst__tx__config__t.html#a9ed823cdf08c1c916e958682d2d001ea", null ],
      [ "channel_count", "structinstaburst__tx__config__t.html#a27259ec43408f1774d2bea48c7e4ceaf", null ],
      [ "radio_mode", "structinstaburst__tx__config__t.html#a8de57b3cf808b983ef3f81cf921080be", null ],
      [ "tx_power", "structinstaburst__tx__config__t.html#ae97bbfd141f61820b97d397d3e918545", null ],
      [ "callback", "structinstaburst__tx__config__t.html#a42119701550d80eb51db2d64f889ba77", null ],
      [ "interval_ms", "structinstaburst__tx__config__t.html#aa8e993485d99b55dcdb1b505250e91d9", null ]
    ] ],
    [ "instaburst_tx_t", "structinstaburst__tx.html", [
      [ "config", "structinstaburst__tx.html#ad06fe9c913526cda66eb3e09f4d27c0c", null ],
      [ "channel_index", "structinstaburst__tx.html#a042b9d84f9250fa48c6fe5fbc8c8d9ab", null ],
      [ "packet_buffer", "structinstaburst__tx.html#a9bad62a6dabfc6c33732342bec48891a", null ],
      [ "p_next_alloc", "structinstaburst__tx.html#a87b5a5192c999df2722417f728c39d75", null ],
      [ "p_alloc_packet", "structinstaburst__tx.html#a531122a6c64b95c1d42fc0d078e4cbba", null ],
      [ "p_alloc_buf", "structinstaburst__tx.html#a9ab04fc063b6be5c99cffafc3ba76ce8", null ],
      [ "p_tx_buf", "structinstaburst__tx.html#a795395409e1c44415acba9b6e8b00b2d", null ],
      [ "broadcast", "structinstaburst__tx.html#a4dbafb6dd1ae9f757e0b17d6b775b00a", null ],
      [ "adv_ext_tx", "structinstaburst__tx.html#ae47e150a0ad551e61e094cdfbeef6525", null ],
      [ "tx_complete_event", "structinstaburst__tx.html#a27794777ea90d0a1e4331f4175b3c9c5", null ],
      [ "prev_tx_timestamp", "structinstaburst__tx.html#aee1f61d5172eb732ba7e56e1ea010e46", null ],
      [ "timer_event", "structinstaburst__tx.html#ab041fa141b40519db0e34af8f2158a12", null ]
    ] ],
    [ "INSTABURST_TX_BUFFER_MIN_SIZE", "group__INSTABURST__TX.html#ga868e06f450e9aa355cb446ad34d7fd85", null ],
    [ "instaburst_tx_complete_t", "group__INSTABURST__TX.html#ga2c2dc13e794aad68e9cd83672e1601dc", null ],
    [ "instaburst_tx_init", "group__INSTABURST__TX.html#ga353eb58286a7cd7200c65fa3e66b7258", null ],
    [ "instaburst_tx_instance_init", "group__INSTABURST__TX.html#ga2aa67c499f7ce1fbf97beea529a2ae54", null ],
    [ "instaburst_tx_enable", "group__INSTABURST__TX.html#ga927a39279d281466193d0d4ac4329620", null ],
    [ "instaburst_tx_disable", "group__INSTABURST__TX.html#gad9773e326198e64795c1e731028f17b9", null ],
    [ "instaburst_tx_buffer_alloc", "group__INSTABURST__TX.html#ga932ce3e090236c792109dd956bb10987", null ],
    [ "instaburst_tx_buffer_commit", "group__INSTABURST__TX.html#gad913f437f6bf5b4884090f8abb286e32", null ],
    [ "instaburst_tx_buffer_discard", "group__INSTABURST__TX.html#ga5be408c4839b7b4e4814013f3cf72b7a", null ],
    [ "instaburst_tx_finalize", "group__INSTABURST__TX.html#gaa0a4c8bb5c898c4d61ac66506fee3d30", null ],
    [ "instaburst_tx_buffer_lock", "group__INSTABURST__TX.html#ga383267a7af860ac1bd0eff72c66047ad", null ],
    [ "instaburst_tx_interval_set", "group__INSTABURST__TX.html#ga5427abe5dd3496256eea2329b11ea47d", null ]
];