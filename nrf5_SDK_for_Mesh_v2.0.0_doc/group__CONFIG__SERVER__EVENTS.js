var group__CONFIG__SERVER__EVENTS =
[
    [ "config_server_evt_appkey_add_t", "structconfig__server__evt__appkey__add__t.html", [
      [ "appkey_handle", "structconfig__server__evt__appkey__add__t.html#abee0d1f26577bdb74a1a888f29e4671a", null ]
    ] ],
    [ "config_server_evt_appkey_update_t", "structconfig__server__evt__appkey__update__t.html", [
      [ "appkey_handle", "structconfig__server__evt__appkey__update__t.html#ae6910d87fb5cbf7f78dd951daac3e5b1", null ]
    ] ],
    [ "config_server_evt_model_publication_set_t", "structconfig__server__evt__model__publication__set__t.html", [
      [ "model_handle", "structconfig__server__evt__model__publication__set__t.html#aa81e6a4f78de5c4caa5789f8c9bc4a6b", null ]
    ] ],
    [ "config_server_evt_appkey_delete_t", "structconfig__server__evt__appkey__delete__t.html", [
      [ "appkey_handle", "structconfig__server__evt__appkey__delete__t.html#a93454ae07ec00eecfc87bc010421116c", null ]
    ] ],
    [ "config_server_evt_beacon_set_t", "structconfig__server__evt__beacon__set__t.html", [
      [ "beacon_state", "structconfig__server__evt__beacon__set__t.html#a6344164bbff5b6e9832c1d88d423d99f", null ]
    ] ],
    [ "config_server_evt_default_ttl_set_t", "structconfig__server__evt__default__ttl__set__t.html", [
      [ "default_ttl", "structconfig__server__evt__default__ttl__set__t.html#a164aced705d64830e3b829006e5f3491", null ]
    ] ],
    [ "config_server_evt_key_refresh_phase_set_t", "structconfig__server__evt__key__refresh__phase__set__t.html", [
      [ "kr_phase", "structconfig__server__evt__key__refresh__phase__set__t.html#a021369dc295ade7073f5bacd333b6176", null ]
    ] ],
    [ "config_server_evt_model_subscription_add_t", "structconfig__server__evt__model__subscription__add__t.html", [
      [ "model_handle", "structconfig__server__evt__model__subscription__add__t.html#af23be659d84fc5e2c0f187e36aee21f0", null ],
      [ "address_handle", "structconfig__server__evt__model__subscription__add__t.html#a22a936c1f476aae671ae3599140f2f81", null ]
    ] ],
    [ "config_server_evt_model_subscription_delete_t", "structconfig__server__evt__model__subscription__delete__t.html", [
      [ "model_handle", "structconfig__server__evt__model__subscription__delete__t.html#a73ac223020788bb45ef8b6b2c4339bb4", null ],
      [ "address_handle", "structconfig__server__evt__model__subscription__delete__t.html#a6ddb2fbfd5cc60b88d6670852d854f3d", null ]
    ] ],
    [ "config_server_evt_model_subscription_delete_all_t", "structconfig__server__evt__model__subscription__delete__all__t.html", [
      [ "model_handle", "structconfig__server__evt__model__subscription__delete__all__t.html#ae53e540004de20316b31b3c0639097bc", null ]
    ] ],
    [ "config_server_evt_model_subscription_overwrite_t", "structconfig__server__evt__model__subscription__overwrite__t.html", [
      [ "model_handle", "structconfig__server__evt__model__subscription__overwrite__t.html#ad8a9e02e3d06aa64d8645fe4b04b3aff", null ],
      [ "address_handle", "structconfig__server__evt__model__subscription__overwrite__t.html#a5a3e41fa887eb7eff82f583f2475d4e1", null ]
    ] ],
    [ "config_server_evt_network_transmit_set_t", "structconfig__server__evt__network__transmit__set__t.html", [
      [ "retransmit_count", "structconfig__server__evt__network__transmit__set__t.html#acbb7aa685a68839319b4ab550ef19336", null ],
      [ "interval_steps", "structconfig__server__evt__network__transmit__set__t.html#a6d298aadc40f21b3e1817c65ca81e12b", null ]
    ] ],
    [ "config_server_evt_relay_set_t", "structconfig__server__evt__relay__set__t.html", [
      [ "enabled", "structconfig__server__evt__relay__set__t.html#afea4ac169f2ba9ab7d90ca8577314107", null ],
      [ "retransmit_count", "structconfig__server__evt__relay__set__t.html#add1cdb2aeb4f0e812c7f4b82673089fd", null ],
      [ "interval_steps", "structconfig__server__evt__relay__set__t.html#a562590592b24be27f843d450aa590289", null ]
    ] ],
    [ "config_server_evt_heartbeat_publication_set_t", "structconfig__server__evt__heartbeat__publication__set__t.html", [
      [ "p_publication_state", "structconfig__server__evt__heartbeat__publication__set__t.html#a17226172e4aedac8f1580bc6870df0a7", null ]
    ] ],
    [ "config_server_evt_heartbeat_subscription_set_t", "structconfig__server__evt__heartbeat__subscription__set__t.html", [
      [ "p_subscription_state", "structconfig__server__evt__heartbeat__subscription__set__t.html#ae8eb7cf06343efc4ea27ae3370d2c079", null ]
    ] ],
    [ "config_server_evt_model_app_bind_t", "structconfig__server__evt__model__app__bind__t.html", [
      [ "model_handle", "structconfig__server__evt__model__app__bind__t.html#a2f170b0871bec12af58d6462d56d7126", null ],
      [ "appkey_handle", "structconfig__server__evt__model__app__bind__t.html#aad1443fc697286121eb7e94c031e438b", null ]
    ] ],
    [ "config_server_evt_model_app_unbind_t", "structconfig__server__evt__model__app__unbind__t.html", [
      [ "model_handle", "structconfig__server__evt__model__app__unbind__t.html#afdafa0e1ae47cdf3ba7e05af83636221", null ],
      [ "appkey_handle", "structconfig__server__evt__model__app__unbind__t.html#a5be655253669856fa50176ef094e3b28", null ]
    ] ],
    [ "config_server_evt_netkey_add_t", "structconfig__server__evt__netkey__add__t.html", [
      [ "netkey_handle", "structconfig__server__evt__netkey__add__t.html#a1d40ebcd10b45b5e81846b6394b2f70e", null ]
    ] ],
    [ "config_server_evt_netkey_delete_t", "structconfig__server__evt__netkey__delete__t.html", [
      [ "netkey_handle", "structconfig__server__evt__netkey__delete__t.html#a355f3b8eb2853ae22fe8fe8b5a34a4dd", null ]
    ] ],
    [ "config_server_evt_netkey_update_t", "structconfig__server__evt__netkey__update__t.html", [
      [ "netkey_handle", "structconfig__server__evt__netkey__update__t.html#a85277e7cc1c495142d3624109b8b64c8", null ]
    ] ],
    [ "config_server_evt_t", "structconfig__server__evt__t.html", [
      [ "type", "structconfig__server__evt__t.html#ad11cd19dadd88a3b9566b44964d9efab", null ],
      [ "appkey_add", "structconfig__server__evt__t.html#a28f36eb21929c57f9babb06ae1dfe8eb", null ],
      [ "appkey_update", "structconfig__server__evt__t.html#a139706c65b072d0382935bb3beca682c", null ],
      [ "model_publication_set", "structconfig__server__evt__t.html#addae29063e008cf27f9efacf814da6d6", null ],
      [ "appkey_delete", "structconfig__server__evt__t.html#af1562bca666762e72ee291cb9eb90fea", null ],
      [ "beacon_set", "structconfig__server__evt__t.html#ad82f8befd1bbfed7737694a71954d08a", null ],
      [ "default_ttl_set", "structconfig__server__evt__t.html#a97efbd00fa59d0b02660b5dcd0ecb6ab", null ],
      [ "key_refresh_phase_set", "structconfig__server__evt__t.html#aaaba253418de5877f6f5e080a2770b91", null ],
      [ "model_subscription_add", "structconfig__server__evt__t.html#a9aa1d849ce7710bb19ade83abb630bbc", null ],
      [ "model_subscription_delete", "structconfig__server__evt__t.html#a1cbfaa051aad5f75acededdcb2c845a9", null ],
      [ "model_subscription_delete_all", "structconfig__server__evt__t.html#af483b64d469cd6202626c47b511f90c9", null ],
      [ "model_subscription_overwrite", "structconfig__server__evt__t.html#a8cc1862a6244e68ba4e92605fcd547bb", null ],
      [ "network_transmit_set", "structconfig__server__evt__t.html#a0b74926cb51e49caeaa92fc47eff6589", null ],
      [ "relay_set", "structconfig__server__evt__t.html#a6cac2d785345a6b166ba6787a05dc672", null ],
      [ "heartbeat_publication_set", "structconfig__server__evt__t.html#a8219ecf958c471e6ebd3f771dbb63e5b", null ],
      [ "heartbeat_subscription_set", "structconfig__server__evt__t.html#aabbf1fb518416bdd94ad4d063672b1b1", null ],
      [ "model_app_bind", "structconfig__server__evt__t.html#a52829e944fb2d9fa7aa67e0b412b1d67", null ],
      [ "model_app_unbind", "structconfig__server__evt__t.html#a8a7ef13a969b559f6ece8c020d0e0370", null ],
      [ "netkey_add", "structconfig__server__evt__t.html#ab174beabc20eb00de3f346aeb9db1381", null ],
      [ "netkey_delete", "structconfig__server__evt__t.html#a95d6a12fdb9aad8521e3b90bd7b9f653", null ],
      [ "netkey_update", "structconfig__server__evt__t.html#aa4c2ee87897d5b565c06d91ef5021461", null ],
      [ "params", "structconfig__server__evt__t.html#aa1d478949ca58ff77193750f52c9ec02", null ]
    ] ],
    [ "config_server_evt_cb_t", "group__CONFIG__SERVER__EVENTS.html#ga772a2b54aa2b45d41f01ee00da756789", null ],
    [ "config_server_evt_type_t", "group__CONFIG__SERVER__EVENTS.html#ga614b93ba0bb2de05fe4852af0c8b4919", [
      [ "CONFIG_SERVER_EVT_APPKEY_ADD", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919ab54d4df28d03460c5114785889d10e49", null ],
      [ "CONFIG_SERVER_EVT_APPKEY_UPDATE", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919a7b8921c2b6b1f198b94161fd63cad2c4", null ],
      [ "CONFIG_SERVER_EVT_MODEL_PUBLICATION_SET", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919ab75109dbce4c6a31f11fa9f4ce12125b", null ],
      [ "CONFIG_SERVER_EVT_APPKEY_DELETE", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919ac85a3f2b5a130888b8f8c0ff55b916a7", null ],
      [ "CONFIG_SERVER_EVT_BEACON_SET", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919a940d5d8ba38328b35e18e8b45c900777", null ],
      [ "CONFIG_SERVER_EVT_DEFAULT_TTL_SET", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919a0cc1511222d5925cae6c349691770a2b", null ],
      [ "CONFIG_SERVER_EVT_FRIEND_SET", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919a78daa3a0d517f4ec7f8edbac07b0a680", null ],
      [ "CONFIG_SERVER_EVT_GATT_PROXY_SET", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919aaf650df6f97990085258b34394b77fb8", null ],
      [ "CONFIG_SERVER_EVT_KEY_REFRESH_PHASE_SET", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919a500bd9fa4428ce3c2876dedbae079b0d", null ],
      [ "CONFIG_SERVER_EVT_MODEL_PUBLICATION_VIRTUAL_ADDRESS_SET", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919a9002d987cbde0ba88272b3d25f2c9f87", null ],
      [ "CONFIG_SERVER_EVT_MODEL_SUBSCRIPTION_ADD", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919aeb5862fd58bf5eaa748d41e98666091b", null ],
      [ "CONFIG_SERVER_EVT_MODEL_SUBSCRIPTION_DELETE", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919a61ecb4626770bb2012ac3b3ed96d21ae", null ],
      [ "CONFIG_SERVER_EVT_MODEL_SUBSCRIPTION_DELETE_ALL", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919a5fc8123b33134eb7937492e98d4d60de", null ],
      [ "CONFIG_SERVER_EVT_MODEL_SUBSCRIPTION_OVERWRITE", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919a3f5bb53f1211e5eb2ac355aae495d7f3", null ],
      [ "CONFIG_SERVER_EVT_MODEL_SUBSCRIPTION_VIRTUAL_ADDRESS_ADD", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919ad03a07e54cfb675258442f0b8cdc4985", null ],
      [ "CONFIG_SERVER_EVT_MODEL_SUBSCRIPTION_VIRTUAL_ADDRESS_DELETE", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919adcab952b2827ec5dd68c15648e3c7792", null ],
      [ "CONFIG_SERVER_EVT_MODEL_SUBSCRIPTION_VIRTUAL_ADDRESS_OVERWRITE", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919a0ab5f27270d8bf56dd8d357c2e57a602", null ],
      [ "CONFIG_SERVER_EVT_NETWORK_TRANSMIT_SET", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919a2a4786abee0a1d64b620f74bde6db31f", null ],
      [ "CONFIG_SERVER_EVT_RELAY_SET", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919a6d348154cb1ba0b8f570a8380e745a10", null ],
      [ "CONFIG_SERVER_EVT_LOW_POWER_NODE_POLLTIMEOUT_SET", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919ab342f7bcd5b226f732a24f34b34543a7", null ],
      [ "CONFIG_SERVER_EVT_HEARTBEAT_PUBLICATION_SET", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919a278fff52ade39ae2155c1941a14c0ab7", null ],
      [ "CONFIG_SERVER_EVT_HEARTBEAT_SUBSCRIPTION_SET", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919abfe7379d5cf4e4df412ccc8afc168244", null ],
      [ "CONFIG_SERVER_EVT_MODEL_APP_BIND", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919af34ce4dad22a49766f53f909b75988c9", null ],
      [ "CONFIG_SERVER_EVT_MODEL_APP_UNBIND", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919a2ea7bb5e5772f7ce9c343aac4dd4d620", null ],
      [ "CONFIG_SERVER_EVT_NETKEY_ADD", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919aa7839d1e6080ce520a08287e6a20d277", null ],
      [ "CONFIG_SERVER_EVT_NETKEY_DELETE", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919a2af2cb9ec45f3bd07dda5d2a8c64cced", null ],
      [ "CONFIG_SERVER_EVT_NETKEY_UPDATE", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919abba741426fb46c0ee28f456ce3406ab1", null ],
      [ "CONFIG_SERVER_EVT_NODE_IDENTITY_SET", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919ae97dd282fec46ddb1cddc08110646a92", null ],
      [ "CONFIG_SERVER_EVT_NODE_RESET", "group__CONFIG__SERVER__EVENTS.html#gga614b93ba0bb2de05fe4852af0c8b4919a0faa88b94d70aeae80c5711e8976ecd0", null ]
    ] ]
];