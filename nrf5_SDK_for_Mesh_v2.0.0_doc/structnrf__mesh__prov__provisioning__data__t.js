var structnrf__mesh__prov__provisioning__data__t =
[
    [ "netkey", "structnrf__mesh__prov__provisioning__data__t.html#a617186cb1be5c741da6382a3f2a8cc4a", null ],
    [ "netkey_index", "structnrf__mesh__prov__provisioning__data__t.html#a560d83057062e54de8b1347519558adb", null ],
    [ "iv_index", "structnrf__mesh__prov__provisioning__data__t.html#a78fbd648ad9feb41f8e5f20f2d863b5d", null ],
    [ "address", "structnrf__mesh__prov__provisioning__data__t.html#a55ef502721676073b58e865ba43bf83b", null ],
    [ "iv_update", "structnrf__mesh__prov__provisioning__data__t.html#a49bb467233a283f8e37eabbe956e2fd2", null ],
    [ "key_refresh", "structnrf__mesh__prov__provisioning__data__t.html#aebf8f61d0afd603fd4a7a113a58a483b", null ],
    [ "flags", "structnrf__mesh__prov__provisioning__data__t.html#a4536ec744f19a556eb4ec2ccd280a215", null ]
];