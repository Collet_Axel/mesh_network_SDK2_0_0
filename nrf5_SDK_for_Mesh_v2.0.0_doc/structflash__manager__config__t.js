var structflash__manager__config__t =
[
    [ "p_area", "structflash__manager__config__t.html#a90e62b01a0813bc7bec5a570defa44c4", null ],
    [ "page_count", "structflash__manager__config__t.html#a96ddda237674154c975f50fb8fe2ea6d", null ],
    [ "min_available_space", "structflash__manager__config__t.html#ab9348efaf5cb1ff2bb36cf8aa0c9451c", null ],
    [ "write_complete_cb", "structflash__manager__config__t.html#a005424f311d9c879d2937fc77eae4685", null ],
    [ "invalidate_complete_cb", "structflash__manager__config__t.html#a485f634c8cbb5167fc62fd8345aabc6a", null ],
    [ "remove_complete_cb", "structflash__manager__config__t.html#a4b145549026df53dd4c6fb393f96fe77", null ]
];