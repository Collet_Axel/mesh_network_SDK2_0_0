var group__SERIAL__PACKET =
[
    [ "serial_packet_t", "structserial__packet__t.html", [
      [ "length", "structserial__packet__t.html#acd5271df102b6a68e82e8e557c300e34", null ],
      [ "opcode", "structserial__packet__t.html#a7f5e6042d64ba090d0d20370a151230a", null ],
      [ "cmd", "structserial__packet__t.html#a6da6ce26de6c7f160e2d88034dda8720", null ],
      [ "evt", "structserial__packet__t.html#ac2238e6a33607f668b11b720be011ec2", null ],
      [ "payload", "structserial__packet__t.html#a10039b0d43589caa95f76bc60fc24b7a", null ]
    ] ],
    [ "SERIAL_PACKET_LENGTH_OVERHEAD", "group__SERIAL__PACKET.html#gaf3d9eac689ebb4591f0c12df0186afd5", null ],
    [ "SERIAL_PACKET_OVERHEAD", "group__SERIAL__PACKET.html#ga9f70c600b61737bcd66e51c73dcc9fef", null ]
];