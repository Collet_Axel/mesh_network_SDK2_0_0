var group__MESH__SERIAL =
[
    [ "Serial interface", "group__SERIAL__INTERFACE.html", "group__SERIAL__INTERFACE" ],
    [ "Serial hardware abstraction layer", "group__SERIAL__BEARER.html", "group__SERIAL__BEARER" ],
    [ "Serial commands", "group__SERIAL__CMD.html", "group__SERIAL__CMD" ],
    [ "Serial Command Response definitions", "group__SERIAL__CMD__RSP.html", "group__SERIAL__CMD__RSP" ],
    [ "Serial events", "group__SERIAL__EVT.html", "group__SERIAL__EVT" ],
    [ "Serial handlers", "group__MESH__SERIAL__HANDLER.html", "group__MESH__SERIAL__HANDLER" ],
    [ "Serial packet", "group__SERIAL__PACKET.html", "group__SERIAL__PACKET" ],
    [ "Serial status codes", "group__SERIAL__STATUS__CODES.html", null ],
    [ "Serial UART bearer", "group__SERIAL__UART.html", "group__SERIAL__UART" ]
];