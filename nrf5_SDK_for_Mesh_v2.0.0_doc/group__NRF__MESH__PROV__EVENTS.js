var group__NRF__MESH__PROV__EVENTS =
[
    [ "nrf_mesh_prov_evt_unprov_t", "structnrf__mesh__prov__evt__unprov__t.html", [
      [ "device_uuid", "structnrf__mesh__prov__evt__unprov__t.html#a9c0c6f08eebed0d9366675f72d9248c5", null ],
      [ "gatt_supported", "structnrf__mesh__prov__evt__unprov__t.html#abb06f5ace019501f30cd9c9a2e143f90", null ],
      [ "uri_hash_present", "structnrf__mesh__prov__evt__unprov__t.html#ab86fe76cdeb1c3c9772995cd54dd2931", null ],
      [ "uri_hash", "structnrf__mesh__prov__evt__unprov__t.html#a1476344f42b035a85a735390cec9e402", null ],
      [ "p_metadata", "structnrf__mesh__prov__evt__unprov__t.html#a018fe06bd70dba9b650ad503e3e3a669", null ]
    ] ],
    [ "nrf_mesh_prov_evt_link_established_t", "structnrf__mesh__prov__evt__link__established__t.html", [
      [ "p_context", "structnrf__mesh__prov__evt__link__established__t.html#abd21a67d97eee8be91104166623449fd", null ]
    ] ],
    [ "nrf_mesh_prov_evt_link_closed_t", "structnrf__mesh__prov__evt__link__closed__t.html", [
      [ "p_context", "structnrf__mesh__prov__evt__link__closed__t.html#a862b0f6bb2bebbd43c29d8878e62df07", null ],
      [ "close_reason", "structnrf__mesh__prov__evt__link__closed__t.html#afff3d1f284ce8df3a80f2198805ea235", null ]
    ] ],
    [ "nrf_mesh_prov_evt_input_request_t", "structnrf__mesh__prov__evt__input__request__t.html", [
      [ "p_context", "structnrf__mesh__prov__evt__input__request__t.html#a33e4bdca5fbb29561c06298b476342bf", null ],
      [ "action", "structnrf__mesh__prov__evt__input__request__t.html#a12997f5749a46036914f16b66d0c1ece", null ],
      [ "size", "structnrf__mesh__prov__evt__input__request__t.html#adca4c3c04a1c01ab255fc12a43d086d4", null ]
    ] ],
    [ "nrf_mesh_prov_evt_output_request_t", "structnrf__mesh__prov__evt__output__request__t.html", [
      [ "p_context", "structnrf__mesh__prov__evt__output__request__t.html#aa344ccc18489187299b8ace6dc64dc5a", null ],
      [ "action", "structnrf__mesh__prov__evt__output__request__t.html#a4df8edc183179069757a5d00c056e276", null ],
      [ "size", "structnrf__mesh__prov__evt__output__request__t.html#a510187f229177cd22e02de7493988b48", null ],
      [ "p_data", "structnrf__mesh__prov__evt__output__request__t.html#ab4d419fc4285b711c9e7d87644f2f042", null ]
    ] ],
    [ "nrf_mesh_prov_evt_static_request_t", "structnrf__mesh__prov__evt__static__request__t.html", [
      [ "p_context", "structnrf__mesh__prov__evt__static__request__t.html#aaa5b43e8d94462a4368c85ec995a5fa1", null ]
    ] ],
    [ "nrf_mesh_prov_evt_caps_received_t", "structnrf__mesh__prov__evt__caps__received__t.html", [
      [ "p_context", "structnrf__mesh__prov__evt__caps__received__t.html#a5adb0260fa915acc312e10d23e187e9b", null ],
      [ "oob_caps", "structnrf__mesh__prov__evt__caps__received__t.html#a4403f4297cad465a274e84136e86ab37", null ]
    ] ],
    [ "nrf_mesh_prov_evt_complete_t", "structnrf__mesh__prov__evt__complete__t.html", [
      [ "p_context", "structnrf__mesh__prov__evt__complete__t.html#abb9ad8c57416139a35d39ed7fe77f2fb", null ],
      [ "p_devkey", "structnrf__mesh__prov__evt__complete__t.html#ac29d844e53f9f4d67d1a5db00408add1", null ],
      [ "p_prov_data", "structnrf__mesh__prov__evt__complete__t.html#a349321dcc3b0fb72c335d215b9f1d51c", null ]
    ] ],
    [ "nrf_mesh_prov_evt_failed_t", "structnrf__mesh__prov__evt__failed__t.html", [
      [ "p_context", "structnrf__mesh__prov__evt__failed__t.html#a8fe92cd91d10094b8843bad9868db26d", null ],
      [ "failure_code", "structnrf__mesh__prov__evt__failed__t.html#a9e347411461af7b6a32e08327b5bf637", null ]
    ] ],
    [ "nrf_mesh_prov_evt_oob_pubkey_request_t", "structnrf__mesh__prov__evt__oob__pubkey__request__t.html", [
      [ "p_context", "structnrf__mesh__prov__evt__oob__pubkey__request__t.html#a0205059b6e80865c9cf72fae0943499b", null ]
    ] ],
    [ "nrf_mesh_prov_evt_ecdh_request_t", "structnrf__mesh__prov__evt__ecdh__request__t.html", [
      [ "p_context", "structnrf__mesh__prov__evt__ecdh__request__t.html#a93f2ec06fa3442b947000efd37ada027", null ],
      [ "p_peer_public", "structnrf__mesh__prov__evt__ecdh__request__t.html#a4346a83b083a1b6801d2590c10756ad9", null ],
      [ "p_node_private", "structnrf__mesh__prov__evt__ecdh__request__t.html#aca1cb8f0d4fb018fde166dbbd042346a", null ]
    ] ],
    [ "nrf_mesh_prov_evt_t", "structnrf__mesh__prov__evt__t.html", [
      [ "type", "structnrf__mesh__prov__evt__t.html#afb614cd2922c5bc62189fa7e7f0af0e9", null ],
      [ "unprov", "structnrf__mesh__prov__evt__t.html#ab9b39ee91ceafa3ce95eab32ead358d4", null ],
      [ "link_established", "structnrf__mesh__prov__evt__t.html#ae90763eecc934e4ce59eada7fd073033", null ],
      [ "link_closed", "structnrf__mesh__prov__evt__t.html#a9311d565bbc4db0d9469e90bceb920cc", null ],
      [ "input_request", "structnrf__mesh__prov__evt__t.html#a85abeb5effacdd3ba49ba7f3c1aae69e", null ],
      [ "output_request", "structnrf__mesh__prov__evt__t.html#af47e9821f42544d71472bf9df22f217e", null ],
      [ "static_request", "structnrf__mesh__prov__evt__t.html#ae21fd3adad69a255c361ece595743c9a", null ],
      [ "oob_pubkey_request", "structnrf__mesh__prov__evt__t.html#afeb83a10c33a054e78bf0a90780be9ed", null ],
      [ "oob_caps_received", "structnrf__mesh__prov__evt__t.html#af515e47388430b8ac37cd99491075214", null ],
      [ "complete", "structnrf__mesh__prov__evt__t.html#aa08f224f50d4890359ca5f052415f56d", null ],
      [ "ecdh_request", "structnrf__mesh__prov__evt__t.html#a6d083bcbdd58b03c57c5446f43e9d93b", null ],
      [ "failed", "structnrf__mesh__prov__evt__t.html#a769ad3681fcb02c1ae32b239f5ebcdd0", null ],
      [ "params", "structnrf__mesh__prov__evt__t.html#aae1cbaa0382a0714e5284d29ee6c4e44", null ]
    ] ],
    [ "nrf_mesh_prov_evt_handler_cb_t", "group__NRF__MESH__PROV__EVENTS.html#ga2eb950596daa42f19bfdc3ed9d097c31", null ],
    [ "nrf_mesh_prov_evt_type_t", "group__NRF__MESH__PROV__EVENTS.html#gabc4b4b8d791bc04b9a42432fc00dce42", [
      [ "NRF_MESH_PROV_EVT_UNPROVISIONED_RECEIVED", "group__NRF__MESH__PROV__EVENTS.html#ggabc4b4b8d791bc04b9a42432fc00dce42a7d3804f58f35a963595845c769d8b64f", null ],
      [ "NRF_MESH_PROV_EVT_LINK_ESTABLISHED", "group__NRF__MESH__PROV__EVENTS.html#ggabc4b4b8d791bc04b9a42432fc00dce42a4cc5f57999ed9719dafa7ea1b31ab39c", null ],
      [ "NRF_MESH_PROV_EVT_LINK_CLOSED", "group__NRF__MESH__PROV__EVENTS.html#ggabc4b4b8d791bc04b9a42432fc00dce42af0bccc0166eba0fab54a45aa116c42ef", null ],
      [ "NRF_MESH_PROV_EVT_OUTPUT_REQUEST", "group__NRF__MESH__PROV__EVENTS.html#ggabc4b4b8d791bc04b9a42432fc00dce42a55f116154789567af9a413d84e87064d", null ],
      [ "NRF_MESH_PROV_EVT_INPUT_REQUEST", "group__NRF__MESH__PROV__EVENTS.html#ggabc4b4b8d791bc04b9a42432fc00dce42abab5ac1f80793c709669936af1c7733f", null ],
      [ "NRF_MESH_PROV_EVT_STATIC_REQUEST", "group__NRF__MESH__PROV__EVENTS.html#ggabc4b4b8d791bc04b9a42432fc00dce42aa1b51efe6417ee21ce64c41d517255d9", null ],
      [ "NRF_MESH_PROV_EVT_OOB_PUBKEY_REQUEST", "group__NRF__MESH__PROV__EVENTS.html#ggabc4b4b8d791bc04b9a42432fc00dce42abfe9d21d65168dbbd1aa4a8d988b1910", null ],
      [ "NRF_MESH_PROV_EVT_CAPS_RECEIVED", "group__NRF__MESH__PROV__EVENTS.html#ggabc4b4b8d791bc04b9a42432fc00dce42a075cff7602e772dfa94c5cfd7ed050d8", null ],
      [ "NRF_MESH_PROV_EVT_COMPLETE", "group__NRF__MESH__PROV__EVENTS.html#ggabc4b4b8d791bc04b9a42432fc00dce42abb803fe886321e9d441fa3f278290c8e", null ],
      [ "NRF_MESH_PROV_EVT_ECDH_REQUEST", "group__NRF__MESH__PROV__EVENTS.html#ggabc4b4b8d791bc04b9a42432fc00dce42accfda8b3a333cbd3bf4570c32587f0f2", null ],
      [ "NRF_MESH_PROV_EVT_FAILED", "group__NRF__MESH__PROV__EVENTS.html#ggabc4b4b8d791bc04b9a42432fc00dce42a8e5209b542964c511fc3b4e900d87898", null ]
    ] ]
];