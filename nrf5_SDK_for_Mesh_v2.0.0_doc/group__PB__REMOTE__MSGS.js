var group__PB__REMOTE__MSGS =
[
    [ "pb_remote_msg_scan_start_filter_t", "structpb__remote__msg__scan__start__filter__t.html", [
      [ "filter_uuid", "structpb__remote__msg__scan__start__filter__t.html#a7903826968174032a262e255b965981f", null ]
    ] ],
    [ "pb_remote_msg_scan_unprov_device_number_t", "structpb__remote__msg__scan__unprov__device__number__t.html", [
      [ "report_count", "structpb__remote__msg__scan__unprov__device__number__t.html#a8936dc339c8ca3eb5ad508773fab1858", null ]
    ] ],
    [ "pb_remote_msg_scan_unprov_device_number_report_t", "structpb__remote__msg__scan__unprov__device__number__report__t.html", [
      [ "unprov_device_count", "structpb__remote__msg__scan__unprov__device__number__report__t.html#a96422825454e4f56aaf5e5fe9470f774", null ]
    ] ],
    [ "pb_remote_msg_scan_uuid_report_t", "structpb__remote__msg__scan__uuid__report__t.html", [
      [ "uuid", "structpb__remote__msg__scan__uuid__report__t.html#a4f6932d12b75dbf7707c230061670e99", null ],
      [ "unprov_device_id", "structpb__remote__msg__scan__uuid__report__t.html#a96d4e21e84fd97565fe97f858a2a3701", null ]
    ] ],
    [ "pb_remote_msg_scan_report_status_t", "structpb__remote__msg__scan__report__status__t.html", [
      [ "status", "structpb__remote__msg__scan__report__status__t.html#a57ba2a990a451b6abb21f84552d0aaed", null ],
      [ "unprov_device_id", "structpb__remote__msg__scan__report__status__t.html#add25d64f4c9537db86ec638fe7dfbecb", null ]
    ] ],
    [ "pb_remote_msg_scan_status_t", "structpb__remote__msg__scan__status__t.html", [
      [ "status", "structpb__remote__msg__scan__status__t.html#abb2dcd601dfa53988c86e4788b9b69b9", null ]
    ] ],
    [ "pb_remote_msg_scan_stopped_t", "structpb__remote__msg__scan__stopped__t.html", [
      [ "status", "structpb__remote__msg__scan__stopped__t.html#a8ca8928c07714ea4d2e942662277ea07", null ]
    ] ],
    [ "pb_remote_msg_link_open_t", "structpb__remote__msg__link__open__t.html", [
      [ "uuid", "structpb__remote__msg__link__open__t.html#a045e0c92ed8effe0e744fb1562f5be8b", null ]
    ] ],
    [ "pb_remote_msg_link_status_t", "structpb__remote__msg__link__status__t.html", [
      [ "status", "structpb__remote__msg__link__status__t.html#a3ac031001402c65c47f727e14fb772cd", null ],
      [ "bearer_type", "structpb__remote__msg__link__status__t.html#a7e70000a75b5845036a305d9e56085a7", null ]
    ] ],
    [ "pb_remote_msg_link_close_t", "structpb__remote__msg__link__close__t.html", [
      [ "reason", "structpb__remote__msg__link__close__t.html#aebfe9c11f3c7f5d31e61231078d6a31b", null ]
    ] ],
    [ "pb_remote_msg_link_status_report_t", "structpb__remote__msg__link__status__report__t.html", [
      [ "status", "structpb__remote__msg__link__status__report__t.html#a1ab4b11c14afd0e34bfe123b41f6568e", null ],
      [ "reason", "structpb__remote__msg__link__status__report__t.html#a2cd359049a3264520bf7c976e3da2c7d", null ]
    ] ],
    [ "pb_remote_msg_packet_transfer_t", "structpb__remote__msg__packet__transfer__t.html", [
      [ "buffer", "structpb__remote__msg__packet__transfer__t.html#a862fd229af747012b44ad36d6244427b", null ]
    ] ],
    [ "pb_remote_msg_packet_transfer_report_t", "structpb__remote__msg__packet__transfer__report__t.html", [
      [ "status", "structpb__remote__msg__packet__transfer__report__t.html#af386189ca6e952ef7b9b83d0cad2e95b", null ]
    ] ],
    [ "pb_remote_msg_packet_transfer_status_t", "structpb__remote__msg__packet__transfer__status__t.html", [
      [ "status", "structpb__remote__msg__packet__transfer__status__t.html#a944fb8ff910e10fd598bb5d01832352d", null ]
    ] ],
    [ "BEARER_LINK_REASON_NOT_SUPPORTED", "group__PB__REMOTE__MSGS.html#gade4d7991635d43873c8d1cf4b665c31a", null ],
    [ "pb_remote_opcode_t", "group__PB__REMOTE__MSGS.html#ga54eb9d3ee6593b94f8b064e1b3f49144", [
      [ "PB_REMOTE_OP_PACKET_TRANSFER_REPORT", "group__PB__REMOTE__MSGS.html#gga54eb9d3ee6593b94f8b064e1b3f49144ad93f8f5a168713c13815e12d5a28878e", null ],
      [ "PB_REMOTE_OP_PACKET_TRANSFER", "group__PB__REMOTE__MSGS.html#gga54eb9d3ee6593b94f8b064e1b3f49144ab4260a15faa8c1160c75c4087e9224aa", null ],
      [ "PB_REMOTE_OP_PACKET_TRANSFER_STATUS", "group__PB__REMOTE__MSGS.html#gga54eb9d3ee6593b94f8b064e1b3f49144ac04b87f053abea9f31ef1c24b6505f49", null ],
      [ "PB_REMOTE_OP_LINK_CLOSE", "group__PB__REMOTE__MSGS.html#gga54eb9d3ee6593b94f8b064e1b3f49144ade4149eb11772619801bf4d3234e138c", null ],
      [ "PB_REMOTE_OP_LINK_OPEN", "group__PB__REMOTE__MSGS.html#gga54eb9d3ee6593b94f8b064e1b3f49144a7578ef7ec0242cf869b6971f7769631a", null ],
      [ "PB_REMOTE_OP_LINK_STATUS", "group__PB__REMOTE__MSGS.html#gga54eb9d3ee6593b94f8b064e1b3f49144a7a94243c90bac006580699bf0f0513f5", null ],
      [ "PB_REMOTE_OP_SCAN_CANCEL", "group__PB__REMOTE__MSGS.html#gga54eb9d3ee6593b94f8b064e1b3f49144a0e3209345da2b8f2a3f13832e7fccc5b", null ],
      [ "PB_REMOTE_OP_SCAN_START", "group__PB__REMOTE__MSGS.html#gga54eb9d3ee6593b94f8b064e1b3f49144acbbea600e9f27617c0ee2bd725dcf0d2", null ],
      [ "PB_REMOTE_OP_SCAN_START_FILTER", "group__PB__REMOTE__MSGS.html#gga54eb9d3ee6593b94f8b064e1b3f49144a75f63d78cb9ee438aa5597cd8091c6c4", null ],
      [ "PB_REMOTE_OP_SCAN_STATUS", "group__PB__REMOTE__MSGS.html#gga54eb9d3ee6593b94f8b064e1b3f49144a3411bc0c01e1b59bebbdd46eb9531bc1", null ],
      [ "PB_REMOTE_OP_SCAN_UNPROVISIONED_DEVICE_NUMBER", "group__PB__REMOTE__MSGS.html#gga54eb9d3ee6593b94f8b064e1b3f49144ad3125b2025ddf457742533b89363c8d9", null ],
      [ "PB_REMOTE_OP_SCAN_UUID_REPORT", "group__PB__REMOTE__MSGS.html#gga54eb9d3ee6593b94f8b064e1b3f49144a17a544d75ff66d4bcc8305f48838afe8", null ],
      [ "PB_REMOTE_OP_SCAN_UUID_NUMBER_REPORT", "group__PB__REMOTE__MSGS.html#gga54eb9d3ee6593b94f8b064e1b3f49144ac1f4a15f4430aa1ffd59217885207040", null ],
      [ "PB_REMOTE_OP_LINK_STATUS_REPORT", "group__PB__REMOTE__MSGS.html#gga54eb9d3ee6593b94f8b064e1b3f49144a0dc18edd0a4c133ba05014eb3a5a2bf2", null ],
      [ "PB_REMOTE_OP_SCAN_REPORT_STATUS", "group__PB__REMOTE__MSGS.html#gga54eb9d3ee6593b94f8b064e1b3f49144a02f0f53fe7e757500b64236d9a4315f0", null ],
      [ "PB_REMOTE_OP_SCAN_STOPPED", "group__PB__REMOTE__MSGS.html#gga54eb9d3ee6593b94f8b064e1b3f49144ade9b12c4694cfb64da3f2f0c6134065e", null ]
    ] ],
    [ "pb_remote_report_status_t", "group__PB__REMOTE__MSGS.html#gafa21fdce5648580873c6d97e8a107f6e", [
      [ "PB_REMOTE_REPORT_STATUS_ACCEPTED", "group__PB__REMOTE__MSGS.html#ggafa21fdce5648580873c6d97e8a107f6eaa67c5ff81f82d8f3ace18965723d375d", null ],
      [ "PB_REMOTE_REPORT_STATUS_REJECTED", "group__PB__REMOTE__MSGS.html#ggafa21fdce5648580873c6d97e8a107f6ead35cfb4acb4148a65f6f882fb9437373", null ]
    ] ],
    [ "pb_remote_scan_status_t", "group__PB__REMOTE__MSGS.html#ga092d4d9a268b7ef2c43b43d7d15a608d", [
      [ "PB_REMOTE_SCAN_STATUS_STARTED", "group__PB__REMOTE__MSGS.html#gga092d4d9a268b7ef2c43b43d7d15a608dab9960aafb2e022971351bdc28e94d65e", null ],
      [ "PB_REMOTE_SCAN_STATUS_CANCELED", "group__PB__REMOTE__MSGS.html#gga092d4d9a268b7ef2c43b43d7d15a608da7f16a18fcaf292e1497406df94afd4b9", null ],
      [ "PB_REMOTE_SCAN_STATUS_CANNOT_START_SCANNING", "group__PB__REMOTE__MSGS.html#gga092d4d9a268b7ef2c43b43d7d15a608daed997f5ab26b01ee50a7a65517d33489", null ],
      [ "PB_REMOTE_SCAN_STATUS_CANNOT_CANCEL_SCANNING", "group__PB__REMOTE__MSGS.html#gga092d4d9a268b7ef2c43b43d7d15a608da7a095e74262236cee0ca188bae9d20dc", null ],
      [ "PB_REMOTE_SCAN_STATUS_ACCEPTED", "group__PB__REMOTE__MSGS.html#gga092d4d9a268b7ef2c43b43d7d15a608da1b57cfecdb89a13b8725d1f4bca83322", null ],
      [ "PB_REMOTE_SCAN_STATUS_REJECTED", "group__PB__REMOTE__MSGS.html#gga092d4d9a268b7ef2c43b43d7d15a608daf2db436de76da4f37e916faeb77902a1", null ]
    ] ],
    [ "pb_remote_scan_stopped_status_t", "group__PB__REMOTE__MSGS.html#ga84a894238fad438a5d0e66b956c0406b", [
      [ "PB_REMOTE_SCAN_STOPPED_OUT_OF_RESOURCES", "group__PB__REMOTE__MSGS.html#gga84a894238fad438a5d0e66b956c0406ba82ba1dc54bf6d71aece5187d2cca27aa", null ],
      [ "PB_REMOTE_SCAN_STOPPED_TIMEOUT", "group__PB__REMOTE__MSGS.html#gga84a894238fad438a5d0e66b956c0406bac8cbd573c111ab6cf21b98c8f214c345", null ]
    ] ],
    [ "pb_remote_link_status_t", "group__PB__REMOTE__MSGS.html#gab57e57078ca0bba52e69e6b48abcc6f5", [
      [ "PB_REMOTE_REMOTE_LINK_STATUS_OPENING", "group__PB__REMOTE__MSGS.html#ggab57e57078ca0bba52e69e6b48abcc6f5aa0e294fa39bec3ba628941648a48ec68", null ],
      [ "PB_REMOTE_REMOTE_LINK_STATUS_ALREADY_OPEN", "group__PB__REMOTE__MSGS.html#ggab57e57078ca0bba52e69e6b48abcc6f5a273555867e1fdaf094c00000c9c46655", null ],
      [ "PB_REMOTE_REMOTE_LINK_STATUS_CANNOT_CLOSE", "group__PB__REMOTE__MSGS.html#ggab57e57078ca0bba52e69e6b48abcc6f5aa24302d6e5deb5799210eecde557a0dd", null ],
      [ "PB_REMOTE_REMOTE_LINK_STATUS_LINK_NOT_ACTIVE", "group__PB__REMOTE__MSGS.html#ggab57e57078ca0bba52e69e6b48abcc6f5adb4e2bb4e2864dafc32562145b304276", null ],
      [ "PB_REMOTE_REMOTE_LINK_STATUS_INVALID_UNPROV_DEVICE_ID", "group__PB__REMOTE__MSGS.html#ggab57e57078ca0bba52e69e6b48abcc6f5a6a0f0ada3e02973182cb849a7d1f83d8", null ],
      [ "PB_REMOTE_REMOTE_LINK_STATUS_ACCEPTED", "group__PB__REMOTE__MSGS.html#ggab57e57078ca0bba52e69e6b48abcc6f5a051e8ccab9d24dac3db80e28db2c1fab", null ],
      [ "PB_REMOTE_REMOTE_LINK_STATUS_REJECTED", "group__PB__REMOTE__MSGS.html#ggab57e57078ca0bba52e69e6b48abcc6f5a054846472b70efbc1355b6368007d97e", null ]
    ] ],
    [ "pb_remote_bearer_type_t", "group__PB__REMOTE__MSGS.html#gac5a7bc70b73b460329a5475633df3ccf", [
      [ "PB_REMOTE_BEARER_TYPE_PB_ADV", "group__PB__REMOTE__MSGS.html#ggac5a7bc70b73b460329a5475633df3ccfa1d513cc576e9d6c01d9d5bbabd2542fa", null ],
      [ "PB_REMOTE_BEARER_TYPE_PB_GATT", "group__PB__REMOTE__MSGS.html#ggac5a7bc70b73b460329a5475633df3ccfa3e949f139a84ded085f264a48c886550", null ]
    ] ],
    [ "pb_remote_link_status_report_t", "group__PB__REMOTE__MSGS.html#gadd2fd445bb217ac58d9b0cc81de1bc2a", [
      [ "PB_REMOTE_LINK_STATUS_REPORT_OPENED", "group__PB__REMOTE__MSGS.html#ggadd2fd445bb217ac58d9b0cc81de1bc2aa843c9c29f40481b25232c3a899ffddb4", null ],
      [ "PB_REMOTE_LINK_STATUS_REPORT_OPEN_TIMEOUT", "group__PB__REMOTE__MSGS.html#ggadd2fd445bb217ac58d9b0cc81de1bc2aa8a68045c00514d1c2e96f46b9d595975", null ],
      [ "PB_REMOTE_LINK_STATUS_REPORT_CLOSED", "group__PB__REMOTE__MSGS.html#ggadd2fd445bb217ac58d9b0cc81de1bc2aa66e468601af6f5d5182fe9b828211e30", null ],
      [ "PB_REMOTE_LINK_STATUS_REPORT_CLOSED_BY_DEVICE", "group__PB__REMOTE__MSGS.html#ggadd2fd445bb217ac58d9b0cc81de1bc2aa56f4659193422a0d9915bd1ebab40dae", null ],
      [ "PB_REMOTE_LINK_STATUS_REPORT_CLOSED_BY_SERVER", "group__PB__REMOTE__MSGS.html#ggadd2fd445bb217ac58d9b0cc81de1bc2aab795ee1b0ab890ea1938e64293068217", null ]
    ] ],
    [ "pb_remote_packet_transfer_delivery_status_t", "group__PB__REMOTE__MSGS.html#gafbe4103ddfdc3ca7730d0af0253d861f", [
      [ "PB_REMOTE_PACKET_TRANSFER_DELIVERY_STATUS_DELIVERED", "group__PB__REMOTE__MSGS.html#ggafbe4103ddfdc3ca7730d0af0253d861fa078a53cfe02f93400349e1ee84b31b3a", null ],
      [ "PB_REMOTE_PACKET_TRANSFER_DELIVERY_STATUS_NOT_DELIVERED", "group__PB__REMOTE__MSGS.html#ggafbe4103ddfdc3ca7730d0af0253d861fa615b2b0eaf66a7f98cf3bb12483a9a4b", null ]
    ] ],
    [ "pb_remote_packet_transfer_status_t", "group__PB__REMOTE__MSGS.html#ga250abf44318a85782de9595c4be823a4", [
      [ "PB_REMOTE_PACKET_TRANSFER_STATUS_BUFFER_ACCEPTED", "group__PB__REMOTE__MSGS.html#gga250abf44318a85782de9595c4be823a4a67551e4c5675731d7d1c29e4036b6634", null ],
      [ "PB_REMOTE_PACKET_TRANSFER_STATUS_LINK_NOT_ACTIVE", "group__PB__REMOTE__MSGS.html#gga250abf44318a85782de9595c4be823a4a81307aa4741bf9c0914ec2b1e78951a3", null ],
      [ "PB_REMOTE_PACKET_TRANSFER_STATUS_CANNOT_ACCEPT_BUFFER", "group__PB__REMOTE__MSGS.html#gga250abf44318a85782de9595c4be823a4a1f6f9c22ecde0bf6d0e3ce58fc2b18cc", null ],
      [ "PB_REMOTE_PACKET_TRANSFER_STATUS_ACCEPTED", "group__PB__REMOTE__MSGS.html#gga250abf44318a85782de9595c4be823a4a6db044a70576bf9b699facac9be8e552", null ],
      [ "PB_REMOTE_PACKET_TRANSFER_STATUS_REJECTED", "group__PB__REMOTE__MSGS.html#gga250abf44318a85782de9595c4be823a4a8b908b5af94cfd9195ef5829071885d0", null ]
    ] ]
];