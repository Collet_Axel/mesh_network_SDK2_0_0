var structnrf__mesh__prov__bearer__gatt__t =
[
    [ "fsm", "structnrf__mesh__prov__bearer__gatt__t.html#a7978c58d1d2fb18f387abbec5ac356f3", null ],
    [ "conn_index", "structnrf__mesh__prov__bearer__gatt__t.html#a3af4108ab8370b054de41f3a256e2394", null ],
    [ "link_timeout_event", "structnrf__mesh__prov__bearer__gatt__t.html#af90c921964c62cd3de0c8923109356f4", null ],
    [ "link_timeout_us", "structnrf__mesh__prov__bearer__gatt__t.html#add1c39e2736a283411491205960bff82", null ],
    [ "bearer_event_seq", "structnrf__mesh__prov__bearer__gatt__t.html#a71b049cb08e8d08544f1899bfe4930e1", null ],
    [ "bearer", "structnrf__mesh__prov__bearer__gatt__t.html#a98f98e1efef3464e3528a6a1d081b7a9", null ]
];