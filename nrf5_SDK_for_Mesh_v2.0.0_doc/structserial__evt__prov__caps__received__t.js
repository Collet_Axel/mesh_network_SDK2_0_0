var structserial__evt__prov__caps__received__t =
[
    [ "context_id", "structserial__evt__prov__caps__received__t.html#ac1a2ae1ad39eae15eb545e4c26e6fa29", null ],
    [ "num_elements", "structserial__evt__prov__caps__received__t.html#adf054f4c438acffa9b1093412fe9e701", null ],
    [ "public_key_type", "structserial__evt__prov__caps__received__t.html#acbfd223db55d3aeac3f6251095a8662a", null ],
    [ "static_oob_types", "structserial__evt__prov__caps__received__t.html#abee63dbf4386213a5bc0e6b7a1e61d94", null ],
    [ "output_oob_size", "structserial__evt__prov__caps__received__t.html#ab1e831193b90f95a2f6ce25f0a039e8a", null ],
    [ "output_oob_actions", "structserial__evt__prov__caps__received__t.html#a5070a0c0b4cb1409ee5b00771551d216", null ],
    [ "input_oob_size", "structserial__evt__prov__caps__received__t.html#a8c8d6010657a8d9d50fd3997779c5eff", null ],
    [ "input_oob_actions", "structserial__evt__prov__caps__received__t.html#a718f5655dbecf675888f93f7c2ac9909", null ]
];