var structconfig__msg__heartbeat__publication__status__t =
[
    [ "status", "structconfig__msg__heartbeat__publication__status__t.html#a4bd3290bd2e433789b41b75cd93cba25", null ],
    [ "destination", "structconfig__msg__heartbeat__publication__status__t.html#adeaba556956d585cd6e8d9189ec56003", null ],
    [ "count_log", "structconfig__msg__heartbeat__publication__status__t.html#a5cbf079a0ddaf245237d5776a05eec96", null ],
    [ "period_log", "structconfig__msg__heartbeat__publication__status__t.html#a49995cfb02c405ada1af460651debc89", null ],
    [ "ttl", "structconfig__msg__heartbeat__publication__status__t.html#acb8c1c6ee025a559b129f6b521b3c7e8", null ],
    [ "features", "structconfig__msg__heartbeat__publication__status__t.html#a15600d712ed130a56beccbbe3adb96d4", null ],
    [ "netkey_index", "structconfig__msg__heartbeat__publication__status__t.html#ad5929d123f3d8b2e2a581bc04962508c", null ]
];