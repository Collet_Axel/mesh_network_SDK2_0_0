var structserial__cmd__device__beacon__params__set__t =
[
    [ "beacon_slot", "structserial__cmd__device__beacon__params__set__t.html#ab4ea513b62b2069b095cf4bb66b4c5bf", null ],
    [ "tx_power", "structserial__cmd__device__beacon__params__set__t.html#ae794fdaac9a4222b32f592fe54e69a77", null ],
    [ "channel_map", "structserial__cmd__device__beacon__params__set__t.html#ae2a75fe0014a0236a51969063e0a6735", null ],
    [ "interval_ms", "structserial__cmd__device__beacon__params__set__t.html#ae3965749f7206752a5a81e958ce36b54", null ]
];