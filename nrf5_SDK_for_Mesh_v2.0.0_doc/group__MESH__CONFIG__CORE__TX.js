var group__MESH__CONFIG__CORE__TX =
[
    [ "CORE_TX_QUEUE_BUFFER_SIZE_ORIGINATOR", "group__MESH__CONFIG__CORE__TX.html#ga59be7d756496054ecdaab00902f0cdcd", null ],
    [ "CORE_TX_QUEUE_BUFFER_SIZE_RELAY", "group__MESH__CONFIG__CORE__TX.html#ga122ba6931334f13ed885f840de322af6", null ],
    [ "CORE_TX_QUEUE_BUFFER_SIZE_INSTABURST_ORIGINATOR", "group__MESH__CONFIG__CORE__TX.html#ga2fc28aaeb365474ad3ca9705d84baff8", null ],
    [ "CORE_TX_QUEUE_BUFFER_SIZE_INSTABURST_RELAY", "group__MESH__CONFIG__CORE__TX.html#ga94ac780d5822d35987b7c6f193128b7c", null ],
    [ "CORE_TX_INSTABURST_CHANNELS", "group__MESH__CONFIG__CORE__TX.html#gaa0984c43945e7a005f7bdf7ec3bceedf", null ],
    [ "CORE_TX_REPEAT_ORIGINATOR_DEFAULT", "group__MESH__CONFIG__CORE__TX.html#gaf7b922dd220d2331d569d28be456d90e", null ],
    [ "CORE_TX_REPEAT_RELAY_DEFAULT", "group__MESH__CONFIG__CORE__TX.html#ga5a6d56a328798794f4491b6904917e32", null ]
];