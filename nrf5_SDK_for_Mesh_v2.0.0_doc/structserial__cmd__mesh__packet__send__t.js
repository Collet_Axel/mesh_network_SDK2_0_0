var structserial__cmd__mesh__packet__send__t =
[
    [ "appkey_handle", "structserial__cmd__mesh__packet__send__t.html#a5b5568ed1b4f3f1bf14d065b23d8b5c6", null ],
    [ "src_addr", "structserial__cmd__mesh__packet__send__t.html#a4deafdd6f16c6b76f085e8ae82eb5d97", null ],
    [ "dst_addr_handle", "structserial__cmd__mesh__packet__send__t.html#a0352b0f4cb5844dd3e397a46cd7769af", null ],
    [ "ttl", "structserial__cmd__mesh__packet__send__t.html#a58f70678d2c96e4fe5c34dddb48848b1", null ],
    [ "force_segmented", "structserial__cmd__mesh__packet__send__t.html#a4d75ac9566d25c2c5bc48ceb8acee07f", null ],
    [ "transmic_size", "structserial__cmd__mesh__packet__send__t.html#a4949999bf117fa5d708f39c9e6040ca9", null ],
    [ "data", "structserial__cmd__mesh__packet__send__t.html#ab15796d6444ee4aa628d48385841d616", null ]
];