var group__MESH__API__GROUP__APP__CONFIG =
[
    [ "Device configuration", "group__DEVICE__CONFIG.html", "group__DEVICE__CONFIG" ],
    [ "Access layer configuration", "group__APP__ACCESS__CONFIG.html", "group__APP__ACCESS__CONFIG" ],
    [ "Device State Manager configuration", "group__DSM__CONFIG.html", "group__DSM__CONFIG" ]
];