var group__SERIAL__UART =
[
    [ "serial_uart_rx_cb_t", "group__SERIAL__UART.html#ga5709a3fcece4befe8cebef37759d2db0", null ],
    [ "serial_uart_tx_cb_t", "group__SERIAL__UART.html#ga1d236568ff3f231bf85b94786b8591ba", null ],
    [ "serial_uart_init", "group__SERIAL__UART.html#gaf66090170059248eefe7bc8268607eb0", null ],
    [ "serial_uart_process", "group__SERIAL__UART.html#gaf5deb5a942bbeef2dcbf5974bfa38721", null ],
    [ "serial_uart_receive_set", "group__SERIAL__UART.html#gaea8adce775d55bc9455646f28fd4a538", null ],
    [ "serial_uart_tx_start", "group__SERIAL__UART.html#gabce8847aa1577aefd234b8686af054a1", null ],
    [ "serial_uart_tx_stop", "group__SERIAL__UART.html#ga89d5bbe5c9ecb1026e047b4a6b48ffc9", null ],
    [ "serial_uart_byte_send", "group__SERIAL__UART.html#ga805d9d344bf99056473ddebb882fdc84", null ]
];