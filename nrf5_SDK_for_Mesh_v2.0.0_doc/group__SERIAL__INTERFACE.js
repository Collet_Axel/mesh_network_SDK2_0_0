var group__SERIAL__INTERFACE =
[
    [ "SERIAL_API_VERSION", "group__SERIAL__INTERFACE.html#ga09f996530d2ddbae3732aaaacbdd48e9", null ],
    [ "serial_init", "group__SERIAL__INTERFACE.html#gaae4c6d3dab71e809d6a740eb8a71264b", null ],
    [ "serial_start", "group__SERIAL__INTERFACE.html#ga28df1566f7d0b1b3c4eef157b70bee8e", null ],
    [ "serial_packet_buffer_get", "group__SERIAL__INTERFACE.html#gaddc3c2d70f1255084dc4b44f2373325d", null ],
    [ "serial_tx", "group__SERIAL__INTERFACE.html#ga38b25e5609a9bbbfc1f295116052a9bd", null ],
    [ "serial_process", "group__SERIAL__INTERFACE.html#gaaa2cb5f14414bf4895ba89c3a09b894e", null ],
    [ "serial_translate_error", "group__SERIAL__INTERFACE.html#ga3518b4aee6b4121b84aaf5167e52ebe5", null ],
    [ "serial_state_get", "group__SERIAL__INTERFACE.html#ga9601b6e465e403e2256598c037d54318", null ],
    [ "serial_cmd_rsp_send", "group__SERIAL__INTERFACE.html#gadf6ccc74f009d7e8c4238c6a7f51f30c", null ]
];