var group__HEALTH__CLIENT =
[
    [ "health_client_evt_fault_status_t", "structhealth__client__evt__fault__status__t.html", [
      [ "test_id", "structhealth__client__evt__fault__status__t.html#ae00b6a5dd9d99a3686c522345b35b6da", null ],
      [ "company_id", "structhealth__client__evt__fault__status__t.html#ab60a0c0693f70c8288802ec404cb01ac", null ],
      [ "fault_array_length", "structhealth__client__evt__fault__status__t.html#a892167a2c4130383ee691473763be48b", null ],
      [ "p_fault_array", "structhealth__client__evt__fault__status__t.html#ad79fd0d4470ca6bf37f3b737ba1f9c17", null ],
      [ "p_meta_data", "structhealth__client__evt__fault__status__t.html#a65c0953d157ced8f42b5a09c8b37c7a8", null ]
    ] ],
    [ "health_client_evt_period_status_t", "structhealth__client__evt__period__status__t.html", [
      [ "fast_period_divisor", "structhealth__client__evt__period__status__t.html#a66d0f85c6b3ce4f1f10eff3b2720e95d", null ]
    ] ],
    [ "health_client_evt_attention_status_t", "structhealth__client__evt__attention__status__t.html", [
      [ "attention", "structhealth__client__evt__attention__status__t.html#a646ea48bb2e1489f3bb6fb4fec691d3f", null ]
    ] ],
    [ "health_client_evt_t", "structhealth__client__evt__t.html", [
      [ "type", "structhealth__client__evt__t.html#af2fdd269f87fa4d577591f87d3153ad5", null ],
      [ "p_meta_data", "structhealth__client__evt__t.html#aedf93dad48e0fc10ae5b7fc0b5f9cfd2", null ],
      [ "fault_status", "structhealth__client__evt__t.html#abc76a4a5a0e8f5b65b4d3c6f898a5956", null ],
      [ "period_status", "structhealth__client__evt__t.html#a547e514b0013728804b96e555dca0304", null ],
      [ "attention_status", "structhealth__client__evt__t.html#add545982f34c7f2093be2d61bc084520", null ],
      [ "data", "structhealth__client__evt__t.html#ac3c493cc1518116da2ccff07828b0aa3", null ]
    ] ],
    [ "health_client_t", "struct____health__client__t.html", [
      [ "model_handle", "struct____health__client__t.html#afd0f3b150ef861e59da182e568b7b301", null ],
      [ "event_handler", "struct____health__client__t.html#a73b6d6494df359577817bd77ec395487", null ],
      [ "waiting_for_reply", "struct____health__client__t.html#a3ea2fb27a8b682065b64d3ad025fa22e", null ],
      [ "p_buffer", "struct____health__client__t.html#a7a1c91d723277e42793ae437aae065aa", null ]
    ] ],
    [ "health_client_evt_cb_t", "group__HEALTH__CLIENT.html#ga87a9364610cbdce2d58aa7c6c828c7a0", null ],
    [ "health_client_evt_type_t", "group__HEALTH__CLIENT.html#ga9094ea3c85203bc10be0a29ba132760f", [
      [ "HEALTH_CLIENT_EVT_TYPE_CURRENT_STATUS_RECEIVED", "group__HEALTH__CLIENT.html#gga9094ea3c85203bc10be0a29ba132760faa09300c60efccdeb140ff41db4528d61", null ],
      [ "HEALTH_CLIENT_EVT_TYPE_FAULT_STATUS_RECEIVED", "group__HEALTH__CLIENT.html#gga9094ea3c85203bc10be0a29ba132760fa3804e1f8fabd6ed16ab2b09cba073adb", null ],
      [ "HEALTH_CLIENT_EVT_TYPE_PERIOD_STATUS_RECEIVED", "group__HEALTH__CLIENT.html#gga9094ea3c85203bc10be0a29ba132760fa75db38a2b5b500ad9880603f61d183c5", null ],
      [ "HEALTH_CLIENT_EVT_TYPE_ATTENTION_STATUS_RECEIVED", "group__HEALTH__CLIENT.html#gga9094ea3c85203bc10be0a29ba132760faabef854997e14dd48da7cb4da88ba5b0", null ],
      [ "HEALTH_CLIENT_EVT_TYPE_TIMEOUT", "group__HEALTH__CLIENT.html#gga9094ea3c85203bc10be0a29ba132760fae0f6184216c03585b874eac211f27b78", null ],
      [ "HEALTH_CLIENT_EVT_TYPE_CANCELLED", "group__HEALTH__CLIENT.html#gga9094ea3c85203bc10be0a29ba132760fa884bc5b81bf7b7e68ecf2a43c778d6c3", null ]
    ] ],
    [ "health_client_fault_get", "group__HEALTH__CLIENT.html#gae0c311430d0c12c0f488966104d47d35", null ],
    [ "health_client_fault_clear", "group__HEALTH__CLIENT.html#ga884327e612b8f03ee3d636ab3fb9c598", null ],
    [ "health_client_fault_test", "group__HEALTH__CLIENT.html#ga9c9116076d76a3bd29aa08b0dd3f39b3", null ],
    [ "health_client_period_get", "group__HEALTH__CLIENT.html#ga611fceca1a2db45da5d288a7694adb87", null ],
    [ "health_client_period_set", "group__HEALTH__CLIENT.html#ga066c4b03ed718c7244689c1664848d1a", null ],
    [ "health_client_attention_get", "group__HEALTH__CLIENT.html#gaad432b8a7a06aa0a3712078644532fca", null ],
    [ "health_client_attention_set", "group__HEALTH__CLIENT.html#gabe2f65a490f6e9e995099c4306017de6", null ],
    [ "health_client_init", "group__HEALTH__CLIENT.html#ga3b5f28f4825d5da09bf222e420adde07", null ],
    [ "health_client_pending_msg_cancel", "group__HEALTH__CLIENT.html#gab4a1f813a47b872927c0279be9f00ef5", null ]
];