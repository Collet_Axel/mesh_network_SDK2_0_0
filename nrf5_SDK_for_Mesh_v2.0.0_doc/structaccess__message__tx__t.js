var structaccess__message__tx__t =
[
    [ "opcode", "structaccess__message__tx__t.html#a895fa0a866b792a924600b842c5657e3", null ],
    [ "p_buffer", "structaccess__message__tx__t.html#ad03a79e397502752498e656161b0eea4", null ],
    [ "length", "structaccess__message__tx__t.html#a77636089c0ee338a3c69f9bda8e7c42c", null ],
    [ "force_segmented", "structaccess__message__tx__t.html#a11fa29733b7aa758728109384efeb6ca", null ],
    [ "transmic_size", "structaccess__message__tx__t.html#acc823572c3a3ce69092035fcbd1c0879", null ],
    [ "access_token", "structaccess__message__tx__t.html#af0c6631412df57cf188fb70a577838a0", null ]
];