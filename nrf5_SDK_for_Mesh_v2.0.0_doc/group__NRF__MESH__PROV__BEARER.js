var group__NRF__MESH__PROV__BEARER =
[
    [ "Provisioning bearer interface", "group__PROV__BEARER__INTERFACE.html", "group__PROV__BEARER__INTERFACE" ],
    [ "Provisioning bearer callbacks", "group__PROV__BEARER__CALLBACKS.html", "group__PROV__BEARER__CALLBACKS" ],
    [ "Provisioning over Advertising Bearer (PB-ADV)", "group__NRF__MESH__PROV__BEARER__ADV.html", "group__NRF__MESH__PROV__BEARER__ADV" ],
    [ "Provisioning over GATT Bearer (PB-GATT)", "group__NRF__MESH__PROV__BEARER__GATT.html", "group__NRF__MESH__PROV__BEARER__GATT" ],
    [ "Provisioning over Mesh (PB-remote) (experimental)", "group__PB__REMOTE.html", "group__PB__REMOTE" ],
    [ "prov_bearer_t", "structprov__bearer.html", [
      [ "node", "structprov__bearer.html#a3438c273abbf1d8c2d5ce2f2a6104c10", null ],
      [ "bearer_type", "structprov__bearer.html#a8ca97c2dac2f134bdfc755c63cec1581", null ],
      [ "p_interface", "structprov__bearer.html#a02cbc40ae13a2eca43b28224b54717d6", null ],
      [ "p_callbacks", "structprov__bearer.html#a300a729ea61290d662f8c136134b1a44", null ],
      [ "p_parent", "structprov__bearer.html#a310b3a66bd77b7170f4f789295d39116", null ]
    ] ],
    [ "NRF_MESH_PROV_BEARER_COUNT", "group__NRF__MESH__PROV__BEARER.html#gabbc96c3a8c2b661f86d7f1dcc30c9641", null ],
    [ "nrf_mesh_prov_link_close_reason_t", "group__NRF__MESH__PROV__BEARER.html#gab62b3d5005f2b6a3d47f2000152b43d7", [
      [ "NRF_MESH_PROV_LINK_CLOSE_REASON_SUCCESS", "group__NRF__MESH__PROV__BEARER.html#ggab62b3d5005f2b6a3d47f2000152b43d7a01eabd355c94e8716f2650b6e2a0b736", null ],
      [ "NRF_MESH_PROV_LINK_CLOSE_REASON_TIMEOUT", "group__NRF__MESH__PROV__BEARER.html#ggab62b3d5005f2b6a3d47f2000152b43d7a63d1f2f6bfdbb1ea32df94ba0a6a1f4e", null ],
      [ "NRF_MESH_PROV_LINK_CLOSE_REASON_ERROR", "group__NRF__MESH__PROV__BEARER.html#ggab62b3d5005f2b6a3d47f2000152b43d7a55a33a2c5e8afc1e170f2a044a5ce87a", null ],
      [ "NRF_MESH_PROV_LINK_CLOSE_REASON_LAST", "group__NRF__MESH__PROV__BEARER.html#ggab62b3d5005f2b6a3d47f2000152b43d7a8a58d7aa8e9f084ccaa4d58e85add14d", null ]
    ] ],
    [ "nrf_mesh_prov_bearer_type_t", "group__NRF__MESH__PROV__BEARER.html#gad9801e23584ffac6fea59e9e49567dbc", [
      [ "NRF_MESH_PROV_BEARER_ADV", "group__NRF__MESH__PROV__BEARER.html#ggad9801e23584ffac6fea59e9e49567dbca18b0cce250be10a79c37c33f775415b8", null ],
      [ "NRF_MESH_PROV_BEARER_GATT", "group__NRF__MESH__PROV__BEARER.html#ggad9801e23584ffac6fea59e9e49567dbcac7c0649692a26d7ddb7092cc2c1cabb8", null ],
      [ "NRF_MESH_PROV_BEARER_MESH", "group__NRF__MESH__PROV__BEARER.html#ggad9801e23584ffac6fea59e9e49567dbcadb11f0e1e40dc1ee85559ef7faf86370", null ]
    ] ]
];