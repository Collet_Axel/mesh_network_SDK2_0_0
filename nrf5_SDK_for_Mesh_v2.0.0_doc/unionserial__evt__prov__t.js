var unionserial__evt__prov__t =
[
    [ "unprov", "unionserial__evt__prov__t.html#af088c89d119bd944f354da2e1165d4bd", null ],
    [ "link_established", "unionserial__evt__prov__t.html#ad92e52d510b64af66e421cc697e1acab", null ],
    [ "link_closed", "unionserial__evt__prov__t.html#a3608650a140fb80aa2ea4e2dd96681f8", null ],
    [ "caps_received", "unionserial__evt__prov__t.html#ada8e63710b2f9fdfb158ecf370b87e55", null ],
    [ "complete", "unionserial__evt__prov__t.html#a05d407d090f804473580afbec62f644a", null ],
    [ "auth_request", "unionserial__evt__prov__t.html#af282e1c2110f2c7a1ab76a27138c4dde", null ],
    [ "ecdh_request", "unionserial__evt__prov__t.html#a3d3fe189b52019c9afbe2b7ca30f9b6a", null ],
    [ "output_request", "unionserial__evt__prov__t.html#a0884666c8aceef9509a0b2ae3a3af01f", null ],
    [ "failed", "unionserial__evt__prov__t.html#ae85dc4d8fa081decec57a9e4b17b1845", null ]
];