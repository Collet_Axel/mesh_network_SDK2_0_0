var group__ACCESS__CONFIG =
[
    [ "Model configuration", "group__ACCESS__CONFIG__MODEL.html", "group__ACCESS__CONFIG__MODEL" ],
    [ "Element configuration", "group__ACCESS__CONFIG__ELEMENT.html", "group__ACCESS__CONFIG__ELEMENT" ],
    [ "access_flash_config_load", "group__ACCESS__CONFIG.html#gab3c3c58d3cfac051c35230726217b0b6", null ],
    [ "access_flash_config_store", "group__ACCESS__CONFIG.html#ga7edbd6e31c7024802b079c9e00c029a0", null ],
    [ "access_default_ttl_set", "group__ACCESS__CONFIG.html#ga83e3d25feb4308df4b68a982a7dfea7a", null ],
    [ "access_default_ttl_get", "group__ACCESS__CONFIG.html#ga1ece98da00b2658cb5cf5afd1daa19c4", null ],
    [ "access_handle_get", "group__ACCESS__CONFIG.html#ga11cc653ae115c43732784bef77adaeb9", null ]
];