var group__HEALTH__MODEL =
[
    [ "Health Client", "group__HEALTH__CLIENT.html", "group__HEALTH__CLIENT" ],
    [ "Fault IDs", "group__HEALTH__FAULT__IDS.html", null ],
    [ "Health model message definitions", "group__HEALTH__MESSAGES.html", "group__HEALTH__MESSAGES" ],
    [ "Health model message opcodes", "group__HEALTH__OPCODES.html", "group__HEALTH__OPCODES" ],
    [ "Health Server", "group__HEALTH__SERVER.html", "group__HEALTH__SERVER" ],
    [ "HEALTH_SERVER_MODEL_ID", "group__HEALTH__MODEL.html#ga238cf5e102231389a4ea3e3a3ebf68a5", null ],
    [ "HEALTH_CLIENT_MODEL_ID", "group__HEALTH__MODEL.html#ga6d7e43c5b1b3e776303bc13583c82db1", null ]
];