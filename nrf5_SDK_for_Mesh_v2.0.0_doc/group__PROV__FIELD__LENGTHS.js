var group__PROV__FIELD__LENGTHS =
[
    [ "NRF_MESH_PROV_PUBKEY_SIZE", "group__PROV__FIELD__LENGTHS.html#gabc8ff0ab463246062425d1e0e52b1571", null ],
    [ "NRF_MESH_PROV_PRIVKEY_SIZE", "group__PROV__FIELD__LENGTHS.html#gabdf812723eda72f6c72a254ea02a4bc1", null ],
    [ "NRF_MESH_PROV_ECDHSECRET_SIZE", "group__PROV__FIELD__LENGTHS.html#ga15d23a8ac52276150bd2ed4900984acf", null ],
    [ "NRF_MESH_PROV_DATANONCE_SIZE", "group__PROV__FIELD__LENGTHS.html#gabbc62d8aef3add5d268f4d99271fbdb9", null ],
    [ "NRF_MESH_PROV_OOB_SIZE_MAX", "group__PROV__FIELD__LENGTHS.html#gae85fcc27709922050d66378a59ae66bf", null ],
    [ "PROV_RANDOM_LEN", "group__PROV__FIELD__LENGTHS.html#gae799d95356320a48b6e37ff179c7537d", null ],
    [ "PROV_CONFIRMATION_LEN", "group__PROV__FIELD__LENGTHS.html#ga8183d8864543dfc61e638aa5254a019d", null ],
    [ "PROV_AUTH_LEN", "group__PROV__FIELD__LENGTHS.html#ga0e788bbcc6bd7c0d49db6488c5469f9f", null ],
    [ "PROV_SALT_LEN", "group__PROV__FIELD__LENGTHS.html#ga0bb793982a51ff3b90cd5a73cb370f2f", null ],
    [ "PROV_NONCE_LEN", "group__PROV__FIELD__LENGTHS.html#ga46f40c71438f70487fc2244e40ce9ed2", null ],
    [ "PROV_CONFIRMATION_INPUT_LEN", "group__PROV__FIELD__LENGTHS.html#gafeb701226db4df81da1e95f0b507739a", null ]
];