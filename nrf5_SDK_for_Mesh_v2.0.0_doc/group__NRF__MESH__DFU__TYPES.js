var group__NRF__MESH__DFU__TYPES =
[
    [ "nrf_mesh_bootloader_id_t", "structnrf__mesh__bootloader__id__t.html", [
      [ "bl_id", "structnrf__mesh__bootloader__id__t.html#a87b4a3171c245e4c3c25333288e8bfde", null ],
      [ "bl_version", "structnrf__mesh__bootloader__id__t.html#a7f112fcb331ada92f900668a084a7046", null ]
    ] ],
    [ "nrf_mesh_app_id_t", "structnrf__mesh__app__id__t.html", [
      [ "company_id", "structnrf__mesh__app__id__t.html#af457365bb54fdf7fda9105a98d59e798", null ],
      [ "app_id", "structnrf__mesh__app__id__t.html#a9878f4d6743c2dd2f5388b7ef2d6bf7f", null ],
      [ "app_version", "structnrf__mesh__app__id__t.html#a051c2983a79c2047e38d795c67391e9f", null ]
    ] ],
    [ "nrf_mesh_fwid_t", "unionnrf__mesh__fwid__t.html", [
      [ "application", "unionnrf__mesh__fwid__t.html#a93c65b6b62c6d6b8c9932b0a1df9a7f6", null ],
      [ "bootloader", "unionnrf__mesh__fwid__t.html#a5ddcaf6579f8c0bc5a3820180dacdd13", null ],
      [ "softdevice", "unionnrf__mesh__fwid__t.html#a84409977b365561924b1bc4c2ceaf0d7", null ]
    ] ],
    [ "nrf_mesh_dfu_transfer_t", "structnrf__mesh__dfu__transfer__t.html", [
      [ "dfu_type", "structnrf__mesh__dfu__transfer__t.html#a7b7ce0791c540a78d6f4e2a9e6f85ccd", null ],
      [ "id", "structnrf__mesh__dfu__transfer__t.html#a894735cf31f1e96f064b40c55019ded4", null ]
    ] ],
    [ "nrf_mesh_dfu_bank_info_t", "structnrf__mesh__dfu__bank__info__t.html", [
      [ "dfu_type", "structnrf__mesh__dfu__bank__info__t.html#a6d2c3edd0a06f7006cd66d911810dac3", null ],
      [ "fwid", "structnrf__mesh__dfu__bank__info__t.html#ae9d044e69ab0acfbd533897ffc2ec2c5", null ],
      [ "is_signed", "structnrf__mesh__dfu__bank__info__t.html#a844f4267f92aff9eef255ab8217e8270", null ],
      [ "p_start_addr", "structnrf__mesh__dfu__bank__info__t.html#a3291be1cc291d3156881a3665786c29f", null ],
      [ "length", "structnrf__mesh__dfu__bank__info__t.html#ade99683a36c1585c432b5d1c89ed175f", null ]
    ] ],
    [ "nrf_mesh_dfu_transfer_state_t", "structnrf__mesh__dfu__transfer__state__t.html", [
      [ "role", "structnrf__mesh__dfu__transfer__state__t.html#aafa094b440c87b99b6dd51e8d1b7013d", null ],
      [ "type", "structnrf__mesh__dfu__transfer__state__t.html#a7e7b0555da97ef597760c7a474d67444", null ],
      [ "fwid", "structnrf__mesh__dfu__transfer__state__t.html#a83e186d089ce90c6e0b135a4f9ecadfc", null ],
      [ "state", "structnrf__mesh__dfu__transfer__state__t.html#a25ea8b49075931923fcc14f2ebf974d9", null ],
      [ "data_progress", "structnrf__mesh__dfu__transfer__state__t.html#ad047756f5f10e991bd8e641dd02435fa", null ]
    ] ],
    [ "nrf_mesh_dfu_source_authority_t", "group__NRF__MESH__DFU__TYPES.html#ga0b981f707b899dd0e6652dfe9b8d0ff2", [
      [ "NRF_MESH_DFU_SOURCE_AUTHORITY_WEAK_NODE", "group__NRF__MESH__DFU__TYPES.html#gga0b981f707b899dd0e6652dfe9b8d0ff2a8ebc7ed620e5d19124257cad85f20836", null ],
      [ "NRF_MESH_DFU_SOURCE_AUTHORITY_STRONG_NODE", "group__NRF__MESH__DFU__TYPES.html#gga0b981f707b899dd0e6652dfe9b8d0ff2a30e591de7706d42b56cc9eb8d71f8970", null ],
      [ "NRF_MESH_DFU_SOURCE_AUTHORITY_GATEWAY", "group__NRF__MESH__DFU__TYPES.html#gga0b981f707b899dd0e6652dfe9b8d0ff2af43955ac16219d8cc5b784204f31a860", null ]
    ] ],
    [ "nrf_mesh_dfu_type_t", "group__NRF__MESH__DFU__TYPES.html#gabd1f10d8527a4b37113e5f2b158f756a", [
      [ "NRF_MESH_DFU_TYPE_INVALID", "group__NRF__MESH__DFU__TYPES.html#ggabd1f10d8527a4b37113e5f2b158f756aaf7cb07c6cce8cf21cb9b9bb15c012eb4", null ],
      [ "NRF_MESH_DFU_TYPE_SOFTDEVICE", "group__NRF__MESH__DFU__TYPES.html#ggabd1f10d8527a4b37113e5f2b158f756aabbe1dfbfdc1cd56c61c2f37d4eb3eeac", null ],
      [ "NRF_MESH_DFU_TYPE_BOOTLOADER", "group__NRF__MESH__DFU__TYPES.html#ggabd1f10d8527a4b37113e5f2b158f756aa725641516ba6eaf96ec4a4d044c59958", null ],
      [ "NRF_MESH_DFU_TYPE_APPLICATION", "group__NRF__MESH__DFU__TYPES.html#ggabd1f10d8527a4b37113e5f2b158f756aa6950826c974b6da50e85789919c9c9ba", null ],
      [ "NRF_MESH_DFU_TYPE_BL_INFO", "group__NRF__MESH__DFU__TYPES.html#ggabd1f10d8527a4b37113e5f2b158f756aa04f081d98a6da1d8ab30ac5c120b1be1", null ]
    ] ],
    [ "nrf_mesh_dfu_state_t", "group__NRF__MESH__DFU__TYPES.html#gaf957705076eb7dce3bbdb3500353bad7", [
      [ "NRF_MESH_DFU_STATE_UNINITIALIZED", "group__NRF__MESH__DFU__TYPES.html#ggaf957705076eb7dce3bbdb3500353bad7aa08c1d7fd4235840d23ce10d3beeb461", null ],
      [ "NRF_MESH_DFU_STATE_INITIALIZED", "group__NRF__MESH__DFU__TYPES.html#ggaf957705076eb7dce3bbdb3500353bad7aaf98c5ce2cb047d24a2ae03a7771e340", null ],
      [ "NRF_MESH_DFU_STATE_FIND_FWID", "group__NRF__MESH__DFU__TYPES.html#ggaf957705076eb7dce3bbdb3500353bad7a791584c768cc1f9b65545c6313645459", null ],
      [ "NRF_MESH_DFU_STATE_DFU_REQ", "group__NRF__MESH__DFU__TYPES.html#ggaf957705076eb7dce3bbdb3500353bad7ac375bc39e464c5e37db987535defdd68", null ],
      [ "NRF_MESH_DFU_STATE_READY", "group__NRF__MESH__DFU__TYPES.html#ggaf957705076eb7dce3bbdb3500353bad7a62769cfe60216a0b131b7f8eaead293e", null ],
      [ "NRF_MESH_DFU_STATE_TARGET", "group__NRF__MESH__DFU__TYPES.html#ggaf957705076eb7dce3bbdb3500353bad7a07c44af19af7e476b84b3830f50a0c8b", null ],
      [ "NRF_MESH_DFU_STATE_VALIDATE", "group__NRF__MESH__DFU__TYPES.html#ggaf957705076eb7dce3bbdb3500353bad7a93c4a43cfb7bdb72b661940904c7e238", null ],
      [ "NRF_MESH_DFU_STATE_STABILIZE", "group__NRF__MESH__DFU__TYPES.html#ggaf957705076eb7dce3bbdb3500353bad7a59299d859dafa2dff22bdb83961ccea2", null ],
      [ "NRF_MESH_DFU_STATE_RELAY_CANDIDATE", "group__NRF__MESH__DFU__TYPES.html#ggaf957705076eb7dce3bbdb3500353bad7ae37802f74a46e69bfe3162c1b8b34c1f", null ],
      [ "NRF_MESH_DFU_STATE_RELAY", "group__NRF__MESH__DFU__TYPES.html#ggaf957705076eb7dce3bbdb3500353bad7a7039d74600cd6d5544263a49aa5c9e3b", null ]
    ] ],
    [ "nrf_mesh_dfu_end_t", "group__NRF__MESH__DFU__TYPES.html#gabae964d0848ffb522d27d7d90ecd0527", [
      [ "NRF_MESH_DFU_END_SUCCESS", "group__NRF__MESH__DFU__TYPES.html#ggabae964d0848ffb522d27d7d90ecd0527ad64822a0fb6ba44b184bcaf87ac59347", null ],
      [ "NRF_MESH_DFU_END_FWID_VALID", "group__NRF__MESH__DFU__TYPES.html#ggabae964d0848ffb522d27d7d90ecd0527aece881e6495aa599c8c2887a0d1bf347", null ],
      [ "NRF_MESH_DFU_END_APP_ABORT", "group__NRF__MESH__DFU__TYPES.html#ggabae964d0848ffb522d27d7d90ecd0527a1fb9c908bc8ac848c0d4f52bb9ca08cb", null ],
      [ "NRF_MESH_DFU_END_ERROR_PACKET_LOSS", "group__NRF__MESH__DFU__TYPES.html#ggabae964d0848ffb522d27d7d90ecd0527a34fdd96e038a401115e2e4106130f3a1", null ],
      [ "NRF_MESH_DFU_END_ERROR_UNAUTHORIZED", "group__NRF__MESH__DFU__TYPES.html#ggabae964d0848ffb522d27d7d90ecd0527aafcebd5d5dc669d17a280b22259ca7ed", null ],
      [ "NRF_MESH_DFU_END_ERROR_NO_START", "group__NRF__MESH__DFU__TYPES.html#ggabae964d0848ffb522d27d7d90ecd0527aedeef47adbba01cec5c762defdd73bf6", null ],
      [ "NRF_MESH_DFU_END_ERROR_TIMEOUT", "group__NRF__MESH__DFU__TYPES.html#ggabae964d0848ffb522d27d7d90ecd0527adcc90e43c72f3fe54bd25469564cf830", null ],
      [ "NRF_MESH_DFU_END_ERROR_NO_MEM", "group__NRF__MESH__DFU__TYPES.html#ggabae964d0848ffb522d27d7d90ecd0527a10c9a142e40d5b2e3fa5ceb0d0f67142", null ],
      [ "NRF_MESH_DFU_END_ERROR_INVALID_PERSISTENT_STORAGE", "group__NRF__MESH__DFU__TYPES.html#ggabae964d0848ffb522d27d7d90ecd0527ae98603ac3be929e510d6a8959a53548d", null ],
      [ "NRF_MESH_DFU_END_ERROR_SEGMENT_VIOLATION", "group__NRF__MESH__DFU__TYPES.html#ggabae964d0848ffb522d27d7d90ecd0527adaea59ea011613b6f847aafa3dffc247", null ],
      [ "NRF_MESH_DFU_END_ERROR_MBR_CALL_FAILED", "group__NRF__MESH__DFU__TYPES.html#ggabae964d0848ffb522d27d7d90ecd0527a6b670b80110c7b67afcddf76ac926fd7", null ],
      [ "NRF_MESH_DFU_END_ERROR_INVALID_TRANSFER", "group__NRF__MESH__DFU__TYPES.html#ggabae964d0848ffb522d27d7d90ecd0527a51ea291e2723740cbfc7266882c694eb", null ],
      [ "NRF_MESH_DFU_END_ERROR_BANK_IN_BOOTLOADER_AREA", "group__NRF__MESH__DFU__TYPES.html#ggabae964d0848ffb522d27d7d90ecd0527a30e48d94577970b92498198a58088634", null ]
    ] ],
    [ "nrf_mesh_dfu_role_t", "group__NRF__MESH__DFU__TYPES.html#ga46fe98186350b9cc4744f15d725e3fe8", [
      [ "NRF_MESH_DFU_ROLE_NONE", "group__NRF__MESH__DFU__TYPES.html#gga46fe98186350b9cc4744f15d725e3fe8a565c28ab919596611050a5ded4493c41", null ],
      [ "NRF_MESH_DFU_ROLE_TARGET", "group__NRF__MESH__DFU__TYPES.html#gga46fe98186350b9cc4744f15d725e3fe8ad7eeb4799e52dc334ae631019623f199", null ],
      [ "NRF_MESH_DFU_ROLE_RELAY", "group__NRF__MESH__DFU__TYPES.html#gga46fe98186350b9cc4744f15d725e3fe8ab1721a7426bd297e8b5b0a43e652424c", null ],
      [ "NRF_MESH_DFU_ROLE_SOURCE", "group__NRF__MESH__DFU__TYPES.html#gga46fe98186350b9cc4744f15d725e3fe8ae6b3bae44f31d2fb64e242c0a0bea8ec", null ]
    ] ]
];