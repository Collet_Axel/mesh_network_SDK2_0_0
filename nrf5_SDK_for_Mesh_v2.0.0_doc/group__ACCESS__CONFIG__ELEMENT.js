var group__ACCESS__CONFIG__ELEMENT =
[
    [ "access_element_location_set", "group__ACCESS__CONFIG__ELEMENT.html#ga677997faf65600a8c298bf1a2657438b", null ],
    [ "access_element_location_get", "group__ACCESS__CONFIG__ELEMENT.html#gab73e6cd226833d4a5d4bb42c5d690e42", null ],
    [ "access_element_sig_model_count_get", "group__ACCESS__CONFIG__ELEMENT.html#ga9cca95ea59a9e1ee8352aede33bcaa13", null ],
    [ "access_element_vendor_model_count_get", "group__ACCESS__CONFIG__ELEMENT.html#gaba94247cfe7f08640bfdde934aa0be11", null ],
    [ "access_element_models_get", "group__ACCESS__CONFIG__ELEMENT.html#gad0c025de39d5d756a97b1baaf0a39a66", null ]
];