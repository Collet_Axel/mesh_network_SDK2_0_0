var struct____health__server__t =
[
    [ "model_handle", "struct____health__server__t.html#ab5693cbd3c7df50459a1a7ea650da489", null ],
    [ "fast_period_divisor", "struct____health__server__t.html#a05d6edd32e5be5e21890fd9d87f7fdd1", null ],
    [ "regular_publish_res", "struct____health__server__t.html#a7aab8227ab03bd0f3e10c28e63187788", null ],
    [ "regular_publish_steps", "struct____health__server__t.html#a9654ec1bad241ab02e7803138df825aa", null ],
    [ "p_selftests", "struct____health__server__t.html#a25b2d09fa4990c291bb6aed56f251cad", null ],
    [ "num_selftests", "struct____health__server__t.html#ac60c803bd29960603de1363fed75cae0", null ],
    [ "previous_test_id", "struct____health__server__t.html#a0dd7a1355975bfc5055b38c6fbfd024f", null ],
    [ "company_id", "struct____health__server__t.html#a65133f2d7f5e88c77a94931d8c98b2ff", null ],
    [ "registered_faults", "struct____health__server__t.html#a4493060358a713ff422d3af5d5066712", null ],
    [ "current_faults", "struct____health__server__t.html#a52404ffecfb4e346e5050eafb5400a93", null ],
    [ "attention_handler", "struct____health__server__t.html#ad9de9ef074adc805cd9b19a728e40694", null ],
    [ "attention_timer", "struct____health__server__t.html#a1415887caacb88831d73a60accef246b", null ],
    [ "p_next", "struct____health__server__t.html#af0036588321436ff7c78e03d86c42e9b", null ]
];