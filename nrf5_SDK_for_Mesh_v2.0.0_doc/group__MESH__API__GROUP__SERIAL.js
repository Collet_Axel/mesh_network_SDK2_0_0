var group__MESH__API__GROUP__SERIAL =
[
    [ "Serial configuration", "group__NRF__MESH__CONFIG__SERIAL.html", "group__NRF__MESH__CONFIG__SERIAL" ],
    [ "Serial API", "group__NRF__MESH__SERIAL.html", "group__NRF__MESH__SERIAL" ],
    [ "Serial components", "group__MESH__SERIAL.html", "group__MESH__SERIAL" ]
];