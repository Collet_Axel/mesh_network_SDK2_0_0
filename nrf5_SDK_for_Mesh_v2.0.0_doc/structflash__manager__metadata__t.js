var structflash__manager__metadata__t =
[
    [ "metadata_len", "structflash__manager__metadata__t.html#adf0d6f2b919b483566509e1049d0cdad", null ],
    [ "entry_header_length", "structflash__manager__metadata__t.html#a147c8e1dbd7939525105cbca56106306", null ],
    [ "entry_len_length_bits", "structflash__manager__metadata__t.html#a97cf55bc39f06766c64c4ab75b8ca578", null ],
    [ "entry_type_length_bits", "structflash__manager__metadata__t.html#a76a6979778f2b8952d8fc63cd0b7483d", null ],
    [ "pages_in_area", "structflash__manager__metadata__t.html#a62a0babe6d35a8581cf1120520ea7998", null ],
    [ "page_index", "structflash__manager__metadata__t.html#abef21152b9a11b1e420eeaf0e7baaf92", null ],
    [ "_padding", "structflash__manager__metadata__t.html#ad7f0cb5cef5a6e4b629352e87c2b67b7", null ]
];