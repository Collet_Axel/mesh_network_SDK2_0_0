var structnrf__mesh__rx__metadata__instaburst__t =
[
    [ "timestamp", "structnrf__mesh__rx__metadata__instaburst__t.html#ac185eeee36e8170c1675f8b552895145", null ],
    [ "channel", "structnrf__mesh__rx__metadata__instaburst__t.html#aa92b58bd7e4e1b70a1136b9e1c13c9f6", null ],
    [ "rssi", "structnrf__mesh__rx__metadata__instaburst__t.html#a7bfa404a94016957353f8448fa1c33bf", null ],
    [ "id", "structnrf__mesh__rx__metadata__instaburst__t.html#a7b0cb4e2fab593ebcf4869f241618da9", null ],
    [ "packet_index", "structnrf__mesh__rx__metadata__instaburst__t.html#a6d8f2188aedd7707e6a5a0930ba831db", null ],
    [ "is_last_in_chain", "structnrf__mesh__rx__metadata__instaburst__t.html#a7bf4aea7082d1860fd585fe7b8fc3c80", null ],
    [ "event", "structnrf__mesh__rx__metadata__instaburst__t.html#a82f1995fb68873abfbc0ff225e39e767", null ]
];