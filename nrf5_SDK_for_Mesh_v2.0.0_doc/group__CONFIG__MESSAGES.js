var group__CONFIG__MESSAGES =
[
    [ "config_msg_key_index_24_t", "structconfig__msg__key__index__24__t.html", [
      [ "key_id_1_lsb", "structconfig__msg__key__index__24__t.html#a77601fcc6225019313839259aecdf293", null ],
      [ "key_id_1_msb", "structconfig__msg__key__index__24__t.html#a07a062df5b896dcb84de02208d55fb03", null ],
      [ "key_id_2_lsb", "structconfig__msg__key__index__24__t.html#a34492721051ce922ac69d20ef60ee526", null ],
      [ "key_id_2_msb", "structconfig__msg__key__index__24__t.html#ada48f489d3e8f42268add489f70598b4", null ]
    ] ],
    [ "config_model_id_t", "unionconfig__model__id__t.html", [
      [ "model_id", "unionconfig__model__id__t.html#a077907739bcebe86469751fb79064e47", null ],
      [ "sig", "unionconfig__model__id__t.html#a674cac4e229d8cf77f1d56a6cf100f7b", null ],
      [ "company_id", "unionconfig__model__id__t.html#a79a769e0812987317e0360c5239bb99f", null ],
      [ "vendor", "unionconfig__model__id__t.html#a687f2a9d7d53eff1cce8fcea6dafb739", null ]
    ] ],
    [ "config_msg_appkey_add_t", "structconfig__msg__appkey__add__t.html", [
      [ "key_indexes", "structconfig__msg__appkey__add__t.html#a8728b3667e054c80a5d81578e0414732", null ],
      [ "appkey", "structconfig__msg__appkey__add__t.html#a2a634ed2699b8653992a2218f1739c91", null ]
    ] ],
    [ "config_msg_appkey_update_t", "structconfig__msg__appkey__update__t.html", [
      [ "key_indexes", "structconfig__msg__appkey__update__t.html#aeb837fdcc814d596d2a024f643cc7d2d", null ],
      [ "appkey", "structconfig__msg__appkey__update__t.html#a11a9480b06c25902dbd9780e9eb33614", null ]
    ] ],
    [ "config_msg_appkey_delete_t", "structconfig__msg__appkey__delete__t.html", [
      [ "key_indexes", "structconfig__msg__appkey__delete__t.html#ad57270a492a4eb7551db3ec87bf9d34d", null ]
    ] ],
    [ "config_msg_appkey_status_t", "structconfig__msg__appkey__status__t.html", [
      [ "status", "structconfig__msg__appkey__status__t.html#ab4c6881de165cf002a60c3dbf4f928e9", null ],
      [ "key_indexes", "structconfig__msg__appkey__status__t.html#aa6f56b57fecbb7b020400d9ad88be06e", null ]
    ] ],
    [ "config_msg_appkey_get_t", "structconfig__msg__appkey__get__t.html", [
      [ "netkey_index", "structconfig__msg__appkey__get__t.html#a13f04d200506565641fa7c691b3ebc58", null ]
    ] ],
    [ "config_msg_appkey_list_t", "structconfig__msg__appkey__list__t.html", [
      [ "status", "structconfig__msg__appkey__list__t.html#a5f782f35344b2277ded96c5bb6cb5696", null ],
      [ "netkey_index", "structconfig__msg__appkey__list__t.html#a8eb1f2578bf704511ef930fb8066a8ea", null ],
      [ "packed_appkey_indexes", "structconfig__msg__appkey__list__t.html#a5e6e4cc781fa006f479a0b7043bc4614", null ]
    ] ],
    [ "config_msg_default_ttl_set_t", "structconfig__msg__default__ttl__set__t.html", [
      [ "ttl", "structconfig__msg__default__ttl__set__t.html#a1b3c2f9b9e3d177f53229e4dd5c49253", null ]
    ] ],
    [ "config_msg_default_ttl_status_t", "structconfig__msg__default__ttl__status__t.html", [
      [ "ttl", "structconfig__msg__default__ttl__status__t.html#ae4af470eaecfcd3f950339d80d6c86d3", null ]
    ] ],
    [ "config_msg_net_beacon_set_t", "structconfig__msg__net__beacon__set__t.html", [
      [ "beacon_state", "structconfig__msg__net__beacon__set__t.html#a68c7dd693410df73f0c75d538ead1cf1", null ]
    ] ],
    [ "config_msg_net_beacon_status_t", "structconfig__msg__net__beacon__status__t.html", [
      [ "beacon_state", "structconfig__msg__net__beacon__status__t.html#ae83f9239ae4b3adb5b662a820d9e9e4b", null ]
    ] ],
    [ "config_publication_params_t", "structconfig__publication__params__t.html", [
      [ "appkey_index", "structconfig__publication__params__t.html#a65527b2e926bb102b2d8a8d288f5ced4", null ],
      [ "credential_flag", "structconfig__publication__params__t.html#a966705f3f8b38966f4096c81242a1a2a", null ],
      [ "rfu", "structconfig__publication__params__t.html#aad5f59fb5d3308b9230813b524bf260a", null ],
      [ "publish_ttl", "structconfig__publication__params__t.html#a919909ad6c87a16d04b41da8f227c973", null ],
      [ "publish_period", "structconfig__publication__params__t.html#a919bcfef9ecd0ef2d77dae1a13dcb031", null ],
      [ "retransmit_count", "structconfig__publication__params__t.html#aae74ecf46d9a612b29b67559bbef231c", null ],
      [ "retransmit_interval", "structconfig__publication__params__t.html#aa6895592e45fab253134026595c0e670", null ],
      [ "model_id", "structconfig__publication__params__t.html#a58429e187e7239ea357abcb8ff37b3ee", null ]
    ] ],
    [ "config_msg_publication_get_t", "structconfig__msg__publication__get__t.html", [
      [ "element_address", "structconfig__msg__publication__get__t.html#ab73d7b95c48500f59025969d099f7ca6", null ],
      [ "model_id", "structconfig__msg__publication__get__t.html#ab4fab0248d9e64d3be8651522efec504", null ]
    ] ],
    [ "config_msg_publication_set_t", "structconfig__msg__publication__set__t.html", [
      [ "element_address", "structconfig__msg__publication__set__t.html#ae63078b0a8ec5af42525f74c3a3f83f4", null ],
      [ "publish_address", "structconfig__msg__publication__set__t.html#a0f3e968a4dd83dc82058f474f64429a7", null ],
      [ "state", "structconfig__msg__publication__set__t.html#a21147b6b58e585cc2c46b09c4fe972f1", null ]
    ] ],
    [ "config_msg_publication_virtual_set_t", "structconfig__msg__publication__virtual__set__t.html", [
      [ "element_address", "structconfig__msg__publication__virtual__set__t.html#acbc49688c01e1772b8af8e7b49e8aace", null ],
      [ "publish_uuid", "structconfig__msg__publication__virtual__set__t.html#a4d88ff3fa97b9b9969348a1d1ad72e75", null ],
      [ "state", "structconfig__msg__publication__virtual__set__t.html#ab8bcdb647f042a443f600e7047eed6d5", null ]
    ] ],
    [ "config_msg_publication_status_t", "structconfig__msg__publication__status__t.html", [
      [ "status", "structconfig__msg__publication__status__t.html#a6fd259e62dd4a6d715923c7d91855443", null ],
      [ "element_address", "structconfig__msg__publication__status__t.html#a9f4f570f9cfe586c9f63c69989319db6", null ],
      [ "publish_address", "structconfig__msg__publication__status__t.html#abe4f14d3e311406487bceb9d284a89f9", null ],
      [ "state", "structconfig__msg__publication__status__t.html#a185b8565d7c1d14684b83401d509e626", null ]
    ] ],
    [ "config_msg_subscription_add_del_owr_t", "structconfig__msg__subscription__add__del__owr__t.html", [
      [ "element_address", "structconfig__msg__subscription__add__del__owr__t.html#a74a5f359a34de3f3abf882db2c39618a", null ],
      [ "address", "structconfig__msg__subscription__add__del__owr__t.html#ad006f1cd0c1f4ed5a9973dcb2adf1300", null ],
      [ "model_id", "structconfig__msg__subscription__add__del__owr__t.html#a32cd183dd25d331ef0b9393733fa900a", null ]
    ] ],
    [ "config_msg_subscription_virtual_add_del_owr_t", "structconfig__msg__subscription__virtual__add__del__owr__t.html", [
      [ "element_address", "structconfig__msg__subscription__virtual__add__del__owr__t.html#af1b9aa62761ba523502f01f6625baefe", null ],
      [ "virtual_uuid", "structconfig__msg__subscription__virtual__add__del__owr__t.html#a4a9cdb1e90958dcc4047f7065d824826", null ],
      [ "model_id", "structconfig__msg__subscription__virtual__add__del__owr__t.html#a88cf2d6ec284017677c162572dba31c6", null ]
    ] ],
    [ "config_msg_subscription_delete_all_t", "structconfig__msg__subscription__delete__all__t.html", [
      [ "element_address", "structconfig__msg__subscription__delete__all__t.html#a2172e360275648f9d62c9cd3cbf699ff", null ],
      [ "model_id", "structconfig__msg__subscription__delete__all__t.html#a693b216ab175664883670cadddef5a2f", null ]
    ] ],
    [ "config_msg_subscription_status_t", "structconfig__msg__subscription__status__t.html", [
      [ "status", "structconfig__msg__subscription__status__t.html#a2035d917b61538feabe27667314f820c", null ],
      [ "element_address", "structconfig__msg__subscription__status__t.html#a66a9034ddd84eb11087d6ee1d9a367c4", null ],
      [ "address", "structconfig__msg__subscription__status__t.html#aa851ee5a7d1922694a70a5d858a2fd6f", null ],
      [ "model_id", "structconfig__msg__subscription__status__t.html#a08360f6eedc33fe38366b68511389ba3", null ]
    ] ],
    [ "config_msg_netkey_add_update_t", "structconfig__msg__netkey__add__update__t.html", [
      [ "netkey_index", "structconfig__msg__netkey__add__update__t.html#ac1ee633fe1fb4017f0e03c22a528e965", null ],
      [ "netkey", "structconfig__msg__netkey__add__update__t.html#a23526e490c8dcb1a9e0192cd8daa5d31", null ]
    ] ],
    [ "config_msg_netkey_delete_t", "structconfig__msg__netkey__delete__t.html", [
      [ "netkey_index", "structconfig__msg__netkey__delete__t.html#a6304d67d37336da7a11b486945ca287c", null ]
    ] ],
    [ "config_msg_netkey_status_t", "structconfig__msg__netkey__status__t.html", [
      [ "status", "structconfig__msg__netkey__status__t.html#a553aea680c2b1d0cd5ae777fc4668a55", null ],
      [ "netkey_index", "structconfig__msg__netkey__status__t.html#a5150408839a5968a4b66ba9b2ea7026d", null ]
    ] ],
    [ "config_msg_proxy_status_t", "structconfig__msg__proxy__status__t.html", [
      [ "proxy_state", "structconfig__msg__proxy__status__t.html#a91882296a60c74c25bd61afaf7c6ca4a", null ]
    ] ],
    [ "config_msg_proxy_set_t", "structconfig__msg__proxy__set__t.html", [
      [ "proxy_state", "structconfig__msg__proxy__set__t.html#ab529a4278490a25a1a9d549cf045fd9e", null ]
    ] ],
    [ "config_msg_friend_set_t", "structconfig__msg__friend__set__t.html", [
      [ "friend_state", "structconfig__msg__friend__set__t.html#a147357410fccc0b33e00f14b33a9761e", null ]
    ] ],
    [ "config_msg_friend_status_t", "structconfig__msg__friend__status__t.html", [
      [ "friend_state", "structconfig__msg__friend__status__t.html#a466e90a454ed88492340a9596d6196ec", null ]
    ] ],
    [ "config_msg_key_refresh_phase_get_t", "structconfig__msg__key__refresh__phase__get__t.html", [
      [ "netkey_index", "structconfig__msg__key__refresh__phase__get__t.html#a0b85ca2af94474dae797cc96d7d92542", null ]
    ] ],
    [ "config_msg_key_refresh_phase_set_t", "structconfig__msg__key__refresh__phase__set__t.html", [
      [ "netkey_index", "structconfig__msg__key__refresh__phase__set__t.html#a2b1a673550499e659b20ca843f89d792", null ],
      [ "transition", "structconfig__msg__key__refresh__phase__set__t.html#a4b2ff94f4b7828f3bfa4d1a1710ac1d6", null ]
    ] ],
    [ "config_msg_key_refresh_phase_status_t", "structconfig__msg__key__refresh__phase__status__t.html", [
      [ "status", "structconfig__msg__key__refresh__phase__status__t.html#acac8de2e548e3a66d4914c1d867b41d4", null ],
      [ "netkey_index", "structconfig__msg__key__refresh__phase__status__t.html#ad28439c6f23bbd21be7e76c2bb2b0fab", null ],
      [ "phase", "structconfig__msg__key__refresh__phase__status__t.html#ab3bdb078c46f1f99c659bd2731e486f0", null ]
    ] ],
    [ "config_msg_heartbeat_publication_set_t", "structconfig__msg__heartbeat__publication__set__t.html", [
      [ "destination", "structconfig__msg__heartbeat__publication__set__t.html#a83f613e8da5a48994cdc834675474085", null ],
      [ "count_log", "structconfig__msg__heartbeat__publication__set__t.html#ad832fbce43fde0a12424d305ed7d02fa", null ],
      [ "period_log", "structconfig__msg__heartbeat__publication__set__t.html#abb612b70aaa918323a3d23702345bea0", null ],
      [ "ttl", "structconfig__msg__heartbeat__publication__set__t.html#a0b74b5016065fa7623ab5e789bfe52ab", null ],
      [ "features", "structconfig__msg__heartbeat__publication__set__t.html#aa89374ab32f8b9f7e6b8d926cced5b1f", null ],
      [ "netkey_index", "structconfig__msg__heartbeat__publication__set__t.html#a3801fac1f41b4ad553246611fb8c5a1c", null ]
    ] ],
    [ "config_msg_heartbeat_publication_status_t", "structconfig__msg__heartbeat__publication__status__t.html", [
      [ "status", "structconfig__msg__heartbeat__publication__status__t.html#a4bd3290bd2e433789b41b75cd93cba25", null ],
      [ "destination", "structconfig__msg__heartbeat__publication__status__t.html#adeaba556956d585cd6e8d9189ec56003", null ],
      [ "count_log", "structconfig__msg__heartbeat__publication__status__t.html#a5cbf079a0ddaf245237d5776a05eec96", null ],
      [ "period_log", "structconfig__msg__heartbeat__publication__status__t.html#a49995cfb02c405ada1af460651debc89", null ],
      [ "ttl", "structconfig__msg__heartbeat__publication__status__t.html#acb8c1c6ee025a559b129f6b521b3c7e8", null ],
      [ "features", "structconfig__msg__heartbeat__publication__status__t.html#a15600d712ed130a56beccbbe3adb96d4", null ],
      [ "netkey_index", "structconfig__msg__heartbeat__publication__status__t.html#ad5929d123f3d8b2e2a581bc04962508c", null ]
    ] ],
    [ "config_msg_heartbeat_subscription_set_t", "structconfig__msg__heartbeat__subscription__set__t.html", [
      [ "source", "structconfig__msg__heartbeat__subscription__set__t.html#a1ad105bc1e379ebbb21d2be79098c550", null ],
      [ "destination", "structconfig__msg__heartbeat__subscription__set__t.html#a20d09c5b618bc61ee2c9e8fd55af79d4", null ],
      [ "period_log", "structconfig__msg__heartbeat__subscription__set__t.html#a7a627c9bccf69dc3116a722384f16aef", null ]
    ] ],
    [ "config_msg_heartbeat_subscription_status_t", "structconfig__msg__heartbeat__subscription__status__t.html", [
      [ "status", "structconfig__msg__heartbeat__subscription__status__t.html#af448531cda75d62bd169965429323c6d", null ],
      [ "source", "structconfig__msg__heartbeat__subscription__status__t.html#ad24f6f7d5d26235212ac83067580a778", null ],
      [ "destination", "structconfig__msg__heartbeat__subscription__status__t.html#a1d6f334c6e287b7d9b0083e7642026a1", null ],
      [ "period_log", "structconfig__msg__heartbeat__subscription__status__t.html#ab2c26ee4f9a730ecd083e118fa5901d2", null ],
      [ "count_log", "structconfig__msg__heartbeat__subscription__status__t.html#af35c8bd28971d1917edde334c029d465", null ],
      [ "min_hops", "structconfig__msg__heartbeat__subscription__status__t.html#afa86d67b8618d49c2682afe3ec18188f", null ],
      [ "max_hops", "structconfig__msg__heartbeat__subscription__status__t.html#a62d14dcef06735a5ebb1ce268754fdb8", null ]
    ] ],
    [ "config_msg_app_bind_unbind_t", "structconfig__msg__app__bind__unbind__t.html", [
      [ "element_address", "structconfig__msg__app__bind__unbind__t.html#a5ffe4f84ddae257e1ca130184d51e4e5", null ],
      [ "appkey_index", "structconfig__msg__app__bind__unbind__t.html#a2e14761872cb6f87cff84ef92e07df12", null ],
      [ "model_id", "structconfig__msg__app__bind__unbind__t.html#ad7f26599745048145d0ea4ce56c04ffe", null ]
    ] ],
    [ "config_msg_app_status_t", "structconfig__msg__app__status__t.html", [
      [ "status", "structconfig__msg__app__status__t.html#aa86ee7d7c02198e5345c8ff8acbc36e2", null ],
      [ "element_address", "structconfig__msg__app__status__t.html#a49eb996c9ec379b0ac0e4915b93aad93", null ],
      [ "appkey_index", "structconfig__msg__app__status__t.html#ab477f2765384d9958781bbe2b1d02716", null ],
      [ "model_id", "structconfig__msg__app__status__t.html#a42e91e5dccc911074f2decc7cbb43596", null ]
    ] ],
    [ "config_msg_identity_get_t", "structconfig__msg__identity__get__t.html", [
      [ "netkey_index", "structconfig__msg__identity__get__t.html#a3f421fc6ea2c4a5cd48168fd57e7d68d", null ]
    ] ],
    [ "config_msg_identity_set_t", "structconfig__msg__identity__set__t.html", [
      [ "netkey_index", "structconfig__msg__identity__set__t.html#a341d5d4c4a5d0ae2a861648a67298e00", null ],
      [ "identity_state", "structconfig__msg__identity__set__t.html#a0d59e3f7c8eb9392ac6b72b8908422c2", null ]
    ] ],
    [ "config_msg_identity_status_t", "structconfig__msg__identity__status__t.html", [
      [ "status", "structconfig__msg__identity__status__t.html#a5c498bed9bfe44236ebdb7f594e47d6f", null ],
      [ "netkey_index", "structconfig__msg__identity__status__t.html#a91b2745dca30936fda9d4e7a5757286c", null ],
      [ "identity_state", "structconfig__msg__identity__status__t.html#a1846182c9e4c88490de4f5137d543d37", null ]
    ] ],
    [ "config_msg_composition_data_get_t", "structconfig__msg__composition__data__get__t.html", [
      [ "page_number", "structconfig__msg__composition__data__get__t.html#aa55a9ff6ae2ffd4a2c9692153926bca8", null ]
    ] ],
    [ "config_msg_composition_data_status_t", "structconfig__msg__composition__data__status__t.html", [
      [ "page_number", "structconfig__msg__composition__data__status__t.html#adda8563ca305ab7d17fa029c43507017", null ],
      [ "data", "structconfig__msg__composition__data__status__t.html#a70a2d10ebd4595b57e94dc46f03bb597", null ]
    ] ],
    [ "config_msg_relay_status_t", "structconfig__msg__relay__status__t.html", [
      [ "relay_state", "structconfig__msg__relay__status__t.html#a906747dc7513b5b848d88156f3b6fa93", null ],
      [ "relay_retransmit_count", "structconfig__msg__relay__status__t.html#a1d3e97d19a193a736b3a50ed74077654", null ],
      [ "relay_retransmit_interval_steps", "structconfig__msg__relay__status__t.html#a2087f556a0cd1475e5ad59b836cf1d61", null ]
    ] ],
    [ "config_msg_relay_set_t", "structconfig__msg__relay__set__t.html", [
      [ "relay_state", "structconfig__msg__relay__set__t.html#a172389dfa7dd76c989a066ed840b517e", null ],
      [ "relay_retransmit_count", "structconfig__msg__relay__set__t.html#aeb6a9f159a80fcb34c03030bd1797c1f", null ],
      [ "relay_retransmit_interval_steps", "structconfig__msg__relay__set__t.html#a593429beca8b6f19580051efa853cb59", null ]
    ] ],
    [ "config_msg_network_transmit_set_t", "structconfig__msg__network__transmit__set__t.html", [
      [ "network_transmit_count", "structconfig__msg__network__transmit__set__t.html#a4ae9147f90de70f52fff671d88b84a3b", null ],
      [ "network_transmit_interval_steps", "structconfig__msg__network__transmit__set__t.html#a84490a82b8e98501597037297631fad4", null ]
    ] ],
    [ "config_msg_network_transmit_status_t", "structconfig__msg__network__transmit__status__t.html", [
      [ "network_transmit_count", "structconfig__msg__network__transmit__status__t.html#a2a5271adc7aa8b5ef3c0497e99cb9b9f", null ],
      [ "network_transmit_interval_steps", "structconfig__msg__network__transmit__status__t.html#a44f537362889a717c7ed139790b3e73b", null ]
    ] ],
    [ "config_msg_model_app_get_t", "structconfig__msg__model__app__get__t.html", [
      [ "element_address", "structconfig__msg__model__app__get__t.html#a1917c7c8f1b9929ddeece4b7940eece8", null ],
      [ "model_id", "structconfig__msg__model__app__get__t.html#a810704623557a72416852bce5ab88d5c", null ]
    ] ],
    [ "config_msg_sig_model_app_list_t", "structconfig__msg__sig__model__app__list__t.html", [
      [ "status", "structconfig__msg__sig__model__app__list__t.html#a42abfaf2b7865d5b6530e86b6baf33a4", null ],
      [ "element_address", "structconfig__msg__sig__model__app__list__t.html#a5ac3f9806e4b8b2043f942ed0fd6ecb2", null ],
      [ "sig_model_id", "structconfig__msg__sig__model__app__list__t.html#a5425223c1927aed7e757af016635218f", null ],
      [ "key_indexes", "structconfig__msg__sig__model__app__list__t.html#a9f816fd3e9b3a2370f04c97e75d1f412", null ]
    ] ],
    [ "config_msg_vendor_model_app_list_t", "structconfig__msg__vendor__model__app__list__t.html", [
      [ "status", "structconfig__msg__vendor__model__app__list__t.html#afe89dfe7eea4de32448b19ed2d8ee29f", null ],
      [ "element_address", "structconfig__msg__vendor__model__app__list__t.html#a87278ff3393d98d3d0b7320164868036", null ],
      [ "vendor_company_id", "structconfig__msg__vendor__model__app__list__t.html#a13def0b79ffd68c174ab3486db4cd2cd", null ],
      [ "vendor_model_id", "structconfig__msg__vendor__model__app__list__t.html#a7e5114eeef104b3530e0f813cc4dc86e", null ],
      [ "key_indexes", "structconfig__msg__vendor__model__app__list__t.html#a6073b268b25e5e5c73883d8f289ab8f0", null ]
    ] ],
    [ "config_msg_model_subscription_get_t", "structconfig__msg__model__subscription__get__t.html", [
      [ "element_address", "structconfig__msg__model__subscription__get__t.html#ac254082d90ca58d3c6c86643554397ab", null ],
      [ "model_id", "structconfig__msg__model__subscription__get__t.html#a29a62d8a9b40728e10b59eb8a0d88c27", null ]
    ] ],
    [ "config_msg_sig_model_subscription_list_t", "structconfig__msg__sig__model__subscription__list__t.html", [
      [ "status", "structconfig__msg__sig__model__subscription__list__t.html#a4ecb3d9b1f6468bd713bd40dd63bda49", null ],
      [ "element_address", "structconfig__msg__sig__model__subscription__list__t.html#a556f81b44036bbd7ddafafeadef68659", null ],
      [ "sig_model_id", "structconfig__msg__sig__model__subscription__list__t.html#a4dde5d68b94ea54dd9d4cdb0609e4d20", null ],
      [ "subscriptions", "structconfig__msg__sig__model__subscription__list__t.html#a1297331d2d4ffd285b2f75f09d84cd46", null ]
    ] ],
    [ "config_msg_vendor_model_subscription_list_t", "structconfig__msg__vendor__model__subscription__list__t.html", [
      [ "status", "structconfig__msg__vendor__model__subscription__list__t.html#a7044e0ae0953c5720f9573939a063f66", null ],
      [ "element_address", "structconfig__msg__vendor__model__subscription__list__t.html#a76bb74f68c6c1e5c02f5bfeb34b35dfc", null ],
      [ "vendor_company_id", "structconfig__msg__vendor__model__subscription__list__t.html#aacca95242e7a5d0ea601a9d876dcaaa5", null ],
      [ "vendor_model_id", "structconfig__msg__vendor__model__subscription__list__t.html#accbe9af895757ee3d84cdc8668c36c90", null ],
      [ "subscriptions", "structconfig__msg__vendor__model__subscription__list__t.html#a450b34f7302d30f25bed95b5c9fa1fe5", null ]
    ] ],
    [ "CONFIG_RETRANSMIT_COUNT_MAX", "group__CONFIG__MESSAGES.html#gaeeb63a9134a8ff232da1d3db6796a156", null ],
    [ "CONFIG_RETRANSMIT_INTERVAL_STEPS_MAX", "group__CONFIG__MESSAGES.html#ga6707f7575c530c9221357b6272024e96", null ],
    [ "CONFIG_RETRANSMIT_INTERVAL_STEP_TO_MS", "group__CONFIG__MESSAGES.html#ga882801757fd609ca506eeeb17e371ae0", null ],
    [ "CONFIG_RETRANSMIT_INTERVAL_MS_TO_STEP", "group__CONFIG__MESSAGES.html#gabc096b543f7fcfb66570d175f63d3e8f", null ],
    [ "CONFIG_MSG_KEY_INDEX_12_MASK", "group__CONFIG__MESSAGES.html#gaba081574323fe144ee45a0eed4afdeb8", null ],
    [ "config_msg_key_index_12_t", "group__CONFIG__MESSAGES.html#ga846df5463940bc02aa9cb55e4e8499d5", null ],
    [ "config_net_beacon_state_t", "group__CONFIG__MESSAGES.html#gae980e50bb7f9c7f9c7bbff0e7d84ba9e", [
      [ "CONFIG_NET_BEACON_STATE_DISABLED", "group__CONFIG__MESSAGES.html#ggae980e50bb7f9c7f9c7bbff0e7d84ba9ea259482378589dffd9ee4e0b0dfce6c1a", null ],
      [ "CONFIG_NET_BEACON_STATE_ENABLED", "group__CONFIG__MESSAGES.html#ggae980e50bb7f9c7f9c7bbff0e7d84ba9eaf753b1f65bbbfb3e8085838326bebd72", null ]
    ] ],
    [ "config_gatt_proxy_state_t", "group__CONFIG__MESSAGES.html#ga96563c3b1334b6ca7cd014de528ec8b3", [
      [ "CONFIG_GATT_PROXY_STATE_RUNNING_DISABLED", "group__CONFIG__MESSAGES.html#gga96563c3b1334b6ca7cd014de528ec8b3a7eb35e937ec3053ef4fde40ccf1cf968", null ],
      [ "CONFIG_GATT_PROXY_STATE_RUNNING_ENABLED", "group__CONFIG__MESSAGES.html#gga96563c3b1334b6ca7cd014de528ec8b3aac29c94c764585e1733f7be5f15b0e63", null ],
      [ "CONFIG_GATT_PROXY_STATE_UNSUPPORTED", "group__CONFIG__MESSAGES.html#gga96563c3b1334b6ca7cd014de528ec8b3a07a8af9e6a971da357b7bbeb1cbe64b8", null ]
    ] ],
    [ "config_friend_state_t", "group__CONFIG__MESSAGES.html#ga10f077def60ba4352a7c5f1913180f95", [
      [ "CONFIG_FRIEND_STATE_SUPPORTED_DISABLED", "group__CONFIG__MESSAGES.html#gga10f077def60ba4352a7c5f1913180f95a29de0c0035ebec4384767179d41b8b72", null ],
      [ "CONFIG_FRIEND_STATE_SUPPORTED_ENABLED", "group__CONFIG__MESSAGES.html#gga10f077def60ba4352a7c5f1913180f95a4e366af582b8a0550f044efb712d3f5e", null ],
      [ "CONFIG_FRIEND_STATE_UNSUPPORTED", "group__CONFIG__MESSAGES.html#gga10f077def60ba4352a7c5f1913180f95adfa770175c21154da1b990d4dadaa7b2", null ]
    ] ],
    [ "config_identity_state_t", "group__CONFIG__MESSAGES.html#gad4bf13f9fe7ae0543619b2fe69286b54", [
      [ "CONFIG_IDENTITY_STATE_STOPPED", "group__CONFIG__MESSAGES.html#ggad4bf13f9fe7ae0543619b2fe69286b54a232020d1b2473d94dcdc6309e40f656a", null ],
      [ "CONFIG_IDENTITY_STATE_RUNNING", "group__CONFIG__MESSAGES.html#ggad4bf13f9fe7ae0543619b2fe69286b54ae34e802c2c6ceef858aeaad62b24a8dc", null ],
      [ "CONFIG_IDENTITY_STATE_UNSUPPORTED", "group__CONFIG__MESSAGES.html#ggad4bf13f9fe7ae0543619b2fe69286b54a6c1e16bc25b4d7a2d06291af6d8499de", null ]
    ] ],
    [ "config_relay_state_t", "group__CONFIG__MESSAGES.html#ga5aa020cc2fbd721a717e83a87e706381", [
      [ "CONFIG_RELAY_STATE_SUPPORTED_DISABLED", "group__CONFIG__MESSAGES.html#gga5aa020cc2fbd721a717e83a87e706381a9e643208bf49473898fdb25b493645e6", null ],
      [ "CONFIG_RELAY_STATE_SUPPORTED_ENABLED", "group__CONFIG__MESSAGES.html#gga5aa020cc2fbd721a717e83a87e706381a6430435c74578bc2bc9110d9e158f47f", null ],
      [ "CONFIG_RELAY_STATE_UNSUPPORTED", "group__CONFIG__MESSAGES.html#gga5aa020cc2fbd721a717e83a87e706381ac7ae76f686307c2ddb48be02b3e7dc71", null ]
    ] ],
    [ "config_msg_key_index_24_set", "group__CONFIG__MESSAGES.html#ga163ed6379b410f7b12c07e25cbe150d4", null ],
    [ "config_msg_key_index_24_get", "group__CONFIG__MESSAGES.html#ga319606769b92e4802f163615c37f3cda", null ],
    [ "config_msg_model_id_set", "group__CONFIG__MESSAGES.html#ga32bc2c842a6e7ee8e55025ce33bcbbb2", null ]
];