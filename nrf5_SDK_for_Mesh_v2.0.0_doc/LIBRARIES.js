var LIBRARIES =
[
    [ "Flash manager", "md_doc_libraries_flash_manager.html", null ],
    [ "Serial interface", "md_doc_libraries_serial.html", "md_doc_libraries_serial" ],
    [ "Nordic Advertiser Extensions (Instaburst)", "md_doc_libraries_instaburst.html", null ],
    [ "nRF5 SDK for Mesh Bootloader", "md_mesh_bootloader_README.html", null ]
];