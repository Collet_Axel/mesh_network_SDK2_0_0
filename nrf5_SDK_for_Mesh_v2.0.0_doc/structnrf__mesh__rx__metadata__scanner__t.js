var structnrf__mesh__rx__metadata__scanner__t =
[
    [ "timestamp", "structnrf__mesh__rx__metadata__scanner__t.html#a2f7b06e0b8dda40580e2ba08dca36665", null ],
    [ "access_addr", "structnrf__mesh__rx__metadata__scanner__t.html#a66a7dfcd4d06986ce57b47ccb18b0cf8", null ],
    [ "channel", "structnrf__mesh__rx__metadata__scanner__t.html#a4d2df311d2a160d48d91d1422b5fe1c1", null ],
    [ "rssi", "structnrf__mesh__rx__metadata__scanner__t.html#a2ccdcc880095d351d9c13cd83833cdc2", null ],
    [ "adv_addr", "structnrf__mesh__rx__metadata__scanner__t.html#a042c1f5568dcf6ba438931ef75993b3a", null ],
    [ "adv_type", "structnrf__mesh__rx__metadata__scanner__t.html#a5458e291aa09198b36520a0e85ba1e6e", null ]
];