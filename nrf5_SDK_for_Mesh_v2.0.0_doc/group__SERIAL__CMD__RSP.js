var group__SERIAL__CMD__RSP =
[
    [ "serial_evt_cmd_rsp_data_subnet_t", "structserial__evt__cmd__rsp__data__subnet__t.html", [
      [ "subnet_handle", "structserial__evt__cmd__rsp__data__subnet__t.html#a99df212d619673fd21daa833c1fb01f0", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_subnet_list_t", "structserial__evt__cmd__rsp__data__subnet__list__t.html", [
      [ "subnet_key_index", "structserial__evt__cmd__rsp__data__subnet__list__t.html#a97534fde82fc0dfff2d3b9712ae04021", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_appkey_t", "structserial__evt__cmd__rsp__data__appkey__t.html", [
      [ "appkey_handle", "structserial__evt__cmd__rsp__data__appkey__t.html#a3b5d4954b27abecd1a70fd801b2bd842", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_appkey_list_t", "structserial__evt__cmd__rsp__data__appkey__list__t.html", [
      [ "subnet_handle", "structserial__evt__cmd__rsp__data__appkey__list__t.html#a1a850e9a4796c90fee40937e3870f744", null ],
      [ "appkey_key_index", "structserial__evt__cmd__rsp__data__appkey__list__t.html#a723be17062685f800746cdba334977b3", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_devkey_t", "structserial__evt__cmd__rsp__data__devkey__t.html", [
      [ "devkey_handle", "structserial__evt__cmd__rsp__data__devkey__t.html#a5e782275fef8b1df4f9b358ba01482a3", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_addr_t", "structserial__evt__cmd__rsp__data__addr__t.html", [
      [ "address_handle", "structserial__evt__cmd__rsp__data__addr__t.html#a4891044903bb8d0081fecc1046da7aa4", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_addr_local_unicast_t", "structserial__evt__cmd__rsp__data__addr__local__unicast__t.html", [
      [ "address_start", "structserial__evt__cmd__rsp__data__addr__local__unicast__t.html#a37440ccbed6800afb5f60fb578723ac9", null ],
      [ "count", "structserial__evt__cmd__rsp__data__addr__local__unicast__t.html#a9be6bc98e1f0b2fb993c85f278202343", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_raw_addr_t", "structserial__evt__cmd__rsp__data__raw__addr__t.html", [
      [ "address_handle", "structserial__evt__cmd__rsp__data__raw__addr__t.html#a0ad34b3259ba734d1238eac774cf43c4", null ],
      [ "addr_type", "structserial__evt__cmd__rsp__data__raw__addr__t.html#ae3603a68d18cc166f5262a4ced415cdf", null ],
      [ "subscribed", "structserial__evt__cmd__rsp__data__raw__addr__t.html#acc98b45282264b991703631a61f9db78", null ],
      [ "raw_short_addr", "structserial__evt__cmd__rsp__data__raw__addr__t.html#a9b1007570625d4ba5899f1676638ac3a", null ],
      [ "virtual_uuid", "structserial__evt__cmd__rsp__data__raw__addr__t.html#aaa80219de378349d8df78efd9cdd064b", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_addr_list_t", "structserial__evt__cmd__rsp__data__addr__list__t.html", [
      [ "address_handles", "structserial__evt__cmd__rsp__data__addr__list__t.html#a14efd5c968671f6a2b1bc222b6b6352f", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_list_size_t", "structserial__evt__cmd__rsp__data__list__size__t.html", [
      [ "list_size", "structserial__evt__cmd__rsp__data__list__size__t.html#a3d71f9c35cf8458e6e79af704d5b2854", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_prov_ctx_t", "structserial__evt__cmd__rsp__data__prov__ctx__t.html", [
      [ "context", "structserial__evt__cmd__rsp__data__prov__ctx__t.html#a06509ccefa4eec693430d64c71718ae1", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_serial_version_t", "structserial__evt__cmd__rsp__data__serial__version__t.html", [
      [ "serial_ver", "structserial__evt__cmd__rsp__data__serial__version__t.html#a03c8daad69d2dec309aaeddca06dce6b", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_firmware_info_t", "structserial__evt__cmd__rsp__data__firmware__info__t.html", [
      [ "fwid", "structserial__evt__cmd__rsp__data__firmware__info__t.html#a0e8e710231be08817901dcd6010fc88f", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_adv_addr_t", "structserial__evt__cmd__rsp__data__adv__addr__t.html", [
      [ "addr_type", "structserial__evt__cmd__rsp__data__adv__addr__t.html#a2d0509d95b758f5a19e3644334feb0ae", null ],
      [ "addr", "structserial__evt__cmd__rsp__data__adv__addr__t.html#afa5ac12dc40f83cd41dfd0452d8029c9", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_device_uuid_t", "structserial__evt__cmd__rsp__data__device__uuid__t.html", [
      [ "device_uuid", "structserial__evt__cmd__rsp__data__device__uuid__t.html#af2d2461ba1efed7bedd6bffddba61b56", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_tx_power_t", "structserial__evt__cmd__rsp__data__tx__power__t.html", [
      [ "tx_power", "structserial__evt__cmd__rsp__data__tx__power__t.html#ae977fceac746a65d90647491c0458413", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_beacon_params_t", "structserial__evt__cmd__rsp__data__beacon__params__t.html", [
      [ "beacon_slot", "structserial__evt__cmd__rsp__data__beacon__params__t.html#a8e9b395815b900e680aa0942c480125c", null ],
      [ "tx_power", "structserial__evt__cmd__rsp__data__beacon__params__t.html#ab1be4746fa0ac07ddcba2cae8de7a669", null ],
      [ "channel_map", "structserial__evt__cmd__rsp__data__beacon__params__t.html#a67cdb89672d724bb97ee7739a866417e", null ],
      [ "interval_ms", "structserial__evt__cmd__rsp__data__beacon__params__t.html#ab611313be24fbde06e4361eaae838bd0", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_dfu_bank_info_t", "structserial__evt__cmd__rsp__data__dfu__bank__info__t.html", [
      [ "dfu_type", "structserial__evt__cmd__rsp__data__dfu__bank__info__t.html#acec74ee5d1a9476817dab112b787ee0b", null ],
      [ "fwid", "structserial__evt__cmd__rsp__data__dfu__bank__info__t.html#a2c2c004ffcbef7c1fa09a01be27bdcfe", null ],
      [ "is_signed", "structserial__evt__cmd__rsp__data__dfu__bank__info__t.html#a71ed6fcfe22176330123c90d8eaa8cf8", null ],
      [ "start_addr", "structserial__evt__cmd__rsp__data__dfu__bank__info__t.html#a663b8b0993daaa6667990c2657c8f228", null ],
      [ "length", "structserial__evt__cmd__rsp__data__dfu__bank__info__t.html#af5115a7fccbaa2fc538530d87bc6a565", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_dfu_state_t", "structserial__evt__cmd__rsp__data__dfu__state__t.html", [
      [ "role", "structserial__evt__cmd__rsp__data__dfu__state__t.html#a560747460ed3bd408c53a0deca4592b6", null ],
      [ "type", "structserial__evt__cmd__rsp__data__dfu__state__t.html#a46ffd52ff0974d0c0657e743b5b65db1", null ],
      [ "fwid", "structserial__evt__cmd__rsp__data__dfu__state__t.html#a8963dbef9d1ac6561d807d6f71ade579", null ],
      [ "state", "structserial__evt__cmd__rsp__data__dfu__state__t.html#a80c4f130503cb6e7deb5d22349d88e56", null ],
      [ "data_progress", "structserial__evt__cmd__rsp__data__dfu__state__t.html#a89af5f513cac2c4bf7cc9630a4fabb99", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_model_pub_addr_get_t", "structserial__evt__cmd__rsp__data__model__pub__addr__get__t.html", [
      [ "addr_handle", "structserial__evt__cmd__rsp__data__model__pub__addr__get__t.html#a4abeab261ca440ac5911673f76a20d0d", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_model_pub_app_get_t", "structserial__evt__cmd__rsp__data__model__pub__app__get__t.html", [
      [ "appkey_handle", "structserial__evt__cmd__rsp__data__model__pub__app__get__t.html#ac90b15049dd328f5004c97e9e8184f3f", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_model_pub_period_get_t", "structserial__evt__cmd__rsp__data__model__pub__period__get__t.html", [
      [ "resolution", "structserial__evt__cmd__rsp__data__model__pub__period__get__t.html#ac485d5188762c4024d52031f34571fac", null ],
      [ "step_number", "structserial__evt__cmd__rsp__data__model__pub__period__get__t.html#a13a41d4480e5d6172227f37374d2ad01", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_model_subs_get_t", "structserial__evt__cmd__rsp__data__model__subs__get__t.html", [
      [ "count", "structserial__evt__cmd__rsp__data__model__subs__get__t.html#ab6bab71cc322c9bfd5fbfdd08f9026c3", null ],
      [ "address_handles", "structserial__evt__cmd__rsp__data__model__subs__get__t.html#a7a3100acce1bf3782c21c4b8ffd449b4", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_model_apps_get_t", "structserial__evt__cmd__rsp__data__model__apps__get__t.html", [
      [ "count", "structserial__evt__cmd__rsp__data__model__apps__get__t.html#a90905c7168f6aa4f3520dfd8e3f7dec1", null ],
      [ "appkey_handles", "structserial__evt__cmd__rsp__data__model__apps__get__t.html#ab914d5b90738d72458ae2686bc1070e6", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_model_pub_ttl_get_t", "structserial__evt__cmd__rsp__data__model__pub__ttl__get__t.html", [
      [ "ttl", "structserial__evt__cmd__rsp__data__model__pub__ttl__get__t.html#a05cad8312ce57ba6baf61f8f29095403", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_elem_loc_get_t", "structserial__evt__cmd__rsp__data__elem__loc__get__t.html", [
      [ "location", "structserial__evt__cmd__rsp__data__elem__loc__get__t.html#ab92d53c5ddb6a8cbb638f4e9e1f0606c", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_elem_model_count_get_t", "structserial__evt__cmd__rsp__data__elem__model__count__get__t.html", [
      [ "model_count", "structserial__evt__cmd__rsp__data__elem__model__count__get__t.html#a02f488ea1231fdf467a7ad0645fe66a0", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_model_id_get_t", "structserial__evt__cmd__rsp__data__model__id__get__t.html", [
      [ "model_id", "structserial__evt__cmd__rsp__data__model__id__get__t.html#a1324e5e10de6c7b8f8366cec7c03ee35", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_model_handle_get_t", "structserial__evt__cmd__rsp__data__model__handle__get__t.html", [
      [ "model_handle", "structserial__evt__cmd__rsp__data__model__handle__get__t.html#af97aa29f8fead76807aeece51ff329c9", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_elem_models_get_t", "structserial__evt__cmd__rsp__data__elem__models__get__t.html", [
      [ "count", "structserial__evt__cmd__rsp__data__elem__models__get__t.html#ae60e9007a4957d16b80bef0ad84b2b8c", null ],
      [ "model_handles", "structserial__evt__cmd__rsp__data__elem__models__get__t.html#ae9fb23c0a61eb46b3c12f1685afc9661", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_models_get_t", "structserial__evt__cmd__rsp__data__models__get__t.html", [
      [ "count", "structserial__evt__cmd__rsp__data__models__get__t.html#a734a5131651e2e5dbd0ca18d98ab1dc7", null ],
      [ "model_ids", "structserial__evt__cmd__rsp__data__models__get__t.html#ae609103e5891a02befa00f9cd155b056", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_model_init_t", "structserial__evt__cmd__rsp__data__model__init__t.html", [
      [ "model_handle", "structserial__evt__cmd__rsp__data__model__init__t.html#a7628ed231e5b519e44d6980380414330", null ]
    ] ],
    [ "serial_evt_cmd_rsp_data_model_cmd_t", "structserial__evt__cmd__rsp__data__model__cmd__t.html", [
      [ "data_len", "structserial__evt__cmd__rsp__data__model__cmd__t.html#a8f5c259d79657e454ad23bf0b33a5a4f", null ],
      [ "data", "structserial__evt__cmd__rsp__data__model__cmd__t.html#af707e00d0351ce9e8594ee0e0c415fcd", null ]
    ] ],
    [ "serial_evt_cmd_rsp_t", "structserial__evt__cmd__rsp__t.html", [
      [ "opcode", "structserial__evt__cmd__rsp__t.html#a4368eed69ffae41fa6619b019344a1d5", null ],
      [ "status", "structserial__evt__cmd__rsp__t.html#ade1bcf3b877600e7e80e4d12cc3d0ff6", null ],
      [ "subnet", "structserial__evt__cmd__rsp__t.html#a4de7164dbac3f2fb19bed160f00343c5", null ],
      [ "subnet_list", "structserial__evt__cmd__rsp__t.html#a7f68f03e2f77d6687d020628231a30b0", null ],
      [ "appkey", "structserial__evt__cmd__rsp__t.html#a7c5e379c9a48b264b698de49a90a4137", null ],
      [ "appkey_list", "structserial__evt__cmd__rsp__t.html#a352f957fb5e827dc764c69d001722cba", null ],
      [ "devkey", "structserial__evt__cmd__rsp__t.html#a85243eec2e1a25b061e39a479ac0498c", null ],
      [ "local_unicast", "structserial__evt__cmd__rsp__t.html#a7fc795f363cc736bb96beca9e5134f40", null ],
      [ "addr", "structserial__evt__cmd__rsp__t.html#a15b05a3136ea6d80a34d076bbdf4ebb2", null ],
      [ "list_size", "structserial__evt__cmd__rsp__t.html#ab64edc908c69aa36a1b3150a2f0736d5", null ],
      [ "adv_addr", "structserial__evt__cmd__rsp__t.html#ae2b676cee6d799cb3ef2eead2660af6e", null ],
      [ "prov_ctx", "structserial__evt__cmd__rsp__t.html#ad149cd2337a589279f4d41f0bd946e88", null ],
      [ "firmware_info", "structserial__evt__cmd__rsp__t.html#a464cd7b1d594e31d762227af4f5bcc4d", null ],
      [ "serial_version", "structserial__evt__cmd__rsp__t.html#a35282b246a8d5bb7dc5453aa0a81a7fe", null ],
      [ "device_uuid", "structserial__evt__cmd__rsp__t.html#ac19a451d212a632a751af3c599c6389a", null ],
      [ "beacon_params", "structserial__evt__cmd__rsp__t.html#ab594d9b94e3f117484e36158724017d4", null ],
      [ "dfu_bank_info", "structserial__evt__cmd__rsp__t.html#a4583f5bfc3a5d7ec568deb9b587d0cc7", null ],
      [ "dfu_state", "structserial__evt__cmd__rsp__t.html#ac5e582c8a82297a34b06c0c9b7f7f166", null ],
      [ "pub_addr", "structserial__evt__cmd__rsp__t.html#a85d09a66209a0b05537b7a88e0bffc85", null ],
      [ "pub_app", "structserial__evt__cmd__rsp__t.html#a0afd1343d1eb32ea243d0753be37963a", null ],
      [ "pub_period", "structserial__evt__cmd__rsp__t.html#a9b17f5a0340a1d7e15dbb8dc604bc901", null ],
      [ "model_subs", "structserial__evt__cmd__rsp__t.html#a27b748faf41512de6915ce343731e0ae", null ],
      [ "model_pub", "structserial__evt__cmd__rsp__t.html#a4e57921d6942bb36f3433c46f9651e14", null ],
      [ "pub_ttl", "structserial__evt__cmd__rsp__t.html#a8afab87dcc8b88526937b6adc9b87689", null ],
      [ "elem_loc", "structserial__evt__cmd__rsp__t.html#acdb220d1814b596c28eb947e9d54d441", null ],
      [ "model_count", "structserial__evt__cmd__rsp__t.html#acdb54855f09a4cafdbae5e07433ee5a1", null ],
      [ "model_id", "structserial__evt__cmd__rsp__t.html#afe19b84a402170131b77d887750134a6", null ],
      [ "model_handle", "structserial__evt__cmd__rsp__t.html#acf7857095b6516cf9dd63820b119b6c7", null ],
      [ "model_handles", "structserial__evt__cmd__rsp__t.html#a75f395289adae515f4a08516cad927da", null ],
      [ "model_ids", "structserial__evt__cmd__rsp__t.html#a409a5b4e4abb9ead4e073bc63c5a3f3c", null ],
      [ "model_init", "structserial__evt__cmd__rsp__t.html#ada9acec3c30543538df3b8b155e679ab", null ],
      [ "data", "structserial__evt__cmd__rsp__t.html#a3bacd716105e99f538766aebe6a6c760", null ]
    ] ],
    [ "SERIAL_EVT_CMD_RSP_OVERHEAD", "group__SERIAL__CMD__RSP.html#ga4489000531d11261949cdab02ecbab0c", null ],
    [ "SERIAL_EVT_CMD_RSP_LEN_OVERHEAD", "group__SERIAL__CMD__RSP.html#gae5894aea689ecd6f11b87d6abf77c661", null ],
    [ "SERIAL_EVT_CMD_RSP_DATA_MAXLEN", "group__SERIAL__CMD__RSP.html#ga3cffdc0b5bd2627589e28c43e97622b0", null ]
];