var group__ADVERTISER =
[
    [ "adv_packet_t", "structadv__packet__t.html", [
      [ "__attribute__", "structadv__packet__t.html#aee914860b49ec6de735433859681b22a", null ],
      [ "token", "structadv__packet__t.html#a30423a6e355ac58ef07c8e7525ae6b96", null ],
      [ "repeats", "structadv__packet__t.html#ac29252c648ad54ec728c727f535b88da", null ],
      [ "config", "structadv__packet__t.html#a77e11b8d2ae5f433fe429d6f197b6db6", null ]
    ] ],
    [ "advertiser_channels_t", "structadvertiser__channels__t.html", [
      [ "channel_map", "structadvertiser__channels__t.html#aff4b07da92a46f466b88fee1f4f2dbe2", null ],
      [ "count", "structadvertiser__channels__t.html#a429f85b800bb2df269373930231ce726", null ],
      [ "randomize_order", "structadvertiser__channels__t.html#a7e41cc732db927658a7064472707f4b5", null ]
    ] ],
    [ "advertiser_config_t", "structadvertiser__config__t.html", [
      [ "adv_addr", "structadvertiser__config__t.html#a4e1fb09d314078fc74e36b1f1bda9d0d", null ],
      [ "advertisement_interval_us", "structadvertiser__config__t.html#aaca9dd0590bc8134f0155d7922440ddb", null ],
      [ "channels", "structadvertiser__config__t.html#ad501804948439e6e212331fbc52ea9a4", null ]
    ] ],
    [ "advertiser_tx_complete_params_t", "structadvertiser__tx__complete__params__t.html", [
      [ "token", "structadvertiser__tx__complete__params__t.html#ae89f295cb6c46e56c86ecaf49be7e8e7", null ],
      [ "timestamp", "structadvertiser__tx__complete__params__t.html#a9ee11f2b56d0acaff9a13921a06bad05", null ]
    ] ],
    [ "advertiser_t", "structadvertiser__t.html", [
      [ "enabled", "structadvertiser__t.html#a9e5ac2e925c56132c562dc6fe0645114", null ],
      [ "p_packet", "structadvertiser__t.html#adf56fc3f57eed500720c7ac5656571f5", null ],
      [ "broadcast", "structadvertiser__t.html#a00fff2f75b74795214009bcee7b1eba6", null ],
      [ "timer", "structadvertiser__t.html#a7025562e2da9be7263dbe25dc25309f0", null ],
      [ "buf", "structadvertiser__t.html#a026b1cbb7c81ad5492b4cd5c269fda9a", null ],
      [ "config", "structadvertiser__t.html#a5b860c53204ea07650f28f13d904a3e3", null ],
      [ "tx_complete_callback", "structadvertiser__t.html#ac8dd9d0bd26425a569676d89aad8581b", null ],
      [ "tx_complete_event", "structadvertiser__t.html#a63820f38d27d513b985339a32bbf99fa", null ],
      [ "tx_complete_params", "structadvertiser__t.html#a065d5100a98f4fc6f27befafcb8598c7", null ]
    ] ],
    [ "ADVERTISER_INTERVAL_RANDOMIZATION_US", "group__ADVERTISER.html#gabe23198e8bf4d087ae82a65f2c5cc155", null ],
    [ "ADVERTISER_REPEAT_INFINITE", "group__ADVERTISER.html#gaba99e61c7d534ca477a534bedd3bbff7", null ],
    [ "ADVERTISER_PACKET_BUFFER_PACKET_MAXLEN", "group__ADVERTISER.html#ga3b26f6f6c5836653e0feceeccadb3c7c", null ],
    [ "advertiser_tx_complete_cb_t", "group__ADVERTISER.html#gafc00278c43de68312b06117cf361e26e", null ],
    [ "advertiser_init", "group__ADVERTISER.html#gab4ffe4e8cbd4a6a9b6d934724c33f307", null ],
    [ "advertiser_instance_init", "group__ADVERTISER.html#ga681f51857cf41a5d53e444f6a4cde019", null ],
    [ "advertiser_enable", "group__ADVERTISER.html#ga67fc11bc52f462cf8955d3d69ae18ed5", null ],
    [ "advertiser_disable", "group__ADVERTISER.html#gad696145a57d058a0513b506c2a550227", null ],
    [ "advertiser_packet_alloc", "group__ADVERTISER.html#ga4601d8abaaa026f07eee7f47ca5c8884", null ],
    [ "advertiser_packet_send", "group__ADVERTISER.html#ga328f190211352da52c5532f5012fa715", null ],
    [ "advertiser_packet_discard", "group__ADVERTISER.html#gac9dc8211b0f457901e1345a63a943831", null ],
    [ "advertiser_config_set", "group__ADVERTISER.html#ga1471d5ceeaf0c8f5049a951c535935aa", null ],
    [ "advertiser_config_get", "group__ADVERTISER.html#ga126cd74cdd43cf425dcb8f38026bf92a", null ],
    [ "advertiser_channels_set", "group__ADVERTISER.html#ga4a29a70f656674c2f7cceb62f8ca2276", null ],
    [ "advertiser_address_set", "group__ADVERTISER.html#ga7dabfed3498003283866266f7a88efd5", null ],
    [ "advertiser_interval_set", "group__ADVERTISER.html#ga8b7a48a4f29fadc395ab1b2cdc983d16", null ],
    [ "advertiser_flush", "group__ADVERTISER.html#ga7abbea3e084cdc11edb18a8fd0e3492f", null ],
    [ "advertiser_address_default_get", "group__ADVERTISER.html#ga27465db9406100655a648707dfbf7582", null ]
];