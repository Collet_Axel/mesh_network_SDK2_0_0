var index =
[
    [ "Release Notes", "md_RELEASE_NOTES.html", null ],
    [ "Migration guide", "md_MIGRATION_GUIDE.html", "md_MIGRATION_GUIDE" ],
    [ "License", "md_COPYING.html", null ],
    [ "Resource usage", "md_doc_introduction_mesh_hw_resources.html", null ],
    [ "Quick Start Guide for the nRF5 SDK for Mesh", "md_doc_getting_started_mesh_quick_start.html", null ],
    [ "Basic Bluetooth Mesh concepts", "md_doc_introduction_basic_concepts.html", null ],
    [ "The nRF5 SDK for Mesh architecture", "md_doc_introduction_basic_architecture.html", null ]
];