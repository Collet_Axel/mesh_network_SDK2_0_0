var group__COMPOSITION__DATA =
[
    [ "config_composition_data_header_t", "structconfig__composition__data__header__t.html", [
      [ "company_id", "structconfig__composition__data__header__t.html#a7a8562bd939ded880f97c52fbb32aa68", null ],
      [ "product_id", "structconfig__composition__data__header__t.html#ad4cf2f39486d0623cd4f9766fbd7cc8f", null ],
      [ "version_id", "structconfig__composition__data__header__t.html#a8e2be02952eee01c71a720bd8ca07aec", null ],
      [ "replay_cache_entries", "structconfig__composition__data__header__t.html#ab6105ece9d4c1d6597cc5ae4adc5d592", null ],
      [ "features", "structconfig__composition__data__header__t.html#a44417cf570ceaa9b85e0beb9b85ec574", null ]
    ] ],
    [ "config_composition_element_header_t", "structconfig__composition__element__header__t.html", [
      [ "location", "structconfig__composition__element__header__t.html#adaa6ad21048352c9862d168b95013075", null ],
      [ "sig_model_count", "structconfig__composition__element__header__t.html#a1aeca6401eacee57337dffedb4482b23", null ],
      [ "vendor_model_count", "structconfig__composition__element__header__t.html#a2135fc56bd3ad468aab975ab797c7a42", null ]
    ] ],
    [ "CONFIG_SIG_MODEL_ID_SIZE", "group__COMPOSITION__DATA.html#ga36ad94964c9a7205f68975b422fcaa0f", null ],
    [ "CONFIG_VENDOR_MODEL_ID_SIZE", "group__COMPOSITION__DATA.html#gab196233392ead90fe32a39b66acb5185", null ],
    [ "CONFIG_MODEL_ID_SIZE_MAX", "group__COMPOSITION__DATA.html#gaf4cc9b3fcabd6a0e435925c550b76cb6", null ],
    [ "COMPOSITION_DATA_LENGTH_MIN", "group__COMPOSITION__DATA.html#ga4fce3afd6f840caa325265aa8d1ce325", null ],
    [ "CONFIG_COMPOSITION_DATA_SIZE", "group__COMPOSITION__DATA.html#ga0c04b9aae52594a95928eed6f04b83c2", null ],
    [ "config_feature_bit_t", "group__COMPOSITION__DATA.html#gafb682a1f05c52012dd21c82a233b0643", [
      [ "CONFIG_FEATURE_RELAY_BIT", "group__COMPOSITION__DATA.html#ggafb682a1f05c52012dd21c82a233b0643ac100874fdb6a602588bf1c648327fd62", null ],
      [ "CONFIG_FEATURE_PROXY_BIT", "group__COMPOSITION__DATA.html#ggafb682a1f05c52012dd21c82a233b0643acce2575bb2e76ba4e4428d0d4b950609", null ],
      [ "CONFIG_FEATURE_FRIEND_BIT", "group__COMPOSITION__DATA.html#ggafb682a1f05c52012dd21c82a233b0643a57df2c2fb391e4e29db5120a2d75a519", null ],
      [ "CONFIG_FEATURE_LOW_POWER_BIT", "group__COMPOSITION__DATA.html#ggafb682a1f05c52012dd21c82a233b0643a472d09778a51c036f87f0d010f45a911", null ]
    ] ],
    [ "config_composition_data_get", "group__COMPOSITION__DATA.html#gac2474cbce1c87df7e938031ebd56e86e", null ]
];