var group__SIMPLE__HAL =
[
    [ "HAL_BUTTON_PRESS_FREQUENCY", "group__SIMPLE__HAL.html#gad35dbf25fe515a3cbe2ff60f5748ef98", null ],
    [ "LED_MASK_STATE_OFF", "group__SIMPLE__HAL.html#gac8c80f58d81f844dc8e2b6b3ba6b5856", null ],
    [ "LED_MASK_STATE_ON", "group__SIMPLE__HAL.html#gab7873c6ae8f2e9f0feb7b0626ff45729", null ],
    [ "BUTTON_BOARD", "group__SIMPLE__HAL.html#gae4c76abb83a70bc7681774278fca9d9d", null ],
    [ "hal_button_handler_cb_t", "group__SIMPLE__HAL.html#ga21640155134bfe70300d7cedfa636e18", null ],
    [ "hal_leds_init", "group__SIMPLE__HAL.html#ga50bf04438972d9ce1bbe68d3bc6a38c1", null ],
    [ "hal_buttons_init", "group__SIMPLE__HAL.html#gaed609b8e09350fce3bdec14710e4bd92", null ],
    [ "hal_led_pin_set", "group__SIMPLE__HAL.html#gae024b8279e41238fb744940efea83d77", null ],
    [ "hal_led_mask_set", "group__SIMPLE__HAL.html#ga9ce26ce37b84c57870a1b8aff30b49a6", null ],
    [ "hal_led_pin_get", "group__SIMPLE__HAL.html#gac1847699138008279bc3b3909d384cd0", null ],
    [ "hal_led_blink_ms", "group__SIMPLE__HAL.html#ga34e6b59f4e756a1468a68d71f06ba202", null ]
];