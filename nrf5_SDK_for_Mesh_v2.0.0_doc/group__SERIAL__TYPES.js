var group__SERIAL__TYPES =
[
    [ "serial_cmd_model_specific_init_header_t", "structserial__cmd__model__specific__init__header__t.html", [
      [ "model_id", "structserial__cmd__model__specific__init__header__t.html#a12e5e7d63a70b0aadb81c7105ebc1b62", null ],
      [ "element_index", "structserial__cmd__model__specific__init__header__t.html#a0a8b04f860b45b84240ccc178cc00305", null ]
    ] ],
    [ "serial_cmd_model_specific_command_header_t", "structserial__cmd__model__specific__command__header__t.html", [
      [ "model_handle", "structserial__cmd__model__specific__command__header__t.html#a58eb49a95cd351aded5ceb01067c2dba", null ]
    ] ],
    [ "nrf_mesh_serial_app_rx_cb_t", "group__SERIAL__TYPES.html#ga3ae16f1560b20dec9d46ef9f449c7d83", null ],
    [ "nrf_mesh_serial_state_t", "group__SERIAL__TYPES.html#ga1bb8d28391c3b0cade9b17cc01c20bd4", [
      [ "NRF_MESH_SERIAL_STATE_UNINITIALIZED", "group__SERIAL__TYPES.html#gga1bb8d28391c3b0cade9b17cc01c20bd4a1dec9a64601e013e1f90a7a7cd00e36e", null ],
      [ "NRF_MESH_SERIAL_STATE_INITIALIZED", "group__SERIAL__TYPES.html#gga1bb8d28391c3b0cade9b17cc01c20bd4ac9933ad7e3026129f0602da1ab254e8d", null ],
      [ "NRF_MESH_SERIAL_STATE_RUNNING", "group__SERIAL__TYPES.html#gga1bb8d28391c3b0cade9b17cc01c20bd4aff33a0230557bd760d728de22f9bdfa3", null ]
    ] ],
    [ "serial_device_operating_mode_t", "group__SERIAL__TYPES.html#ga6d0cb2bde467ddd571b2452948004c54", [
      [ "SERIAL_DEVICE_OPERATING_MODE_TEST", "group__SERIAL__TYPES.html#gga6d0cb2bde467ddd571b2452948004c54ae194cb728ef2183823cc089f4667f41e", null ],
      [ "SERIAL_DEVICE_OPERATING_MODE_BOOTLOADER", "group__SERIAL__TYPES.html#gga6d0cb2bde467ddd571b2452948004c54a31ce74230879bae755a8adbb1e98d516", null ],
      [ "SERIAL_DEVICE_OPERATING_MODE_APPLICATION", "group__SERIAL__TYPES.html#gga6d0cb2bde467ddd571b2452948004c54afdfe479ec1c2cb73403a65a5bd67f1cb", null ]
    ] ],
    [ "serial_cmd_tx_power_value_t", "group__SERIAL__TYPES.html#gabdc141be30dd418d10197516cc152b9b", [
      [ "SERIAL_CMD_TX_POWER_VALUE_0dBm", "group__SERIAL__TYPES.html#ggabdc141be30dd418d10197516cc152b9ba3c80d910f1b5ce5713857947a5ffb673", null ],
      [ "SERIAL_CMD_TX_POWER_VALUE_Pos4dBm", "group__SERIAL__TYPES.html#ggabdc141be30dd418d10197516cc152b9ba244cea7428b6c9099e377a3a517565cf", null ],
      [ "SERIAL_CMD_TX_POWER_VALUE_Neg30dBm", "group__SERIAL__TYPES.html#ggabdc141be30dd418d10197516cc152b9ba72207cd6b43678588f946d3af59e23d1", null ],
      [ "SERIAL_CMD_TX_POWER_VALUE_Neg20dBm", "group__SERIAL__TYPES.html#ggabdc141be30dd418d10197516cc152b9bafc378965efa3225fbde371c2201a1c29", null ],
      [ "SERIAL_CMD_TX_POWER_VALUE_Neg16dBm", "group__SERIAL__TYPES.html#ggabdc141be30dd418d10197516cc152b9baef2d4e6adb99baa33ea43ab6595c8db5", null ],
      [ "SERIAL_CMD_TX_POWER_VALUE_Neg12dBm", "group__SERIAL__TYPES.html#ggabdc141be30dd418d10197516cc152b9ba66f7fc8b43209cdd5d0b1f1339d16f4c", null ],
      [ "SERIAL_CMD_TX_POWER_VALUE_Neg8dBm", "group__SERIAL__TYPES.html#ggabdc141be30dd418d10197516cc152b9bace90f5f056488eab3cf2dd7e5c830b6d", null ],
      [ "SERIAL_CMD_TX_POWER_VALUE_Neg4dBm", "group__SERIAL__TYPES.html#ggabdc141be30dd418d10197516cc152b9ba3c927292ed5a2ae4dc2bb98b40dda1c1", null ]
    ] ]
];