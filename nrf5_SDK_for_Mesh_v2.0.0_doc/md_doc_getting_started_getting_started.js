var md_doc_getting_started_getting_started =
[
    [ "Installing the toolchain", "md_doc_getting_started_how_to_toolchain.html", "md_doc_getting_started_how_to_toolchain" ],
    [ "Building the mesh stack and examples", "md_doc_getting_started_how_to_build.html", "md_doc_getting_started_how_to_build" ],
    [ "DFU quick start guide", "md_doc_getting_started_dfu_quick_start.html", null ],
    [ "Interrupt priority levels", "md_doc_getting_started_mesh_interrupt_priorities.html", null ],
    [ "Provisioning process and APIs", "md_doc_getting_started_provisioning.html", null ],
    [ "Creating new models", "md_doc_getting_started_how_to_models.html", null ],
    [ "Coexistence with nRF5 SDK BLE functionality", "md_doc_getting_started_how_to_nordicSDK.html", null ]
];