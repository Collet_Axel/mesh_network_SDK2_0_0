var structserial__evt__mesh__message__received__t =
[
    [ "src", "structserial__evt__mesh__message__received__t.html#a69b8a40729617f09d7454c623e1e25f3", null ],
    [ "dst", "structserial__evt__mesh__message__received__t.html#a0611e6e4317820c1103ece48d37aa20a", null ],
    [ "appkey_handle", "structserial__evt__mesh__message__received__t.html#ab8755221a70c086cc8364770561a5086", null ],
    [ "subnet_handle", "structserial__evt__mesh__message__received__t.html#a5f4caababe4c9f8658c186494e09621a", null ],
    [ "ttl", "structserial__evt__mesh__message__received__t.html#adc696eca2e3aea31fa89a622a9d4e789", null ],
    [ "adv_addr_type", "structserial__evt__mesh__message__received__t.html#a00f4083eb360327d51d4e1da077df667", null ],
    [ "adv_addr", "structserial__evt__mesh__message__received__t.html#a46b15657c39d524e99a9e06617456787", null ],
    [ "rssi", "structserial__evt__mesh__message__received__t.html#af300d4e98e84c2426b69043a6fadf0ce", null ],
    [ "actual_length", "structserial__evt__mesh__message__received__t.html#afde163ec54c25b6a65c46845813b2f9c", null ],
    [ "data", "structserial__evt__mesh__message__received__t.html#a4e29e3adaf7d2ccfc0a5257e93d2186c", null ]
];