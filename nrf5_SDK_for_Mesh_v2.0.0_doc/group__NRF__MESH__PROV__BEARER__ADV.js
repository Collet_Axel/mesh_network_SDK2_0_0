var group__NRF__MESH__PROV__BEARER__ADV =
[
    [ "nrf_mesh_prov_bearer_adv_t", "structnrf__mesh__prov__bearer__adv__t.html", [
      [ "__attribute__", "structnrf__mesh__prov__bearer__adv__t.html#ad85734e037160445dff901a52065afcc", null ],
      [ "advertiser", "structnrf__mesh__prov__bearer__adv__t.html#aa46aa4a15b7a33afe2eabbe00f7b5e18", null ],
      [ "link_id", "structnrf__mesh__prov__bearer__adv__t.html#af0ae902a60c4de06d696a9dd195a69e9", null ],
      [ "instance_state", "structnrf__mesh__prov__bearer__adv__t.html#afff0e2087a3408c11aa8e619285f885c", null ],
      [ "state", "structnrf__mesh__prov__bearer__adv__t.html#aa49176874d018915068939538d64ec15", null ],
      [ "transaction_in", "structnrf__mesh__prov__bearer__adv__t.html#ae1c2a1804080d59255cb8375b85ce81f", null ],
      [ "transaction_out", "structnrf__mesh__prov__bearer__adv__t.html#a04cb8c7a015d012f6469294d97188df1", null ],
      [ "close_reason", "structnrf__mesh__prov__bearer__adv__t.html#aa47d26d124b6a850594bf3284a35e2c1", null ],
      [ "timeout_event", "structnrf__mesh__prov__bearer__adv__t.html#a33da8de06131e2dc71cbaad76a7d52c3", null ],
      [ "link_timeout_event", "structnrf__mesh__prov__bearer__adv__t.html#a4dcf2434723f1429c4057cff8c84674b", null ],
      [ "sar_timeout", "structnrf__mesh__prov__bearer__adv__t.html#a3bc82a226efa97718b6de61b3f3687f7", null ],
      [ "link_timeout", "structnrf__mesh__prov__bearer__adv__t.html#aecc55a33b1a9ed18b1acce9649ff95d2", null ],
      [ "buffer", "structnrf__mesh__prov__bearer__adv__t.html#acf5a49878bfc5b4f82d721f667516e2a", null ],
      [ "last_token", "structnrf__mesh__prov__bearer__adv__t.html#aa3d8aefd65050ab7822518f5954d67cc", null ],
      [ "queue_empty_pending", "structnrf__mesh__prov__bearer__adv__t.html#ac15e2c7fd9dad36f401323d01a5ac1ae", null ],
      [ "prov_bearer", "structnrf__mesh__prov__bearer__adv__t.html#a44cf058bb60ebea1534317dac0c9a269", null ],
      [ "p_next", "structnrf__mesh__prov__bearer__adv__t.html#a54baaf628557445007c2c5ff86568d49", null ]
    ] ],
    [ "nrf_mesh_prov_bearer_adv_interface_get", "group__NRF__MESH__PROV__BEARER__ADV.html#ga7305aadb052c422899d50e2f38f10984", null ]
];