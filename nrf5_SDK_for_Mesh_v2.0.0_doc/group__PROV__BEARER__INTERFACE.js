var group__PROV__BEARER__INTERFACE =
[
    [ "prov_bearer_interface_t", "structprov__bearer__interface__t.html", [
      [ "tx", "structprov__bearer__interface__t.html#a39b6e1c4fde7f4e9826e8182181d7eac", null ],
      [ "listen_start", "structprov__bearer__interface__t.html#a3cc41e9bd9d09d4825c59fa0ad07decb", null ],
      [ "listen_stop", "structprov__bearer__interface__t.html#acd273592fa83953db49d879b9fafa0a4", null ],
      [ "link_open", "structprov__bearer__interface__t.html#a1f9808ab85190556560e38f8a327eef6", null ],
      [ "link_close", "structprov__bearer__interface__t.html#a770e69917e01ae3f9d35e13c13cd2e62", null ]
    ] ],
    [ "prov_bearer_if_tx_t", "group__PROV__BEARER__INTERFACE.html#gaa687325f7b5fd79fdde1762489444f0d", null ],
    [ "prov_bearer_if_listen_start_t", "group__PROV__BEARER__INTERFACE.html#ga74b46964906181d515a439e2aa34f5c2", null ],
    [ "prov_bearer_if_listen_stop_t", "group__PROV__BEARER__INTERFACE.html#gaa60ed9b2d35acc5305ff7b89306cef96", null ],
    [ "prov_bearer_if_link_open_t", "group__PROV__BEARER__INTERFACE.html#gac52e853dfcad3f8f6359f59432ddd1d3", null ],
    [ "prov_bearer_if_link_close_t", "group__PROV__BEARER__INTERFACE.html#ga50b5452e7c96bc993ba54250b95ab590", null ]
];