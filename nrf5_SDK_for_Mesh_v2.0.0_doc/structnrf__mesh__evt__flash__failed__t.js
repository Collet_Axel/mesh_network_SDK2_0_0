var structnrf__mesh__evt__flash__failed__t =
[
    [ "user", "structnrf__mesh__evt__flash__failed__t.html#a7b1bf5997e112b2b917aab911808c9e6", null ],
    [ "p_flash_entry", "structnrf__mesh__evt__flash__failed__t.html#aa40efcde01861482a343734fc8ac8c3f", null ],
    [ "p_flash_page", "structnrf__mesh__evt__flash__failed__t.html#aa5ba14c8ce7442b7545ae5eacb984ff8", null ],
    [ "p_area", "structnrf__mesh__evt__flash__failed__t.html#ae0fd44ef2adb87cd222182df3d5f3e1b", null ],
    [ "page_count", "structnrf__mesh__evt__flash__failed__t.html#ae150ac4476a12b867f2b8dd72187d731", null ]
];