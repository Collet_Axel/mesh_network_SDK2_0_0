var searchData=
[
  ['scanner_5frx_5fcallback_5ft',['scanner_rx_callback_t',['../group__SCANNER.html#gaa7681a5af3ab29d9ab2a5923ccbedc99',1,'scanner.h']]],
  ['serial_5fhandler_5fcommon_5fcmd_5fcb_5ft',['serial_handler_common_cmd_cb_t',['../group__SERIAL__HANDLER__COMMON.html#ga27c8b2bc28b0bb4610d803f34114e504',1,'serial_handler_common.h']]],
  ['serial_5fhandler_5fmodels_5fmodel_5fcommand_5fcb_5ft',['serial_handler_models_model_command_cb_t',['../group__SERIAL__HANDLER__MODELS.html#ga85f226df835633c39bf59184a6038427',1,'serial_handler_models.h']]],
  ['serial_5fhandler_5fmodels_5fmodel_5finit_5fcb_5ft',['serial_handler_models_model_init_cb_t',['../group__SERIAL__HANDLER__MODELS.html#ga3f0097e877f99c985ac05f35283826e1',1,'serial_handler_models.h']]],
  ['serial_5fuart_5frx_5fcb_5ft',['serial_uart_rx_cb_t',['../group__SERIAL__UART.html#ga5709a3fcece4befe8cebef37759d2db0',1,'serial_uart.h']]],
  ['serial_5fuart_5ftx_5fcb_5ft',['serial_uart_tx_cb_t',['../group__SERIAL__UART.html#ga1d236568ff3f231bf85b94786b8591ba',1,'serial_uart.h']]],
  ['simple_5fon_5foff_5fget_5fcb_5ft',['simple_on_off_get_cb_t',['../group__SIMPLE__ON__OFF__SERVER.html#ga6c45931ded7d3e068213337e5e3ec7a2',1,'simple_on_off_server.h']]],
  ['simple_5fon_5foff_5fset_5fcb_5ft',['simple_on_off_set_cb_t',['../group__SIMPLE__ON__OFF__SERVER.html#gaab88d1a3c386beab817cf433e21c2c16',1,'simple_on_off_server.h']]],
  ['simple_5fon_5foff_5fstatus_5fcb_5ft',['simple_on_off_status_cb_t',['../group__SIMPLE__ON__OFF__CLIENT.html#ga7083bbfd583bae2e31bbaf8d0f0d5166',1,'simple_on_off_client.h']]],
  ['simple_5fon_5foff_5ftimeout_5fcb_5ft',['simple_on_off_timeout_cb_t',['../group__SIMPLE__ON__OFF__CLIENT.html#gac938c97e29a9fca24f83dc521543bf64',1,'simple_on_off_client.h']]]
];
