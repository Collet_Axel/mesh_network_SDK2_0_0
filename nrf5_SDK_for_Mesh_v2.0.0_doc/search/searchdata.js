var indexSectionsWithContent =
{
  0: "_abcdefghiklmnopqrstuvw",
  1: "_acdfhimnps",
  2: "_abcdefhimnprs",
  3: "abcdefghiklmnopqrstuvw",
  4: "acdfhimnprs",
  5: "acfhinps",
  6: "acfhinps",
  7: "abcdefghiklmnoprstu",
  8: "bcdefgilmnpqrst"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "functions",
  3: "variables",
  4: "typedefs",
  5: "enums",
  6: "enumvalues",
  7: "groups",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Functions",
  3: "Variables",
  4: "Typedefs",
  5: "Enumerations",
  6: "Enumerator",
  7: "Modules",
  8: "Pages"
};

