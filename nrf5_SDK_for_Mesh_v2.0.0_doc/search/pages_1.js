var searchData=
[
  ['cmake_20on_20debian_2fubuntu',['CMake on Debian/Ubuntu',['../md_doc_getting_started_how_to_debian_tools.html',1,'md_doc_getting_started_how_to_toolchain']]],
  ['creating_20new_20models',['Creating new models',['../md_doc_getting_started_how_to_models.html',1,'md_doc_getting_started_getting_started']]],
  ['coexistence_20with_20nrf5_20sdk_20ble_20functionality',['Coexistence with nRF5 SDK BLE functionality',['../md_doc_getting_started_how_to_nordicSDK.html',1,'md_doc_getting_started_getting_started']]],
  ['cmake_20on_20windows',['CMake on Windows',['../md_doc_getting_started_how_to_windows_tools.html',1,'md_doc_getting_started_how_to_toolchain']]],
  ['common_20example_20modules',['Common example modules',['../md_examples_common_README.html',1,'md_examples_README']]]
];
