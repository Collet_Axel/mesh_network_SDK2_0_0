var structinstaburst__tx__config__t =
[
    [ "set_id", "structinstaburst__tx__config__t.html#ada59971be2aa99e4b7a1d78002350410", null ],
    [ "p_channels", "structinstaburst__tx__config__t.html#a9ed823cdf08c1c916e958682d2d001ea", null ],
    [ "channel_count", "structinstaburst__tx__config__t.html#a27259ec43408f1774d2bea48c7e4ceaf", null ],
    [ "radio_mode", "structinstaburst__tx__config__t.html#a8de57b3cf808b983ef3f81cf921080be", null ],
    [ "tx_power", "structinstaburst__tx__config__t.html#ae97bbfd141f61820b97d397d3e918545", null ],
    [ "callback", "structinstaburst__tx__config__t.html#a42119701550d80eb51db2d64f889ba77", null ],
    [ "interval_ms", "structinstaburst__tx__config__t.html#aa8e993485d99b55dcdb1b505250e91d9", null ]
];