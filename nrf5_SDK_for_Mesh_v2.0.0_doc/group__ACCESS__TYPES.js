var group__ACCESS__TYPES =
[
    [ "access_model_id_t", "structaccess__model__id__t.html", [
      [ "company_id", "structaccess__model__id__t.html#add0d4a2a82fe91ac31647f5230ec49a9", null ],
      [ "model_id", "structaccess__model__id__t.html#a6f183535cdfa0e31dcbad225077d0c97", null ]
    ] ],
    [ "access_opcode_t", "structaccess__opcode__t.html", [
      [ "opcode", "structaccess__opcode__t.html#a8507ef79dc5e2144d04482e8b5dae65e", null ],
      [ "company_id", "structaccess__opcode__t.html#a424c7c0c920e655d77b922fdaca68b7c", null ]
    ] ],
    [ "access_message_rx_meta_t", "structaccess__message__rx__meta__t.html", [
      [ "src", "structaccess__message__rx__meta__t.html#a175476de7940009d9462a199c698fe04", null ],
      [ "dst", "structaccess__message__rx__meta__t.html#a3a149b675b2901a506041ef280156bf5", null ],
      [ "ttl", "structaccess__message__rx__meta__t.html#a1ac1bc99118ba151c24dd6b13005c6c0", null ],
      [ "appkey_handle", "structaccess__message__rx__meta__t.html#aaa84f49527f93ecd6f78470e4ab7bcfb", null ],
      [ "p_core_metadata", "structaccess__message__rx__meta__t.html#a6e8c648a298b802885aaff0b6459a5c6", null ],
      [ "subnet_handle", "structaccess__message__rx__meta__t.html#ad6e8ee8ea30687d8a412fc19574d5348", null ]
    ] ],
    [ "access_message_rx_t", "structaccess__message__rx__t.html", [
      [ "opcode", "structaccess__message__rx__t.html#a70a75764ffc75138c429ab559bacbb05", null ],
      [ "p_data", "structaccess__message__rx__t.html#a14b3bacda89c39c284e178fbd6c525eb", null ],
      [ "length", "structaccess__message__rx__t.html#a30b8b3a44ecf574122b32ec8455c6a73", null ],
      [ "meta_data", "structaccess__message__rx__t.html#a56cb11ef2bcb4bee23e871895a67573b", null ]
    ] ],
    [ "access_message_tx_t", "structaccess__message__tx__t.html", [
      [ "opcode", "structaccess__message__tx__t.html#a895fa0a866b792a924600b842c5657e3", null ],
      [ "p_buffer", "structaccess__message__tx__t.html#ad03a79e397502752498e656161b0eea4", null ],
      [ "length", "structaccess__message__tx__t.html#a77636089c0ee338a3c69f9bda8e7c42c", null ],
      [ "force_segmented", "structaccess__message__tx__t.html#a11fa29733b7aa758728109384efeb6ca", null ],
      [ "transmic_size", "structaccess__message__tx__t.html#acc823572c3a3ce69092035fcbd1c0879", null ],
      [ "access_token", "structaccess__message__tx__t.html#af0c6631412df57cf188fb70a577838a0", null ]
    ] ],
    [ "access_opcode_handler_t", "structaccess__opcode__handler__t.html", [
      [ "opcode", "structaccess__opcode__handler__t.html#a0dae1196499cc84ae912b4f7e97e5b58", null ],
      [ "handler", "structaccess__opcode__handler__t.html#a3e8c680bd5ccbeeae1e9ab1e256f7807", null ]
    ] ],
    [ "access_model_add_params_t", "structaccess__model__add__params__t.html", [
      [ "model_id", "structaccess__model__add__params__t.html#a78f412233d47d8d2518bc167141cb633", null ],
      [ "element_index", "structaccess__model__add__params__t.html#afc4c46bac8e1f78f7c2964d9c7999d13", null ],
      [ "p_opcode_handlers", "structaccess__model__add__params__t.html#a02cd3f429c6cbc5fb731383ad7a7d087", null ],
      [ "opcode_count", "structaccess__model__add__params__t.html#a67d973f92b0be68ec4b00075a4acf3c6", null ],
      [ "p_args", "structaccess__model__add__params__t.html#aede5b596d282de32a18a53fd3c61a012", null ],
      [ "publish_timeout_cb", "structaccess__model__add__params__t.html#aa6e0d134acf4321bdcc5c1a6c121dc36", null ]
    ] ],
    [ "access_publish_period_t", "structaccess__publish__period__t.html", [
      [ "step_res", "structaccess__publish__period__t.html#a9af932760a39b99232a4b66c9706b318", null ],
      [ "step_num", "structaccess__publish__period__t.html#a9c48ecb1aaca3e1f14bfd895fc089e1c", null ]
    ] ],
    [ "access_publish_retransmit_t", "structaccess__publish__retransmit__t.html", [
      [ "count", "structaccess__publish__retransmit__t.html#a373161601936e8f690f9f45d314b4f38", null ],
      [ "interval_steps", "structaccess__publish__retransmit__t.html#a1843314dbf8b4bbc9dfe1935a0d65de9", null ]
    ] ],
    [ "access_model_handle_t", "group__ACCESS__TYPES.html#gaafbafe5b30b5124fafca908acb150375", null ],
    [ "access_publish_timeout_cb_t", "group__ACCESS__TYPES.html#gaba04060fd1427541ab4abe5bba2c6458", null ],
    [ "access_opcode_handler_cb_t", "group__ACCESS__TYPES.html#ga5ae1f7befc20c1d9bc11e8188f50a9c9", null ],
    [ "access_publish_resolution_t", "group__ACCESS__TYPES.html#ga196e8055b297a6d2437dc2eb437de504", [
      [ "ACCESS_PUBLISH_RESOLUTION_100MS", "group__ACCESS__TYPES.html#gga196e8055b297a6d2437dc2eb437de504aa12d27ee0dd0ad854c485e27e1bcc02e", null ],
      [ "ACCESS_PUBLISH_RESOLUTION_1S", "group__ACCESS__TYPES.html#gga196e8055b297a6d2437dc2eb437de504aee59122d1044226506e6754c90ada822", null ],
      [ "ACCESS_PUBLISH_RESOLUTION_10S", "group__ACCESS__TYPES.html#gga196e8055b297a6d2437dc2eb437de504a153391ed53026f26327ad28cc96b5868", null ],
      [ "ACCESS_PUBLISH_RESOLUTION_10MIN", "group__ACCESS__TYPES.html#gga196e8055b297a6d2437dc2eb437de504a0adec326965ecdfe3e72562769e8ebb6", null ],
      [ "ACCESS_PUBLISH_RESOLUTION_MAX", "group__ACCESS__TYPES.html#gga196e8055b297a6d2437dc2eb437de504a560491bfcad944c6e2c9abf971a98308", null ]
    ] ],
    [ "access_status_t", "group__ACCESS__TYPES.html#ga19a7118868a8fb34ee7def8e1e6cd0ff", [
      [ "ACCESS_STATUS_SUCCESS", "group__ACCESS__TYPES.html#gga19a7118868a8fb34ee7def8e1e6cd0ffac0142c0fcc8b32aea38c11a6b3d13636", null ],
      [ "ACCESS_STATUS_INVALID_ADDRESS", "group__ACCESS__TYPES.html#gga19a7118868a8fb34ee7def8e1e6cd0ffad53402ac2e6b52839ecc75a341b6d5c8", null ],
      [ "ACCESS_STATUS_INVALID_MODEL", "group__ACCESS__TYPES.html#gga19a7118868a8fb34ee7def8e1e6cd0ffa55296de30c0e027877320a9f709baf8b", null ],
      [ "ACCESS_STATUS_INVALID_APPKEY", "group__ACCESS__TYPES.html#gga19a7118868a8fb34ee7def8e1e6cd0ffa1a7bbb05f62d368dff830c13909a1fc2", null ],
      [ "ACCESS_STATUS_INVALID_NETKEY", "group__ACCESS__TYPES.html#gga19a7118868a8fb34ee7def8e1e6cd0ffa1aaa1fc0c1e8da2df348ebc6f77eec38", null ],
      [ "ACCESS_STATUS_INSUFFICIENT_RESOURCES", "group__ACCESS__TYPES.html#gga19a7118868a8fb34ee7def8e1e6cd0ffa005767155220b4a11f7c8f9ed16ad281", null ],
      [ "ACCESS_STATUS_KEY_INDEX_ALREADY_STORED", "group__ACCESS__TYPES.html#gga19a7118868a8fb34ee7def8e1e6cd0ffa57995bd324201770f834f0512c67cbc2", null ],
      [ "ACCESS_STATUS_NOT_A_PUBLISH_MODEL", "group__ACCESS__TYPES.html#gga19a7118868a8fb34ee7def8e1e6cd0ffa118b071b4e6bbf9682dfda9ba8cd0b17", null ],
      [ "ACCESS_STATUS_NOT_A_SUBSCRIBE_MODEL", "group__ACCESS__TYPES.html#gga19a7118868a8fb34ee7def8e1e6cd0ffa6ebfe4125556791b69fd0c36df847cd9", null ],
      [ "ACCESS_STATUS_STORAGE_FAILURE", "group__ACCESS__TYPES.html#gga19a7118868a8fb34ee7def8e1e6cd0ffa62befd6bb3fcabcf6a8b42205639d20c", null ],
      [ "ACCESS_STATUS_FEATURE_NOT_SUPPORTED", "group__ACCESS__TYPES.html#gga19a7118868a8fb34ee7def8e1e6cd0ffa673b28a9454583728851a58c3f951804", null ],
      [ "ACCESS_STATUS_CANNOT_UPDATE", "group__ACCESS__TYPES.html#gga19a7118868a8fb34ee7def8e1e6cd0ffad3ba6dd3a86f048506efd9bd705695a2", null ],
      [ "ACCESS_STATUS_CANNOT_REMOVE", "group__ACCESS__TYPES.html#gga19a7118868a8fb34ee7def8e1e6cd0ffa58c93194fd499208eb5fc1449788a107", null ],
      [ "ACCESS_STATUS_CANNOT_BIND", "group__ACCESS__TYPES.html#gga19a7118868a8fb34ee7def8e1e6cd0ffa5d999cb0be30ccd305ce219c27cc7d56", null ],
      [ "ACCESS_STATUS_TEMPORARILY_UNABLE_TO_CHANGE_STATE", "group__ACCESS__TYPES.html#gga19a7118868a8fb34ee7def8e1e6cd0ffa39f90af326e2a187401c74d334a3336a", null ],
      [ "ACCESS_STATUS_CANNOT_SET", "group__ACCESS__TYPES.html#gga19a7118868a8fb34ee7def8e1e6cd0ffafddc786122403f74a281b65181a68c0e", null ],
      [ "ACCESS_STATUS_UNSPECIFIED_ERROR", "group__ACCESS__TYPES.html#gga19a7118868a8fb34ee7def8e1e6cd0ffa6c85a96101639982649ff8ebc85505c9", null ],
      [ "ACCESS_STATUS_INVALID_BINDING", "group__ACCESS__TYPES.html#gga19a7118868a8fb34ee7def8e1e6cd0ffa20b66d69c1dce340c28e8c7582741e75", null ]
    ] ]
];