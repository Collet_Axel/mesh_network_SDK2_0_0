var group__PB__REMOTE__CLIENT =
[
    [ "pb_remote_event_remote_uuid_t", "structpb__remote__event__remote__uuid__t.html", [
      [ "p_uuid", "structpb__remote__event__remote__uuid__t.html#a4f5716a00a91718db3281ccd78830e25", null ],
      [ "device_id", "structpb__remote__event__remote__uuid__t.html#a85c444427bb90a9fbdf1f8f9327b03e6", null ]
    ] ],
    [ "pb_remote_event_t", "structpb__remote__event__t.html", [
      [ "type", "structpb__remote__event__t.html#aa370fbe25a3a4961c6c375f5e660b46c", null ],
      [ "remote_uuid", "structpb__remote__event__t.html#a1946ec7a2d831e9139c2835ed240c832", null ]
    ] ],
    [ "pb_remote_client_t", "structpb__remote__client__t.html", [
      [ "model_handle", "structpb__remote__client__t.html#a37d3f0686ab8614529a35f31018f6a04", null ],
      [ "prov_bearer", "structpb__remote__client__t.html#a632bc72cb4c308b9c723c92475d0e273", null ],
      [ "state", "structpb__remote__client__t.html#a98c528da6cff457f08c96b723ceb0d3a", null ],
      [ "reliable", "structpb__remote__client__t.html#ad6cdfe9e5f91712805eb53c81d0a2f73", null ],
      [ "event_cb", "structpb__remote__client__t.html#a30eb0d7afabb2e1744c4a05a774fd753", null ]
    ] ],
    [ "PB_REMOTE_CLIENT_MODEL_ID", "group__PB__REMOTE__CLIENT.html#gae10d66fbc8a5e57b998ad316b866b7a5", null ],
    [ "pb_remote_client_event_cb_t", "group__PB__REMOTE__CLIENT.html#ga7c23ed2e9e40fe8b70559594bd5a3cbd", null ],
    [ "pb_remote_event_type_t", "group__PB__REMOTE__CLIENT.html#ga16a84554aa8deb13a4383b6bf8f7dd65", [
      [ "PB_REMOTE_EVENT_SCAN_STARTED", "group__PB__REMOTE__CLIENT.html#gga16a84554aa8deb13a4383b6bf8f7dd65accd7f342d268d73e6048631a7fabdc82", null ],
      [ "PB_REMOTE_EVENT_SCAN_CANCELED", "group__PB__REMOTE__CLIENT.html#gga16a84554aa8deb13a4383b6bf8f7dd65a7477944f3d4c5f8bddbab2994e605b53", null ],
      [ "PB_REMOTE_EVENT_CANNOT_START_SCANNING", "group__PB__REMOTE__CLIENT.html#gga16a84554aa8deb13a4383b6bf8f7dd65a2ab9b47b7f38f7f12b32c1308bb2b43a", null ],
      [ "PB_REMOTE_EVENT_CANNOT_CANCEL_SCANNING", "group__PB__REMOTE__CLIENT.html#gga16a84554aa8deb13a4383b6bf8f7dd65ac4548db8c688d10cb3d5a9548ac29dda", null ],
      [ "PB_REMOTE_EVENT_LINK_CLOSED", "group__PB__REMOTE__CLIENT.html#gga16a84554aa8deb13a4383b6bf8f7dd65a961b32bf95be3518442acf4f60564d99", null ],
      [ "PB_REMOTE_EVENT_LINK_NOT_ACTIVE", "group__PB__REMOTE__CLIENT.html#gga16a84554aa8deb13a4383b6bf8f7dd65a330851d611dea488611d033da204834a", null ],
      [ "PB_REMOTE_EVENT_LINK_ALREADY_OPEN", "group__PB__REMOTE__CLIENT.html#gga16a84554aa8deb13a4383b6bf8f7dd65a42ea612b0d77f463980cbaecb2bdb69e", null ],
      [ "PB_REMOTE_EVENT_CANNOT_OPEN_LINK", "group__PB__REMOTE__CLIENT.html#gga16a84554aa8deb13a4383b6bf8f7dd65a646768356a3f2a532646b19b7495056f", null ],
      [ "PB_REMOTE_EVENT_CANNOT_CLOSE_LINK", "group__PB__REMOTE__CLIENT.html#gga16a84554aa8deb13a4383b6bf8f7dd65a91f010f76eac9d026ae0dbbaac5bfa0f", null ],
      [ "PB_REMOTE_EVENT_INVALID_UNPROV_DEVICE_ID", "group__PB__REMOTE__CLIENT.html#gga16a84554aa8deb13a4383b6bf8f7dd65a284c1cee0012ecf2c595b2012b4d8d5d", null ],
      [ "PB_REMOTE_EVENT_TX_FAILED", "group__PB__REMOTE__CLIENT.html#gga16a84554aa8deb13a4383b6bf8f7dd65af6dba5fe418d311f52555acdc8af9f7f", null ],
      [ "PB_REMOTE_EVENT_REMOTE_UUID", "group__PB__REMOTE__CLIENT.html#gga16a84554aa8deb13a4383b6bf8f7dd65a15d20830f4122ff40e2e7908e05b1cd5", null ]
    ] ],
    [ "pb_remote_client_state_t", "group__PB__REMOTE__CLIENT.html#ga07cd0ed5f853db4e138aeca70bbd7f15", [
      [ "PB_REMOTE_CLIENT_STATE_NONE", "group__PB__REMOTE__CLIENT.html#gga07cd0ed5f853db4e138aeca70bbd7f15a3a60913aba74097e860882f1c6ade864", null ],
      [ "PB_REMOTE_CLIENT_STATE_IDLE", "group__PB__REMOTE__CLIENT.html#gga07cd0ed5f853db4e138aeca70bbd7f15a5f66e9e656769cfd3e494707d9df6be8", null ],
      [ "PB_REMOTE_CLIENT_STATE_LINK_ESTABLISHED", "group__PB__REMOTE__CLIENT.html#gga07cd0ed5f853db4e138aeca70bbd7f15a9e7b5c255c68289103c2e10e7155f925", null ]
    ] ],
    [ "pb_remote_client_init", "group__PB__REMOTE__CLIENT.html#ga1bd1e346f9187121e05d05fbd3ae9516", null ],
    [ "pb_remote_client_bearer_interface_get", "group__PB__REMOTE__CLIENT.html#gaeee24eff6d0465e3b43ab1c918d316e6", null ],
    [ "pb_remote_client_remote_scan_start", "group__PB__REMOTE__CLIENT.html#gae61b9112c0dbe53f7cce42c1b9df2b5c", null ],
    [ "pb_remote_client_remote_scan_cancel", "group__PB__REMOTE__CLIENT.html#ga0323853a9076c8005780782611b98f29", null ]
];