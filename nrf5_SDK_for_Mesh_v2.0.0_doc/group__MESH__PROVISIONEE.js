var group__MESH__PROVISIONEE =
[
    [ "mesh_provisionee_start_params_t", "structmesh__provisionee__start__params__t.html", [
      [ "prov_complete_cb", "structmesh__provisionee__start__params__t.html#aaa78590a183ba348f43f6b5a2e09511a", null ],
      [ "p_device_uri", "structmesh__provisionee__start__params__t.html#ae6480300bef410c0e3540b80f9de6bf7", null ],
      [ "p_static_data", "structmesh__provisionee__start__params__t.html#a60006eb2dd66964380342d7a46eeaf10", null ]
    ] ],
    [ "mesh_provisionee_prov_complete_cb_t", "group__MESH__PROVISIONEE.html#ga9f356674b0c54cd576113eed6e9aca16", null ],
    [ "mesh_provisionee_prov_start", "group__MESH__PROVISIONEE.html#gad6253f3762026620f045e51a35aef46f", null ]
];