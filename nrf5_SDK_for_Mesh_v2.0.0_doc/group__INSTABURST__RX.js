var group__INSTABURST__RX =
[
    [ "instaburst_rx_packet_t", "structinstaburst__rx__packet__t.html", [
      [ "metadata", "structinstaburst__rx__packet__t.html#a41655c9d87a14fc1b134815fb378ffca", null ],
      [ "payload_len", "structinstaburst__rx__packet__t.html#a500e4c4c97e678956eb62b4c9575c902", null ],
      [ "p_payload", "structinstaburst__rx__packet__t.html#a7a3f125cb4db905e85b1478dd8d4f8dd", null ]
    ] ],
    [ "instaburst_rx_stats_t", "structinstaburst__rx__stats__t.html", [
      [ "rx_ok", "structinstaburst__rx__stats__t.html#ab8a3d98581d05ae1b9d4118b85875d63", null ],
      [ "crc_fail", "structinstaburst__rx__stats__t.html#a442f6f4c3ee7fa82ba5c359189bc0c39", null ],
      [ "too_late", "structinstaburst__rx__stats__t.html#a307b325133e42d65e517ba98c037e97b", null ],
      [ "no_rx", "structinstaburst__rx__stats__t.html#acc43386b32de56a7c7ee06bf3fa50ade", null ]
    ] ],
    [ "INSTABURST_RX_DEBUG", "group__INSTABURST__RX.html#gafe796ce60d5811c4341754a2384e219d", null ],
    [ "instaburst_rx_init", "group__INSTABURST__RX.html#ga0d954f6a2902e617467ff4f9b8b2552d", null ],
    [ "instaburst_rx_enable", "group__INSTABURST__RX.html#ga9e36ab6dda14850ac6b3611e4fe2576a", null ],
    [ "instaburst_rx_disable", "group__INSTABURST__RX.html#ga84d5240f4d570c2405531968aeeff10c", null ],
    [ "instaburst_rx", "group__INSTABURST__RX.html#gad13d30aea6599ecc42bccd9167dd540f", null ],
    [ "instaburst_rx_pending", "group__INSTABURST__RX.html#ga44ddc929604ccf819e20d78ce3583fe0", null ],
    [ "instaburst_rx_packet_release", "group__INSTABURST__RX.html#gaea1b3dccd7c1c2b37084e206db6fcef1", null ],
    [ "instaburst_rx_stats_get", "group__INSTABURST__RX.html#ga79b87d628862385bddc6f2acbda8dbc4", null ]
];