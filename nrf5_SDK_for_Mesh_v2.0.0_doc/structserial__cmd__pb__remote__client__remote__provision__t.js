var structserial__cmd__pb__remote__client__remote__provision__t =
[
    [ "server_address", "structserial__cmd__pb__remote__client__remote__provision__t.html#aa9ef70e46ef15bd966e6e6b741c28bda", null ],
    [ "unprovisioned_device_index", "structserial__cmd__pb__remote__client__remote__provision__t.html#a1140e0b1f8e125a508e2d24b109b1dba", null ],
    [ "network_key", "structserial__cmd__pb__remote__client__remote__provision__t.html#aa8f7825489e2b1ac5602d9f58b256777", null ],
    [ "iv_index", "structserial__cmd__pb__remote__client__remote__provision__t.html#aa20c91469be2a7608dfc16e9fed75651", null ],
    [ "address", "structserial__cmd__pb__remote__client__remote__provision__t.html#ab9a84db197a2d1e8ee136c3f55381be4", null ]
];