var group__SIMPLE__ON__OFF__MODEL =
[
    [ "Simple OnOff Client", "group__SIMPLE__ON__OFF__CLIENT.html", "group__SIMPLE__ON__OFF__CLIENT" ],
    [ "Common Simple OnOff definitions", "group__SIMPLE__ON__OFF__COMMON.html", "group__SIMPLE__ON__OFF__COMMON" ],
    [ "Simple OnOff Server", "group__SIMPLE__ON__OFF__SERVER.html", "group__SIMPLE__ON__OFF__SERVER" ]
];