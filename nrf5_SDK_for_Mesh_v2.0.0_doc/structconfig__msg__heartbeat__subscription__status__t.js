var structconfig__msg__heartbeat__subscription__status__t =
[
    [ "status", "structconfig__msg__heartbeat__subscription__status__t.html#af448531cda75d62bd169965429323c6d", null ],
    [ "source", "structconfig__msg__heartbeat__subscription__status__t.html#ad24f6f7d5d26235212ac83067580a778", null ],
    [ "destination", "structconfig__msg__heartbeat__subscription__status__t.html#a1d6f334c6e287b7d9b0083e7642026a1", null ],
    [ "period_log", "structconfig__msg__heartbeat__subscription__status__t.html#ab2c26ee4f9a730ecd083e118fa5901d2", null ],
    [ "count_log", "structconfig__msg__heartbeat__subscription__status__t.html#af35c8bd28971d1917edde334c029d465", null ],
    [ "min_hops", "structconfig__msg__heartbeat__subscription__status__t.html#afa86d67b8618d49c2682afe3ec18188f", null ],
    [ "max_hops", "structconfig__msg__heartbeat__subscription__status__t.html#a62d14dcef06735a5ebb1ce268754fdb8", null ]
];