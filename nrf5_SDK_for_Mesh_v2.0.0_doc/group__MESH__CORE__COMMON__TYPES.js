var group__MESH__CORE__COMMON__TYPES =
[
    [ "nrf_mesh_rx_metadata_scanner_t", "structnrf__mesh__rx__metadata__scanner__t.html", [
      [ "timestamp", "structnrf__mesh__rx__metadata__scanner__t.html#a2f7b06e0b8dda40580e2ba08dca36665", null ],
      [ "access_addr", "structnrf__mesh__rx__metadata__scanner__t.html#a66a7dfcd4d06986ce57b47ccb18b0cf8", null ],
      [ "channel", "structnrf__mesh__rx__metadata__scanner__t.html#a4d2df311d2a160d48d91d1422b5fe1c1", null ],
      [ "rssi", "structnrf__mesh__rx__metadata__scanner__t.html#a2ccdcc880095d351d9c13cd83833cdc2", null ],
      [ "adv_addr", "structnrf__mesh__rx__metadata__scanner__t.html#a042c1f5568dcf6ba438931ef75993b3a", null ],
      [ "adv_type", "structnrf__mesh__rx__metadata__scanner__t.html#a5458e291aa09198b36520a0e85ba1e6e", null ]
    ] ],
    [ "nrf_mesh_instaburst_event_id_t", "structnrf__mesh__instaburst__event__id__t.html", [
      [ "data_id", "structnrf__mesh__instaburst__event__id__t.html#a2c24c128dfa862ae5cd56d7bf3f44e43", null ],
      [ "set_id", "structnrf__mesh__instaburst__event__id__t.html#a0a8a9754d2fb494d2ccf5dc33b4fbd2a", null ]
    ] ],
    [ "nrf_mesh_rx_metadata_instaburst_t", "structnrf__mesh__rx__metadata__instaburst__t.html", [
      [ "timestamp", "structnrf__mesh__rx__metadata__instaburst__t.html#ac185eeee36e8170c1675f8b552895145", null ],
      [ "channel", "structnrf__mesh__rx__metadata__instaburst__t.html#aa92b58bd7e4e1b70a1136b9e1c13c9f6", null ],
      [ "rssi", "structnrf__mesh__rx__metadata__instaburst__t.html#a7bfa404a94016957353f8448fa1c33bf", null ],
      [ "id", "structnrf__mesh__rx__metadata__instaburst__t.html#a7b0cb4e2fab593ebcf4869f241618da9", null ],
      [ "packet_index", "structnrf__mesh__rx__metadata__instaburst__t.html#a6d8f2188aedd7707e6a5a0930ba831db", null ],
      [ "is_last_in_chain", "structnrf__mesh__rx__metadata__instaburst__t.html#a7bf4aea7082d1860fd585fe7b8fc3c80", null ],
      [ "event", "structnrf__mesh__rx__metadata__instaburst__t.html#a82f1995fb68873abfbc0ff225e39e767", null ]
    ] ],
    [ "nrf_mesh_rx_metadata_gatt_t", "structnrf__mesh__rx__metadata__gatt__t.html", [
      [ "timestamp", "structnrf__mesh__rx__metadata__gatt__t.html#a132a04c0c40f820c06735167229291fb", null ],
      [ "connection_index", "structnrf__mesh__rx__metadata__gatt__t.html#a3a5c30d473fbe230f652b68596ab5bee", null ]
    ] ],
    [ "nrf_mesh_rx_metadata_loopback_t", "structnrf__mesh__rx__metadata__loopback__t.html", [
      [ "tx_token", "structnrf__mesh__rx__metadata__loopback__t.html#a9f878193ac6f1e5fb732d3c9b38ddf6b", null ]
    ] ],
    [ "nrf_mesh_rx_metadata_t", "structnrf__mesh__rx__metadata__t.html", [
      [ "source", "structnrf__mesh__rx__metadata__t.html#a8a1c9c49a6ba80461ed0a1e8f20a614f", null ],
      [ "scanner", "structnrf__mesh__rx__metadata__t.html#adb5641fc9ad5640f78de649452d9336d", null ],
      [ "instaburst", "structnrf__mesh__rx__metadata__t.html#a56a8d2a8577a38cc71fd096fc118353b", null ],
      [ "gatt", "structnrf__mesh__rx__metadata__t.html#a9c291785e79acb47c2531fb3e132f40c", null ],
      [ "loopback", "structnrf__mesh__rx__metadata__t.html#af9dfe34145ff431a1262c249c64eea62", null ],
      [ "params", "structnrf__mesh__rx__metadata__t.html#a807ef11e44c37bc63809947f51b3d96a", null ]
    ] ],
    [ "nrf_mesh_adv_packet_rx_data_t", "structnrf__mesh__adv__packet__rx__data__t.html", [
      [ "adv_type", "structnrf__mesh__adv__packet__rx__data__t.html#abc56ddb31b36c30a0d170eea90abdd71", null ],
      [ "length", "structnrf__mesh__adv__packet__rx__data__t.html#a89419cd03b0bc29ef9231488a45073d1", null ],
      [ "p_payload", "structnrf__mesh__adv__packet__rx__data__t.html#ac1555fcb432a967801feb14f14e9ed15", null ],
      [ "p_metadata", "structnrf__mesh__adv__packet__rx__data__t.html#af7c32a85d7991cf9e27f976a409a1545", null ]
    ] ],
    [ "nrf_mesh_application_secmat_t", "structnrf__mesh__application__secmat__t.html", [
      [ "is_device_key", "structnrf__mesh__application__secmat__t.html#a5c9c1e661772e62ed038f8e5a85d5de0", null ],
      [ "aid", "structnrf__mesh__application__secmat__t.html#a491f3233523f218b52c6bdc0b858e3e3", null ],
      [ "key", "structnrf__mesh__application__secmat__t.html#ade2ae04b448b1125e98389d3745d55c1", null ]
    ] ],
    [ "nrf_mesh_network_secmat_t", "structnrf__mesh__network__secmat__t.html", [
      [ "nid", "structnrf__mesh__network__secmat__t.html#a94b833b048dd0a60579c1595ab21f6a8", null ],
      [ "encryption_key", "structnrf__mesh__network__secmat__t.html#a14e1186125b94e2538f82b82a0060657", null ],
      [ "privacy_key", "structnrf__mesh__network__secmat__t.html#a0b140bac1fe24975326bfdde3850963a", null ]
    ] ],
    [ "nrf_mesh_beacon_secmat_t", "structnrf__mesh__beacon__secmat__t.html", [
      [ "key", "structnrf__mesh__beacon__secmat__t.html#a38096efa3ffff7fe21667b59e77e9709", null ],
      [ "net_id", "structnrf__mesh__beacon__secmat__t.html#ab551bb88ed861ab71210b2af4aba6f13", null ]
    ] ],
    [ "nrf_mesh_beacon_tx_info_t", "structnrf__mesh__beacon__tx__info__t.html", [
      [ "rx_count", "structnrf__mesh__beacon__tx__info__t.html#a122bfe88366f7363ea6b6c5d26274e35", null ],
      [ "tx_timestamp", "structnrf__mesh__beacon__tx__info__t.html#a1d35f827bbc08fe52a00566f6f650e68", null ]
    ] ],
    [ "nrf_mesh_beacon_info_t", "structnrf__mesh__beacon__info__t.html", [
      [ "iv_update_permitted", "structnrf__mesh__beacon__info__t.html#af014249c01e0a2ee26d85ea1e3398095", null ],
      [ "p_tx_info", "structnrf__mesh__beacon__info__t.html#a3e985a71df7bdd4564d36133de0bcf17", null ],
      [ "secmat", "structnrf__mesh__beacon__info__t.html#a5d3d61a18294d655ca814796abd05dbf", null ],
      [ "secmat_updated", "structnrf__mesh__beacon__info__t.html#acc34682b7e1209e6b8fc206ea4e2c762", null ]
    ] ],
    [ "nrf_mesh_secmat_t", "structnrf__mesh__secmat__t.html", [
      [ "p_net", "structnrf__mesh__secmat__t.html#a9131d1738c95e64b21c3fa5febba5878", null ],
      [ "p_app", "structnrf__mesh__secmat__t.html#a1e2b001d8046ec5a58ae64aa2d099287", null ]
    ] ],
    [ "nrf_mesh_address_t", "structnrf__mesh__address__t.html", [
      [ "type", "structnrf__mesh__address__t.html#a2eaad0b1a9455e5d8d014cb56fdbfc4d", null ],
      [ "value", "structnrf__mesh__address__t.html#ae8c45fd3b8d9106aabd494b09fbbe66a", null ],
      [ "p_virtual_uuid", "structnrf__mesh__address__t.html#a2f8988543fc98df25b8972beeae9cff6", null ]
    ] ],
    [ "nrf_mesh_tx_params_t", "structnrf__mesh__tx__params__t.html", [
      [ "dst", "structnrf__mesh__tx__params__t.html#ae8ea31532b5f2b74d6d9eae012ebd23d", null ],
      [ "src", "structnrf__mesh__tx__params__t.html#af6b85f9674183d7f65534f6ae3962344", null ],
      [ "ttl", "structnrf__mesh__tx__params__t.html#a5fffcd41b7aaf4183e72557f4b61d02b", null ],
      [ "force_segmented", "structnrf__mesh__tx__params__t.html#a00d34ca85cdd2f64e1cdde5de6bc3954", null ],
      [ "transmic_size", "structnrf__mesh__tx__params__t.html#afe3e23642a8d0d7d84d0d25d9ae043f2", null ],
      [ "p_data", "structnrf__mesh__tx__params__t.html#a15fd6994fdfbcba88d0b14f39e26233a", null ],
      [ "data_len", "structnrf__mesh__tx__params__t.html#aac1de21d3948a77eb1be070b98e15a9c", null ],
      [ "security_material", "structnrf__mesh__tx__params__t.html#a0cf3c05eb1207e50639083dabfd15ec7", null ],
      [ "tx_token", "structnrf__mesh__tx__params__t.html#aa4ce3325e68f05e8d7665cf23ea6a4e4", null ]
    ] ],
    [ "nrf_mesh_init_params_t", "structnrf__mesh__init__params__t.html", [
      [ "relay_cb", "structnrf__mesh__init__params__t.html#acaa2bff2663b8219c15906f4c1495088", null ],
      [ "irq_priority", "structnrf__mesh__init__params__t.html#ad41f7d35516c895ec84ac9b2cd9e77af", null ],
      [ "p_uuid", "structnrf__mesh__init__params__t.html#a034a0dce8d96a7d786e4ce1b7b69952e", null ]
    ] ],
    [ "nrf_mesh_assertion_handler_t", "group__MESH__CORE__COMMON__TYPES.html#gac4abd6f6a7c7f5a819c4b17204878ba3", null ],
    [ "nrf_mesh_tx_token_t", "group__MESH__CORE__COMMON__TYPES.html#ga0b09a54c9ff6cbc4667ac34acec6ada0", null ],
    [ "nrf_mesh_relay_check_cb_t", "group__MESH__CORE__COMMON__TYPES.html#gae1ec4a684ea60f259b2bf9614e409e14", null ],
    [ "nrf_mesh_rx_cb_t", "group__MESH__CORE__COMMON__TYPES.html#ga4a675ec49934bba006ae209ebbee4f72", null ],
    [ "nrf_mesh_rx_source_t", "group__MESH__CORE__COMMON__TYPES.html#ga12e5bc2554a33105ce3056e519d0b3da", [
      [ "NRF_MESH_RX_SOURCE_SCANNER", "group__MESH__CORE__COMMON__TYPES.html#gga12e5bc2554a33105ce3056e519d0b3daaf73838f8c9f32f8fca2aba87bba6770b", null ],
      [ "NRF_MESH_RX_SOURCE_GATT", "group__MESH__CORE__COMMON__TYPES.html#gga12e5bc2554a33105ce3056e519d0b3daac8725759495a9673d111e36e4d646237", null ],
      [ "NRF_MESH_RX_SOURCE_FRIEND", "group__MESH__CORE__COMMON__TYPES.html#gga12e5bc2554a33105ce3056e519d0b3daa6c23c1e4547da05fd0da87ab20392eda", null ],
      [ "NRF_MESH_RX_SOURCE_LOW_POWER", "group__MESH__CORE__COMMON__TYPES.html#gga12e5bc2554a33105ce3056e519d0b3daa9269a3b31d5332129d60533ba88dfede", null ],
      [ "NRF_MESH_RX_SOURCE_INSTABURST", "group__MESH__CORE__COMMON__TYPES.html#gga12e5bc2554a33105ce3056e519d0b3daabf702ac51c2e016b925b9deb6e9ac938", null ],
      [ "NRF_MESH_RX_SOURCE_LOOPBACK", "group__MESH__CORE__COMMON__TYPES.html#gga12e5bc2554a33105ce3056e519d0b3daa00ec7bff22eef060db3e9f75cc76576b", null ]
    ] ],
    [ "nrf_mesh_key_refresh_phase_t", "group__MESH__CORE__COMMON__TYPES.html#ga0329e64bb09a399e9ae8d8f038c9ff39", [
      [ "NRF_MESH_KEY_REFRESH_PHASE_0", "group__MESH__CORE__COMMON__TYPES.html#gga0329e64bb09a399e9ae8d8f038c9ff39a1b98a8dfb629bd4915b60c334c5756aa", null ],
      [ "NRF_MESH_KEY_REFRESH_PHASE_1", "group__MESH__CORE__COMMON__TYPES.html#gga0329e64bb09a399e9ae8d8f038c9ff39a857b7ce551285d2022d16d0cf7e84661", null ],
      [ "NRF_MESH_KEY_REFRESH_PHASE_2", "group__MESH__CORE__COMMON__TYPES.html#gga0329e64bb09a399e9ae8d8f038c9ff39acb66941af8d8aa1a923dd6b69787e68e", null ],
      [ "NRF_MESH_KEY_REFRESH_PHASE_3", "group__MESH__CORE__COMMON__TYPES.html#gga0329e64bb09a399e9ae8d8f038c9ff39a7e1c1bf7135abf421956d670ba0d0922", null ]
    ] ],
    [ "net_state_iv_update_t", "group__MESH__CORE__COMMON__TYPES.html#gaded26330ad173a35d5cf5a2f0604b599", [
      [ "NET_STATE_IV_UPDATE_NORMAL", "group__MESH__CORE__COMMON__TYPES.html#ggaded26330ad173a35d5cf5a2f0604b599ad104f4acdca8f76df8dd15882de3849b", null ],
      [ "NET_STATE_IV_UPDATE_IN_PROGRESS", "group__MESH__CORE__COMMON__TYPES.html#ggaded26330ad173a35d5cf5a2f0604b599a1cc68be67833bbc25484c5d85e43e697", null ]
    ] ],
    [ "nrf_mesh_address_type_t", "group__MESH__CORE__COMMON__TYPES.html#gafb5fdc3538c5fd37f688512a65cacefb", [
      [ "NRF_MESH_ADDRESS_TYPE_INVALID", "group__MESH__CORE__COMMON__TYPES.html#ggafb5fdc3538c5fd37f688512a65cacefba61f2eb16a3e3e4b134950e6b26029736", null ],
      [ "NRF_MESH_ADDRESS_TYPE_UNICAST", "group__MESH__CORE__COMMON__TYPES.html#ggafb5fdc3538c5fd37f688512a65cacefba5cc751c2f0e2b63058aa9cff3e6686ff", null ],
      [ "NRF_MESH_ADDRESS_TYPE_VIRTUAL", "group__MESH__CORE__COMMON__TYPES.html#ggafb5fdc3538c5fd37f688512a65cacefba12dcce62d57dfaad0d8e17fa95d0ce1c", null ],
      [ "NRF_MESH_ADDRESS_TYPE_GROUP", "group__MESH__CORE__COMMON__TYPES.html#ggafb5fdc3538c5fd37f688512a65cacefbab41a9c569e087409b0ddde63b5aa4be0", null ]
    ] ],
    [ "nrf_mesh_transmic_size_t", "group__MESH__CORE__COMMON__TYPES.html#gaca5284d096ab82171c9d0a18fbaf9623", [
      [ "NRF_MESH_TRANSMIC_SIZE_SMALL", "group__MESH__CORE__COMMON__TYPES.html#ggaca5284d096ab82171c9d0a18fbaf9623a3b2bfa590834332f242ec8f897fe46a3", null ],
      [ "NRF_MESH_TRANSMIC_SIZE_LARGE", "group__MESH__CORE__COMMON__TYPES.html#ggaca5284d096ab82171c9d0a18fbaf9623ae5c3c49aff45da1bbdd49f7fc5ea6e08", null ],
      [ "NRF_MESH_TRANSMIC_SIZE_DEFAULT", "group__MESH__CORE__COMMON__TYPES.html#ggaca5284d096ab82171c9d0a18fbaf9623ada5114f8b7057024f9a3208cb330be1c", null ],
      [ "NRF_MESH_TRANSMIC_SIZE_INVALID", "group__MESH__CORE__COMMON__TYPES.html#ggaca5284d096ab82171c9d0a18fbaf9623ad34ae083c3e55ec4e08b9d993b04e501", null ]
    ] ]
];