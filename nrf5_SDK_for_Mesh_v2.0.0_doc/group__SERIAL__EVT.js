var group__SERIAL__EVT =
[
    [ "serial_evt_device_started_t", "structserial__evt__device__started__t.html", [
      [ "operating_mode", "structserial__evt__device__started__t.html#a91fb56bd9ccda350e57138556386e7bd", null ],
      [ "hw_error", "structserial__evt__device__started__t.html#ab88bd84506c7679fb3615791fd99584c", null ],
      [ "data_credit_available", "structserial__evt__device__started__t.html#a7d248460162407f1ddee0481410fb845", null ]
    ] ],
    [ "serial_evt_device_echo_t", "structserial__evt__device__echo__t.html", [
      [ "data", "structserial__evt__device__echo__t.html#a566bc7f096315c44dd08636b5ffeae36", null ]
    ] ],
    [ "serial_evt_device_internal_event_t", "structserial__evt__device__internal__event__t.html", [
      [ "event_type", "structserial__evt__device__internal__event__t.html#aa1667639b6482daf5caf39124ccfd366", null ],
      [ "state", "structserial__evt__device__internal__event__t.html#a184cac5c79b580c1eb65c4f65df2530f", null ],
      [ "packet_size", "structserial__evt__device__internal__event__t.html#a3fc54ae3e63b8b4d5a0d83306f16c1fe", null ],
      [ "packet", "structserial__evt__device__internal__event__t.html#a81de9fc3fe39992b006a6dfcab015b3f", null ]
    ] ],
    [ "serial_evt_device_t", "unionserial__evt__device__t.html", [
      [ "started", "unionserial__evt__device__t.html#a3f6126903b9da2546e8c45bef7c83d96", null ],
      [ "echo", "unionserial__evt__device__t.html#a2b44f1621e0e65bb27fc8a79bb403527", null ],
      [ "internal_event", "unionserial__evt__device__t.html#a03a056101ff8d4a8b3c02c86b0f82bb8", null ]
    ] ],
    [ "serial_evt_prov_unprov_t", "structserial__evt__prov__unprov__t.html", [
      [ "uuid", "structserial__evt__prov__unprov__t.html#a255b13b930693d1f39d139d449571638", null ],
      [ "rssi", "structserial__evt__prov__unprov__t.html#adc8216c7a1887295dd30a3e5d30410a4", null ],
      [ "gatt_supported", "structserial__evt__prov__unprov__t.html#a9bdedec4f9f1e9c889f3d8a828bc080d", null ],
      [ "adv_addr_type", "structserial__evt__prov__unprov__t.html#ac37c3f62aeea2c94de26c83fd2b5b705", null ],
      [ "adv_addr", "structserial__evt__prov__unprov__t.html#a5bc628bdeb531d48e664b60deb73f17e", null ]
    ] ],
    [ "serial_evt_prov_link_established_t", "structserial__evt__prov__link__established__t.html", [
      [ "context_id", "structserial__evt__prov__link__established__t.html#ab05c875ed685ff52f0f8f49141b413ce", null ]
    ] ],
    [ "serial_evt_prov_link_closed_t", "structserial__evt__prov__link__closed__t.html", [
      [ "context_id", "structserial__evt__prov__link__closed__t.html#a42d674ba446133f03038e7547a845889", null ],
      [ "close_reason", "structserial__evt__prov__link__closed__t.html#a76773d41856def5a1cecd8af98c552c1", null ]
    ] ],
    [ "serial_evt_prov_caps_received_t", "structserial__evt__prov__caps__received__t.html", [
      [ "context_id", "structserial__evt__prov__caps__received__t.html#ac1a2ae1ad39eae15eb545e4c26e6fa29", null ],
      [ "num_elements", "structserial__evt__prov__caps__received__t.html#adf054f4c438acffa9b1093412fe9e701", null ],
      [ "public_key_type", "structserial__evt__prov__caps__received__t.html#acbfd223db55d3aeac3f6251095a8662a", null ],
      [ "static_oob_types", "structserial__evt__prov__caps__received__t.html#abee63dbf4386213a5bc0e6b7a1e61d94", null ],
      [ "output_oob_size", "structserial__evt__prov__caps__received__t.html#ab1e831193b90f95a2f6ce25f0a039e8a", null ],
      [ "output_oob_actions", "structserial__evt__prov__caps__received__t.html#a5070a0c0b4cb1409ee5b00771551d216", null ],
      [ "input_oob_size", "structserial__evt__prov__caps__received__t.html#a8c8d6010657a8d9d50fd3997779c5eff", null ],
      [ "input_oob_actions", "structserial__evt__prov__caps__received__t.html#a718f5655dbecf675888f93f7c2ac9909", null ]
    ] ],
    [ "serial_evt_prov_complete_t", "structserial__evt__prov__complete__t.html", [
      [ "context_id", "structserial__evt__prov__complete__t.html#ae55cbea207d481d0ff9b11880fd016c3", null ],
      [ "iv_index", "structserial__evt__prov__complete__t.html#a2ab2a37f7bab0297c4bc6a305241268f", null ],
      [ "net_key_index", "structserial__evt__prov__complete__t.html#a54b7ba1c1737c2d2939158b255f754b7", null ],
      [ "address", "structserial__evt__prov__complete__t.html#aeddd4a4a81c83bd7bd9526d86258b372", null ],
      [ "iv_update_flag", "structserial__evt__prov__complete__t.html#a0287a383bb13a67d2cc9c04d8fcf8be6", null ],
      [ "key_refresh_flag", "structserial__evt__prov__complete__t.html#a9c3e37b8ffb9fce24c96432dc01f7732", null ],
      [ "device_key", "structserial__evt__prov__complete__t.html#a4b91b8db26aa57ebc56f7c9d25043855", null ],
      [ "net_key", "structserial__evt__prov__complete__t.html#aa3e31cfe3bb827df3e71ad12945219c0", null ]
    ] ],
    [ "serial_evt_prov_auth_request_t", "structserial__evt__prov__auth__request__t.html", [
      [ "context_id", "structserial__evt__prov__auth__request__t.html#a4d289221c1360ffad9bf2ef339fb6ae3", null ],
      [ "method", "structserial__evt__prov__auth__request__t.html#a7f74154d61d089e07e068905007fbea8", null ],
      [ "action", "structserial__evt__prov__auth__request__t.html#a2e303c8760189e2fd28d92d7d562a76e", null ],
      [ "size", "structserial__evt__prov__auth__request__t.html#adcaa63ea7a6f02549b0084ed56049939", null ]
    ] ],
    [ "serial_evt_prov_ecdh_request_t", "structserial__evt__prov__ecdh__request__t.html", [
      [ "context_id", "structserial__evt__prov__ecdh__request__t.html#a67bb6c30efaf3ae72632f02029c9535e", null ],
      [ "peer_public", "structserial__evt__prov__ecdh__request__t.html#a356947f278a43d138e394667768d2b6c", null ],
      [ "node_private", "structserial__evt__prov__ecdh__request__t.html#ab8feb9243ac4cc580a0873b9de8461df", null ]
    ] ],
    [ "serial_evt_prov_output_request_t", "structserial__evt__prov__output__request__t.html", [
      [ "context_id", "structserial__evt__prov__output__request__t.html#abafc08adebcdba4e930ae1cc3f8a33b6", null ],
      [ "output_action", "structserial__evt__prov__output__request__t.html#a71accc8d6fe30d1ebfa0e49ab0dc54cc", null ],
      [ "data", "structserial__evt__prov__output__request__t.html#a85df978d62c674beb7c5173184726ff3", null ]
    ] ],
    [ "serial_evt_prov_failed_t", "structserial__evt__prov__failed__t.html", [
      [ "context_id", "structserial__evt__prov__failed__t.html#a8e7e2e51e5b2e850210e86cb2eef4e91", null ],
      [ "error_code", "structserial__evt__prov__failed__t.html#a553cf9d6ac4ebc8f962ab099bb26df95", null ]
    ] ],
    [ "serial_evt_prov_t", "unionserial__evt__prov__t.html", [
      [ "unprov", "unionserial__evt__prov__t.html#af088c89d119bd944f354da2e1165d4bd", null ],
      [ "link_established", "unionserial__evt__prov__t.html#ad92e52d510b64af66e421cc697e1acab", null ],
      [ "link_closed", "unionserial__evt__prov__t.html#a3608650a140fb80aa2ea4e2dd96681f8", null ],
      [ "caps_received", "unionserial__evt__prov__t.html#ada8e63710b2f9fdfb158ecf370b87e55", null ],
      [ "complete", "unionserial__evt__prov__t.html#a05d407d090f804473580afbec62f644a", null ],
      [ "auth_request", "unionserial__evt__prov__t.html#af282e1c2110f2c7a1ab76a27138c4dde", null ],
      [ "ecdh_request", "unionserial__evt__prov__t.html#a3d3fe189b52019c9afbe2b7ca30f9b6a", null ],
      [ "output_request", "unionserial__evt__prov__t.html#a0884666c8aceef9509a0b2ae3a3af01f", null ],
      [ "failed", "unionserial__evt__prov__t.html#ae85dc4d8fa081decec57a9e4b17b1845", null ]
    ] ],
    [ "serial_evt_model_specific_header_t", "structserial__evt__model__specific__header__t.html", [
      [ "model_id", "structserial__evt__model__specific__header__t.html#a0717ec9920007058176433189cf36390", null ],
      [ "evt_type", "structserial__evt__model__specific__header__t.html#a0e32a31621a5a5c3d3ea168adab3dffa", null ]
    ] ],
    [ "serial_evt_model_specific_t", "structserial__evt__model__specific__t.html", [
      [ "model_evt_info", "structserial__evt__model__specific__t.html#a2893b633692ba96b7f6109cc08c51dea", null ],
      [ "data", "structserial__evt__model__specific__t.html#abbfd57e9d60a475e19e24cea08178837", null ]
    ] ],
    [ "serial_evt_application_t", "structserial__evt__application__t.html", [
      [ "data", "structserial__evt__application__t.html#a6dce592a1c25cdc56e8bbab7c2c5cc67", null ]
    ] ],
    [ "serial_evt_mesh_message_received_t", "structserial__evt__mesh__message__received__t.html", [
      [ "src", "structserial__evt__mesh__message__received__t.html#a69b8a40729617f09d7454c623e1e25f3", null ],
      [ "dst", "structserial__evt__mesh__message__received__t.html#a0611e6e4317820c1103ece48d37aa20a", null ],
      [ "appkey_handle", "structserial__evt__mesh__message__received__t.html#ab8755221a70c086cc8364770561a5086", null ],
      [ "subnet_handle", "structserial__evt__mesh__message__received__t.html#a5f4caababe4c9f8658c186494e09621a", null ],
      [ "ttl", "structserial__evt__mesh__message__received__t.html#adc696eca2e3aea31fa89a622a9d4e789", null ],
      [ "adv_addr_type", "structserial__evt__mesh__message__received__t.html#a00f4083eb360327d51d4e1da077df667", null ],
      [ "adv_addr", "structserial__evt__mesh__message__received__t.html#a46b15657c39d524e99a9e06617456787", null ],
      [ "rssi", "structserial__evt__mesh__message__received__t.html#af300d4e98e84c2426b69043a6fadf0ce", null ],
      [ "actual_length", "structserial__evt__mesh__message__received__t.html#afde163ec54c25b6a65c46845813b2f9c", null ],
      [ "data", "structserial__evt__mesh__message__received__t.html#a4e29e3adaf7d2ccfc0a5257e93d2186c", null ]
    ] ],
    [ "serial_evt_mesh_iv_update_t", "structserial__evt__mesh__iv__update__t.html", [
      [ "iv_index", "structserial__evt__mesh__iv__update__t.html#a59599af483fadd1227b6d13ce541351d", null ]
    ] ],
    [ "serial_evt_mesh_key_refresh_t", "structserial__evt__mesh__key__refresh__t.html", [
      [ "netkey_index", "structserial__evt__mesh__key__refresh__t.html#a6bf614d7090b523cbf4035001551d2dd", null ],
      [ "phase", "structserial__evt__mesh__key__refresh__t.html#a63dbca17c352a0365b801b4a74ffd6cb", null ]
    ] ],
    [ "serial_evt_mesh_t", "unionserial__evt__mesh__t.html", [
      [ "message_received", "unionserial__evt__mesh__t.html#a599dc9c50c1c683d1498a0fe7c5dfa8b", null ],
      [ "iv_update", "unionserial__evt__mesh__t.html#a7c3c9fab4ebc3e756f902737091ac281", null ],
      [ "key_refresh", "unionserial__evt__mesh__t.html#a76b262e609d65b35fdd6c4df54f9b7fe", null ]
    ] ],
    [ "serial_evt_dfu_req_relay_t", "structserial__evt__dfu__req__relay__t.html", [
      [ "dfu_type", "structserial__evt__dfu__req__relay__t.html#a9710478c3b051f3cdb44d00a766d3e20", null ],
      [ "fwid", "structserial__evt__dfu__req__relay__t.html#a477f229e8302c35eeef00e90ea2d7b9e", null ],
      [ "authority", "structserial__evt__dfu__req__relay__t.html#aa89868cc1b867b3bcbc4551aa000a202", null ]
    ] ],
    [ "serial_evt_dfu_req_source_t", "structserial__evt__dfu__req__source__t.html", [
      [ "dfu_type", "structserial__evt__dfu__req__source__t.html#a82ec65d364fbe228f7bd608784fc99f7", null ]
    ] ],
    [ "serial_evt_dfu_start_t", "structserial__evt__dfu__start__t.html", [
      [ "role", "structserial__evt__dfu__start__t.html#a882cdf5efcbea91cfce3b3c6dc206a2e", null ],
      [ "dfu_type", "structserial__evt__dfu__start__t.html#a41c4f516f3e5f668b1a8a5ea11a7e788", null ],
      [ "fwid", "structserial__evt__dfu__start__t.html#ad4d46b5253afe991d9fd565e7ddbe1cc", null ]
    ] ],
    [ "serial_evt_dfu_end_t", "structserial__evt__dfu__end__t.html", [
      [ "role", "structserial__evt__dfu__end__t.html#ad32fade974df876d48e00c6adafdddd9", null ],
      [ "dfu_type", "structserial__evt__dfu__end__t.html#a142a7e50f67e9eb86210022cb0fac6cd", null ],
      [ "fwid", "structserial__evt__dfu__end__t.html#af3328e40569a821e0f1cc61860883be6", null ],
      [ "end_reason", "structserial__evt__dfu__end__t.html#ab45dddf22d3d244ee230756801bde76b", null ]
    ] ],
    [ "serial_evt_dfu_bank_t", "structserial__evt__dfu__bank__t.html", [
      [ "dfu_type", "structserial__evt__dfu__bank__t.html#a7d0485823c9cb9bee969e1442041663c", null ],
      [ "fwid", "structserial__evt__dfu__bank__t.html#ac101d712f5dc0d3fe87ce2396f02c356", null ],
      [ "start_addr", "structserial__evt__dfu__bank__t.html#ac0449a113007bafaa7ffdb8bb8c53622", null ],
      [ "length", "structserial__evt__dfu__bank__t.html#ab9d797919df664b23777d0bbbcd55fe8", null ],
      [ "is_signed", "structserial__evt__dfu__bank__t.html#a264b1990671105099211752c2314ad66", null ]
    ] ],
    [ "serial_evt_dfu_firmware_outdated_t", "structserial__evt__dfu__firmware__outdated__t.html", [
      [ "dfu_type", "structserial__evt__dfu__firmware__outdated__t.html#a367e69502dcf203f3a8d6838c4ee837f", null ],
      [ "available_fwid", "structserial__evt__dfu__firmware__outdated__t.html#af4e400f0c87a40d58310d9359a38fd99", null ],
      [ "current_fwid", "structserial__evt__dfu__firmware__outdated__t.html#ac783f3c7d50fd68709029b9b5c157aa4", null ]
    ] ],
    [ "serial_evt_dfu_t", "unionserial__evt__dfu__t.html", [
      [ "req_relay", "unionserial__evt__dfu__t.html#a28ece4a12b3fe432abffa1a32c3ce037", null ],
      [ "req_source", "unionserial__evt__dfu__t.html#a1c9d8ecf8a789f1d72173bde0003b93e", null ],
      [ "start", "unionserial__evt__dfu__t.html#a48c6f241fe122ab2b35285aa5dc735e0", null ],
      [ "end", "unionserial__evt__dfu__t.html#a97a11e97cba592cdacf21e57581db85d", null ],
      [ "bank", "unionserial__evt__dfu__t.html#a32d3c2a2c5510d225f4cc14020cd3072", null ],
      [ "firmware_outdated", "unionserial__evt__dfu__t.html#acd9e5f37c94d53f13b0c2dde524a2d6a", null ]
    ] ],
    [ "serial_evt_t", "unionserial__evt__t.html", [
      [ "cmd_rsp", "unionserial__evt__t.html#ac2b072d82637be87565f28fe9b788bf2", null ],
      [ "device", "unionserial__evt__t.html#a914b6749067bf96876d9822c44ec772f", null ],
      [ "prov", "unionserial__evt__t.html#a03098a8fe7f80e7f4a205f5da44ecbb4", null ],
      [ "application", "unionserial__evt__t.html#a5221d7ca19fbe81ab3ed7c7735249eec", null ],
      [ "mesh", "unionserial__evt__t.html#a2a1c58c5c35feeb5bacd8a8764ec2a99", null ],
      [ "dfu", "unionserial__evt__t.html#af3a11044cc6dc867360cf7d95ff66168", null ],
      [ "model", "unionserial__evt__t.html#a5dd6f57c5579955afdd66133e5b9cc2b", null ]
    ] ],
    [ "SERIAL_OPCODE_EVT_CMD_RSP", "group__SERIAL__EVT.html#ga3718bfc55b34115256039211fa88658d", null ],
    [ "SERIAL_OPCODE_EVT_DEVICE_STARTED", "group__SERIAL__EVT.html#gac839ac94d46bfc3b433be0c449dfcb87", null ],
    [ "SERIAL_OPCODE_EVT_DEVICE_ECHO_RSP", "group__SERIAL__EVT.html#ga9b37d913bb5b6f6d9ede00630560cbcf", null ],
    [ "SERIAL_OPCODE_EVT_DEVICE_INTERNAL_EVENT", "group__SERIAL__EVT.html#gaac3de3a4921a180ad4541538a8cbda72", null ],
    [ "SERIAL_OPCODE_EVT_APPLICATION", "group__SERIAL__EVT.html#ga15109ffcbdd04633fb201a5e0935bac2", null ],
    [ "SERIAL_OPCODE_EVT_SAR_START", "group__SERIAL__EVT.html#gac1295aaeeaacfd8c82eb0bff5a555ebb", null ],
    [ "SERIAL_OPCODE_EVT_SAR_CONTINUE", "group__SERIAL__EVT.html#gaa589dab1f4ea2c6a3df2a66c9cb05aed", null ],
    [ "SERIAL_OPCODE_EVT_DFU_REQ_RELAY", "group__SERIAL__EVT.html#ga2934149a764dd5caf4424d066003358e", null ],
    [ "SERIAL_OPCODE_EVT_DFU_REQ_SOURCE", "group__SERIAL__EVT.html#gaf4bfe9d5fd749d60c8bb59b39035c899", null ],
    [ "SERIAL_OPCODE_EVT_DFU_START", "group__SERIAL__EVT.html#ga7927dc84b68e29f13194b857db42a643", null ],
    [ "SERIAL_OPCODE_EVT_DFU_END", "group__SERIAL__EVT.html#gaab4f2ee00f9aaab28cb446f6c7b22cf1", null ],
    [ "SERIAL_OPCODE_EVT_DFU_BANK_AVAILABLE", "group__SERIAL__EVT.html#gab5e7ed230a536c06bf40adcf496c3213", null ],
    [ "SERIAL_OPCODE_EVT_DFU_FIRMWARE_OUTDATED", "group__SERIAL__EVT.html#ga2a23d82b75f28059113b30a1af18b3ac", null ],
    [ "SERIAL_OPCODE_EVT_DFU_FIRMWARE_OUTDATED_NO_AUTH", "group__SERIAL__EVT.html#ga69812fdfcd6cff5b45c1ac1680fa8137", null ],
    [ "SERIAL_OPCODE_EVT_OPENMESH_NEW", "group__SERIAL__EVT.html#gae84671ea1d6617265fed679fa30d38bc", null ],
    [ "SERIAL_OPCODE_EVT_OPENMESH_UPDATE", "group__SERIAL__EVT.html#gad5c580515bc03a2460db5e6d225c49af", null ],
    [ "SERIAL_OPCODE_EVT_OPENMESH_CONFLICTING", "group__SERIAL__EVT.html#ga444eb2eed89a1793aa664b752e79066d", null ],
    [ "SERIAL_OPCODE_EVT_OPENMESH_TX", "group__SERIAL__EVT.html#ga361203fb0a32fa87193ea91e3267ff74", null ],
    [ "SERIAL_OPCODE_EVT_PROV_UNPROVISIONED_RECEIVED", "group__SERIAL__EVT.html#gac709b7fd4621ca6c642d47fac15eadf8", null ],
    [ "SERIAL_OPCODE_EVT_PROV_LINK_ESTABLISHED", "group__SERIAL__EVT.html#gae6546dd5b44e854df6afa08fd3bbb129", null ],
    [ "SERIAL_OPCODE_EVT_PROV_LINK_CLOSED", "group__SERIAL__EVT.html#gaa32f067d4a5cee8e32339b403ab07e53", null ],
    [ "SERIAL_OPCODE_EVT_PROV_CAPS_RECEIVED", "group__SERIAL__EVT.html#gafda4985ae73f9b6178d934888c6af471", null ],
    [ "SERIAL_OPCODE_EVT_PROV_COMPLETE", "group__SERIAL__EVT.html#ga0fa6fdaaf8ae1619e4f2dab50da56f2a", null ],
    [ "SERIAL_OPCODE_EVT_PROV_AUTH_REQUEST", "group__SERIAL__EVT.html#ga8834f75c35e902fdac19635e0d0df286", null ],
    [ "SERIAL_OPCODE_EVT_PROV_ECDH_REQUEST", "group__SERIAL__EVT.html#ga2e9ff1ce73779aaeddb0ac09630c11bc", null ],
    [ "SERIAL_OPCODE_EVT_PROV_OUTPUT_REQUEST", "group__SERIAL__EVT.html#ga0fb33c9d1404d6da6d27a38ac229eaf7", null ],
    [ "SERIAL_OPCODE_EVT_PROV_FAILED", "group__SERIAL__EVT.html#gad8135fe7c88ce9b9f22666f974918f8f", null ],
    [ "SERIAL_OPCODE_EVT_MESH_MESSAGE_RECEIVED_UNICAST", "group__SERIAL__EVT.html#ga76db791e0780e255e29a6525dddb4059", null ],
    [ "SERIAL_OPCODE_EVT_MESH_MESSAGE_RECEIVED_SUBSCRIPTION", "group__SERIAL__EVT.html#gac3c2f8bff0996c4075cfd84b259409d7", null ],
    [ "SERIAL_OPCODE_EVT_MESH_TX_COMPLETE", "group__SERIAL__EVT.html#ga582d88a29018ba1d3754500d277592f8", null ],
    [ "SERIAL_OPCODE_EVT_MESH_IV_UPDATE_NOTIFICATION", "group__SERIAL__EVT.html#ga3d48f4010fcc907293ea9aafbee9d1d1", null ],
    [ "SERIAL_OPCODE_EVT_MESH_KEY_REFRESH_NOTIFICATION", "group__SERIAL__EVT.html#ga7e22281f8f5173ea1d6c56882f8a435c", null ],
    [ "SERIAL_OPCODE_EVT_MESH_SAR_FAILED", "group__SERIAL__EVT.html#gacc0f67b65846dc94aae9d68774e02e28", null ],
    [ "SERIAL_OPCODE_EVT_MODEL_SPECIFIC", "group__SERIAL__EVT.html#ga0461cb60f8b3074202280408cb75c643", null ]
];