var structconfig__publication__state__t =
[
    [ "element_address", "structconfig__publication__state__t.html#abf5cf48669de7d86b147f1b0f1c04e01", null ],
    [ "publish_address", "structconfig__publication__state__t.html#a3e36c494f999806a1413e9102d80b699", null ],
    [ "appkey_index", "structconfig__publication__state__t.html#a73e19a1dedcae8cdf30ebad7202ef3b7", null ],
    [ "frendship_credential_flag", "structconfig__publication__state__t.html#a74d534213e5c5ec6540e10436778d3ae", null ],
    [ "publish_ttl", "structconfig__publication__state__t.html#a538feae027ae5d95047b0f64e2cb506d", null ],
    [ "publish_period", "structconfig__publication__state__t.html#a96dfb70b66e26c367b7e055e8ba2c560", null ],
    [ "retransmit_count", "structconfig__publication__state__t.html#a78ca10d553d26efd2f13c2ee04bfd91e", null ],
    [ "retransmit_interval", "structconfig__publication__state__t.html#a48111953585606b738e3bf14986660c0", null ],
    [ "model_id", "structconfig__publication__state__t.html#a475153a026b879b2055c764847e29651", null ]
];