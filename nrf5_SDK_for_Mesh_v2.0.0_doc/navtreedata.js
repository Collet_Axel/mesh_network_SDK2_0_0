var NAVTREE =
[
  [ "nRF5 SDK for Mesh v2.0.0", "index.html", [
    [ "Introduction to nRF5 SDK for Mesh", "index.html", "index" ],
    [ "Libraries", "LIBRARIES.html", "LIBRARIES" ],
    [ "Scripts", "SCRIPTS.html", "SCRIPTS" ],
    [ "Getting started", "md_doc_getting_started_getting_started.html", "md_doc_getting_started_getting_started" ],
    [ "Examples", "md_examples_README.html", "md_examples_README" ],
    [ "API Reference", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"LIBRARIES.html",
"group__CONFIG__CLIENT.html#gaa29255fbc54f71ff3f9ce6f0814c54e9",
"group__FLASH__MANAGER__TYPES.html#ga4f56320a6e1b02b3cbe77707a9127280",
"group__MESH__CORE__COMMON__TYPES.html#ggaca5284d096ab82171c9d0a18fbaf9623a3b2bfa590834332f242ec8f897fe46a3",
"group__NRF__MESH__PROV.html#ga8e5ffea0ce2ca0644f6c11d0466de885",
"group__PROV__BEARER__INTERFACE.html",
"group__SERIAL__INTERFACE.html",
"md_doc_libraries_serial_evt.html#openmesh-tx",
"structconfig__msg__model__app__get__t.html#a1917c7c8f1b9929ddeece4b7940eece8",
"structhealth__msg__period__set__t.html#a8716ec1636207fc95e636d174487518e",
"structnrf__mesh__prov__evt__link__closed__t.html#afff3d1f284ce8df3a80f2198805ea235",
"structserial__cmd__mesh__appkey__update__t.html",
"structserial__evt__mesh__message__received__t.html#a00f4083eb360327d51d4e1da077df667",
"unionserial__evt__t.html#af3a11044cc6dc867360cf7d95ff66168"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';