var structserial__cmd__prov__caps__t =
[
    [ "num_elements", "structserial__cmd__prov__caps__t.html#a812a2d1034418970deb3825c2cdb03a4", null ],
    [ "public_key_type", "structserial__cmd__prov__caps__t.html#ad16d6be61fedd42a438daf888954db59", null ],
    [ "static_oob_types", "structserial__cmd__prov__caps__t.html#a2f5d3780aa9f53b8533469a3d3e29a59", null ],
    [ "output_oob_size", "structserial__cmd__prov__caps__t.html#a308da47632e3dfa7b28aae5ba9410d19", null ],
    [ "output_oob_actions", "structserial__cmd__prov__caps__t.html#a97b3563e088c0c622a1e563e6692f91a", null ],
    [ "input_oob_size", "structserial__cmd__prov__caps__t.html#a7700284f28905703a8a8b5e4164eb270", null ],
    [ "input_oob_actions", "structserial__cmd__prov__caps__t.html#a7d3a4898326eaca68efe7d0340c6c9b8", null ]
];