var structnrf__mesh__prov__evt__unprov__t =
[
    [ "device_uuid", "structnrf__mesh__prov__evt__unprov__t.html#a9c0c6f08eebed0d9366675f72d9248c5", null ],
    [ "gatt_supported", "structnrf__mesh__prov__evt__unprov__t.html#abb06f5ace019501f30cd9c9a2e143f90", null ],
    [ "uri_hash_present", "structnrf__mesh__prov__evt__unprov__t.html#ab86fe76cdeb1c3c9772995cd54dd2931", null ],
    [ "uri_hash", "structnrf__mesh__prov__evt__unprov__t.html#a1476344f42b035a85a735390cec9e402", null ],
    [ "p_metadata", "structnrf__mesh__prov__evt__unprov__t.html#a018fe06bd70dba9b650ad503e3e3a669", null ]
];