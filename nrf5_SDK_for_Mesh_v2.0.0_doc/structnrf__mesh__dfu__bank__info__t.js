var structnrf__mesh__dfu__bank__info__t =
[
    [ "dfu_type", "structnrf__mesh__dfu__bank__info__t.html#a6d2c3edd0a06f7006cd66d911810dac3", null ],
    [ "fwid", "structnrf__mesh__dfu__bank__info__t.html#ae9d044e69ab0acfbd533897ffc2ec2c5", null ],
    [ "is_signed", "structnrf__mesh__dfu__bank__info__t.html#a844f4267f92aff9eef255ab8217e8270", null ],
    [ "p_start_addr", "structnrf__mesh__dfu__bank__info__t.html#a3291be1cc291d3156881a3665786c29f", null ],
    [ "length", "structnrf__mesh__dfu__bank__info__t.html#ade99683a36c1585c432b5d1c89ed175f", null ]
];