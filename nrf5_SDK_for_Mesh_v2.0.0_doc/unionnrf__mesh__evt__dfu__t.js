var unionnrf__mesh__evt__dfu__t =
[
    [ "transfer", "unionnrf__mesh__evt__dfu__t.html#a4a4fdda44a76a7927032b5f197271e8b", null ],
    [ "current", "unionnrf__mesh__evt__dfu__t.html#a872ac8d0ee07e758c94ba38612a423a8", null ],
    [ "fw_outdated", "unionnrf__mesh__evt__dfu__t.html#a413feb83aef81a34b95a92c548ef36af", null ],
    [ "authority", "unionnrf__mesh__evt__dfu__t.html#a83d0e48015323d842960adc1e033eb68", null ],
    [ "req_relay", "unionnrf__mesh__evt__dfu__t.html#a1d47cbe21743cb7d6457076faddb6785", null ],
    [ "dfu_type", "unionnrf__mesh__evt__dfu__t.html#af9d594240f04663f90cae33fcf4e77c2", null ],
    [ "req_source", "unionnrf__mesh__evt__dfu__t.html#a4217d83fa050d35c5a91269152c229ab", null ],
    [ "role", "unionnrf__mesh__evt__dfu__t.html#a65dad8210ed574c85f358e883f265f73", null ],
    [ "start", "unionnrf__mesh__evt__dfu__t.html#a9d3c36ef5b3a8bc1dd8c091e2aaa85fe", null ],
    [ "end_reason", "unionnrf__mesh__evt__dfu__t.html#a59fd8f27595860d244f24693d9db110a", null ],
    [ "end", "unionnrf__mesh__evt__dfu__t.html#afbc1dedb01e40a60ba245d84cb2cb65c", null ],
    [ "p_start_addr", "unionnrf__mesh__evt__dfu__t.html#a297aaa37324f8e08bccfcf86980ba5dc", null ],
    [ "length", "unionnrf__mesh__evt__dfu__t.html#accec55f610639d6ddf1e082c2fc2d082", null ],
    [ "is_signed", "unionnrf__mesh__evt__dfu__t.html#ada46e8dfe39c5ec7fbc4d918156a0821", null ],
    [ "bank", "unionnrf__mesh__evt__dfu__t.html#a2f338b6069206eebf9945be7fb1635e8", null ]
];