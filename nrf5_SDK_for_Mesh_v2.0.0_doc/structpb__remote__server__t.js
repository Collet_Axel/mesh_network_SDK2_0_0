var structpb__remote__server__t =
[
    [ "model_handle", "structpb__remote__server__t.html#a2f607dbf18929c9353c4b8526c115af3", null ],
    [ "state", "structpb__remote__server__t.html#a3a799d476b0d75b1366b60603ba99081", null ],
    [ "prev_state", "structpb__remote__server__t.html#a9d8248625e3689114800b29dfd13e964", null ],
    [ "p_prov_bearer", "structpb__remote__server__t.html#adf97f5933acdecec0f3da2f0f9331e7f", null ],
    [ "reliable", "structpb__remote__server__t.html#ab4035e81e17155513bd9459a0c43621e", null ],
    [ "return_to_scan_enabled", "structpb__remote__server__t.html#aa19e2f0b48077b54ebac0624fdb2b0a9", null ],
    [ "ctid", "structpb__remote__server__t.html#a7848ce7903068d4344ee800964edbc90", null ]
];