var group__MESH__API__GROUP__BEARER__FILTER =
[
    [ "AD type filtering", "group__AD__TYPE__FILTER.html", "group__AD__TYPE__FILTER" ],
    [ "Advertisement packet type filtering", "group__ADV__TYPE__FILTER.html", "group__ADV__TYPE__FILTER" ],
    [ "GAP address filtering", "group__GAP__ADDRESS__FILTER.html", "group__GAP__ADDRESS__FILTER" ],
    [ "RSSI packet filtering", "group__RSSI__FILTER.html", "group__RSSI__FILTER" ]
];