var group__FLASH__MANAGER =
[
    [ "Defines", "group__FLASH__MANAGER__DEFINES.html", "group__FLASH__MANAGER__DEFINES" ],
    [ "Types", "group__FLASH__MANAGER__TYPES.html", "group__FLASH__MANAGER__TYPES" ],
    [ "flash_manager_init", "group__FLASH__MANAGER.html#gaa86518f5db7281636a436c294f7d26b6", null ],
    [ "flash_manager_add", "group__FLASH__MANAGER.html#gab6e54a4c38987fec929d5814057617d9", null ],
    [ "flash_manager_remove", "group__FLASH__MANAGER.html#ga9f669dd67610667de482fddf8f18fd98", null ],
    [ "flash_manager_entry_get", "group__FLASH__MANAGER.html#ga52045717ba7a7a13e61dd42cf45a7733", null ],
    [ "flash_manager_entry_next_get", "group__FLASH__MANAGER.html#gaa41485a5838b2fd1214cb13a217ed335", null ],
    [ "flash_manager_entry_count_get", "group__FLASH__MANAGER.html#ga86f8b86e915715efdef7d347ac7e47fc", null ],
    [ "flash_manager_entry_alloc", "group__FLASH__MANAGER.html#ga2939a867d8e719bc71d532c0b92d4eda", null ],
    [ "flash_manager_entry_commit", "group__FLASH__MANAGER.html#ga66eb895ac9278563ffb4a3b0ccdeb031", null ],
    [ "flash_manager_entry_invalidate", "group__FLASH__MANAGER.html#ga019781b71fbd76fcfc8c7aca5c94550a", null ],
    [ "flash_manager_entry_release", "group__FLASH__MANAGER.html#ga4c77a7f0c51b4dc02c06ac0d7c996c92", null ],
    [ "flash_manager_mem_listener_register", "group__FLASH__MANAGER.html#ga473da0769e090be92d94b3eb799946b9", null ],
    [ "flash_manager_is_stable", "group__FLASH__MANAGER.html#gac268125ffabd5dee64198bd32d55c5b9", null ],
    [ "flash_manager_action_queue_empty_cb_set", "group__FLASH__MANAGER.html#gad48b0d5562de5f259e7cb82206933f81", null ],
    [ "flash_manager_recovery_page_get", "group__FLASH__MANAGER.html#ga8d71b1dfcbd54d403ba878af8a879c71", null ],
    [ "flash_manager_wait", "group__FLASH__MANAGER.html#ga81d10b4c8fdbf403f1228a35dc80bf1b", null ]
];