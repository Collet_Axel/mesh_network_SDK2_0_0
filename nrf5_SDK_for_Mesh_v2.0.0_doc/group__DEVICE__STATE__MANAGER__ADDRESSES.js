var group__DEVICE__STATE__MANAGER__ADDRESSES =
[
    [ "dsm_address_publish_add", "group__DEVICE__STATE__MANAGER__ADDRESSES.html#ga7b476185af2eb0d85888dd96d8777634", null ],
    [ "dsm_address_publish_add_handle", "group__DEVICE__STATE__MANAGER__ADDRESSES.html#gacee818bbf838186c2b2731f6b791b913", null ],
    [ "dsm_address_publish_virtual_add", "group__DEVICE__STATE__MANAGER__ADDRESSES.html#gafce6a8754ddf1ba3cbe5bf4825bbb2dc", null ],
    [ "dsm_address_publish_remove", "group__DEVICE__STATE__MANAGER__ADDRESSES.html#ga983f4d3e0bd84eccb1f80cfe7bfbf181", null ],
    [ "dsm_local_unicast_addresses_set", "group__DEVICE__STATE__MANAGER__ADDRESSES.html#ga58474c743fe38504256d3f0a9416c12f", null ],
    [ "dsm_local_unicast_addresses_get", "group__DEVICE__STATE__MANAGER__ADDRESSES.html#gab2ecfa4fbfd7fa1ceb60a1b079d0cb03", null ],
    [ "dsm_address_subscription_add", "group__DEVICE__STATE__MANAGER__ADDRESSES.html#gac51ab6b7bd274a54ab26ca19cd6ad7bc", null ],
    [ "dsm_address_subscription_virtual_add", "group__DEVICE__STATE__MANAGER__ADDRESSES.html#gad00e349a7d706dec47f5d7903bb83219", null ],
    [ "dsm_address_subscription_add_handle", "group__DEVICE__STATE__MANAGER__ADDRESSES.html#gab3ebcc96e9676eb677178e18ef4dcc5f", null ],
    [ "dsm_address_subscription_get", "group__DEVICE__STATE__MANAGER__ADDRESSES.html#ga8bae9453d84c2558f44ead09a2731e05", null ],
    [ "dsm_address_subscription_count_get", "group__DEVICE__STATE__MANAGER__ADDRESSES.html#ga44a9fc0a0bc7fbafa68879c04c9eb41a", null ],
    [ "dsm_address_subscription_remove", "group__DEVICE__STATE__MANAGER__ADDRESSES.html#gad6b43f51b30aae48ea2cbb319aea34fe", null ],
    [ "dsm_address_get", "group__DEVICE__STATE__MANAGER__ADDRESSES.html#ga0007030f3bf34e7c56e3ea47aa7bcd90", null ],
    [ "dsm_address_get_all", "group__DEVICE__STATE__MANAGER__ADDRESSES.html#ga3a7d028f1b82524bbee08a8967d6c348", null ],
    [ "dsm_address_handle_get", "group__DEVICE__STATE__MANAGER__ADDRESSES.html#ga779c8afa3d489b7b35dda08ffee80a48", null ]
];