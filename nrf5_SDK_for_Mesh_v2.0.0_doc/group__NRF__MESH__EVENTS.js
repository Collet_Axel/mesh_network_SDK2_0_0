var group__NRF__MESH__EVENTS =
[
    [ "nrf_mesh_evt_message_t", "structnrf__mesh__evt__message__t.html", [
      [ "p_buffer", "structnrf__mesh__evt__message__t.html#ab0503fa847446d288d53ca7cd1ed3679", null ],
      [ "length", "structnrf__mesh__evt__message__t.html#a9e6613ed62e2ddd706b605b044b2c0b3", null ],
      [ "src", "structnrf__mesh__evt__message__t.html#a2b546364b29b5f90dfbba03f45c3116f", null ],
      [ "dst", "structnrf__mesh__evt__message__t.html#a90df61096470736f82c04693d36d0e45", null ],
      [ "secmat", "structnrf__mesh__evt__message__t.html#af50d5f27a08dfad5932961a5602c5403", null ],
      [ "ttl", "structnrf__mesh__evt__message__t.html#ac19b5d19aff6161382251c5953e9ff01", null ],
      [ "p_metadata", "structnrf__mesh__evt__message__t.html#a9b671a5c8d2dbe56e755ab79f162e74a", null ]
    ] ],
    [ "nrf_mesh_evt_iv_update_notification_t", "structnrf__mesh__evt__iv__update__notification__t.html", [
      [ "state", "structnrf__mesh__evt__iv__update__notification__t.html#ae735ee1fad8094fd5bac20926b7a782a", null ],
      [ "p_network_id", "structnrf__mesh__evt__iv__update__notification__t.html#a08ab0fcfa51fbe89d4f544c2bc58df7b", null ],
      [ "iv_index", "structnrf__mesh__evt__iv__update__notification__t.html#ac8b3c1ad0ed84784773260abb1cde0ae", null ]
    ] ],
    [ "nrf_mesh_evt_key_refresh_notification_t", "structnrf__mesh__evt__key__refresh__notification__t.html", [
      [ "subnet_index", "structnrf__mesh__evt__key__refresh__notification__t.html#a6c317c9b51d3e63026961f29bb4b10e2", null ],
      [ "p_network_id", "structnrf__mesh__evt__key__refresh__notification__t.html#abca04fdaf106cde99abeffc031f926d6", null ],
      [ "phase", "structnrf__mesh__evt__key__refresh__notification__t.html#ab979ae70d1c66ed47b50ffd3337224dc", null ]
    ] ],
    [ "nrf_mesh_evt_net_beacon_received_t", "structnrf__mesh__evt__net__beacon__received__t.html", [
      [ "p_beacon_info", "structnrf__mesh__evt__net__beacon__received__t.html#ac57fcb6ef52c5fb08f60609cd96a5b97", null ],
      [ "p_beacon_secmat", "structnrf__mesh__evt__net__beacon__received__t.html#a99dbf915240e323abd3d7403b06fb912", null ],
      [ "p_rx_metadata", "structnrf__mesh__evt__net__beacon__received__t.html#a81e250ad960285cc165a40f4ad0e0d2f", null ],
      [ "p_auth_value", "structnrf__mesh__evt__net__beacon__received__t.html#ac482bfe8f18844df8ea89f3f2500c755", null ],
      [ "iv_index", "structnrf__mesh__evt__net__beacon__received__t.html#a1a8e5fe22fad8efae6adc7c064656e3b", null ],
      [ "iv_update", "structnrf__mesh__evt__net__beacon__received__t.html#ae2b92cfad72c0997f47d9eb177608e13", null ],
      [ "key_refresh", "structnrf__mesh__evt__net__beacon__received__t.html#a569a6d10fe7f584a7b09c65be14f265a", null ],
      [ "flags", "structnrf__mesh__evt__net__beacon__received__t.html#a85da6d0243f3e08196ac1c160e11b8f0", null ]
    ] ],
    [ "nrf_mesh_evt_hb_message_t", "structnrf__mesh__evt__hb__message__t.html", [
      [ "init_ttl", "structnrf__mesh__evt__hb__message__t.html#a45a5823b2744a7de17580e5feb5e2dc4", null ],
      [ "hops", "structnrf__mesh__evt__hb__message__t.html#a42837b4dd2372b2c2ca18a914643ebdc", null ],
      [ "features", "structnrf__mesh__evt__hb__message__t.html#ad84e3ca1e55d4d2e163b05076c6b4563", null ],
      [ "src", "structnrf__mesh__evt__hb__message__t.html#a10bdd1a4eab3b57787e214887081bc5d", null ]
    ] ],
    [ "nrf_mesh_evt_tx_complete_t", "structnrf__mesh__evt__tx__complete__t.html", [
      [ "token", "structnrf__mesh__evt__tx__complete__t.html#a0245c02c9a131ea25824e913803fa2f4", null ]
    ] ],
    [ "nrf_mesh_evt_dfu_t", "unionnrf__mesh__evt__dfu__t.html", [
      [ "transfer", "unionnrf__mesh__evt__dfu__t.html#a4a4fdda44a76a7927032b5f197271e8b", null ],
      [ "current", "unionnrf__mesh__evt__dfu__t.html#a872ac8d0ee07e758c94ba38612a423a8", null ],
      [ "fw_outdated", "unionnrf__mesh__evt__dfu__t.html#a413feb83aef81a34b95a92c548ef36af", null ],
      [ "authority", "unionnrf__mesh__evt__dfu__t.html#a83d0e48015323d842960adc1e033eb68", null ],
      [ "req_relay", "unionnrf__mesh__evt__dfu__t.html#a1d47cbe21743cb7d6457076faddb6785", null ],
      [ "dfu_type", "unionnrf__mesh__evt__dfu__t.html#af9d594240f04663f90cae33fcf4e77c2", null ],
      [ "req_source", "unionnrf__mesh__evt__dfu__t.html#a4217d83fa050d35c5a91269152c229ab", null ],
      [ "role", "unionnrf__mesh__evt__dfu__t.html#a65dad8210ed574c85f358e883f265f73", null ],
      [ "start", "unionnrf__mesh__evt__dfu__t.html#a9d3c36ef5b3a8bc1dd8c091e2aaa85fe", null ],
      [ "end_reason", "unionnrf__mesh__evt__dfu__t.html#a59fd8f27595860d244f24693d9db110a", null ],
      [ "end", "unionnrf__mesh__evt__dfu__t.html#afbc1dedb01e40a60ba245d84cb2cb65c", null ],
      [ "p_start_addr", "unionnrf__mesh__evt__dfu__t.html#a297aaa37324f8e08bccfcf86980ba5dc", null ],
      [ "length", "unionnrf__mesh__evt__dfu__t.html#accec55f610639d6ddf1e082c2fc2d082", null ],
      [ "is_signed", "unionnrf__mesh__evt__dfu__t.html#ada46e8dfe39c5ec7fbc4d918156a0821", null ],
      [ "bank", "unionnrf__mesh__evt__dfu__t.html#a2f338b6069206eebf9945be7fb1635e8", null ]
    ] ],
    [ "nrf_mesh_evt_rx_failed_t", "structnrf__mesh__evt__rx__failed__t.html", [
      [ "src", "structnrf__mesh__evt__rx__failed__t.html#ac92b7168a3c8982071fa4a01521d5693", null ],
      [ "ivi", "structnrf__mesh__evt__rx__failed__t.html#a13e92c05c8b8ae83bfeb091aaa8fcf2d", null ],
      [ "reason", "structnrf__mesh__evt__rx__failed__t.html#a7f083c7379d0b92a52206e3bca73c3bf", null ]
    ] ],
    [ "nrf_mesh_evt_sar_failed_t", "structnrf__mesh__evt__sar__failed__t.html", [
      [ "token", "structnrf__mesh__evt__sar__failed__t.html#a7506fb4267444961f1c05f7f0957c2b2", null ],
      [ "reason", "structnrf__mesh__evt__sar__failed__t.html#a7997d1d235e1aac1b6f49086a8310065", null ]
    ] ],
    [ "nrf_mesh_evt_flash_failed_t", "structnrf__mesh__evt__flash__failed__t.html", [
      [ "user", "structnrf__mesh__evt__flash__failed__t.html#a7b1bf5997e112b2b917aab911808c9e6", null ],
      [ "p_flash_entry", "structnrf__mesh__evt__flash__failed__t.html#aa40efcde01861482a343734fc8ac8c3f", null ],
      [ "p_flash_page", "structnrf__mesh__evt__flash__failed__t.html#aa5ba14c8ce7442b7545ae5eacb984ff8", null ],
      [ "p_area", "structnrf__mesh__evt__flash__failed__t.html#ae0fd44ef2adb87cd222182df3d5f3e1b", null ],
      [ "page_count", "structnrf__mesh__evt__flash__failed__t.html#ae150ac4476a12b867f2b8dd72187d731", null ]
    ] ],
    [ "nrf_mesh_evt_t", "structnrf__mesh__evt__t.html", [
      [ "type", "structnrf__mesh__evt__t.html#a048088324c4efc9d6b8b9bd7eb5776c7", null ],
      [ "message", "structnrf__mesh__evt__t.html#a42d64c5764d6da38229b58089fcf9ca0", null ],
      [ "tx_complete", "structnrf__mesh__evt__t.html#aea7d680a17fd8cdc68636fd4b2cd9ef3", null ],
      [ "iv_update", "structnrf__mesh__evt__t.html#a21a99751204ca492eec6b875777f71e5", null ],
      [ "key_refresh", "structnrf__mesh__evt__t.html#a0cbd24fa9afa48103abfe2226d07e2c3", null ],
      [ "net_beacon", "structnrf__mesh__evt__t.html#a1ed7851a3c350eaca69caef19ad0723b", null ],
      [ "hb_message", "structnrf__mesh__evt__t.html#adc287e72efc93cb2328325758b9248a1", null ],
      [ "dfu", "structnrf__mesh__evt__t.html#a632466c82261ee42c2639d887df9586b", null ],
      [ "rx_failed", "structnrf__mesh__evt__t.html#a55958469d60840dec3996b0cf485382c", null ],
      [ "sar_failed", "structnrf__mesh__evt__t.html#a246478b48c0597e64d99bbfdaf73cdee", null ],
      [ "flash_failed", "structnrf__mesh__evt__t.html#afa7dba5432ee97098c04e76e92aa52cf", null ],
      [ "params", "structnrf__mesh__evt__t.html#af1fdbe33e4eea75082d0160fb43ea585", null ]
    ] ],
    [ "nrf_mesh_evt_handler_t", "structnrf__mesh__evt__handler__t.html", [
      [ "evt_cb", "structnrf__mesh__evt__handler__t.html#a6cd5693bca3ecd16ecb263c09d491f96", null ],
      [ "p_next", "structnrf__mesh__evt__handler__t.html#a03da8f8d9d43a0a7dab76b712430f168", null ]
    ] ],
    [ "nrf_mesh_evt_handler_cb_t", "group__NRF__MESH__EVENTS.html#gacb21e2811908a031b32bf519a1f0499a", null ],
    [ "nrf_mesh_evt_type_t", "group__NRF__MESH__EVENTS.html#gab3f98b4c62dde77094fd28f39ba9856c", [
      [ "NRF_MESH_EVT_MESSAGE_RECEIVED", "group__NRF__MESH__EVENTS.html#ggab3f98b4c62dde77094fd28f39ba9856ca54984a8e4a919f50b9d443e49739e282", null ],
      [ "NRF_MESH_EVT_TX_COMPLETE", "group__NRF__MESH__EVENTS.html#ggab3f98b4c62dde77094fd28f39ba9856ca49263cb75b4726bbbe4b6ebd281938b5", null ],
      [ "NRF_MESH_EVT_IV_UPDATE_NOTIFICATION", "group__NRF__MESH__EVENTS.html#ggab3f98b4c62dde77094fd28f39ba9856ca93b412a572d1b0d5aaebfd39c1a1ae9a", null ],
      [ "NRF_MESH_EVT_KEY_REFRESH_NOTIFICATION", "group__NRF__MESH__EVENTS.html#ggab3f98b4c62dde77094fd28f39ba9856caf3c818c3ce34e4532437af5d0e87f548", null ],
      [ "NRF_MESH_EVT_NET_BEACON_RECEIVED", "group__NRF__MESH__EVENTS.html#ggab3f98b4c62dde77094fd28f39ba9856cae2c4eb740fcd42560d49fa8d8a3ac2dd", null ],
      [ "NRF_MESH_EVT_HB_MESSAGE_RECEIVED", "group__NRF__MESH__EVENTS.html#ggab3f98b4c62dde77094fd28f39ba9856caa5b29b800ac7f7137d93922e6c661da0", null ],
      [ "NRF_MESH_EVT_DFU_REQ_RELAY", "group__NRF__MESH__EVENTS.html#ggab3f98b4c62dde77094fd28f39ba9856cab00468845c035c7d0f57d67fbe8cdb8a", null ],
      [ "NRF_MESH_EVT_DFU_REQ_SOURCE", "group__NRF__MESH__EVENTS.html#ggab3f98b4c62dde77094fd28f39ba9856ca70a1c50638fd749ca5606fc4a70efbd7", null ],
      [ "NRF_MESH_EVT_DFU_START", "group__NRF__MESH__EVENTS.html#ggab3f98b4c62dde77094fd28f39ba9856ca515869d8ec78d77490c81a7482e06c3c", null ],
      [ "NRF_MESH_EVT_DFU_END", "group__NRF__MESH__EVENTS.html#ggab3f98b4c62dde77094fd28f39ba9856cab759063c4ec8064c55e1f9ced1154d1d", null ],
      [ "NRF_MESH_EVT_DFU_BANK_AVAILABLE", "group__NRF__MESH__EVENTS.html#ggab3f98b4c62dde77094fd28f39ba9856ca9ecb99bcb084cbee6d783c58a5c127c1", null ],
      [ "NRF_MESH_EVT_DFU_FIRMWARE_OUTDATED", "group__NRF__MESH__EVENTS.html#ggab3f98b4c62dde77094fd28f39ba9856ca645aa596b210e352f9578c6611e6c0d7", null ],
      [ "NRF_MESH_EVT_DFU_FIRMWARE_OUTDATED_NO_AUTH", "group__NRF__MESH__EVENTS.html#ggab3f98b4c62dde77094fd28f39ba9856cac701530d77dbc33dbf081faa55643412", null ],
      [ "NRF_MESH_EVT_FLASH_STABLE", "group__NRF__MESH__EVENTS.html#ggab3f98b4c62dde77094fd28f39ba9856ca8fd6c2827a14c6ff07f169ac9a096044", null ],
      [ "NRF_MESH_EVT_RX_FAILED", "group__NRF__MESH__EVENTS.html#ggab3f98b4c62dde77094fd28f39ba9856cadb796e13ca73c425cfb695178c703a0e", null ],
      [ "NRF_MESH_EVT_SAR_FAILED", "group__NRF__MESH__EVENTS.html#ggab3f98b4c62dde77094fd28f39ba9856ca92626778020470870b198af65f2e45a0", null ],
      [ "NRF_MESH_EVT_FLASH_FAILED", "group__NRF__MESH__EVENTS.html#ggab3f98b4c62dde77094fd28f39ba9856ca03857397ac689a3b1abb8c8f0fd98a4e", null ]
    ] ],
    [ "nrf_mesh_rx_failed_reason_t", "group__NRF__MESH__EVENTS.html#ga94aed435807b2f6b7302f2d020372a5c", [
      [ "NRF_MESH_RX_FAILED_REASON_REPLAY_CACHE_FULL", "group__NRF__MESH__EVENTS.html#gga94aed435807b2f6b7302f2d020372a5ca1aabfbf333931963cf4abf8613ed6469", null ]
    ] ],
    [ "nrf_mesh_sar_session_cancel_reason_t", "group__NRF__MESH__EVENTS.html#ga90fab110504e710c34f201afe288633c", [
      [ "NRF_MESH_SAR_CANCEL_REASON_TIMEOUT", "group__NRF__MESH__EVENTS.html#gga90fab110504e710c34f201afe288633caf92aa8a9d5b8ecbb147d7eef0ad135eb", null ],
      [ "NRF_MESH_SAR_CANCEL_REASON_RETRY", "group__NRF__MESH__EVENTS.html#gga90fab110504e710c34f201afe288633caa75ebf4ef28d7d3fefef3e48750454c0", null ],
      [ "NRF_MESH_SAR_CANCEL_REASON_NO_MEM", "group__NRF__MESH__EVENTS.html#gga90fab110504e710c34f201afe288633ca0ce7db03eb83cb06af79b5f1ac6fe7fa", null ],
      [ "NRF_MESH_SAR_CANCEL_BY_PEER", "group__NRF__MESH__EVENTS.html#gga90fab110504e710c34f201afe288633cae2964c71a2b0d0ba5e43c489dca57b5e", null ],
      [ "NRF_MESH_SAR_CANCEL_REASON_INVALID_FORMAT", "group__NRF__MESH__EVENTS.html#gga90fab110504e710c34f201afe288633ca77dc975eabb12074521092e8088de2c9", null ],
      [ "NRF_MESH_SAR_CANCEL_PEER_STARTED_ANOTHER_SESSION", "group__NRF__MESH__EVENTS.html#gga90fab110504e710c34f201afe288633ca2ad748b221836ac4e19dc9a6942565f8", null ]
    ] ],
    [ "nrf_mesh_flash_user_module_t", "group__NRF__MESH__EVENTS.html#gabf2ebd528e0177fb02521a69a0fb4a24", [
      [ "NRF_MESH_FLASH_USER_CORE", "group__NRF__MESH__EVENTS.html#ggabf2ebd528e0177fb02521a69a0fb4a24a74a00191abf9dc395153c3fe4f1d123a", null ],
      [ "NRF_MESH_FLASH_USER_DEVICE_STATE_MANAGER", "group__NRF__MESH__EVENTS.html#ggabf2ebd528e0177fb02521a69a0fb4a24a0eb3ec86b6c16ea32fd7f1e7f59d68c7", null ],
      [ "NRF_MESH_FLASH_USER_ACCESS", "group__NRF__MESH__EVENTS.html#ggabf2ebd528e0177fb02521a69a0fb4a24a979bad8f9d972d83874750690cfcbe95", null ]
    ] ],
    [ "nrf_mesh_evt_handler_add", "group__NRF__MESH__EVENTS.html#gaf80ff374c3b181dad3bec19debf0f5f0", null ],
    [ "nrf_mesh_evt_handler_remove", "group__NRF__MESH__EVENTS.html#gac5bfce27641e48703a15c95c827c7288", null ]
];