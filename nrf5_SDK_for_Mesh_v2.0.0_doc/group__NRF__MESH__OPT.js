var group__NRF__MESH__OPT =
[
    [ "nrf_mesh_opt_t", "structnrf__mesh__opt__t.html", [
      [ "len", "structnrf__mesh__opt__t.html#af2af1b88704322569709e635a6844174", null ],
      [ "val", "structnrf__mesh__opt__t.html#a22d84c1465c32b803834b4bda5ead48d", null ],
      [ "p_array", "structnrf__mesh__opt__t.html#ad9b3fe15d99a0856f17540dc3f4667c6", null ],
      [ "opt", "structnrf__mesh__opt__t.html#a5b9dbf122acd266688b31b9cfd22b589", null ]
    ] ],
    [ "NRF_MESH_OPT_PROV_START", "group__NRF__MESH__OPT.html#gaaf33c0a8ba6ea9274c87ee60e9ec0f4e", null ],
    [ "NRF_MESH_OPT_TRS_START", "group__NRF__MESH__OPT.html#gacce097e21de024ec764c2b45efd86382", null ],
    [ "NRF_MESH_OPT_NET_START", "group__NRF__MESH__OPT.html#ga49f060784d880adf5413fdc7ac58eaef", null ],
    [ "nrf_mesh_opt_id_t", "group__NRF__MESH__OPT.html#ga5173abd75ba73ea1c994108c5e10dcc2", [
      [ "NRF_MESH_OPT_PROV_ECDH_OFFLOADING", "group__NRF__MESH__OPT.html#gga5173abd75ba73ea1c994108c5e10dcc2a22426810af4c91d957012dd4543432aa", null ],
      [ "NRF_MESH_OPT_TRS_SAR_RX_TIMEOUT", "group__NRF__MESH__OPT.html#gga5173abd75ba73ea1c994108c5e10dcc2adb09f56ec790bab0ba548f7dc689972c", null ],
      [ "NRF_MESH_OPT_TRS_SAR_RX_ACK_TIMEOUT_BASE", "group__NRF__MESH__OPT.html#gga5173abd75ba73ea1c994108c5e10dcc2aa213dd12ebbe244157652cea2dde9509", null ],
      [ "NRF_MESH_OPT_TRS_SAR_RX_ACK_TIMEOUT_PER_HOP_ADDITION", "group__NRF__MESH__OPT.html#gga5173abd75ba73ea1c994108c5e10dcc2ac1cfe05be3d566218afb4c0f956d04aa", null ],
      [ "NRF_MESH_OPT_TRS_SAR_TX_RETRY_TIMEOUT_BASE", "group__NRF__MESH__OPT.html#gga5173abd75ba73ea1c994108c5e10dcc2abd993596890cde40f043518698c069fb", null ],
      [ "NRF_MESH_OPT_TRS_SAR_TX_RETRY_TIMEOUT_PER_HOP_ADDITION", "group__NRF__MESH__OPT.html#gga5173abd75ba73ea1c994108c5e10dcc2a043c714d9058989278a0b3ac424dc364", null ],
      [ "NRF_MESH_OPT_TRS_SAR_TX_RETRIES", "group__NRF__MESH__OPT.html#gga5173abd75ba73ea1c994108c5e10dcc2ad98429f70095677127069cfdffbfc0df", null ],
      [ "NRF_MESH_OPT_TRS_SAR_SEGACK_TTL", "group__NRF__MESH__OPT.html#gga5173abd75ba73ea1c994108c5e10dcc2af796bd619f73e49d0b367539f4d98aa2", null ],
      [ "NRF_MESH_OPT_TRS_SZMIC", "group__NRF__MESH__OPT.html#gga5173abd75ba73ea1c994108c5e10dcc2a5d4f0ed6f1006edea10cb5fd1326b373", null ],
      [ "NRF_MESH_OPT_NET_RELAY_ENABLE", "group__NRF__MESH__OPT.html#gga5173abd75ba73ea1c994108c5e10dcc2a446bbc1957320dc2344be8724068050f", null ],
      [ "NRF_MESH_OPT_NET_RELAY_RETRANSMIT_COUNT", "group__NRF__MESH__OPT.html#gga5173abd75ba73ea1c994108c5e10dcc2af40f46c221e281b16d2399bbb325a500", null ],
      [ "NRF_MESH_OPT_NET_RELAY_RETRANSMIT_INTERVAL_MS", "group__NRF__MESH__OPT.html#gga5173abd75ba73ea1c994108c5e10dcc2a9c9d2fcd61867f275c04d1f6d89cffa4", null ],
      [ "NRF_MESH_OPT_NET_NETWORK_TRANSMIT_COUNT", "group__NRF__MESH__OPT.html#gga5173abd75ba73ea1c994108c5e10dcc2aae73bbd15819277a53e7d9e77d3d03fb", null ],
      [ "NRF_MESH_OPT_NET_NETWORK_TRANSMIT_INTERVAL_MS", "group__NRF__MESH__OPT.html#gga5173abd75ba73ea1c994108c5e10dcc2af494132ccea3f50966f9678fa3ea5ea3", null ]
    ] ],
    [ "nrf_mesh_opt_set", "group__NRF__MESH__OPT.html#ga35b7c71bb4d757648a509a42f28d7669", null ],
    [ "nrf_mesh_opt_get", "group__NRF__MESH__OPT.html#ga1a99cf862e223ad61e7663659cfb8ef4", null ]
];