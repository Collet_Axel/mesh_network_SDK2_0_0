var structserial__cmd__prov__data__t =
[
    [ "context_id", "structserial__cmd__prov__data__t.html#ad4e53a91b805118af9ef2d0d5ad03670", null ],
    [ "target_uuid", "structserial__cmd__prov__data__t.html#a36a345ffc42824531f71509e4f0848bc", null ],
    [ "network_key", "structserial__cmd__prov__data__t.html#a4e7db09a8e9c2aef01881b7a68a5c78b", null ],
    [ "network_key_index", "structserial__cmd__prov__data__t.html#afe2ae66e19b37e1fcc21ed59945c9de3", null ],
    [ "iv_index", "structserial__cmd__prov__data__t.html#a408a83eb6609f21ef57ebc0c26a8806e", null ],
    [ "address", "structserial__cmd__prov__data__t.html#aa2fa64ad52ac0c1156f1898edfb742d2", null ],
    [ "iv_update_flag", "structserial__cmd__prov__data__t.html#a67c04acd149d654fbe889c20b717e3c8", null ],
    [ "key_refresh_flag", "structserial__cmd__prov__data__t.html#a5a9f3c77ecc42da1cb7cda5ef34534e6", null ]
];