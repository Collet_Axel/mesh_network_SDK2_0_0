var group__INTERNAL__EVT =
[
    [ "internal_event_t", "structinternal__event__t.html", [
      [ "type", "structinternal__event__t.html#a11914980cd2564c2553cf1a924fae9a0", null ],
      [ "reason", "structinternal__event__t.html#a965d977c0df4e98f01e99245f6490e85", null ],
      [ "value", "structinternal__event__t.html#a3d8533e28740da516ad4c8a543bcae5b", null ],
      [ "state", "structinternal__event__t.html#a5d481379dd9ca15a96630dc7542f2f1d", null ],
      [ "packet_size", "structinternal__event__t.html#a857b3714c2f9affde2447a676498cba2", null ],
      [ "p_packet", "structinternal__event__t.html#ac9fa07103c57eeea161f547c2cbda194", null ]
    ] ],
    [ "__INTERNAL_EVENT_PUSH", "group__INTERNAL__EVT.html#gaab713dcfe649a9f44f21a77dff9c6736", null ],
    [ "internal_event_report_cb_t", "group__INTERNAL__EVT.html#gaa83ba029f025cf3cfc2bd3cc5231da94", null ],
    [ "internal_event_type_t", "group__INTERNAL__EVT.html#ga40b6d421e12f9fe3f6ebb352482ddf9f", [
      [ "INTERNAL_EVENT_DECRYPT_APP", "group__INTERNAL__EVT.html#gga40b6d421e12f9fe3f6ebb352482ddf9fa15a78ee3cbffb13c4bca6e7f4f70a77b", null ],
      [ "INTERNAL_EVENT_DECRYPT_TRS", "group__INTERNAL__EVT.html#gga40b6d421e12f9fe3f6ebb352482ddf9fab2b59fc605c1abbd5b2722ca450c39f3", null ],
      [ "INTERNAL_EVENT_DECRYPT_TRS_SEG", "group__INTERNAL__EVT.html#gga40b6d421e12f9fe3f6ebb352482ddf9fa8fed9b48e82f10a8a962f628c67a6184", null ],
      [ "INTERNAL_EVENT_PACKET_DROPPED", "group__INTERNAL__EVT.html#gga40b6d421e12f9fe3f6ebb352482ddf9faac49116c5c3786db0a86ec3e3ddcb1de", null ],
      [ "INTERNAL_EVENT_PACKET_RELAYED", "group__INTERNAL__EVT.html#gga40b6d421e12f9fe3f6ebb352482ddf9fa54a54efca9b3095c87b84f773b271068", null ],
      [ "INTERNAL_EVENT_NET_PACKET_QUEUED_TX", "group__INTERNAL__EVT.html#gga40b6d421e12f9fe3f6ebb352482ddf9fae473a4586c163546f62cc65ea888d7b3", null ],
      [ "INTERNAL_EVENT_TRS_ACK_RECEIVED", "group__INTERNAL__EVT.html#gga40b6d421e12f9fe3f6ebb352482ddf9fa12f0c4ff453d740ccd5e1b7202586200", null ],
      [ "INTERNAL_EVENT_ACK_QUEUED", "group__INTERNAL__EVT.html#gga40b6d421e12f9fe3f6ebb352482ddf9fa162038ea3df96f99d944536e7723302d", null ],
      [ "INTERNAL_EVENT_SAR_CANCELLED", "group__INTERNAL__EVT.html#gga40b6d421e12f9fe3f6ebb352482ddf9fa88d57726b6e4e1f895a15e20f036a10c", null ],
      [ "INTERNAL_EVENT_FM_ACTION", "group__INTERNAL__EVT.html#gga40b6d421e12f9fe3f6ebb352482ddf9faa03524c2ae69a21b5f4f97292de8fda2", null ],
      [ "INTERNAL_EVENT_FM_DEFRAG", "group__INTERNAL__EVT.html#gga40b6d421e12f9fe3f6ebb352482ddf9fad13df9245ef07c47f20243e6a5a49f34", null ],
      [ "INTERNAL_EVENT_SAR_SUCCESS", "group__INTERNAL__EVT.html#gga40b6d421e12f9fe3f6ebb352482ddf9fa9c8b7ac07cbe4dd28c8323d51d5e37b2", null ]
    ] ],
    [ "internal_event_packet_dropped_t", "group__INTERNAL__EVT.html#gaf99ea9808ff6f9e0ab29a75ce7f30fe2", [
      [ "PACKET_DROPPED_INVALID_APPKEY", "group__INTERNAL__EVT.html#ggaf99ea9808ff6f9e0ab29a75ce7f30fe2a162e434a687a627ace34a246ae99be7c", null ],
      [ "PACKET_DROPPED_INVALID_NETKEY", "group__INTERNAL__EVT.html#ggaf99ea9808ff6f9e0ab29a75ce7f30fe2a5161143c75a1d8abf1e6cf65fba67e8c", null ],
      [ "PACKET_DROPPED_INVALID_DEVKEY", "group__INTERNAL__EVT.html#ggaf99ea9808ff6f9e0ab29a75ce7f30fe2af38671bfc5c5957456d5419b1a08e468", null ],
      [ "PACKET_DROPPED_INVALID_ADDRESS", "group__INTERNAL__EVT.html#ggaf99ea9808ff6f9e0ab29a75ce7f30fe2ade4f4945043e99b42fc864c6e70d66c0", null ],
      [ "PACKET_DROPPED_UNKNOWN_ADDRESS", "group__INTERNAL__EVT.html#ggaf99ea9808ff6f9e0ab29a75ce7f30fe2a772cbb8b053575b2f20eb74be019e89b", null ],
      [ "PACKET_DROPPED_REPLAY_CACHE", "group__INTERNAL__EVT.html#ggaf99ea9808ff6f9e0ab29a75ce7f30fe2a20e4f83f2c5ee01ef6a71055f4dee55f", null ],
      [ "PACKET_DROPPED_NETWORK_CACHE", "group__INTERNAL__EVT.html#ggaf99ea9808ff6f9e0ab29a75ce7f30fe2a9bb9fff1448d16c8138f26e8dee660a9", null ],
      [ "PACKET_DROPPED_NETWORK_DECRYPT_FAILED", "group__INTERNAL__EVT.html#ggaf99ea9808ff6f9e0ab29a75ce7f30fe2ac9fbfa3e176b8851322330eaef438bc1", null ],
      [ "PACKET_DROPPED_INVALID_ADTYPE", "group__INTERNAL__EVT.html#ggaf99ea9808ff6f9e0ab29a75ce7f30fe2a6b9b7535048f168778b1d7045891e806", null ],
      [ "PACKET_DROPPED_INVALID_PACKET_LEN", "group__INTERNAL__EVT.html#ggaf99ea9808ff6f9e0ab29a75ce7f30fe2a14689a7b96c0238fc25c1dcdc56c8fd1", null ],
      [ "PACKET_DROPPED_NO_MEM", "group__INTERNAL__EVT.html#ggaf99ea9808ff6f9e0ab29a75ce7f30fe2a27cbbbc7bbc774df829857167bc3ad86", null ]
    ] ],
    [ "internal_event_init", "group__INTERNAL__EVT.html#ga1c95d2bfb93f0111e055e13ec5d0b6f1", null ],
    [ "internal_event_push", "group__INTERNAL__EVT.html#ga16444ce3eaca4d69a07fc9c36dba45ce", null ],
    [ "internal_event_pop", "group__INTERNAL__EVT.html#ga5e7429406059bef8fc50c836e00073e0", null ]
];