var group__NRF__MESH__PROV__DEFINES =
[
    [ "Provisioning field lengths", "group__PROV__FIELD__LENGTHS.html", "group__PROV__FIELD__LENGTHS" ],
    [ "Provisioning capabilities bit fields", "group__NRF__MESH__PROV__CAPABILITIES.html", "group__NRF__MESH__PROV__CAPABILITIES" ],
    [ "Provisioning OOB information sources.", "group__NRF__MESH__PROV__OOB__INFO__SOURCES.html", "group__NRF__MESH__PROV__OOB__INFO__SOURCES" ],
    [ "NRF_MESH_PROV_OOB_CAPS_DEFAULT", "group__NRF__MESH__PROV__DEFINES.html#ga38894ea1c7272f1f82abaf96d50fc588", null ]
];