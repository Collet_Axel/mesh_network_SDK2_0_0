var group__DEVICE__STATE__MANAGER__TYPES =
[
    [ "dsm_local_unicast_address_t", "structdsm__local__unicast__address__t.html", [
      [ "address_start", "structdsm__local__unicast__address__t.html#a327a32f401afa72607913a115aff1a29", null ],
      [ "count", "structdsm__local__unicast__address__t.html#a433e7792aae7ba4c2a5956303369e137", null ]
    ] ],
    [ "mesh_key_index_t", "group__DEVICE__STATE__MANAGER__TYPES.html#gac4878f98ffd5bb7ec6bcef90a4f2b2e0", null ],
    [ "dsm_handle_t", "group__DEVICE__STATE__MANAGER__TYPES.html#gaed12b7058cd32649ae3421999770a0d3", null ]
];