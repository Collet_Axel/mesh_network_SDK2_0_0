var modules =
[
    [ "Mesh stack", "group__MESH__STACK.html", "group__MESH__STACK" ],
    [ "Application support modules", "group__MESH__API__GROUP__APP__SUPPORT.html", "group__MESH__API__GROUP__APP__SUPPORT" ],
    [ "Application configuration", "group__MESH__API__GROUP__APP__CONFIG.html", "group__MESH__API__GROUP__APP__CONFIG" ],
    [ "Mesh Models", "group__MESH__API__GROUP__MODELS.html", "group__MESH__API__GROUP__MODELS" ],
    [ "Access", "group__MESH__API__GROUP__ACCESS.html", "group__MESH__API__GROUP__ACCESS" ],
    [ "Device State Manager", "group__DEVICE__STATE__MANAGER.html", "group__DEVICE__STATE__MANAGER" ],
    [ "Core", "group__MESH__API__GROUP__CORE.html", "group__MESH__API__GROUP__CORE" ],
    [ "Bearer", "group__MESH__API__GROUP__BEARER.html", "group__MESH__API__GROUP__BEARER" ],
    [ "DFU", "group__MESH__API__GROUP__DFU.html", "group__MESH__API__GROUP__DFU" ],
    [ "Provisioning", "group__MESH__API__GROUP__PROV.html", "group__MESH__API__GROUP__PROV" ],
    [ "Serial", "group__MESH__API__GROUP__SERIAL.html", "group__MESH__API__GROUP__SERIAL" ],
    [ "Experimental features", "group__MESH__API__GROUP__EXPERIMENTAL.html", "group__MESH__API__GROUP__EXPERIMENTAL" ]
];