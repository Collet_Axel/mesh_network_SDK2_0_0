var group__NRF__MESH__PROV__TYPES =
[
    [ "nrf_mesh_prov_ctx_t", "structnrf__mesh__prov__ctx.html", [
      [ "p_bearers", "structnrf__mesh__prov__ctx.html#a1dac93f7f424f28f1c4a26a00afa2e5d", null ],
      [ "supported_bearers", "structnrf__mesh__prov__ctx.html#a890004f63f88e7bc4273759a5053a02e", null ],
      [ "p_active_bearer", "structnrf__mesh__prov__ctx.html#af1f151e71789f7ccff73339a313f9750", null ],
      [ "event_handler", "structnrf__mesh__prov__ctx.html#aa0be8edc23c67c8ea9e25ea807a8d28e", null ],
      [ "p_public_key", "structnrf__mesh__prov__ctx.html#a9c1f686f988b795f87be4095ddca9381", null ],
      [ "p_private_key", "structnrf__mesh__prov__ctx.html#ace95ece14eb4dd127d22a7d8b5af8c96", null ],
      [ "peer_public_key", "structnrf__mesh__prov__ctx.html#afa03fd7f19361c8fe032fba4c138d02d", null ],
      [ "shared_secret", "structnrf__mesh__prov__ctx.html#a8d56e1455b10a18dcb5a1a1fcfc1ebfc", null ],
      [ "device_key", "structnrf__mesh__prov__ctx.html#a5ecc534847021ee9181e3c8d566053f6", null ],
      [ "session_key", "structnrf__mesh__prov__ctx.html#a0c18ad36887b5f8488668868dfd915fa", null ],
      [ "data_nonce", "structnrf__mesh__prov__ctx.html#a7006d6df09328f4b1a304da21318c5a6", null ],
      [ "node_random", "structnrf__mesh__prov__ctx.html#aafbc480b326f1af68f75db2fbf4bb53f", null ],
      [ "peer_random", "structnrf__mesh__prov__ctx.html#a0cc8c5c6d1cafbba65f22f2b845c5312", null ],
      [ "auth_value", "structnrf__mesh__prov__ctx.html#ade72512aff1a0a25f5aee9cecfb514ff", null ],
      [ "confirmation_salt", "structnrf__mesh__prov__ctx.html#a81e7abebef38fc36a1a9fbd2f552549b", null ],
      [ "peer_confirmation", "structnrf__mesh__prov__ctx.html#afe73a3593c3b8b5a2ef09ba0dd270aca", null ],
      [ "confirmation_inputs", "structnrf__mesh__prov__ctx.html#a7774144e776b1365dc65c6c54b71335d", null ],
      [ "oob_size", "structnrf__mesh__prov__ctx.html#a4dc94a438bcafbe2203582e0c739b3eb", null ],
      [ "oob_action", "structnrf__mesh__prov__ctx.html#aec450d502e6099d1bce816a4e59f1207", null ],
      [ "pubkey_oob", "structnrf__mesh__prov__ctx.html#af9947580ef30d94d0ce39d98fbed160f", null ],
      [ "role", "structnrf__mesh__prov__ctx.html#a1d2a4fba6a71308168e8fe75e3338880", null ],
      [ "failure_code", "structnrf__mesh__prov__ctx.html#a5474a8969616c893f5cd36ecde0aadca", null ],
      [ "state", "structnrf__mesh__prov__ctx.html#ac465bec48c060f4f7bbbaa1986cc63b9", null ],
      [ "oob_method", "structnrf__mesh__prov__ctx.html#a42c4ab5b391437e4a31a5985f0de4050", null ],
      [ "capabilities", "structnrf__mesh__prov__ctx.html#ac7afaebfc2b8ad4b771fa8d27d1227fc", null ],
      [ "data", "structnrf__mesh__prov__ctx.html#a4ad7ab6bb401af2206672fd39e5ecc32", null ]
    ] ],
    [ "nrf_mesh_prov_oob_caps_t", "structnrf__mesh__prov__oob__caps__t.html", [
      [ "num_elements", "structnrf__mesh__prov__oob__caps__t.html#ac9b8eea193284d275be04005d8c0c6a0", null ],
      [ "algorithms", "structnrf__mesh__prov__oob__caps__t.html#af73951cd77affe5e6f74eb05d984b28f", null ],
      [ "pubkey_type", "structnrf__mesh__prov__oob__caps__t.html#af982f91552488dd26432e67346ad6232", null ],
      [ "oob_static_types", "structnrf__mesh__prov__oob__caps__t.html#aee0e8f693640fd24ef23c7e1b131b202", null ],
      [ "oob_output_size", "structnrf__mesh__prov__oob__caps__t.html#a7e725df25fa662c0d7e2962e600a697c", null ],
      [ "oob_output_actions", "structnrf__mesh__prov__oob__caps__t.html#a44acaab8ea2007e6b83e9f98a38945a9", null ],
      [ "oob_input_size", "structnrf__mesh__prov__oob__caps__t.html#a07a7dfd813db0b4e9c9cf23fc38f7221", null ],
      [ "oob_input_actions", "structnrf__mesh__prov__oob__caps__t.html#a5bd6eae79ec72fc56ecfc2acf33f7b42", null ]
    ] ],
    [ "nrf_mesh_prov_provisioning_data_t", "structnrf__mesh__prov__provisioning__data__t.html", [
      [ "netkey", "structnrf__mesh__prov__provisioning__data__t.html#a617186cb1be5c741da6382a3f2a8cc4a", null ],
      [ "netkey_index", "structnrf__mesh__prov__provisioning__data__t.html#a560d83057062e54de8b1347519558adb", null ],
      [ "iv_index", "structnrf__mesh__prov__provisioning__data__t.html#a78fbd648ad9feb41f8e5f20f2d863b5d", null ],
      [ "address", "structnrf__mesh__prov__provisioning__data__t.html#a55ef502721676073b58e865ba43bf83b", null ],
      [ "iv_update", "structnrf__mesh__prov__provisioning__data__t.html#a49bb467233a283f8e37eabbe956e2fd2", null ],
      [ "key_refresh", "structnrf__mesh__prov__provisioning__data__t.html#aebf8f61d0afd603fd4a7a113a58a483b", null ],
      [ "flags", "structnrf__mesh__prov__provisioning__data__t.html#a4536ec744f19a556eb4ec2ccd280a215", null ]
    ] ],
    [ "nrf_mesh_prov_algorithm_t", "group__NRF__MESH__PROV__TYPES.html#gac2559992952db1f27f838200d4a7dc6a", [
      [ "NRF_MESH_PROV_ALGORITHM_FIPS_P256", "group__NRF__MESH__PROV__TYPES.html#ggac2559992952db1f27f838200d4a7dc6aa8059fef9c8072d83b7d78a31def3b91f", null ],
      [ "NRF_MESH_PROV_ALGORITHM_RFU", "group__NRF__MESH__PROV__TYPES.html#ggac2559992952db1f27f838200d4a7dc6aa7897d70ee9b2b4c87401e8d33edb5ea0", null ]
    ] ],
    [ "nrf_mesh_prov_public_key_usage_t", "group__NRF__MESH__PROV__TYPES.html#ga065290b2e9c3bcda776f77a1337f9d20", [
      [ "NRF_MESH_PROV_PUBLIC_KEY_NO_OOB", "group__NRF__MESH__PROV__TYPES.html#gga065290b2e9c3bcda776f77a1337f9d20a3234909f21c992974ed7534f9feb4047", null ],
      [ "NRF_MESH_PROV_PUBLIC_KEY_OOB", "group__NRF__MESH__PROV__TYPES.html#gga065290b2e9c3bcda776f77a1337f9d20abe909b63cad96d430f6d1cb588b1bf0d", null ],
      [ "NRF_MESH_PROV_PUBLIC_KEY_PROHIBITED", "group__NRF__MESH__PROV__TYPES.html#gga065290b2e9c3bcda776f77a1337f9d20ac85e9b41c0e0b1424fa293733e72ad57", null ]
    ] ],
    [ "nrf_mesh_prov_oob_method_t", "group__NRF__MESH__PROV__TYPES.html#ga48d50791ff925ab61b15affaeef08925", [
      [ "NRF_MESH_PROV_OOB_METHOD_NONE", "group__NRF__MESH__PROV__TYPES.html#gga48d50791ff925ab61b15affaeef08925ad3ab9c71bca01c823e7d63098e7b5a05", null ],
      [ "NRF_MESH_PROV_OOB_METHOD_STATIC", "group__NRF__MESH__PROV__TYPES.html#gga48d50791ff925ab61b15affaeef08925aa73930c11e1f9da7a15376e6d7347f35", null ],
      [ "NRF_MESH_PROV_OOB_METHOD_OUTPUT", "group__NRF__MESH__PROV__TYPES.html#gga48d50791ff925ab61b15affaeef08925aed9a04d3fa10d5052a683fd9ed6696e1", null ],
      [ "NRF_MESH_PROV_OOB_METHOD_INPUT", "group__NRF__MESH__PROV__TYPES.html#gga48d50791ff925ab61b15affaeef08925a86c0af7eadf408fc559b3fe99739b250", null ],
      [ "NRF_MESH_PROV_OOB_METHOD_PROHIBITED", "group__NRF__MESH__PROV__TYPES.html#gga48d50791ff925ab61b15affaeef08925a454e9e98ead78669f2ce0635643f2810", null ]
    ] ],
    [ "nrf_mesh_prov_input_action_t", "group__NRF__MESH__PROV__TYPES.html#ga80726820da1c41b4624246e08e94317f", [
      [ "NRF_MESH_PROV_INPUT_ACTION_PUSH", "group__NRF__MESH__PROV__TYPES.html#gga80726820da1c41b4624246e08e94317fafc252602ce3d98479da35891128b6c9c", null ],
      [ "NRF_MESH_PROV_INPUT_ACTION_TWIST", "group__NRF__MESH__PROV__TYPES.html#gga80726820da1c41b4624246e08e94317fa4e53f6b19c27c2ec3d251e87fb50a772", null ],
      [ "NRF_MESH_PROV_INPUT_ACTION_ENTER_NUMBER", "group__NRF__MESH__PROV__TYPES.html#gga80726820da1c41b4624246e08e94317fa876aed86752970190c50489d2baa4b55", null ],
      [ "NRF_MESH_PROV_INPUT_ACTION_ENTER_STRING", "group__NRF__MESH__PROV__TYPES.html#gga80726820da1c41b4624246e08e94317fad4e6d7b99915fff0bc9cd5b7d85a6287", null ],
      [ "NRF_MESH_PROV_INPUT_ACTION_RFU", "group__NRF__MESH__PROV__TYPES.html#gga80726820da1c41b4624246e08e94317fa9f63324f28c7a91e907c3a69fd7c8f72", null ]
    ] ],
    [ "nrf_mesh_prov_output_action_t", "group__NRF__MESH__PROV__TYPES.html#gab698ffc4a7071d5d865274809f32bbf4", [
      [ "NRF_MESH_PROV_OUTPUT_ACTION_BLINK", "group__NRF__MESH__PROV__TYPES.html#ggab698ffc4a7071d5d865274809f32bbf4a454424d09f31f6761455ba34e4a003ce", null ],
      [ "NRF_MESH_PROV_OUTPUT_ACTION_BEEP", "group__NRF__MESH__PROV__TYPES.html#ggab698ffc4a7071d5d865274809f32bbf4a8d43e86b55e880120271104d330c4845", null ],
      [ "NRF_MESH_PROV_OUTPUT_ACTION_VIBRATE", "group__NRF__MESH__PROV__TYPES.html#ggab698ffc4a7071d5d865274809f32bbf4a80113e8506552ffc0a16cb6d24dc6c8a", null ],
      [ "NRF_MESH_PROV_OUTPUT_ACTION_DISPLAY_NUMERIC", "group__NRF__MESH__PROV__TYPES.html#ggab698ffc4a7071d5d865274809f32bbf4a73a135d4d08dda8987d62fb8c5e26fe3", null ],
      [ "NRF_MESH_PROV_OUTPUT_ACTION_ALPHANUMERIC", "group__NRF__MESH__PROV__TYPES.html#ggab698ffc4a7071d5d865274809f32bbf4a4f3096a5ec4c51e0a3a5408b75539dee", null ],
      [ "NRF_MESH_PROV_OUTPUT_ACTION_RFU", "group__NRF__MESH__PROV__TYPES.html#ggab698ffc4a7071d5d865274809f32bbf4a047ba056ad7aac65112bbc2d831add47", null ]
    ] ],
    [ "nrf_mesh_prov_failure_code_t", "group__NRF__MESH__PROV__TYPES.html#gadc09012acf40498e67e831863c69f240", [
      [ "NRF_MESH_PROV_FAILURE_CODE_INVALID_PDU", "group__NRF__MESH__PROV__TYPES.html#ggadc09012acf40498e67e831863c69f240afc0422a07e703a16779eeaaf453b66a5", null ],
      [ "NRF_MESH_PROV_FAILURE_CODE_INVALID_FORMAT", "group__NRF__MESH__PROV__TYPES.html#ggadc09012acf40498e67e831863c69f240a7362644c8e52b672323b364dd2b6d612", null ],
      [ "NRF_MESH_PROV_FAILURE_CODE_UNEXPECTED_PDU", "group__NRF__MESH__PROV__TYPES.html#ggadc09012acf40498e67e831863c69f240a93d68b16fd672ee51035dcbd099fb02c", null ],
      [ "NRF_MESH_PROV_FAILURE_CODE_CONFIRMATION_FAILED", "group__NRF__MESH__PROV__TYPES.html#ggadc09012acf40498e67e831863c69f240a22813805aee53b7a3b93abaead7686db", null ],
      [ "NRF_MESH_PROV_FAILURE_CODE_OUT_OF_RESOURCES", "group__NRF__MESH__PROV__TYPES.html#ggadc09012acf40498e67e831863c69f240ac8e26ba6d23d59fa30064dde93c90093", null ],
      [ "NRF_MESH_PROV_FAILURE_CODE_DECRYPTION_FAILED", "group__NRF__MESH__PROV__TYPES.html#ggadc09012acf40498e67e831863c69f240a182d81244c0011334130e70f2fffa224", null ],
      [ "NRF_MESH_PROV_FAILURE_CODE_UNEXPECTED_ERROR", "group__NRF__MESH__PROV__TYPES.html#ggadc09012acf40498e67e831863c69f240a7a3aafbd42aaf41f46dbffc27a13ff0c", null ],
      [ "NRF_MESH_PROV_FAILURE_CODE_CANNOT_ASSIGN_ADDR", "group__NRF__MESH__PROV__TYPES.html#ggadc09012acf40498e67e831863c69f240af24b13de01eaa69da6e6a1c4426d1632", null ]
    ] ],
    [ "nrf_mesh_prov_state_t", "group__NRF__MESH__PROV__TYPES.html#ga98172665ccebf7298f150b501fb52e8d", [
      [ "NRF_MESH_PROV_STATE_IDLE", "group__NRF__MESH__PROV__TYPES.html#gga98172665ccebf7298f150b501fb52e8da6ec597fe5850c8c8db4012c27689dbb1", null ],
      [ "NRF_MESH_PROV_STATE_WAIT_LINK", "group__NRF__MESH__PROV__TYPES.html#gga98172665ccebf7298f150b501fb52e8dab31b3adbab946acc32c48701e40bf6ff", null ],
      [ "NRF_MESH_PROV_STATE_INVITE", "group__NRF__MESH__PROV__TYPES.html#gga98172665ccebf7298f150b501fb52e8da3d4b6fc22d69020718a8f3accf8d0f1b", null ],
      [ "NRF_MESH_PROV_STATE_WAIT_CAPS", "group__NRF__MESH__PROV__TYPES.html#gga98172665ccebf7298f150b501fb52e8daf8718dfd9c071d06b827338154e849d4", null ],
      [ "NRF_MESH_PROV_STATE_WAIT_CAPS_CONFIRM", "group__NRF__MESH__PROV__TYPES.html#gga98172665ccebf7298f150b501fb52e8da25d0d901e0ee7832acc65f5f9a20dad4", null ],
      [ "NRF_MESH_PROV_STATE_WAIT_START", "group__NRF__MESH__PROV__TYPES.html#gga98172665ccebf7298f150b501fb52e8daafda81c2a55021ffa5fbb19f9883fd16", null ],
      [ "NRF_MESH_PROV_STATE_WAIT_START_ACK", "group__NRF__MESH__PROV__TYPES.html#gga98172665ccebf7298f150b501fb52e8da63f60f457e6fbf1cc702c71345117d28", null ],
      [ "NRF_MESH_PROV_STATE_WAIT_PUB_KEY_ACK", "group__NRF__MESH__PROV__TYPES.html#gga98172665ccebf7298f150b501fb52e8dafa0cbdfec511af8ca9290b1fe1e4fcaf", null ],
      [ "NRF_MESH_PROV_STATE_WAIT_PUB_KEY", "group__NRF__MESH__PROV__TYPES.html#gga98172665ccebf7298f150b501fb52e8da9a7b527c416bfb36950351c92fdbdcfe", null ],
      [ "NRF_MESH_PROV_STATE_WAIT_OOB_PUB_KEY", "group__NRF__MESH__PROV__TYPES.html#gga98172665ccebf7298f150b501fb52e8dad80a037a504df9732455a90e2ec60fd6", null ],
      [ "NRF_MESH_PROV_STATE_WAIT_EXTERNAL_ECDH", "group__NRF__MESH__PROV__TYPES.html#gga98172665ccebf7298f150b501fb52e8da8ff38bde70a82c15065b27135efa6e14", null ],
      [ "NRF_MESH_PROV_STATE_WAIT_OOB_INPUT", "group__NRF__MESH__PROV__TYPES.html#gga98172665ccebf7298f150b501fb52e8da7b0f8a20ce85eddff1a12a8bf632cab6", null ],
      [ "NRF_MESH_PROV_STATE_WAIT_OOB_STATIC", "group__NRF__MESH__PROV__TYPES.html#gga98172665ccebf7298f150b501fb52e8dae129ddc3d6213e4ff8f907bb8dbf5e7e", null ],
      [ "NRF_MESH_PROV_STATE_WAIT_OOB_STATIC_C_RCVD", "group__NRF__MESH__PROV__TYPES.html#gga98172665ccebf7298f150b501fb52e8da8afe58aa3dfb43fb2d9a626011e9694c", null ],
      [ "NRF_MESH_PROV_STATE_WAIT_CONFIRMATION_ACK", "group__NRF__MESH__PROV__TYPES.html#gga98172665ccebf7298f150b501fb52e8dad197e4c7947bb51002b5f0c42eb6b565", null ],
      [ "NRF_MESH_PROV_STATE_WAIT_CONFIRMATION", "group__NRF__MESH__PROV__TYPES.html#gga98172665ccebf7298f150b501fb52e8da7dd554d0884d5556100b79d000772e8e", null ],
      [ "NRF_MESH_PROV_STATE_WAIT_INPUT_COMPLETE", "group__NRF__MESH__PROV__TYPES.html#gga98172665ccebf7298f150b501fb52e8da289ed0d0d42c4db85ccc4ff758b43f26", null ],
      [ "NRF_MESH_PROV_STATE_WAIT_RANDOM", "group__NRF__MESH__PROV__TYPES.html#gga98172665ccebf7298f150b501fb52e8da5d3ad78e513787cbecaf3c70141bc40f", null ],
      [ "NRF_MESH_PROV_STATE_WAIT_DATA", "group__NRF__MESH__PROV__TYPES.html#gga98172665ccebf7298f150b501fb52e8da67fba3cfd035a31dd0c6c7c126a1c345", null ],
      [ "NRF_MESH_PROV_STATE_WAIT_COMPLETE", "group__NRF__MESH__PROV__TYPES.html#gga98172665ccebf7298f150b501fb52e8da47da922b44e51c86f400d1bbecf1997c", null ],
      [ "NRF_MESH_PROV_STATE_COMPLETE", "group__NRF__MESH__PROV__TYPES.html#gga98172665ccebf7298f150b501fb52e8dad091359e353aa6e2812298d290b311ba", null ],
      [ "NRF_MESH_PROV_STATE_FAILED", "group__NRF__MESH__PROV__TYPES.html#gga98172665ccebf7298f150b501fb52e8dace5d1bf108412ad8f4de1e62f989fd19", null ]
    ] ],
    [ "nrf_mesh_prov_role_t", "group__NRF__MESH__PROV__TYPES.html#ga8716aa3e62c5ac18ed4bfcada01005ba", [
      [ "NRF_MESH_PROV_ROLE_PROVISIONER", "group__NRF__MESH__PROV__TYPES.html#gga8716aa3e62c5ac18ed4bfcada01005baadfc9c6aa48c6666451fc664573dc28b8", null ],
      [ "NRF_MESH_PROV_ROLE_PROVISIONEE", "group__NRF__MESH__PROV__TYPES.html#gga8716aa3e62c5ac18ed4bfcada01005baa7723296cda21e3555fabba335545876a", null ]
    ] ]
];